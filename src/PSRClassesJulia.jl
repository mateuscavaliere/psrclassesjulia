module PSRClassesJulia

using Libdl
using Dates

export

#- Genericos
PSRClasses_init, get_parm_psrc, load_masks , PSRStudy_getCollectionByString , read_study , create_time_controller ,

#- Tipos
Graf, SddpStudy, SddpOptions, SddpData, SddpSizes,

#- Construtores
constr_graph,

#- Graf functions
graph_create_ptr! , graph_get_config! , graph_load_init , graph_set_config! , graph_save_init , graph_close, graph_copy_config! ,

#- Database read functions
read_sddp_config! , read_sistemas! , read_duration! , read_sistemas , read_contratos , read_duracao , read_agentes , read_plants , read_portfolio , 
read_garantia_fisica , read_aversao 


#--- Loading files
include( joinpath( dirname(@__FILE__) , "PSRClasses.jl" ) )
include( joinpath( dirname(@__FILE__) , "PSRClassesTypes.jl" ) )
include( joinpath( dirname(@__FILE__) , "PSRClassesFunction.jl" ) )

end # module
