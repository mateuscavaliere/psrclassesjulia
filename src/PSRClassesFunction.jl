
"""
    Construtores
"""
#--- constr_graph: Funcao construtora de variveis do tipo Graph
function constr_graph( path::String , file_name::String )
    TempGraph = Graf()
    TempGraph.Path = path
    TempGraph.FileName = file_name
    return TempGraph
end



"""
    Funcoes do tipo Graph
"""

#--- graph_create_ptr: Funcao para criar ponteiro e abrir o grafico
function graph_create_ptr!( graph::Graf , flag::String )
    
    if flag == "load"
        if isfile( joinpath( graph.Path , string( graph.FileName , ".bin" ) ) ) & isfile( joinpath( graph.Path , string( graph.FileName , ".hdr" ) ) )
            graph.Ptr = PSRIOGrafResultBinary_create(0)
            graph.Ext = 1
            PSRIOGrafResultBinary_initLoad( graph.Ptr , joinpath( graph.Path , string( graph.FileName ,".hdr" ) ) , joinpath( graph.Path , string( graph.FileName ,".bin" ) ) )
        elseif isfile( joinpath( graph.Path , string( graph.FileName , ".csv" ) ) )
            graph.Ptr = PSRIOGrafResult_create(0)
            graph.Ext = 0
            PSRIOGrafResult_initLoad( graph.Ptr , joinpath( graph.Path , string( graph.FileName , ".csv" ) ) , PSRIO_GRAF_FORMAT_DEFAULT )
        else
            return -1
        end
    else
        if graph.Ext == 0
            graph.Ptr = PSRIOGrafResult_create(0);
        else
            graph.Ptr = PSRIOGrafResultBinary_create(0);
        end
    end
    return 0
end

#--- graph_get_config: Funcao para obter a configuracao o grafico
function graph_get_config!( graph::Graf )
    graph.IniStg     = PSRIOGrafResultBase_getInitialStage( graph.Ptr )
    graph.IniYear    = PSRIOGrafResultBase_getInitialYear(  graph.Ptr )
    graph.NumStg     = PSRIOGrafResultBase_getTotalStages( graph.Ptr )
    graph.NumSer     = PSRIOGrafResultBase_getTotalSeries( graph.Ptr )
    graph.NumBlc     = PSRIOGrafResultBase_getTotalBlocks( graph.Ptr )
    graph.NumAgentes = PSRIOGrafResultBase_maxAgent( graph.Ptr )
    graph.Unit2      = PSRIOGrafResultBase_getUnit( graph.Ptr )
    graph.StgType    = PSRIOGrafResultBase_getStageType( graph.Ptr )
    graph.Simb       = PSRIOGrafResultBase_getVariableByBlock( graph.Ptr )
    graph.Sims       = PSRIOGrafResultBase_getVariableBySerie( graph.Ptr )
    graph.Simh       = PSRIOGrafResultBase_getVariableByHour(  graph.Ptr )
    graph.Agentes    = Vector{String}( undef , graph.NumAgentes )

    for iAgente in 1:graph.NumAgentes
        graph.Agentes[ iAgente ] = PSRIOGrafResultBase_getAgent( graph.Ptr , iAgente - 1 )
    end

    return 0
end

#--- graph_load_init: Funcao para carregar o grafico
function graph_load_init( graph::Graf )

    #- Cria ponteiro e abre o arquivo
    if graph_create_ptr!( graph , "load" ) == -1
        return -1
    end

    #--- Obtem a configuracao do grafico
    graph_get_config!( graph )

    return 0
end

#--- graph_set_config: Funcao para configurar arquivo de saida
function graph_set_config!( graph::Graf ; sequential::Bool = true )

    #--- Setting header configuration
    PSRIOGrafResultBase_setInitialStage( graph.Ptr , graph.IniStg )
    PSRIOGrafResultBase_setInitialYear(  graph.Ptr , graph.IniYear )

    PSRIOGrafResultBase_setTotalStages( graph.Ptr , graph.NumStg );
    PSRIOGrafResultBase_setTotalSeries( graph.Ptr , graph.NumSer );
    PSRIOGrafResultBase_setTotalBlocks( graph.Ptr , graph.NumBlc );

    PSRIOGrafResultBase_setUnit( graph.Ptr , graph.Unit2    );
    PSRIOGrafResultBase_setStageType( graph.Ptr , graph.StgType  );
    PSRIOGrafResultBase_setVariableBySerie( graph.Ptr , graph.Sims );
    PSRIOGrafResultBase_setVariableByHour( graph.Ptr,  graph.Simh );
    PSRIOGrafResultBase_setVariableByBlock( graph.Ptr , graph.Simb  );

    if graph.Simh != 0
        PSRIOGrafResultBase_generateDefautHourInformation( graph.Ptr )
    end

    #PSRIOGrafResultBase_setSequencialModel( graph.ptr , sequential     );

    #--- Adding agents
    for Agente in graph.Agentes
        PSRIOGrafResultBase_addAgent( graph.Ptr , Agente )
    end

    return 0
end

#--- graph_save_init: Funcao para inicializar arquivo de saida
function graph_save_init( graph::Graf ; sequential::Bool = true )

    #- Cria o ponteiro para arquivo de saida
    graph_create_ptr!( graph , "save" )

    #- Configura os parametros do arquivo de saida
    graph_set_config!( graph )
    
    #- Abre o arquivo de saida
    if graph.Ext == 0
        PSRIOGrafResult_initSave( graph.Ptr , joinpath( graph.Path , string( graph.FileName , ".csv" ) ) , PSRIO_GRAF_FORMAT_DEFAULT )
    else
        PSRIOGrafResultBinary_initSave( graph.Ptr , joinpath( graph.Path , string( graph.FileName ,".hdr" ) ) , joinpath( graph.Path , string( graph.FileName ,".bin" ) ) );
    end

    return 0
end

#--- graph_close: Funcao para fechar o grafico
function graph_close( graph::Graf , flag::String )

    if graph.Ext == 0
        if flag == "save"
            PSRIOGrafResult_closeSave( graph.Ptr )
        else
            PSRIOGrafResult_closeLoad( graph.Ptr )
        end
    else
        if flag == "save"
            PSRIOGrafResultBinary_closeSave( graph.Ptr )
        else
            PSRIOGrafResultBinary_closeLoad( graph.Ptr )
        end
    end

    graph.Ptr = C_NULL

    return 0
end

function graph_copy_config!( graph_new::Graf , graph_ori::Graf )

    graph_new.Ext        = graph_ori.Ext
    graph_new.IniStg     = graph_ori.IniStg
    graph_new.IniYear    = graph_ori.IniYear
    graph_new.NumStg     = graph_ori.NumStg
    graph_new.NumSer     = graph_ori.NumSer
    graph_new.NumBlc     = graph_ori.NumBlc
    graph_new.NumAgentes = graph_ori.NumAgentes
    graph_new.Simb       = graph_ori.Simb
    graph_new.Simh       = graph_ori.Simh
    graph_new.Sims       = graph_ori.Sims
    graph_new.StgType    = graph_ori.StgType
    graph_new.Unit2      = graph_ori.Unit2
    graph_new.Agentes    = graph_ori.Agentes

    return 0
end

"""
    Funcoes Auxiliares
"""

#--- get_parm_psrc: Funcao para obter dado do estudo de acordo com a keyword
function get_parm_psrc( ptr::Ptr{UInt8} , key_word::String , arg_type::String , default_val )

    if PSRModel_parm2( ptr , key_word ) == C_NULL
        if default_val == nothing
            Base.print("  ERROR. Please contact support")
            exit()
            return -1
        else
            return default_val
        end
    else

        if arg_type == "Int"
            return PSRParm_getInteger( PSRModel_parm2( ptr , key_word ) )
        else
            # do nothing
        end
    end
end

#--- load_masks: Funcao para carregar mascaras
function load_masks( path::String )
    
    path = normpath(path)

    #- Configuring Log Manager
    ilog = PSRManagerLog_getInstance(0);
    PSRManagerLog_initPortuguese(ilog);
    ilogcons = PSRLogSimpleConsole_create(0);
    PSRManagerLog_addLog(ilog, ilogcons);

    #- Configuring the Mask and Models Manager
    igmsk = PSRManagerIOMask_getInstance(0);
    iret  = PSRManagerIOMask_importFile(igmsk, joinpath(path,"Masks_SDDP_V10.2.pmk"));
    iret  = PSRManagerIOMask_importFile(igmsk, joinpath(path,"Masks_SDDP_V10.3.pmk"));
    iret  = PSRManagerIOMask_importFile(igmsk, joinpath(path,"Masks_SDDP_Blocks.pmk"));
    igmdl = PSRManagerModels_getInstance(0);
    iret  = PSRManagerModels_importFile(igmdl, joinpath(path,"Models_SDDP_V10.2.pmd"));
    iret  = PSRManagerModels_importFile(igmdl, joinpath(path,"Models_SDDP_V10.3.pmd"));
    iret  = PSRManagerModels_importFile(igmdl, joinpath(path,"Models_SDDP_Keywords.pmd"));

    return nothing;
end

#--- load_masks: Funcao para carregar mascara
function load_masks( path::String , mask_name::String , model_name::String )
    
    path = normpath(path)

    igmsk = PSRManagerIOMask_getInstance(0);
    igmdl = PSRManagerModels_getInstance(0);
    
    if PSRManagerIOMask_importFile( igmsk , joinpath( path , string( mask_name , ".pmk" ) ) ) == 0
        Base.print( string( "  Error when loading mask " , mask_name , ".pmk" ) )
    end

    if PSRManagerModels_importFile( igmdl , joinpath( path , string( model_name , ".pmd" ) ) ) == 0
        Base.print( string( "  Error when loading model " , model_name , ".pmd" ) )
    end

    return nothing;
end

"""
    Funcoes de Leitura dos dados de Entrada
"""

#--- read_sddp_config!: Funcao para ler configuracoes do SDDP
function read_sddp_config!( stdy::SddpStudy , path::String , vSDDP::Int = PSR_SDDP_VERSION_14 )

    #---------------------------
    #---  Defining variables ---
    #---------------------------

    local ptr_sddp      ::  Ptr{UInt8}
    local ptr_config    ::  Ptr{UInt8}
    local ptr_iexec     ::  Ptr{UInt8}
    local iosddp        ::  Ptr{UInt8}
    local iret          ::  Int

    local n             ::  SddpSizes
    local d             ::  SddpData
    local o             ::  SddpOptions

    n = stdy.sizes
    d = stdy.data
    o = stdy.options

    #---------------------------
    #---       Routines      ---
    #---------------------------

    #--- Corrige path
    path = normpath( string( path , "/" ) )

    #- Loading case
    ptr_sddp = PSRStudy_create(0);
    iosddp   = PSRIOSDDP_create(0);
    iret     = PSRIOSDDP_load( iosddp , ptr_sddp , path , path , vSDDP );
    
    #- Defining the collections to be used
    ptr_config = PSRStudy_getConfigurationModel( ptr_sddp );
    ptr_iexec  = PSRModel_model2( ptr_config , "ExecutionParameters")

    #--- Loading study configuration
    d.IniStg   = get_parm_psrc( ptr_config , "Etapa_inicial" , "Int" , nothing )
    d.IniYear  = get_parm_psrc( ptr_config , "Ano_inicial" , "Int" , nothing )
    d.StgType  = get_parm_psrc( ptr_config , "Tipo_Etapa" , "Int" , nothing )
    o.Simh     = get_parm_psrc( ptr_iexec  , "SIMH" , "Int" , nothing )
    o.Binf     = get_parm_psrc( ptr_iexec  , "BINF" , "Int" , 0 )
        
    n.Stg = PSRStudy_getNumberStages( ptr_sddp )
    n.Ser = PSRStudy_getNumberSimulations( ptr_sddp )
    n.Blc = PSRStudy_getNumberBlocks( ptr_sddp )

    return 0
end

#--- read_duration!: Funcao para ler duracao dos blocos
function read_duration!( stdy::SddpStudy , path::String , vSDDP::Int = PSR_SDDP_VERSION_14 )

    #---------------------------
    #---  Defining variables ---
    #---------------------------

    local ptr_sddp      ::  Ptr{UInt8}
    local ptr_config    ::  Ptr{UInt8}
    local ptr_iexec     ::  Ptr{UInt8}
    local iosddp        ::  Ptr{UInt8}
    local iret          ::  Int
    local iStg          ::  Int
    local iBlc          ::  Int
    local temp_simh     ::  Int
    local temp_nstg     ::  Int
    local temp_nblc     ::  Int

    local d             ::  SddpData

    d = stdy.data
    
    #---------------------------
    #---       Routines      ---
    #---------------------------

    #--- Corrige path
    path = normpath( string( path , "/" ) )

    #- Loading case
    ptr_sddp = PSRStudy_create(0);
    iosddp   = PSRIOSDDP_create(0);
    iret     = PSRIOSDDP_load( iosddp , ptr_sddp , path , path , vSDDP );
    
    #- Defining the collections to be used
    ptr_config = PSRStudy_getConfigurationModel( ptr_sddp );
    ptr_iexec  = PSRModel_model2( ptr_config , "ExecutionParameters")

    #--- Loading study configuration
    temp_simh  = get_parm_psrc( ptr_iexec  , "SIMH" , "Int" , nothing )        
    temp_nstg  = PSRStudy_getNumberStages( ptr_sddp )
    temp_nblc = PSRStudy_getNumberBlocks( ptr_sddp )

    #--- Numero de horas em cada estagio
    d.Hours = zeros( Int , temp_nstg )

    for iStg in 1:temp_nstg
        d.Hours[ iStg ] = PSRStudy_getStageDuration2( ptr_sddp , iStg )
    end

    #--- Numero de horas em cada bloco
    if temp_simh == 2
        d.Duraci = ones( Int , temp_nstg , maximum( d.Hours ) )
    else
        d.Duraci = zeros( Int , temp_nstg , temp_nblc )

        for iStg in 1:temp_nstg
            for iBlc in 1:temp_nblc
                d.Duraci[ iStg , iBlc ] = Int( round( PSRStudy_getStageDuration( ptr_sddp , iStg , iBlc ) , digits = 0 ) )
            end
        end
    end

    return 0
end

#--- read_sistemas: Function to get the data of simulated systems ---
function read_sistemas!( stdy::SddpStudy , path::String , PSR_STRING_SIZE::Int = 12 , vSDDP::Int = PSR_SDDP_VERSION_14 )

    #---------------------------
    #---  Defining variables ---
    #---------------------------

    local temp_sist_name    ::  String          # Local variable to buffer the name of systems name
    local temp_sist_id      ::  String          # Local variable to buffer the id of systems name

    local n                 ::  SddpSizes
    local d                 ::  SddpData
    local o                 ::  SddpOptions

    n = stdy.sizes
    d = stdy.data
    o = stdy.options

    #------------------
    #---- Routines ----
    #------------------

    #--- Corrige path
    path = normpath( string( path , "/" ) )

    #- Criando ponteiros
    ptr_sddp     = PSRStudy_create(0);
    iosddp       = PSRIOSDDP_create(0);
    iret         = PSRIOSDDP_load( iosddp , ptr_sddp , path , path , vSDDP );
    ptr_sist     = PSRStudy_getCollectionSystems(  ptr_sddp );
    ptr_map_sist = PSRMapData_create(0)
    PSRMapData_addElements( ptr_map_sist , ptr_sist )

    #- Creating arrays to allocate parameters
    n.Sis      = PSRCollectionElement_maxElements( ptr_sist )
    d.SisName  = Vector{String}( undef , n.Sis );
    d.SisId    = Vector{String}( undef , n.Sis );
    d.SisCode  = zeros( Int32 , n.Sis )

    temp_sist_name = allocstring( n.Sis , PSR_STRING_SIZE );
    temp_sist_id   = allocstring( n.Sis , PSR_STRING_SIZE );
    
    #- Mapping parameters
    PSRMapData_mapParm( ptr_map_sist , "name" , temp_sist_name , PSR_STRING_SIZE )
    PSRMapData_mapParm( ptr_map_sist , "id"   , temp_sist_id , PSR_STRING_SIZE )
    PSRMapData_mapParm( ptr_map_sist , "code" , d.SisCode , 0  )
    
    PSRMapData_pullToMemory( ptr_map_sist )

    d.SisName = splitstring( temp_sist_name , PSR_STRING_SIZE );
    d.SisId   = splitstring( temp_sist_id   , PSR_STRING_SIZE );

    return 0
end

#--- read_sistemas: Funcao para ler dados dos sistemas simulados no formato CSV (mascara do OptContract)
function read_sistemas( study::Ptr{UInt8} )

    #---------------------------
    #---  Defining variables ---
    #---------------------------
    
    local PtrSis        ::  Ptr{UInt8}
    local PtrMapSis     ::  Ptr{UInt8}
    local NumSis        ::  Int32
    local SisCode       ::  Vector{Int32}
    local SisNome       ::  Vector{String}
    local SisId         ::  Vector{String}
    local TempSisName   ::  String
    local TempSisId     ::  String

    #------------------
    #---- Routines ----
    #------------------

    #--- Cria ponteiros para colecao e de mapeamento
    PtrSis = PSRStudy_getCollectionByString( study , "Sistemas" )
    PtrMapSis = PSRMapData_create(0)
    PSRMapData_addElements( PtrMapSis , PtrSis )

    #--- Obtem numero de sistemas no arquivo
    NumSis = PSRCollectionElement_maxElements( PtrSis )
    
    #--- Cria variaveis auxiliares
    SisCode = zeros( Int32 , NumSis )
    SisNome = Vector{String}(undef , NumSis )
    SisId   = Vector{String}(undef , NumSis )

    TempSisName = allocstring( NumSis , 12 );
    TempSisId   = allocstring( NumSis , 2 );

    #--- Mapeia parametros
    PSRMapData_mapParm( PtrMapSis , "Nome" , TempSisName , 12 )
    PSRMapData_mapParm( PtrMapSis , "Codigo" , TempSisId , 2 )
    PSRMapData_mapParm( PtrMapSis , "Numero" , SisCode , 0 )

    #--- Coloca os dados na memoria
    PSRMapData_pullToMemory( PtrMapSis )

    #--- Split do nome e ID
    SisNome = splitstring( TempSisName , 12 );
    SisId = splitstring( TempSisId , 2 )

    return( PtrSis , PtrMapSis , NumSis , SisCode , SisNome , SisId )
end

#--- read_contratos: Funcao para ler dados dos contratos a depender do contexto utilizado
function read_contratos( study::Ptr{UInt8} , context::String )

    #---------------------------
    #---  Defining variables ---
    #---------------------------

    local PtrContr      ::  Ptr{UInt8}
    local PtrMapContr   ::  Ptr{UInt8}
    local NumContratos  ::  Int32
    local ContrFlag     ::  Vector{Int32}
    local ContrCode     ::  Vector{Int32}
    local ContrNome     ::  Vector{String}
    local ContrIni      ::  Vector{Date}
    local ContrFin      ::  Vector{Date}
    local ContrMont     ::  Vector{Float64}
    local ContrPreco    ::  Vector{Float64}
    local ContrSist     ::  Vector{String}
    local ContrTipoNeg  ::  Vector{Int32}
    local ContrTipoEne  ::  Vector{Int32}
    local ContrSazo     ::  Vector{Int32}
    local ContrMod      ::  Vector{Int32}
    local TempContrName ::  String
    local TempContrIni  ::  String
    local TempContrFin  ::  String
    local TempContrSis  ::  String
    local AuxContrIni   ::  Array{AbstractString,1}
    local AuxContrFin   ::  Array{AbstractString,1}
    local iContr        ::  Int

    #------------------
    #---- Routines ----
    #------------------

    if context == "OptContract"

        #--- Cria ponteiros para colecao e de mapeamento
        PtrContr = PSRStudy_getCollectionByString( study , "Contratos" )
        PtrMapContr = PSRMapData_create(0)
        PSRMapData_addElements( PtrMapContr , PtrContr )

        #--- Obtem numero de contratos no arquivo
        NumContratos = PSRCollectionElement_maxElements( PtrContr )
        
        #--- Cria variaveis auxiliares
        ContrFlag       = zeros( Int32 , NumContratos )
        ContrCode       = zeros( Int32 , NumContratos )
        ContrNome       = Vector{String}( undef , NumContratos )
        ContrIni        = Vector{Date}( undef , NumContratos )
        ContrFin        = Vector{Date}( undef , NumContratos )
        ContrMont       = zeros( Float64 , NumContratos )
        ContrPreco      = zeros( Float64 , NumContratos )
        ContrSist       = Vector{String}( undef , NumContratos )
        ContrTipoNeg    = zeros( Int32 , NumContratos )
        ContrTipoEne    = zeros( Int32 , NumContratos )
        ContrSazo       = zeros( Int32 , NumContratos )
        ContrMod        = zeros( Int32 , NumContratos )

        TempContrName   = allocstring( NumContratos , 12 );
        TempContrIni    = allocstring( NumContratos , 10 );
        TempContrFin    = allocstring( NumContratos , 10 );
        TempContrSis    = allocstring( NumContratos , 2 );

        #--- Mapeia parametros
        PSRMapData_mapParm( PtrMapContr , "Flag" , ContrFlag , 0 )
        PSRMapData_mapParm( PtrMapContr , "Codigo" , ContrCode , 0 )
        PSRMapData_mapParm( PtrMapContr , "Nome" , TempContrName , 12 )
        PSRMapData_mapParm( PtrMapContr , "Inicio" , TempContrIni , 10 )
        PSRMapData_mapParm( PtrMapContr , "Termino" , TempContrFin , 10 )
        PSRMapData_mapParm( PtrMapContr , "Montante" , ContrMont , 0 )
        PSRMapData_mapParm( PtrMapContr , "Preco" , ContrPreco , 0 )
        PSRMapData_mapParm( PtrMapContr , "Submercado" , TempContrSis , 2 )
        PSRMapData_mapParm( PtrMapContr , "TipoNeg" , ContrTipoNeg , 0 )
        PSRMapData_mapParm( PtrMapContr , "TipoEne" , ContrTipoEne , 0 )
        PSRMapData_mapParm( PtrMapContr , "Sazonalizacao" , ContrSazo , 0 )
        PSRMapData_mapParm( PtrMapContr , "Modulacao" , ContrMod , 0 )
        
        #--- Coloca os dados na memoria
        PSRMapData_pullToMemory( PtrMapContr )

        #--- Split do nome e das datas de inicio e fim
        ContrNome   = splitstring( TempContrName , 12 );
        ContrSist   = splitstring( TempContrSis , 2 );
        AuxContrIni = splitstring( TempContrIni , 10 );
        AuxContrFin = splitstring( TempContrFin , 10 );

        #--- Conversao para formato de data
        for iContr in 1:NumContratos
            try
                ContrIni[ iContr ] = Date( strip( AuxContrIni[ iContr ] ) , "d/m/y" )
                ContrFin[ iContr ] = Date( strip( AuxContrFin[ iContr ] ) , "d/m/y" )
            catch
                Base.print( string( "  Error when loading date of contract " , iContr ) )
                exit()
            end
        end
        
        return( PtrContr , PtrMapContr , NumContratos , ContrFlag , ContrCode , ContrNome , ContrIni , ContrFin , ContrMont , ContrPreco , ContrSist , ContrTipoNeg , ContrTipoEne , ContrSazo , ContrMod )
    else
        return nothing
    end
end

#--- read_duracao: Funcao para ler duracao dos blocos
function read_duracao( study::Ptr{UInt8} , context::String )

    #---------------------------
    #---  Defining variables ---
    #---------------------------

    local PtrDur        ::  Ptr{UInt8}
    local PtrMapDur     ::  Ptr{UInt8}
    local TempLenDados  ::  Int32
    local DurEtapas     ::  Vector{Int32}
    local DurBlocos     ::  Vector{Int32}
    local DurHoras      ::  Vector{Int32}
    
    #------------------
    #---- Routines ----
    #------------------

    if context == "OptContract"

        #--- Cria ponteiros para colecao e de mapeamento
        PtrDur = PSRStudy_getCollectionByString( study , "Blocos" )
        PtrMapDur = PSRMapData_create(0)
        PSRMapData_addElements( PtrMapDur , PtrDur )

        #--- Obtem numero de contratos no arquivo
        TempLenDados = PSRCollectionElement_maxElements( PtrDur )
                
        #--- Cria variaveis auxiliares
        DurEtapas = zeros( Int32 , TempLenDados )
        DurBlocos = zeros( Int32 , TempLenDados )
        DurHoras  = zeros( Int32 , TempLenDados )

        
        #--- Mapeia parametros
        PSRMapData_mapParm( PtrMapDur , "Etapa" , DurEtapas , 0 )
        PSRMapData_mapParm( PtrMapDur , "Bloco" , DurBlocos , 0 )
        PSRMapData_mapParm( PtrMapDur , "Hora" , DurHoras , 0 )
        
        #--- Coloca os dados na memoria
        PSRMapData_pullToMemory( PtrMapDur )
        
        return( DurEtapas , DurBlocos , DurHoras )
    else
        return nothing
    end
end

#--- read_agentes: Funcao para ler dados dos agentes simulados
function read_agentes( study::Ptr{UInt8} , context::String )

    #---------------------------
    #---  Defining variables ---
    #---------------------------
    
    local PtrAgente     ::  Ptr{UInt8}
    local PtrMapAgente  ::  Ptr{UInt8}
    local NumAgentes    ::  Int32
    local AgentCode     ::  Vector{Int32}
    local AgentNome     ::  Vector{String}
    local AgentId       ::  Vector{String}
    local TempAgentName ::  String
    local TempAgentId   ::  String

    #------------------
    #---- Routines ----
    #------------------

    if context == "OptContract"

        #--- Cria ponteiros para colecao e de mapeamento
        PtrAgente = PSRStudy_getCollectionByString( study , "Agentes" )
        PtrMapAgente = PSRMapData_create(0)
        PSRMapData_addElements( PtrMapAgente , PtrAgente )

        #--- Obtem numero de sistemas no arquivo
        NumAgentes = PSRCollectionElement_maxElements( PtrAgente )
        
        #--- Cria variaveis auxiliares
        AgentCode = zeros( Int32 , NumAgentes )
        AgentNome = Vector{String}(undef , NumAgentes )
        AgentId   = Vector{String}(undef , NumAgentes )

        TempAgentName = allocstring( NumAgentes , 24 );
        TempAgentId   = allocstring( NumAgentes , 12 );

        #--- Mapeia parametros
        PSRMapData_mapParm( PtrMapAgente , "Nome" , TempAgentName , 24 )
        PSRMapData_mapParm( PtrMapAgente , "Codigo" , TempAgentId , 12 )
        PSRMapData_mapParm( PtrMapAgente , "Numero" , AgentCode , 0 )

        #--- Coloca os dados na memoria
        PSRMapData_pullToMemory( PtrMapAgente )

        #--- Split do nome e ID
        AgentNome = splitstring( TempAgentName , 24 );
        AgentId = splitstring( TempAgentId , 12 )

        return( PtrAgente , PtrMapAgente , NumAgentes , AgentCode , AgentNome , AgentId )

    else

        return  nothing

    end
end

#--- read_plants: Funcao para ler usinas na base de dados dos agentes simulados
function read_plants( study::Ptr{UInt8} , context::String )

    #---------------------------
    #---  Defining variables ---
    #---------------------------
    
    local PtrPlants         ::  Ptr{UInt8}
    local PtrMapPlants      ::  Ptr{UInt8}
    local NumPlants         ::  Int32
    local PlantsCode        ::  Vector{Int32}
    local AgentNome         ::  Vector{String}
    local PlantsTipo        ::  Vector{String}
    local PlantsSis         ::  Vector{Int32}
    local TempPlantsName    ::  String
    local TempPlantsTipo    ::  String

    #------------------
    #---- Routines ----
    #------------------

    if context == "OptContract"

        #--- Cria ponteiros para colecao e de mapeamento
        PtrPlants = PSRStudy_getCollectionByString( study , "Usinas" )
        PtrMapPlants = PSRMapData_create(0)
        PSRMapData_addElements( PtrMapPlants , PtrPlants )

        #--- Obtem numero de sistemas no arquivo
        NumPlants = PSRCollectionElement_maxElements( PtrPlants )
        
        #--- Cria variaveis auxiliares
        PlantsCode = zeros( Int32 , NumPlants )
        PlantsNome = Vector{String}(undef , NumPlants )
        PlantsTipo = Vector{String}(undef , NumPlants )
        PlantsSis  = zeros( Int32 , NumPlants )

        TempPlantsName = allocstring( NumPlants , 24 );
        TempPlantsTipo = allocstring( NumPlants , 1 );

        #--- Mapeia parametros
        PSRMapData_mapParm( PtrMapPlants , "Numero" , PlantsCode , 0 )
        PSRMapData_mapParm( PtrMapPlants , "Nome" , TempPlantsName , 24 )
        PSRMapData_mapParm( PtrMapPlants , "Tipo" , TempPlantsTipo , 1 )
        PSRMapData_mapParm( PtrMapPlants , "Sistema" , PlantsSis , 0 )

        #--- Coloca os dados na memoria
        PSRMapData_pullToMemory( PtrMapPlants )

        #--- Split do nome e ID
        PlantsNome = splitstring( TempPlantsName , 24 );
        PlantsTipo = splitstring( TempPlantsTipo , 1 )

        return( PtrPlants , PtrMapPlants , NumPlants , PlantsCode , PlantsNome , PlantsTipo , PlantsSis )

    else

        return  nothing

    end
end

#--- read_portfolio: Funcao para ler dados dos portfolios
function read_portfolio( study::Ptr{UInt8} , context::String )

    #---------------------------
    #---  Defining variables ---
    #---------------------------
    
    local PtrPortfolio      ::  Ptr{UInt8}
    local PtrMapPortfolio   ::  Ptr{UInt8}
    local LenDados          ::  Int32
    local PlantCode         ::  Vector{Int32}
    local PlantNome         ::  Vector{String}
    local AgentId           ::  Vector{String}
    local PlantShare        ::  Vector{Float64}
    local TempPlantNome     ::  String
    local TempAgentId       ::  String

    #------------------
    #---- Routines ----
    #------------------

    if context == "OptContract"

        #--- Cria ponteiros para colecao e de mapeamento
        PtrPortfolio = PSRStudy_getCollectionByString( study , "Portfolio" )
        PtrMapPortfolio = PSRMapData_create(0)
        PSRMapData_addElements( PtrMapPortfolio , PtrPortfolio )

        #--- Obtem numero de sistemas no arquivo
        LenDados = PSRCollectionElement_maxElements( PtrPortfolio )
        
        #--- Cria variaveis auxiliares
        PlantCode  = zeros( Int32 , LenDados )
        PlantNome  = Vector{String}( undef , LenDados )
        AgentId    = Vector{String}( undef , LenDados )
        PlantShare = zeros( Float64 , LenDados )

        TempPlantNome = allocstring( LenDados , 24 );
        TempAgentId   = allocstring( LenDados , 12 );

        #--- Mapeia parametros
        PSRMapData_mapParm( PtrMapPortfolio , "Numero" , PlantCode , 0 )
        PSRMapData_mapParm( PtrMapPortfolio , "Nome" , TempPlantNome , 24 )
        PSRMapData_mapParm( PtrMapPortfolio , "Agente" , TempAgentId , 12 )
        PSRMapData_mapParm( PtrMapPortfolio , "Share" , PlantShare , 0 )

        #--- Coloca os dados na memoria
        PSRMapData_pullToMemory( PtrMapPortfolio )

        #--- Split do nome e ID
        PlantNome = splitstring( TempPlantNome , 24 );
        AgentId = splitstring( TempAgentId , 12 );

        return( PtrPortfolio , PtrMapPortfolio , PlantCode , PlantNome , AgentId , PlantShare )

    else

        return  nothing

    end
end

#--- read_garantia_fisica: Funcao para ler dados de garantia fisica
function read_garantia_fisica( study::Ptr{UInt8} , context::String )

    #---------------------------
    #---  Defining variables ---
    #---------------------------
    
    local PtrPlantGF        ::  Ptr{UInt8}
    local PtrMapPlantGF     ::  Ptr{UInt8}
    local LenDados          ::  Int32
    local PlantCode         ::  Vector{Int32}
    local PlantNome         ::  Vector{String}
    local TipoGF            ::  Vector{String}
    local DataInicial       ::  Vector{Date}
    local DataFinal         ::  Vector{Date}
    local TempPlantNome     ::  String
    local TempTipoGF        ::  String
    local TempDataIni       ::  String
    local TempDataFin       ::  String
    local AuxDataIni        ::  Array{AbstractString,1}
    local AuxDataFin        ::  Array{AbstractString,1}
    local iRow              ::  Int

    #------------------
    #---- Routines ----
    #------------------

    if context == "OptContract"

        #--- Cria ponteiros para colecao e de mapeamento
        PtrPlantGF      = PSRStudy_getCollectionByString( study , "GarantiaFisica" )
        PtrMapPlantGF   = PSRMapData_create(0)
        PSRMapData_addElements( PtrMapPlantGF , PtrPlantGF )

        #--- Obtem numero de sistemas no arquivo
        LenDados = PSRCollectionElement_maxElements( PtrPlantGF )
        
        #--- Cria variaveis auxiliares
        PlantCode       =   zeros( Int32 , LenDados )
        PlantNome       =   Vector{String}( undef , LenDados )
        TipoGF          =   Vector{String}( undef , LenDados )
        DataInicial     =   Vector{Date}( undef , LenDados )
        DataFinal       =   Vector{Date}( undef , LenDados )
        GarantiaFisica  =   zeros( Float64 , LenDados )
        
        TempPlantNome   = allocstring( LenDados , 24 );
        TempTipoGF      = allocstring( LenDados , 3 );
        TempDataIni     = allocstring( LenDados , 10 );
        TempDataFin     = allocstring( LenDados , 10 );

        #--- Mapeia parametros
        PSRMapData_mapParm( PtrMapPlantGF , "Numero" , PlantCode , 0 )
        PSRMapData_mapParm( PtrMapPlantGF , "Nome" , TempPlantNome , 24 )
        PSRMapData_mapParm( PtrMapPlantGF , "Tipo" , TempTipoGF , 3 )
        PSRMapData_mapParm( PtrMapPlantGF , "DataInicial" , TempDataIni , 10 )
        PSRMapData_mapParm( PtrMapPlantGF , "DataFinal" , TempDataFin , 10 )
        PSRMapData_mapParm( PtrMapPlantGF , "GarantiaFisica" , GarantiaFisica , 0 )

        #--- Coloca os dados na memoria
        PSRMapData_pullToMemory( PtrMapPlantGF )

        #--- Split do nome e ID
        PlantNome   = splitstring( TempPlantNome , 24 );
        TipoGF      = splitstring( TempTipoGF , 3 );
        AuxDataIni  = splitstring( TempDataIni , 10 )
        AuxDataFin  = splitstring( TempDataFin , 10 )

        #--- Conversao para formato de data
        for iRow in 1:LenDados
            try
                DataInicial[ iRow ] = Date( strip( AuxDataIni[ iRow ] ) , "d/m/y" )
                DataFinal[ iRow ]   = Date( strip( AuxDataFin[ iRow ] ) , "d/m/y" )
            catch
                Base.print( string( "  Error when loading date. Row " , iRow , " - garfis.dat" ) )
                exit()
            end
        end

        return( PtrPlantGF , PtrMapPlantGF , LenDados , PlantCode , PlantNome , TipoGF , DataInicial , DataFinal , GarantiaFisica )

    else

        return  nothing

    end
end

#--- read_aversao: Funcao para ler dados de aversao ao risco dos agentes
function read_aversao( study::Ptr{UInt8} , context::String )

    #---------------------------
    #---  Defining variables ---
    #---------------------------
    
    local PtrAgente     ::  Ptr{UInt8}
    local PtrMapAgente  ::  Ptr{UInt8}
    local NumAgentes    ::  Int32
    local AgentCode     ::  Vector{Int32}
    local AgentNome     ::  Vector{String}
    local AgentId       ::  Vector{String}
    local TempAgentName ::  String
    local TempAgentId   ::  String

    #------------------
    #---- Routines ----
    #------------------

    if context == "OptContract"

        #--- Cria ponteiros para colecao e de mapeamento
        PtrClass    = PSRStudy_getCollectionByString( study , "AversaoAgentes" )
        PtrMapClass = PSRMapData_create(0)
        PSRMapData_addElements( PtrMapClass , PtrClass )

        #--- Obtem numero de sistemas no arquivo
        LenDados = PSRCollectionElement_maxElements( PtrClass )
        
        #--- Cria variaveis auxiliares
        AgentCode       =   zeros( Int32 , LenDados )
        AgentNome       =   Vector{String}(undef , LenDados )
        AgentId         =   Vector{String}(undef , LenDados )
        AgentAver       =   zeros( Float64 , LenDados )
        TempAgentName   =   allocstring( LenDados , 24 );
        TempAgentId     =   allocstring( LenDados , 12 );

        #--- Mapeia parametros
        PSRMapData_mapParm( PtrMapClass , "Numero" , AgentCode , 0 )
        PSRMapData_mapParm( PtrMapClass , "Nome" , TempAgentName , 24 )
        PSRMapData_mapParm( PtrMapClass , "Codigo" , TempAgentId , 12 )
        PSRMapData_mapParm( PtrMapClass , "Aversao" , AgentAver , 0 )
        
        #--- Coloca os dados na memoria
        PSRMapData_pullToMemory( PtrMapClass )

        #--- Split do nome e ID
        AgentNome   =   splitstring( TempAgentName , 24 );
        AgentId     =   splitstring( TempAgentId , 12 )

        return( PtrClass , PtrMapClass , LenDados , AgentCode , AgentNome , AgentId , AgentAver )

    else

        return  nothing

    end
end

"""
    Funcoes de Tratamento de Colecoes Genericas
"""

function PSRStudy_getCollectionByString(istdy::Ptr{UInt8}, entity::String)
    # "PSRLoan"
    istring = PSRCollectionString_create(istdy)
  
    # ptrClassNameFilters->addString("PSRLoan");
    PSRCollectionString_addString(istring, entity)
  
    # PSRCollectionElement *ptrColElement = getCollectionElements(NULL, ptrClassNameFilters);
    lst = PSRStudy_getCollectionElements(istdy, C_NULL, istring)
    # delete ptrClassNameFilters;
  
    # ptrColElement->removeRedundant();
    PSRCollectionElement_removeRedundant( lst )
  
    return lst;
end

#--- read_study: Funcao que cria ponteiro para o estudo e carrega contexto associado a ele
function read_study( path::String , context::String )

    iStdy = PSRStudy_create(0)
    iGmsk = PSRManagerIOMask_getInstance(0);

    if PSRManagerIOMask_loadAutomaticData( iGmsk , iStdy , context , path ) == PSR_IO_FAILED
        Base.print( string( "  Error when loading the " , context , " database" ) )
    end

    return( iStdy )
end

#--- create_time_controller: Funcao que cria ponteiro para o TimeController
function create_time_controller( study::Ptr{UInt8} )
    MyCtrl = PSRTimeController_create(0);
    PSRTimeController_addElement(    MyCtrl , study , true );
    PSRTimeController_configureFrom( MyCtrl , study        );
    return( MyCtrl )
end