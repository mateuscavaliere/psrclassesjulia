@static if VERSION < v"0.7"
  const Nothing = Void
end
function PSRClasses_init(dllpath)
  Libdl.dlopen(joinpath(dllpath,"PSRClasses"))
end
function allocstring(numelements, numcharacters)
  return " "^(numelements*numcharacters)
end
function splitstring(str, numcharacters)
  len = length(str)
  qtde = Int32(round(len/numcharacters))
  @static if VERSION >= v"0.7"
    retarray = Array{AbstractString}(undef, qtde)
  else
    retarray = Array{AbstractString}(qtde)
  end
  pos = 1
  for num = 1:qtde
    retarray[num] = str[pos:(pos+numcharacters-1)]
    pos = pos+numcharacters
  end
  return retarray  
end
CLASS_PSRElement = 0
CLASS_PSRStudy = 1
CLASS_PSRSystem = 2
CLASS_PSRPlant = 3
CLASS_PSRBus = 4
CLASS_PSRArea = 5
CLASS_PSRDevice = 6
CLASS_PSRNetwork = 7
CLASS_PSRSerie = 8
CLASS_PSRTransformer = 9
CLASS_PSRShunt = 10
CLASS_PSRCapacitor = 11
CLASS_PSRReactor = 12
CLASS_PSRGenerator = 13
CLASS_PSRLoad = 14
CLASS_PSRThermalPlant = 16
CLASS_PSRHydroPlant = 17
CLASS_PSRBusDC = 18
CLASS_PSRCircuitDC = 19
CLASS_PSRConverterDCAC = 20
CLASS_PSRLinkDC = 21
CLASS_PSRSerieCapacitor = 22
CLASS_PSRLineReactor = 23
CLASS_PSRFuel = 24
CLASS_PSRDemand = 25
CLASS_PSRGasNode = 26
CLASS_PSRGasPipeline = 27
CLASS_PSRGasNetwork = 28
CLASS_PSRMaintenanceData = 29
CLASS_PSRHydrologicalNetwork = 30
CLASS_PSRHydrologicalPlantNetwork = 31
CLASS_PSRGaugingStation = 32
CLASS_PSRPowerInjection = 33
CLASS_PSRInterconnection = 34
CLASS_PSRConstraintData = 35
CLASS_PSRDemandSegment = 36
CLASS_PSRNetworkDC = 37
CLASS_PSRSynchronousCompensator = 38
CLASS_PSRGndPlant = 39
CLASS_PSRInterconnectionNetwork = 40
CLASS_PSRConstraintSumData = 41
CLASS_PSRRegion = 42
CLASS_PSRExpansionProject = 43
CLASS_PSRExpansionDecision = 44
CLASS_PSRSubstation = 45
CLASS_PSRExpansionConstraint = 46
CLASS_PSRExpansionDisbursement = 47
CLASS_PSRExpansionCapacity = 48
CLASS_PSRExpansionPrecedence = 49
CLASS_PSRFuelConsumption = 50
CLASS_PSRBattery = 51
CLASS_PSRGenerationConstraintData = 52
CLASS_PSRReserveGenerationConstraintData = 53
CLASS_PSRHydrologicalPlantConnection = 54
CLASS_PSRContract = 55
CLASS_PSRGndGaugingStation = 56
CLASS_PSRHydrologicalConnection = 57
CLASS_PSRWeatherStation = 58
CLASS_PSRFuelContract = 59
CLASS_PSRFuelReservoir = 60
CLASS_PSRInterconnectionSumData = 61
CLASS_PSRCircuitSumData = 62
CLASS_PSRThermalCombinedCycle = 63
CLASS_PSRExpansionGeneric = 64
CLASS_PSRRiverNetwork = 65
CLASS_PSRRiverConnection = 66
CLASS_PSRExpansionRiverTopology = 67
CLASS_PSRExpansionSatisfaction = 68
CLASS_PSRBalancingArea = 69
CLASS_PSRContingencyReserve = 70
CLASS_PSRExpansionAssociated = 71
CLASS_PSRExpansionExclusive = 72
CLASS_PSRGasEmission = 73
CLASS_PSRBusSumData = 74
CLASS_PSRCircuitFlowConstraint = 75
CLASS_PSRCircuitCorridorConstraint = 76
CLASS_PSRCircuitEnviromentalConstraint = 77
CLASS_PSRCircuitPathConstraint = 78
CLASS_PSRBusAngleConstraint = 79
CLASS_PSRTransformer3Winding = 80
CLASS_PSRGenericConstraint = 81
CLASS_PSRReservoirSet = 82
CLASS_PSRStaticVarCompensator = 83
CLASS_PSRSensitivity = 84
CLASS_PSRConverterDCAC_LCC = 85
CLASS_PSRConverterDCAC_P2P = 86
CLASS_PSRConverterDCAC_VSC = 87
CLASS_PSRSpinningReserveGroup = 88
PSR_OK = 1
PSR_FAILED = 0
PSR_END = 2
PSR_PARM_STRING = 1
PSR_PARM_INTEGER = 2
PSR_PARM_REAL = 3
PSR_PARM_DATE = 4
PSR_PARM_STRING_POINTER = 5
PSR_PARM_INTEGER_POINTER = 6
PSR_PARM_REAL_POINTER = 7
PSR_PARM_DATE_POINTER = 8
PSR_PARM_REFERENCE = 9
PSR_DATEFORMAT_MMYY = 1
PSR_DATEFORMAT_MMYYYY = 2
PSR_DATEFORMAT_DDMMYY = 3
PSR_DATEFORMAT_MMDDYY = 4
PSR_DATEFORMAT_DDMMYYYY = 5
PSR_DATEFORMAT_MMDDYYYY = 6
PSR_DATEFORMAT_WWYYYY = 7
PSR_DATEFORMAT_TYYYYMM = 8
PSR_DATEFORMAT_AUTOMATIC = 9
PSR_DATEFORMAT_DDMMYYYYTIME = 10
PSR_DEVICETYPE_GENERIC = 0
PSR_DEVICETYPE_SHUNT = 1
PSR_DEVICETYPE_GENERATOR = 2
PSR_DEVICETYPE_LOAD = 3
PSR_DEVICETYPE_SERIE = 4
PSR_DEVICETYPE_TRANSFORMER = 5
PSR_DEVICETYPE_CAPACITOR = 6
PSR_DEVICETYPE_REACTOR = 7
PSR_DEVICETYPE_LINEREACTOR = 8
PSR_DEVICETYPE_SERIECAPACITOR = 9
PSR_DEVICETYPE_POWERINJECTION = 10
PSR_DEVICETYPE_SYNCHRONOUS_COMPENSATOR = 11
PSR_DEVICETYPE_CIRCUIT_DC = 12
PSR_DEVICETYPE_CONVERTER_DCAC = 13
PSR_DEVICETYPE_LINKDC = 14
PSR_DEVICETYPE_BATTERY = 15
PSR_DEVICETYPE_TRANSMISSIONLINE = 16
PSR_DEVICETYPE_TRANSFORMER3WINDING = 17
PSR_DEVICETYPE_STATICVARCOMPENSATOR = 18
PSR_DEVICETYPE_CONVERTER_DCAC_P2P = 20
PSR_DEVICETYPE_CONVERTER_DCAC_VSC = 21
PSR_STAGETYPE_WEEKLY = 1
PSR_STAGETYPE_MONTHLY = 2
PSR_STAGETYPE_QUARTERLY = 3
PSR_STAGETYPE_HOURLY = 4
PSR_STAGETYPE_DAILY = 5
PSR_STAGETYPE_13MONTHLY = 6
PSR_STAGETYPE_YEARLY = 10
PSR_INDEXEDSEARCH_GENERAL = 0
PSR_INDEXEDSEARCH_COMPLETED_WEEK = 1
PSR_INDEXEDSEARCH_COMPLETED_MONTH = 2
PSR_PLANTTYPE_GENERIC = 0
PSR_PLANTTYPE_HYDRO = 1
PSR_PLANTTYPE_THERMAL = 2
PSR_PLANTTYPE_GND = 3
PSR_THERMALPLANTTYPE_CONVENTIONAL = 0
PSR_THERMALPLANTTYPE_MUSTRUN = 1
PSR_THERMALPLANTTYPE_BENEFIT = 2
PSR_MAINTENANCETYPE_UNITY = 1
PSR_MAINTENANCETYPE_PERCENTAGE = 2
PSR_MAINTENANCETYPE_POWER = 3
PSR_MAINTENANCETYPE_INFLOW = 4
PSR_MAINTENANCE_ACTIONTYPE_REDUCTION = 1
PSR_MAINTENANCE_ACTIONTYPE_ABSOLUTE = 2
PSR_DECISIONTYPE_EXACTLY = 1
PSR_DECISIONTYPE_ATLEAST = 2
PSR_DECISIONTYPE_ATMOST = 3
PSR_RELATIONSHIP_1TO1 = 1
PSR_RELATIONSHIP_NTO1 = 2
PSR_RELATIONSHIP_1TON = 3
PSR_RELATIONSHIP_FROM = 4
PSR_RELATIONSHIP_TO = 5
PSR_RELATIONSHIP_CONTROLLED_BY = 6
PSR_RELATIONSHIP_TURBINED_TO = 7
PSR_RELATIONSHIP_SPILLED_TO = 8
PSR_RELATIONSHIP_FILTRATION_TO = 9
PSR_RELATIONSHIP_ASSOCIATED_RESERVOIR = 10
PSR_RELATIONSHIP_NEUTRAL = 11
PSR_MASK_GENERCIC = 0
PSR_MASK_ROWDATA = 1
PSR_MASK_MIXROWDATA = 2
PSR_MASK_CSVDATA = 3
PSR_MASK_TSDATA = 4
PSR_MASK_AUTOSET_PARAMETRO = 1
PSR_MASK_AUTOSET_VETOR = 2
PSR_SYSTEM_OK = 1
PSR_SYSTEM_PLANT_EXIST = 2
PSR_SYSTEM_DEMAND_UNDEFINED = 3
PSR_SYSTEM_DEMAND_NULL = 4
PSR_SYSTEM_DATA_ERROR = 5
PSR_UPDATE_FAILED = 0
PSR_UPDATE_OK = 1
PSR_UPDATE_MODEL_ERROR = 2
PSR_UPDATE_NOACTION = 3
PSR_IO_FAILED = 0
PSR_IO_OK = 1
PSR_IO_END = 2
PSR_IO_DATAERROR = 3
PSR_IO_HEADER_FAILED = 4
PSR_IO_BLANKDATA = 5
PSR_IO_CONFIGERROR = 6
PSR_IO_DATAWARNING = 7
PSR_MANAGER_IOMASK_FAILED = 0
PSR_MANAGER_IOMASK_OK = 1
PSR_MANAGER_MODEL_FAILED = 0
PSR_MANAGER_MODEL_OK = 1
PSR_MANAGER_MODEL_DUPLICATED = 2
PSR_MANAGER_MODEL_UNDEFINED = 3
PSR_MANAGER_ELEMENT_FAILED = 0
PSR_MANAGER_ELEMENT_OK = 1
PSR_MANAGER_ELEMENT_CONTAINER_UNDEFINED = 2
PSR_MANAGER_ELEMENT_ELEMENT_UNDEFINED = 3
PSR_BUILDER_FAILED = 0
PSR_BUILDER_OK = 1
PSR_LOG_GENERAL = 1
PSR_LOG_IO = 2
PSR_LOG_BUS = 3
PSR_LOG_CIRCUIT = 4
PSR_LOG_PLANT = 5
PSR_LOG_MASK = 6
PSR_LOG_READER = 7
PSR_LOG_MODEL = 8
PSR_LOG_VECTOR = 9
PSR_LOG_SYSTEM = 10
PSR_LOG_AREA = 11
PSR_LOG_PARM = 12
PSR_LOG_MATH = 13
PSR_LOG_GENERATOR = 14
PSR_LOG_LOAD = 15
PSR_LOG_SHUNT = 16
PSR_LOG_FUEL = 17
PSR_LOG_GAUGINGSTATION = 18
PSR_LOG_DEMAND = 19
PSR_LOG_DEMANDSEGMENT = 20
PSR_LOG_GASNODE = 21
PSR_LOG_GASPIPELINE = 22
PSR_LOG_GNDGAUGINGSTATION = 23
PSR_LOG_CONSTRAINT = 24
PSR_LOG_FUELCONTRACT = 25
PSR_LOG_FUELRESERVOIR = 26
PSR_LOG_GASEMISSION = 27
PSR_LOG_INFO = 1
PSR_LOG_FAILED = 2
PSR_LOG_UNDEFINED = 3
PSR_LOG_NOGENERATOR = 4
PSR_LOG_NOMATCH = 5
PSR_LOG_NOREAD = 6
PSR_LOG_NOWRITE = 7
PSR_LOG_UNDEFINEDCHILD = 8
PSR_LOG_INVALIDPARAMETERSNUMBER = 9
PSR_LOG_INVALIDDATATYPE = 10
PSR_LOG_UNEXPECTEDINSTRUCTION = 11
PSR_LOG_DUPLICATED = 12
PSR_LOG_EMPTY = 13
PSR_LOG_OUT_OF_RANGE = 14
PSR_LOG_NULL = 15
PSR_LOG_INVALIDREFERENCE = 16
PSR_LOG_INVALIDFORMAT = 17
PSR_LOG_INVALIDVERSION = 18
PSR_MERGE_SOURCE_DOMINANT = 1
PSR_MERGE_TARGET_DOMINANT = 2
PSR_MERGE_NOSOURCE_IGNORE = 4
PSR_MERGE_NOTARGET_IGNORE = 8
PSR_MERGE_CHILD = 16
PSR_MERGE_DELETE = 32
PSR_MAPDATA_NOTNULL = 1
PSR_DEFAULT = 0
PSR_REPEAT_LAST = 1
PSR_OPTION_VECTOR_ONLYFIRST = 1
PSR_OPTION_VECTOR_EXCEPTFIRST = 2
PSR_OPTION_VECTOR_ALL = 3
PSR_ORDER_ASCENDING = 1
PSR_ORDER_DESCENDING = 2
PSR_SERIALIZATION_TYPE_FULL = 0
PSR_SERIALIZATION_TYPE_ONLYSTATIC = 1
PSR_COMPARISON_PREFERENCE_CODE = 0
PSR_COMPARISON_PREFERENCE_NAME = 1
PSR_SDDP_VERSION_10_2 = 1
PSR_SDDP_VERSION_12 = 2
PSR_SDDP_VERSION_14 = 3
PSR_SDDP_VERSION_15 = 4
PSR_NETPLAN_VERSION_2_6 = 1
PSR_NETPLAN_VERSION_3_0 = 2
PSR_NETPLAN_VERSION_4_0 = 3
STRUCTURED_REGISTRY_GENERAL = 0
STRUCTURED_REGISTRY_VERSION = 1
STRUCTURED_REGISTRY_AGENT_INFORMATION = 2
STRUCTURED_REGISTRY_STAGE_INFORMATION = 3
STRUCTURED_REGISTRY_DATA = 4
SIMPLE_CURRENT_VERSION = 1
PSRIO_GRAF_FORMAT_DEFAULT = 1
PSRIO_GRAF_FORMAT_NORMALIZED = 2
PSRIO_GRAF_FORMAT_NORMALIZED_PRINTZEROS = 3
PSRIO_GRAF_FORMAT_EXTENDED = 4
PSRIO_GRAF_FORMAT_BINARY = 5
PSRIO_GRAF_NAMELENGTH_AUTO = -1
MAX_SEG = 20
MAP_INFOTYPE_NULL = -1
MAP_INFOTYPE_DATA = 0
MAP_INFOTYPE_BEFOREDATA = 1
MAP_INFOTYPE_AFTERDAY = 2
MAP_INFOTYPE_AFTERWEEK = 3
MAP_INFOTYPE_AFTERMONTH = 4
MAP_INFOTYPE_AFTERYEAR = 5
YEAR_TRANSFORM = 400
QUERY_CONDITION_EQUAL = 1
QUERY_CONDITION_GREATER = 2
QUERY_CONDITION_LOWER = 3
QUERY_CONDITION_GREATEREQUAL = 4
QUERY_CONDITION_LOWEREQUAL = 5
QUERY_CONDITION_INEQUAL = 6
VECTOR_POSITION_STATUS_DATA = 0
VECTOR_POSITION_STATUS_BEFOREDATA = 1
VECTOR_POSITION_STATUS_AFTERDAY = 2
VECTOR_POSITION_STATUS_AFTERWEEK = 3
VECTOR_POSITION_STATUS_AFTERMONTH = 4
VECTOR_POSITION_STATUS_AFTERYEAR = 5
function PSRIOGrafResultCassandra_create(iprt)
  ptr_pointer =  ccall((:PSRIOGrafResultCassandra_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGrafResultCassandra_initSave( ptr_pointer,ptrStudy, namebinary )
  return ccall((:PSRIOGrafResultCassandra_initSave, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,namebinary)
end
function PSRIOGrafResultS3_create(iprt, aws)
  ptr_pointer =  ccall((:PSRIOGrafResultS3_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},Ptr{UInt8},),C_NULL,aws)
  return ptr_pointer
end
function PSRIOGrafResultS3_initSave( ptr_pointer, namebinary, format, manual_mode )
  return ccall((:PSRIOGrafResultS3_initSave, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),ptr_pointer ,namebinary, format, manual_mode)
end
function PSRIOGrafResultS3_setRevision( ptr_pointer,revision )
  return ccall((:PSRIOGrafResultS3_setRevision, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,revision)
end
function SDDPExtension_create(iprt)
  ptr_pointer =  ccall((:SDDPExtension_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function SDDPExtension_call( ptr_pointer,_descricao )
  return ccall((:SDDPExtension_call , "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_descricao)
end
function SDDPExtension_setStudy( ptr_pointer,ptrStudy )
  return ccall((:SDDPExtension_setStudy, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIO_HEADEREDBIN_AgentRule_create(iprt,_ptrStudy)
end
function PSRIO_HEADEREDBIN_AgentRule_isGrafAgents( ptr_pointer )
end
function PSRIO_HEADEREDBIN_AgentRule_getCode( ptr_pointer,name_agent )
end
function PSRIO_HEADEREDBIN_AgentRule_getCode2( ptr_pointer,index_agent )
end
function PSRIO_HEADEREDBIN_AgentRule_getAgentsNumber( ptr_pointer )
end
function PSRIO_HEADEREDBIN_AgentRule_getGrafAgent( ptr_pointer,index_agent )
  retPtr = "                                                                                                                  "
  return split(retPtr,"\u00")[1]
end
function PSRIO_HEADEREDBIN_AgentRule_getAgentElement( ptr_pointer,index_agent )
end
function PSRIO_HEADEREDBIN_AgentRule_getAgentFactor( ptr_pointer,index_agent )
end
function PSRIO_HEADEREDBIN_AgentRule_updateScenary( ptr_pointer,stageDate, simulation, block )
end
function PSRIO_HEADEREDBIN_AgentRule_mapIndexToAgent( ptr_pointer,index, code )
end
function PSRIO_HEADEREDBIN_AgentRule_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_create(iprt,ptrStudy)
  ptr_pointer =  ccall((:PSRIO_HEADEREDBIN_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,ptrStudy)
  return ptr_pointer
end
function PSRIO_HEADEREDBIN_selectStage( ptr_pointer,stage )
  return ccall((:PSRIO_HEADEREDBIN_selectStage, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,stage)
end
function PSRIO_HEADEREDBIN_selectScenario( ptr_pointer,scenario )
  return ccall((:PSRIO_HEADEREDBIN_selectScenario, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,scenario)
end
function PSRIO_HEADEREDBIN_addGrafResult( ptr_pointer,ptrIOGrafResult, ptrAgentRule )
  return ccall((:PSRIO_HEADEREDBIN_addGrafResult, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrIOGrafResult,ptrAgentRule)
end
function PSRIO_HEADEREDBIN_addGrafResultBinary( ptr_pointer,ptrIOGrafResultBinary, ptrAgentRule )
  return ccall((:PSRIO_HEADEREDBIN_addGrafResultBinary, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrIOGrafResultBinary,ptrAgentRule)
end
function PSRIO_HEADEREDBIN_saveFromGrafResult( ptr_pointer,nomeheader, nomeData )
  return ccall((:PSRIO_HEADEREDBIN_saveFromGrafResult, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nomeheader,nomeData)
end
function PSRIO_HEADEREDBIN_saveDefaultResult( ptr_pointer,nomeheader, nomeData, maxStages, maxSeries, maxBlocks, defaultValue, ptrAgentRule )
  return ccall((:PSRIO_HEADEREDBIN_saveDefaultResult, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Int32, Int32, Float64, Ptr{UInt8},),ptr_pointer ,nomeheader,nomeData,maxStages,maxSeries,maxBlocks,defaultValue,ptrAgentRule)
end
function PSRIO_HEADEREDBIN_saveDimensionedVector( ptr_pointer,nomeheader, nomeData, ptrStudy, vectorid, ptrAgentRule )
  return ccall((:PSRIO_HEADEREDBIN_saveDimensionedVector, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nomeheader,nomeData,ptrStudy,vectorid,ptrAgentRule)
end
function PSRIO_HEADEREDBIN_subtractTo( ptr_pointer,nameheader_source1, namedata_source1, nameheader_source2, namedata_source2, nameheader_target, namedata_target )
  return ccall((:PSRIO_HEADEREDBIN_subtractTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nameheader_source1,namedata_source1,nameheader_source2,namedata_source2,nameheader_target,namedata_target)
end
function PSRIO_HEADEREDBIN_multiplyTo( ptr_pointer,nameheader_source1, namedata_source1, nameheader_target, namedata_target, factor )
  return ccall((:PSRIO_HEADEREDBIN_multiplyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Float64,),ptr_pointer ,nameheader_source1,namedata_source1,nameheader_target,namedata_target,factor)
end
function PSRIO_HEADEREDBIN_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerIOMask_getInstance( ptr_pointer )
  return ccall((:PSRManagerIOMask_getInstance, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL )
end
function PSRManagerIOMask_addPath( ptr_pointer,nomePath )
  return ccall((:PSRManagerIOMask_addPath, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nomePath)
end
function PSRManagerIOMask_getPathsSize( ptr_pointer )
  return ccall((:PSRManagerIOMask_getPathsSize, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerIOMask_getPath( ptr_pointer,i )
  retPtr = "                                                                                                                  "
  ccall((:PSRManagerIOMask_getPath, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,i,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRManagerIOMask_setPath( ptr_pointer,nomePath )
  return ccall((:PSRManagerIOMask_setPath, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nomePath)
end
function PSRManagerIOMask_importFile( ptr_pointer,nome )
  return ccall((:PSRManagerIOMask_importFile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRManagerIOMask_getMask( ptr_pointer,id )
  return ccall((:PSRManagerIOMask_getMask, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRManagerIOMask_addMask( ptr_pointer,ptrIOMask )
  return ccall((:PSRManagerIOMask_addMask, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrIOMask)
end
function PSRManagerIOMask_loadAutomaticData( ptr_pointer,ptrStudy, context, pathdata )
  return ccall((:PSRManagerIOMask_loadAutomaticData, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,context,pathdata)
end
function PSRManagerIOMask_saveAutomaticData( ptr_pointer,ptrStudy, context, pathdata )
  return ccall((:PSRManagerIOMask_saveAutomaticData, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,context,pathdata)
end
function PSRManagerIOMask_deleteInstance( ptr_pointer )
  return ccall((:PSRManagerIOMask_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerIOMask_create(iprt)
  ptr_pointer =  ccall((:PSRManagerIOMask_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIO_COLDATA_FORMAT_create(iprt,_startcol, _endcol)
  ptr_pointer =  ccall((:PSRIO_COLDATA_FORMAT_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),C_NULL,_startcol,_endcol)
  return ptr_pointer
end
function PSRIO_COLDATA_FORMAT_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_COLDATA_FORMAT_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_In_create(iprt)
  ptr_pointer =  ccall((:PSRIO_HEADEREDBIN_In_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIO_HEADEREDBIN_In_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_In_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOTSLHistoricalSerie_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOTSLHistoricalSerie_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOTSLHistoricalSerie_deleteInstance( ptr_pointer )
  return ccall((:PSRIOTSLHistoricalSerie_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOTSLHistoricalSerie_create(iprt)
  ptr_pointer =  ccall((:PSRIOTSLHistoricalSerie_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOTSLEnvironmental_load( ptr_pointer,ptrStudy, pathdata )
  return ccall((:PSRIOTSLEnvironmental_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathdata)
end
function PSRIOTSLEnvironmental_deleteInstance( ptr_pointer )
  return ccall((:PSRIOTSLEnvironmental_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOTSLEnvironmental_create(iprt)
  ptr_pointer =  ccall((:PSRIOTSLEnvironmental_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMathInterpreterSum_create(iprt,expressao)
  ptr_pointer =  ccall((:PSRMathInterpreterSum_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,expressao)
  return ptr_pointer
end
function PSRMathInterpreterSum_deleteInstance( ptr_pointer )
  return ccall((:PSRMathInterpreterSum_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMapVector_create(iprt)
  ptr_pointer =  ccall((:PSRMapVector_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMapVector_useReal4( ptr_pointer,status )
  return ccall((:PSRMapVector_useReal4, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRMapVector_setMemory( ptr_pointer,_memory, _size )
  return ccall((:PSRMapVector_setMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,pointer(_memory),_size)
end
function PSRMapVector_setGain( ptr_pointer,_gain )
  return ccall((:PSRMapVector_setGain, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,_gain)
end
function PSRMapVector_useInformationOutput( ptr_pointer,status )
  return ccall((:PSRMapVector_useInformationOutput, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRMapVector_useNoParmOutput( ptr_pointer,status )
  return ccall((:PSRMapVector_useNoParmOutput, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRMapVector_add( ptr_pointer,ptrElement, vetorData, defaultVetorData )
  return ccall((:PSRMapVector_add, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement,vetorData,defaultVetorData)
end
function PSRMapVector_pullToMemory( ptr_pointer,ptrElement )
  return ccall((:PSRMapVector_pullToMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRMapVector_pushFromMemory( ptr_pointer,ptrElement )
  return ccall((:PSRMapVector_pushFromMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRMapVector_deleteInstance( ptr_pointer )
  return ccall((:PSRMapVector_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerLogMessage_deleteInstance( ptr_pointer )
  return ccall((:PSRManagerLogMessage_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerLog_getInstance( ptr_pointer )
  return ccall((:PSRManagerLog_getInstance, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL )
end
function PSRManagerLog_setContext( ptr_pointer,contexto )
  return ccall((:PSRManagerLog_setContext, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,contexto)
end
function PSRManagerLog_enableDetailedLog( ptr_pointer,status )
  return ccall((:PSRManagerLog_enableDetailedLog, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRManagerLog_addLog( ptr_pointer,ptrLog )
  return ccall((:PSRManagerLog_addLog, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrLog)
end
function PSRManagerLog_close( ptr_pointer )
  return ccall((:PSRManagerLog_close, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerLog_warning( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRManagerLog_warning, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRManagerLog_error( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRManagerLog_error, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRManagerLog_info( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRManagerLog_info, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRManagerLog_detail( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRManagerLog_detail, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRManagerLog_initPortuguese( ptr_pointer )
  return ccall((:PSRManagerLog_initPortuguese, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerLog_initEnglish( ptr_pointer )
  return ccall((:PSRManagerLog_initEnglish, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerLog_deleteInstance( ptr_pointer )
  return ccall((:PSRManagerLog_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerLog_create(iprt)
  ptr_pointer =  ccall((:PSRManagerLog_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageProcessor_create(iprt)
  ptr_pointer =  ccall((:PSRMessageProcessor_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageProcessor_setReferencePath( ptr_pointer,path )
  return ccall((:PSRMessageProcessor_setReferencePath, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,path)
end
function PSRMessageProcessor_getReferencePath( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRMessageProcessor_getReferencePath, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRMessageProcessor_process( ptr_pointer,ptrMessageNode )
  return ccall((:PSRMessageProcessor_process, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageNode)
end
function PSRMessageProcessor_posProcess( ptr_pointer,ptrMessageNode )
  return ccall((:PSRMessageProcessor_posProcess, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageNode)
end
function PSRMessageProcessor_getPointer( ptr_pointer,classtype, referenceAddress )
  return ccall((:PSRMessageProcessor_getPointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Nothing,),ptr_pointer ,classtype,referenceAddress)
end
function PSRMessageProcessor_addPointer( ptr_pointer,classtype, referenceAddress, pointer )
  return ccall((:PSRMessageProcessor_addPointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Nothing, Nothing,),ptr_pointer ,classtype,referenceAddress,pointer)
end
function PSRMessageProcessor_addPointer2( ptr_pointer,classtype, referenceAddress, pointer )
  return ccall((:PSRMessageProcessor_addPointer2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Nothing, Ptr{UInt8},),ptr_pointer ,classtype,referenceAddress,pointer(pointer))
end
function PSRMessageProcessor_currentStudy( ptr_pointer )
  return ccall((:PSRMessageProcessor_currentStudy, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageProcessor_setStudy( ptr_pointer,ptrStudy )
  return ccall((:PSRMessageProcessor_setStudy, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRMessageProcessor_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageProcessor_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMathInterpreter_create(iprt)
  ptr_pointer =  ccall((:PSRMathInterpreter_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMathInterpreter_solve( ptr_pointer,expressao )
  return ccall((:PSRMathInterpreter_solve, "PSRClasses"),Float64,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,expressao)
end
function PSRMathInterpreter_solve2( ptr_pointer,ptrModel, expressao )
  return ccall((:PSRMathInterpreter_solve2, "PSRClasses"),Float64,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrModel,expressao)
end
function PSRMathInterpreter_deleteInstance( ptr_pointer )
  return ccall((:PSRMathInterpreter_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataNode_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataNode_nodeType( ptr_pointer )
  return ccall((:PSRMessageDataNode_nodeType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_getParentNode( ptr_pointer )
  return ccall((:PSRMessageDataNode_getParentNode, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_messageProcessor( ptr_pointer )
  return ccall((:PSRMessageDataNode_messageProcessor, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_replacePointer( ptr_pointer,currentPointer, oldPointer )
  return ccall((:PSRMessageDataNode_replacePointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Nothing, Nothing,),ptr_pointer ,currentPointer,oldPointer)
end
function PSRMessageDataNode_maxAttributeInt( ptr_pointer )
  return ccall((:PSRMessageDataNode_maxAttributeInt, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_getIdAttributeInt( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRMessageDataNode_getIdAttributeInt, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRMessageDataNode_getAttributeInt( ptr_pointer,index )
  return ccall((:PSRMessageDataNode_getAttributeInt, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRMessageDataNode_maxAttributeReal( ptr_pointer )
  return ccall((:PSRMessageDataNode_maxAttributeReal, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_getIdAttributeReal( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRMessageDataNode_getIdAttributeReal, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRMessageDataNode_getAttributeReal( ptr_pointer,index )
  return ccall((:PSRMessageDataNode_getAttributeReal, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRMessageDataNode_maxAttributeString( ptr_pointer )
  return ccall((:PSRMessageDataNode_maxAttributeString, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_getIdAttributeString( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRMessageDataNode_getIdAttributeString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRMessageDataNode_getAttributeString( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRMessageDataNode_getAttributeString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRMessageDataNode_maxAttributePointer( ptr_pointer )
  return ccall((:PSRMessageDataNode_maxAttributePointer, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_getIdAttributePointer( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRMessageDataNode_getIdAttributePointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRMessageDataNode_getAttributePointer( ptr_pointer,index )
  return ccall((:PSRMessageDataNode_getAttributePointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRMessageDataNode_maxAttributeElementPointer( ptr_pointer )
  return ccall((:PSRMessageDataNode_maxAttributeElementPointer, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_getIdAttributeElementPointer( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRMessageDataNode_getIdAttributeElementPointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRMessageDataNode_getAttributeElementPointer( ptr_pointer,index )
  return ccall((:PSRMessageDataNode_getAttributeElementPointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRMessageDataNode_maxAttributeCollectionElementPointer( ptr_pointer )
  return ccall((:PSRMessageDataNode_maxAttributeCollectionElementPointer, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_getIdAttributeCollectionElementPointer( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRMessageDataNode_getIdAttributeCollectionElementPointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRMessageDataNode_getAttributeCollectionElementPointerTotal( ptr_pointer,index )
  return ccall((:PSRMessageDataNode_getAttributeCollectionElementPointerTotal, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRMessageDataNode_getAttributeCollectionElementPointerIndex( ptr_pointer,index, index_element )
  return ccall((:PSRMessageDataNode_getAttributeCollectionElementPointerIndex, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,index,index_element)
end
function PSRMessageDataNode_getAttributeCollectionElementPointerIndex2( ptr_pointer,name )
  return ccall((:PSRMessageDataNode_getAttributeCollectionElementPointerIndex2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name)
end
function PSRMessageDataNode_delAttributeCollectionElementPointer( ptr_pointer,index )
  return ccall((:PSRMessageDataNode_delAttributeCollectionElementPointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRMessageDataNode_getAttributeInt2( ptr_pointer,id )
  return ccall((:PSRMessageDataNode_getAttributeInt2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRMessageDataNode_getAttributeReal2( ptr_pointer,id )
  return ccall((:PSRMessageDataNode_getAttributeReal2, "PSRClasses"),Float64,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRMessageDataNode_getAttributeString2( ptr_pointer,id )
  retPtr = "                                                                                                                  "
  ccall((:PSRMessageDataNode_getAttributeString2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,id,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRMessageDataNode_getAttributePointer2( ptr_pointer,id )
  return ccall((:PSRMessageDataNode_getAttributePointer2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRMessageDataNode_getAttributeElementPointer2( ptr_pointer,id )
  return ccall((:PSRMessageDataNode_getAttributeElementPointer2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRMessageDataNode_addAttribute( ptr_pointer,id, value )
  return ccall((:PSRMessageDataNode_addAttribute, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,id,value)
end
function PSRMessageDataNode_addAttribute2( ptr_pointer,id, value )
  return ccall((:PSRMessageDataNode_addAttribute2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Float64,),ptr_pointer ,id,value)
end
function PSRMessageDataNode_addAttribute3( ptr_pointer,id, value )
  return ccall((:PSRMessageDataNode_addAttribute3, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int64,),ptr_pointer ,id,value)
end
function PSRMessageDataNode_addAttribute4( ptr_pointer,id, value )
  return ccall((:PSRMessageDataNode_addAttribute4, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id,value)
end
function PSRMessageDataNode_addAttributePointer( ptr_pointer,id, value )
  return ccall((:PSRMessageDataNode_addAttributePointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Nothing,),ptr_pointer ,id,value)
end
function PSRMessageDataNode_addAttributePointer2( ptr_pointer,id, value )
  return ccall((:PSRMessageDataNode_addAttributePointer2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id,pointer(value))
end
function PSRMessageDataNode_addAttributeElementPointer( ptr_pointer,id, value )
  return ccall((:PSRMessageDataNode_addAttributeElementPointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Nothing,),ptr_pointer ,id,value)
end
function PSRMessageDataNode_addAttributeElementPointer2( ptr_pointer,id, value )
  return ccall((:PSRMessageDataNode_addAttributeElementPointer2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id,value)
end
function PSRMessageDataNode_addAttributeCollectionElementPointer( ptr_pointer,id, value )
  return ccall((:PSRMessageDataNode_addAttributeCollectionElementPointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id,value)
end
function PSRMessageDataNode_addAttributeCollectionElementPointer3( ptr_pointer,id, value )
  return ccall((:PSRMessageDataNode_addAttributeCollectionElementPointer3, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id,value)
end
function PSRMessageDataNode_maxNode( ptr_pointer )
  return ccall((:PSRMessageDataNode_maxNode, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_node( ptr_pointer,index )
  return ccall((:PSRMessageDataNode_node, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRMessageDataNode_addNode( ptr_pointer,ptrDataNode )
  return ccall((:PSRMessageDataNode_addNode, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataNode)
end
function PSRMessageDataNode_print( ptr_pointer )
  return ccall((:PSRMessageDataNode_print, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_getSize( ptr_pointer )
  return ccall((:PSRMessageDataNode_getSize, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataNode_writeToMemory( ptr_pointer,memory )
  return ccall((:PSRMessageDataNode_writeToMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,memory)
end
function PSRMessageDataNode_readFromMemory( ptr_pointer,memory )
  return ccall((:PSRMessageDataNode_readFromMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,memory)
end
function PSRMessageDataNode_readFromFile( ptr_pointer,filename )
  return ccall((:PSRMessageDataNode_readFromFile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRMessageDataNode_useMessageProcessor( ptr_pointer,ptrMessageProcessor )
  return ccall((:PSRMessageDataNode_useMessageProcessor, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageProcessor)
end
function PSRMessageDataNode_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataNode_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageProcessorObjectList_getPointer( ptr_pointer,referenceAddress )
  return ccall((:PSRMessageProcessorObjectList_getPointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Nothing,),ptr_pointer ,referenceAddress)
end
function PSRMessageProcessorObjectList_addPointer( ptr_pointer,referenceAddress, pointer )
  return ccall((:PSRMessageProcessorObjectList_addPointer, "PSRClasses"),Nothing,(Ptr{UInt8}, Nothing, Nothing,),ptr_pointer ,referenceAddress,pointer)
end
function PSRMessageProcessorObjectList_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageProcessorObjectList_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageProcessorObjectList_create(iprt)
  ptr_pointer =  ccall((:PSRMessageProcessorObjectList_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSpreadsheet_create(iprt)
  ptr_pointer =  ccall((:PSRSpreadsheet_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSpreadsheet_fitToMask( ptr_pointer,ptrIOMask, row )
  return ccall((:PSRSpreadsheet_fitToMask, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrIOMask,row)
end
function PSRSpreadsheet_maxCol( ptr_pointer )
  return ccall((:PSRSpreadsheet_maxCol, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSpreadsheet_col( ptr_pointer,ndx )
  return ccall((:PSRSpreadsheet_col, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRSpreadsheet_col2( ptr_pointer,id )
  return ccall((:PSRSpreadsheet_col2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRSpreadsheet_addCol( ptr_pointer,vetorCol )
  return ccall((:PSRSpreadsheet_addCol, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,vetorCol)
end
function PSRSpreadsheet_maxRow( ptr_pointer )
  return ccall((:PSRSpreadsheet_maxRow, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSpreadsheet_firstRow( ptr_pointer )
  return ccall((:PSRSpreadsheet_firstRow, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSpreadsheet_nextRow( ptr_pointer )
  return ccall((:PSRSpreadsheet_nextRow, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSpreadsheet_gotoRow( ptr_pointer,row )
  return ccall((:PSRSpreadsheet_gotoRow, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,row)
end
function PSRSpreadsheet_isEOD( ptr_pointer )
  return ccall((:PSRSpreadsheet_isEOD, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRSpreadsheet_isEmpty( ptr_pointer,row, col )
  return ccall((:PSRSpreadsheet_isEmpty, "PSRClasses"),Bool,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,row,col)
end
function PSRSpreadsheet_asReal( ptr_pointer,row, col )
  return ccall((:PSRSpreadsheet_asReal, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,row,col)
end
function PSRSpreadsheet_asInteger( ptr_pointer,row, col )
  return ccall((:PSRSpreadsheet_asInteger, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,row,col)
end
function PSRSpreadsheet_asString( ptr_pointer,row, col )
  retPtr = "                                                                                                                  "
  ccall((:PSRSpreadsheet_asString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,Ptr{UInt8},),ptr_pointer ,row,col,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRSpreadsheet_asDate( ptr_pointer,row, col )
  return ccall((:PSRSpreadsheet_asDate, "PSRClasses"),Int64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,row,col)
end
function PSRSpreadsheet_stringData( ptr_pointer,col )
  retPtr = "                                                                                                                  "
  ccall((:PSRSpreadsheet_stringData, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,col,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRSpreadsheet_setCellFromString( ptr_pointer,row, col, cellvalue )
  return ccall((:PSRSpreadsheet_setCellFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8},),ptr_pointer ,row,col,cellvalue)
end
function PSRSpreadsheet_delRow( ptr_pointer,row )
  return ccall((:PSRSpreadsheet_delRow, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,row)
end
function PSRSpreadsheet_reset( ptr_pointer )
  return ccall((:PSRSpreadsheet_reset, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSpreadsheet_releaseColumnMemory( ptr_pointer,col )
  return ccall((:PSRSpreadsheet_releaseColumnMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,col)
end
function PSRSpreadsheet_deleteInstance( ptr_pointer )
  return ccall((:PSRSpreadsheet_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRResultList_maxResult( ptr_pointer )
  return ccall((:PSRResultList_maxResult, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRResultList_result( ptr_pointer,ndx )
  return ccall((:PSRResultList_result, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRResultList_addResult( ptr_pointer,ptrResultado )
  return ccall((:PSRResultList_addResult, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrResultado)
end
function PSRResultList_deleteInstance( ptr_pointer )
  return ccall((:PSRResultList_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRResultList_create(iprt)
  ptr_pointer =  ccall((:PSRResultList_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRThermalContractMap_create(iprt)
  ptr_pointer =  ccall((:PSRThermalContractMap_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRThermalContractMap_setCode( ptr_pointer,code )
  return ccall((:PSRThermalContractMap_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRThermalContractMap_setSysCode( ptr_pointer,code )
  return ccall((:PSRThermalContractMap_setSysCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRThermalContractMap_setCvu( ptr_pointer,cvu )
  return ccall((:PSRThermalContractMap_setCvu, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,cvu)
end
function PSRThermalContractMap_setCesp1( ptr_pointer,cesp )
  return ccall((:PSRThermalContractMap_setCesp1, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,cesp)
end
function PSRThermalContractMap_setCesp2( ptr_pointer,cesp )
  return ccall((:PSRThermalContractMap_setCesp2, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,cesp)
end
function PSRThermalContractMap_setCesp3( ptr_pointer,cesp )
  return ccall((:PSRThermalContractMap_setCesp3, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,cesp)
end
function PSRThermalContractMap_setPotInst( ptr_pointer,potInst )
  return ccall((:PSRThermalContractMap_setPotInst, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,potInst)
end
function PSRThermalContractMap_setG1( ptr_pointer,g )
  return ccall((:PSRThermalContractMap_setG1, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,g)
end
function PSRThermalContractMap_setG2( ptr_pointer,g )
  return ccall((:PSRThermalContractMap_setG2, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,g)
end
function PSRThermalContractMap_setG3( ptr_pointer,g )
  return ccall((:PSRThermalContractMap_setG3, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,g)
end
function PSRThermalContractMap_getCode( ptr_pointer )
  return ccall((:PSRThermalContractMap_getCode, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalContractMap_getSysCode( ptr_pointer )
  return ccall((:PSRThermalContractMap_getSysCode, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalContractMap_getCvu( ptr_pointer )
  return ccall((:PSRThermalContractMap_getCvu, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalContractMap_getCesp1( ptr_pointer )
  return ccall((:PSRThermalContractMap_getCesp1, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalContractMap_getCesp2( ptr_pointer )
  return ccall((:PSRThermalContractMap_getCesp2, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalContractMap_getCesp3( ptr_pointer )
  return ccall((:PSRThermalContractMap_getCesp3, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalContractMap_getPotInst( ptr_pointer )
  return ccall((:PSRThermalContractMap_getPotInst, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalContractMap_getG1( ptr_pointer )
  return ccall((:PSRThermalContractMap_getG1, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalContractMap_getG2( ptr_pointer )
  return ccall((:PSRThermalContractMap_getG2, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalContractMap_getG3( ptr_pointer )
  return ccall((:PSRThermalContractMap_getG3, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalContractMap_deleteInstance( ptr_pointer )
  return ccall((:PSRThermalContractMap_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationModelRule_create(iprt,_expression, _retcode, _vecForEachId)
  ptr_pointer =  ccall((:PSRValidationModelRule_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),C_NULL,_expression,_retcode,_vecForEachId)
  return ptr_pointer
end
function PSRValidationModelRule_deleteInstance( ptr_pointer )
  return ccall((:PSRValidationModelRule_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_create(iprt,id)
  ptr_pointer =  ccall((:PSRVector_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,id)
  return ptr_pointer
end
function PSRVector_id( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRVector_id, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVector_setId( ptr_pointer,Id )
  return ccall((:PSRVector_setId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,Id)
end
function PSRVector_getDataType( ptr_pointer )
  return ccall((:PSRVector_getDataType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_isHourlyDense( ptr_pointer )
  return ccall((:PSRVector_isHourlyDense, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_setIsHourlyDense( ptr_pointer,status )
  return ccall((:PSRVector_setIsHourlyDense, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRVector_getDimensionInformation( ptr_pointer )
  return ccall((:PSRVector_getDimensionInformation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_setDimensionInformation( ptr_pointer,dimensionInformation )
  return ccall((:PSRVector_setDimensionInformation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,dimensionInformation)
end
function PSRVector_noParm( ptr_pointer,index )
  return ccall((:PSRVector_noParm, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVector_noParm2( ptr_pointer,index, _noParm )
  return ccall((:PSRVector_noParm2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Bool,),ptr_pointer ,index,_noParm)
end
function PSRVector_getCurrentNoParm( ptr_pointer )
  return ccall((:PSRVector_getCurrentNoParm, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_size( ptr_pointer )
  return ccall((:PSRVector_size, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_position( ptr_pointer )
  return ccall((:PSRVector_position, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_positionStatus( ptr_pointer )
  return ccall((:PSRVector_positionStatus, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_first( ptr_pointer )
  return ccall((:PSRVector_first, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_next( ptr_pointer )
  return ccall((:PSRVector_next, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_setPosition( ptr_pointer,__position )
  return ccall((:PSRVector_setPosition, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,__position)
end
function PSRVector_toString( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRVector_toString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVector_setDataFromString( ptr_pointer,ndx, data )
  return ccall((:PSRVector_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ndx,data)
end
function PSRVector_clear( ptr_pointer )
  return ccall((:PSRVector_clear, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_resize( ptr_pointer,NewSize )
  return ccall((:PSRVector_resize, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,NewSize)
end
function PSRVector_reserve( ptr_pointer,reserveSize )
  return ccall((:PSRVector_reserve, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,reserveSize)
end
function PSRVector_remove( ptr_pointer,index )
  return ccall((:PSRVector_remove, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVector_repeat( ptr_pointer,number_repetitions )
  return ccall((:PSRVector_repeat, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number_repetitions)
end
function PSRVector_hasIndexRepetition( ptr_pointer )
  return ccall((:PSRVector_hasIndexRepetition, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_add( ptr_pointer )
  return ccall((:PSRVector_add, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_add2( ptr_pointer,data )
  return ccall((:PSRVector_add2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRVector_add3( ptr_pointer,data )
  return ccall((:PSRVector_add3, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,data)
end
function PSRVector_add4( ptr_pointer,data )
  return ccall((:PSRVector_add4, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,data)
end
function PSRVector_add5( ptr_pointer,data )
  return ccall((:PSRVector_add5, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64,),ptr_pointer ,data)
end
function PSRVector_addIndexed( ptr_pointer,date, data )
  return ccall((:PSRVector_addIndexed, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Int32,),ptr_pointer ,date,data)
end
function PSRVector_addIndexed2( ptr_pointer,date, data )
  return ccall((:PSRVector_addIndexed2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Float64,),ptr_pointer ,date,data)
end
function PSRVector_addIndexed3( ptr_pointer,date, data )
  return ccall((:PSRVector_addIndexed3, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Ptr{UInt8},),ptr_pointer ,date,data)
end
function PSRVector_insert( ptr_pointer,ndx )
  return ccall((:PSRVector_insert, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVector_getString( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRVector_getString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVector_getInteger( ptr_pointer,ndx )
  return ccall((:PSRVector_getInteger, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVector_getReal( ptr_pointer,ndx )
  return ccall((:PSRVector_getReal, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVector_getDate( ptr_pointer,ndx )
  return ccall((:PSRVector_getDate, "PSRClasses"),Int64,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVector_getCurrentInteger( ptr_pointer )
  return ccall((:PSRVector_getCurrentInteger, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_getCurrentReal( ptr_pointer )
  return ccall((:PSRVector_getCurrentReal, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_getCurrentString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRVector_getCurrentString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVector_getIndexVector( ptr_pointer )
  return ccall((:PSRVector_getIndexVector, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_setIndexVector( ptr_pointer,_indexVetor )
  return ccall((:PSRVector_setIndexVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_indexVetor)
end
function PSRVector_maxIndexed( ptr_pointer )
  return ccall((:PSRVector_maxIndexed, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_getIndexedVector( ptr_pointer,index )
  return ccall((:PSRVector_getIndexedVector, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVector_getIndexedReal( ptr_pointer,date, indexedType )
  return ccall((:PSRVector_getIndexedReal, "PSRClasses"),Float64,(Ptr{UInt8}, Int64, Int32,),ptr_pointer ,date,indexedType)
end
function PSRVector_getIndexedInteger( ptr_pointer,date, indexedType )
  return ccall((:PSRVector_getIndexedInteger, "PSRClasses"),Int32,(Ptr{UInt8}, Int64, Int32,),ptr_pointer ,date,indexedType)
end
function PSRVector_getIndexedString( ptr_pointer,date, indexedType )
  retPtr = "                                                                                                                  "
  ccall((:PSRVector_getIndexedString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Int32,Ptr{UInt8},),ptr_pointer ,date,indexedType,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVector_getIndexedNoParm( ptr_pointer,date, indexedType )
  return ccall((:PSRVector_getIndexedNoParm, "PSRClasses"),Bool,(Ptr{UInt8}, Int64, Int32,),ptr_pointer ,date,indexedType)
end
function PSRVector_getIndexPosition( ptr_pointer,date, indexedType )
  return ccall((:PSRVector_getIndexPosition, "PSRClasses"),Int32,(Ptr{UInt8}, Int64, Int32,),ptr_pointer ,date,indexedType)
end
function PSRVector_getIndexPosition2( ptr_pointer,date, sequence, indexedType )
  return ccall((:PSRVector_getIndexPosition2, "PSRClasses"),Int32,(Ptr{UInt8}, Int64, Int32, Int32,),ptr_pointer ,date,sequence,indexedType)
end
function PSRVector_setIndexedPosition( ptr_pointer,date )
  return ccall((:PSRVector_setIndexedPosition, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64,),ptr_pointer ,date)
end
function PSRVector_setIndexedPosition2( ptr_pointer,date, sequence )
  return ccall((:PSRVector_setIndexedPosition2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Int32,),ptr_pointer ,date,sequence)
end
function PSRVector_hasIndexedPosition( ptr_pointer,date, index )
  return ccall((:PSRVector_hasIndexedPosition, "PSRClasses"),Bool,(Ptr{UInt8}, Int64, Ptr{Int32},),ptr_pointer ,date,index)
end
function PSRVector_indexOf( ptr_pointer,data )
  return ccall((:PSRVector_indexOf, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,data)
end
function PSRVector_indexOf2( ptr_pointer,data )
  return ccall((:PSRVector_indexOf2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRVector_fitToIndex( ptr_pointer )
  return ccall((:PSRVector_fitToIndex, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_clone( ptr_pointer )
  return ccall((:PSRVector_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_concatenate( ptr_pointer,ptrVector )
  return ccall((:PSRVector_concatenate, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrVector)
end
function PSRVector_copyTo( ptr_pointer,ptrTarget )
  return ccall((:PSRVector_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrTarget)
end
function PSRVector_copyTo2( ptr_pointer,ptrTarget, fromPosition, toPosition )
  return ccall((:PSRVector_copyTo2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),ptr_pointer ,ptrTarget,fromPosition,toPosition)
end
function PSRVector_duplicateValue( ptr_pointer,fromPosition, toPosition )
  return ccall((:PSRVector_duplicateValue, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,fromPosition,toPosition)
end
function PSRVector_copyNoParmTo( ptr_pointer,data, initialIndex )
  return ccall((:PSRVector_copyNoParmTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{Int32}, Int32,),ptr_pointer ,data,initialIndex)
end
function PSRVector_sort( ptr_pointer,sort_type, exclusive_data )
  return ccall((:PSRVector_sort, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Bool,),ptr_pointer ,sort_type,exclusive_data)
end
function PSRVector_createIndexRepetitions( ptr_pointer )
  return ccall((:PSRVector_createIndexRepetitions, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_serialize( ptr_pointer )
  return ccall((:PSRVector_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVector_buildFrom( ptr_pointer,ptrMessageDataVector )
  return ccall((:PSRVector_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataVector)
end
function PSRVector_deleteInstance( ptr_pointer )
  return ccall((:PSRVector_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileRegistry_create(iprt)
  ptr_pointer =  ccall((:StructuredFileRegistry_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function StructuredFileRegistry_type( ptr_pointer )
  return ccall((:StructuredFileRegistry_type, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileRegistry_deleteInstance( ptr_pointer )
  return ccall((:StructuredFileRegistry_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDimensionInformation_setNumberDimensions( ptr_pointer,number )
  return ccall((:PSRVectorDimensionInformation_setNumberDimensions, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number)
end
function PSRVectorDimensionInformation_getNumberDimensions( ptr_pointer )
  return ccall((:PSRVectorDimensionInformation_getNumberDimensions, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDimensionInformation_getNumberVectors( ptr_pointer )
  return ccall((:PSRVectorDimensionInformation_getNumberVectors, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDimensionInformation_setDimensionName( ptr_pointer,index, dimension )
  return ccall((:PSRVectorDimensionInformation_setDimensionName, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,index,dimension)
end
function PSRVectorDimensionInformation_getDimensionName( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorDimensionInformation_getDimensionName, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorDimensionInformation_getDimensionSize( ptr_pointer,index )
  return ccall((:PSRVectorDimensionInformation_getDimensionSize, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorDimensionInformation_setDimensionSize( ptr_pointer,index, size )
  return ccall((:PSRVectorDimensionInformation_setDimensionSize, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,index,size)
end
function PSRVectorDimensionInformation_useDimension( ptr_pointer,dimension, value )
  return ccall((:PSRVectorDimensionInformation_useDimension, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,dimension,value)
end
function PSRVectorDimensionInformation_useDimension2( ptr_pointer,dimension, value )
  return ccall((:PSRVectorDimensionInformation_useDimension2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,dimension,value)
end
function PSRVectorDimensionInformation_baseVector( ptr_pointer )
  return ccall((:PSRVectorDimensionInformation_baseVector, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDimensionInformation_setBaseVector( ptr_pointer,ptrVector )
  return ccall((:PSRVectorDimensionInformation_setBaseVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrVector)
end
function PSRVectorDimensionInformation_getCurrent( ptr_pointer )
  return ccall((:PSRVectorDimensionInformation_getCurrent, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDimensionInformation_addVector( ptr_pointer,ptrVector )
  return ccall((:PSRVectorDimensionInformation_addVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrVector)
end
function PSRVectorDimensionInformation_getVector( ptr_pointer,index )
  return ccall((:PSRVectorDimensionInformation_getVector, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorDimensionInformation_sortVectors( ptr_pointer )
  return ccall((:PSRVectorDimensionInformation_sortVectors, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDimensionInformation_serialize( ptr_pointer )
  return ccall((:PSRVectorDimensionInformation_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDimensionInformation_buildFrom( ptr_pointer,ptrMessageDataVector )
  return ccall((:PSRVectorDimensionInformation_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataVector)
end
function PSRVectorDimensionInformation_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorDimensionInformation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDimensionInformation_create(iprt)
  ptr_pointer =  ccall((:PSRVectorDimensionInformation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRUpdaterSDDP_create(iprt)
  ptr_pointer =  ccall((:PSRUpdaterSDDP_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRUpdaterSDDP_setBlocks( ptr_pointer,numero )
  return ccall((:PSRUpdaterSDDP_setBlocks, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,numero)
end
function PSRUpdaterSDDP_setStageType( ptr_pointer,tipo )
  return ccall((:PSRUpdaterSDDP_setStageType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,tipo)
end
function PSRUpdaterSDDP_setFlagConvertThermal( ptr_pointer,flag )
  return ccall((:PSRUpdaterSDDP_setFlagConvertThermal, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRUpdaterSDDP_setFlagConvertDemand( ptr_pointer,flag )
  return ccall((:PSRUpdaterSDDP_setFlagConvertDemand, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRUpdaterSDDP_update( ptr_pointer,ptrEstudo )
  return ccall((:PSRUpdaterSDDP_update, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo)
end
function PSRUpdaterSDDP_updateV10_3( ptr_pointer,ptrEstudo )
  return ccall((:PSRUpdaterSDDP_updateV10_3, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo)
end
function PSRUpdaterSDDP_toFuelConsumptionRepresentation( ptr_pointer,ptrEstudo )
  return ccall((:PSRUpdaterSDDP_toFuelConsumptionRepresentation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo)
end
function PSRUpdaterSDDP_toFuelOnlyRepresentation( ptr_pointer,ptrEstudo )
  return ccall((:PSRUpdaterSDDP_toFuelOnlyRepresentation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo)
end
function PSRUpdaterSDDP_applyMaintenanceData( ptr_pointer,ptrStudy )
  return ccall((:PSRUpdaterSDDP_applyMaintenanceData, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRUpdaterSDDP_applyMaintenanceDataWithoutICP( ptr_pointer,ptrStudy )
  return ccall((:PSRUpdaterSDDP_applyMaintenanceDataWithoutICP, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRUpdaterSDDP_applyRulesForDefaultBlocks( ptr_pointer,ptrStudy )
  return ccall((:PSRUpdaterSDDP_applyRulesForDefaultBlocks, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRUpdaterSDDP_applyExpansionDecisions( ptr_pointer,ptrStudy )
  return ccall((:PSRUpdaterSDDP_applyExpansionDecisions, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRUpdaterSDDP_freezeConfiguration( ptr_pointer,ptrStudy )
  return ccall((:PSRUpdaterSDDP_freezeConfiguration, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRUpdaterSDDP_unfreezeConfiguration( ptr_pointer,ptrStudy )
  return ccall((:PSRUpdaterSDDP_unfreezeConfiguration, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRUpdaterSDDP_toWeeklyRepresentation( ptr_pointer,ptrStudy )
  return ccall((:PSRUpdaterSDDP_toWeeklyRepresentation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRUpdaterSDDP_to13MonthsRepresentation( ptr_pointer,ptrStudy )
  return ccall((:PSRUpdaterSDDP_to13MonthsRepresentation, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRUpdaterSDDP_hourBlockMapToVariableDurationModel( ptr_pointer,ptrStudy )
  return ccall((:PSRUpdaterSDDP_hourBlockMapToVariableDurationModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRUpdaterSDDP_mergeWithNetplanStudy( ptr_pointer,ptrSDDPStudy, ptrNetplanStudy, path_log )
  return ccall((:PSRUpdaterSDDP_mergeWithNetplanStudy, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSDDPStudy,ptrNetplanStudy,path_log)
end
function PSRUpdaterSDDP_fixHydroModifications( ptr_pointer,ptrSystem )
  return ccall((:PSRUpdaterSDDP_fixHydroModifications, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRUpdaterSDDP_toFixedDurationRepresentation( ptr_pointer,ptrStudy )
  return ccall((:PSRUpdaterSDDP_toFixedDurationRepresentation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRUpdaterSDDP_toIsolatedStudy( ptr_pointer,sourceStudy, selectedSystem, targetStudy, pathdata )
  return ccall((:PSRUpdaterSDDP_toIsolatedStudy, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,sourceStudy,selectedSystem,targetStudy,pathdata)
end
function PSRUpdaterSDDP_blockDemandToHourly( ptr_pointer,ptrSystem, vHourBlockMap )
  return ccall((:PSRUpdaterSDDP_blockDemandToHourly, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,vHourBlockMap)
end
function PSRUpdaterSDDP_deleteInstance( ptr_pointer )
  return ccall((:PSRUpdaterSDDP_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLog_create(iprt)
  ptr_pointer =  ccall((:PSRLog_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRLog_getContext( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRLog_getContext, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRLog_setContext( ptr_pointer,contexto )
  return ccall((:PSRLog_setContext, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,contexto)
end
function PSRLog_setUsingUnitInformation( ptr_pointer,status )
  return ccall((:PSRLog_setUsingUnitInformation, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRLog_setWarningMsg( ptr_pointer,_warn_msg )
  return ccall((:PSRLog_setWarningMsg, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_warn_msg)
end
function PSRLog_setErrorMsg( ptr_pointer,_error_msg )
  return ccall((:PSRLog_setErrorMsg, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_error_msg)
end
function PSRLog_setInfoMsg( ptr_pointer,_info_msg )
  return ccall((:PSRLog_setInfoMsg, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_info_msg)
end
function PSRLog_addFilterIncludeClass( ptr_pointer,classe )
  return ccall((:PSRLog_addFilterIncludeClass, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,classe)
end
function PSRLog_addFilterExcludeClass( ptr_pointer,classe )
  return ccall((:PSRLog_addFilterExcludeClass, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,classe)
end
function PSRLog_isValidFilter( ptr_pointer,classe )
  return ccall((:PSRLog_isValidFilter, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,classe)
end
function PSRLog_warning( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLog_warning, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLog_error( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLog_error, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLog_info( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLog_info, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLog_deleteInstance( ptr_pointer )
  return ccall((:PSRLog_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerElements_addToSystem( ptr_pointer,ptrElementContainer, ptrElement )
  return ccall((:PSRManagerElements_addToSystem, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElementContainer,ptrElement)
end
function PSRManagerElements_addToContainer( ptr_pointer,ptrElementContainer, ptrElement )
  return ccall((:PSRManagerElements_addToContainer, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElementContainer,ptrElement)
end
function PSRManagerElements_deleteInstance( ptr_pointer )
  return ccall((:PSRManagerElements_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerElements_create(iprt)
  ptr_pointer =  ccall((:PSRManagerElements_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMaintenanceList_maxMaintenance( ptr_pointer )
  return ccall((:PSRMaintenanceList_maxMaintenance, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMaintenanceList_clearMaintenance( ptr_pointer )
  return ccall((:PSRMaintenanceList_clearMaintenance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMaintenanceList_maintenance( ptr_pointer,ndx )
  return ccall((:PSRMaintenanceList_maintenance, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRMaintenanceList_maintenance2( ptr_pointer,ptrElement )
  return ccall((:PSRMaintenanceList_maintenance2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRMaintenanceList_addMaintenance( ptr_pointer,ptrManutencaoData )
  return ccall((:PSRMaintenanceList_addMaintenance, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrManutencaoData)
end
function PSRMaintenanceList_deleteInstance( ptr_pointer )
  return ccall((:PSRMaintenanceList_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMaintenanceList_create(iprt)
  ptr_pointer =  ccall((:PSRMaintenanceList_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIO_TEXTDATA_IN_getVersion( ptr_pointer,line, default_version )
  return ccall((:PSRIO_TEXTDATA_IN_getVersion, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32,),C_NULL ,line,default_version)
end
function PSRIO_TEXTDATA_IN_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_TEXTDATA_IN_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_TEXTDATA_IN_create(iprt)
  ptr_pointer =  ccall((:PSRIO_TEXTDATA_IN_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRTimeController_create(iprt)
  ptr_pointer =  ccall((:PSRTimeController_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRTimeController_addIndexFilter( ptr_pointer,name )
  return ccall((:PSRTimeController_addIndexFilter, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name)
end
function PSRTimeController_addElement( ptr_pointer,ptrElement, usechild )
  return ccall((:PSRTimeController_addElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrElement,usechild)
end
function PSRTimeController_addCollection( ptr_pointer,ptrColElements, usechild )
  return ccall((:PSRTimeController_addCollection, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrColElements,usechild)
end
function PSRTimeController_addVector( ptr_pointer,ptrVector )
  return ccall((:PSRTimeController_addVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrVector)
end
function PSRTimeController_configureFrom( ptr_pointer,ptrStudy )
  return ccall((:PSRTimeController_configureFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRTimeController_configureFromVectors( ptr_pointer,stagetype )
  return ccall((:PSRTimeController_configureFromVectors, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,stagetype)
end
function PSRTimeController_setFirstDate( ptr_pointer,_firstdate )
  return ccall((:PSRTimeController_setFirstDate, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64,),ptr_pointer ,_firstdate)
end
function PSRTimeController_setLastDate( ptr_pointer,_lastdate )
  return ccall((:PSRTimeController_setLastDate, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64,),ptr_pointer ,_lastdate)
end
function PSRTimeController_setStageType( ptr_pointer,_stagetype )
  return ccall((:PSRTimeController_setStageType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_stagetype)
end
function PSRTimeController_resetDate( ptr_pointer )
  return ccall((:PSRTimeController_resetDate, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_gotoFirstDate( ptr_pointer )
  return ccall((:PSRTimeController_gotoFirstDate, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_gotoLastDate( ptr_pointer )
  return ccall((:PSRTimeController_gotoLastDate, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_gotoDate( ptr_pointer,dia, mes, ano )
  return ccall((:PSRTimeController_gotoDate, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,dia,mes,ano)
end
function PSRTimeController_nextStage( ptr_pointer )
  return ccall((:PSRTimeController_nextStage, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_nextModification( ptr_pointer,ptrModifiedElements )
  return ccall((:PSRTimeController_nextModification, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrModifiedElements)
end
function PSRTimeController_gotoStage( ptr_pointer,stage )
  return ccall((:PSRTimeController_gotoStage, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,stage)
end
function PSRTimeController_gotoStageHour( ptr_pointer,stage, hour )
  return ccall((:PSRTimeController_gotoStageHour, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,hour)
end
function PSRTimeController_getCurrentDate( ptr_pointer )
  return ccall((:PSRTimeController_getCurrentDate, "PSRClasses"),Int64,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getCurrentStage( ptr_pointer )
  return ccall((:PSRTimeController_getCurrentStage, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getCurrentDay( ptr_pointer )
  return ccall((:PSRTimeController_getCurrentDay, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getCurrentMonth( ptr_pointer )
  return ccall((:PSRTimeController_getCurrentMonth, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getCurrentWeek( ptr_pointer )
  return ccall((:PSRTimeController_getCurrentWeek, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getCurrentYear( ptr_pointer )
  return ccall((:PSRTimeController_getCurrentYear, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getCurrentYearPeriod( ptr_pointer )
  return ccall((:PSRTimeController_getCurrentYearPeriod, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getFirstDay( ptr_pointer )
  return ccall((:PSRTimeController_getFirstDay, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getFirstMonth( ptr_pointer )
  return ccall((:PSRTimeController_getFirstMonth, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getFirstYear( ptr_pointer )
  return ccall((:PSRTimeController_getFirstYear, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getLastDay( ptr_pointer )
  return ccall((:PSRTimeController_getLastDay, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getLastMonth( ptr_pointer )
  return ccall((:PSRTimeController_getLastMonth, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getLastYear( ptr_pointer )
  return ccall((:PSRTimeController_getLastYear, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getCurrentBlockHour( ptr_pointer )
  return ccall((:PSRTimeController_getCurrentBlockHour, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getBlockFromStageHour( ptr_pointer,stage, hour )
  return ccall((:PSRTimeController_getBlockFromStageHour, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,hour)
end
function PSRTimeController_setDimension( ptr_pointer,dimension, index )
  return ccall((:PSRTimeController_setDimension, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,dimension,index)
end
function PSRTimeController_createDimensionMappers( ptr_pointer )
  return ccall((:PSRTimeController_createDimensionMappers, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_getStageType( ptr_pointer )
  return ccall((:PSRTimeController_getStageType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTimeController_deleteInstance( ptr_pointer )
  return ccall((:PSRTimeController_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_create(iprt)
  ptr_pointer =  ccall((:PSRModel_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRModel_id( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRModel_id, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRModel_setId( ptr_pointer,__id )
  return ccall((:PSRModel_setId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,__id)
end
function PSRModel_sourceId( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRModel_sourceId, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRModel_setSourceId( ptr_pointer,_sourceid )
  return ccall((:PSRModel_setSourceId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_sourceid)
end
function PSRModel_getGenericIdentification( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRModel_getGenericIdentification, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRModel_element( ptr_pointer )
  return ccall((:PSRModel_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_usingDimensionedVector( ptr_pointer,status )
  return ccall((:PSRModel_usingDimensionedVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRModel_maxParm( ptr_pointer )
  return ccall((:PSRModel_maxParm, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_parm( ptr_pointer,ndx )
  return ccall((:PSRModel_parm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRModel_parm2( ptr_pointer,id )
  return ccall((:PSRModel_parm2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRModel_parm3( ptr_pointer,id, dim1 )
  return ccall((:PSRModel_parm3, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,id,dim1)
end
function PSRModel_findParm( ptr_pointer,id )
  return ccall((:PSRModel_findParm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRModel_addParm( ptr_pointer,parm )
  return ccall((:PSRModel_addParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,parm)
end
function PSRModel_delParm( ptr_pointer,id )
  return ccall((:PSRModel_delParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRModel_delParm2( ptr_pointer,index )
  return ccall((:PSRModel_delParm2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRModel_addParmAt( ptr_pointer,parm, index )
  return ccall((:PSRModel_addParmAt, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,parm,index)
end
function PSRModel_maxVector( ptr_pointer )
  return ccall((:PSRModel_maxVector, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_vector( ptr_pointer,ndx )
  return ccall((:PSRModel_vector, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRModel_vector2( ptr_pointer,id )
  return ccall((:PSRModel_vector2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRModel_vector3( ptr_pointer,id, dim1 )
  return ccall((:PSRModel_vector3, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,id,dim1)
end
function PSRModel_vector4( ptr_pointer,id, dim1, dim2 )
  return ccall((:PSRModel_vector4, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),ptr_pointer ,id,dim1,dim2)
end
function PSRModel_findVector( ptr_pointer,id )
  return ccall((:PSRModel_findVector, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRModel_addVector( ptr_pointer,vetor )
  return ccall((:PSRModel_addVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,vetor)
end
function PSRModel_delVector( ptr_pointer,id )
  return ccall((:PSRModel_delVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRModel_delVector2( ptr_pointer,ptrVector )
  return ccall((:PSRModel_delVector2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrVector)
end
function PSRModel_getIntervalEndVector( ptr_pointer,vInitialDate )
  return ccall((:PSRModel_getIntervalEndVector, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,vInitialDate)
end
function PSRModel_resetVectorPosition( ptr_pointer )
  return ccall((:PSRModel_resetVectorPosition, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_setVectorPosition( ptr_pointer,pos )
  return ccall((:PSRModel_setVectorPosition, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,pos)
end
function PSRModel_duplicateVector( ptr_pointer,id, newid )
  return ccall((:PSRModel_duplicateVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id,newid)
end
function PSRModel_updateIndexed( ptr_pointer,ptrIndex )
  return ccall((:PSRModel_updateIndexed, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrIndex)
end
function PSRModel_maxObject( ptr_pointer )
  return ccall((:PSRModel_maxObject, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_object( ptr_pointer,ndx )
  return ccall((:PSRModel_object, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRModel_object2( ptr_pointer,id )
  return ccall((:PSRModel_object2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRModel_addObject( ptr_pointer,object )
  return ccall((:PSRModel_addObject, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,object)
end
function PSRModel_maxExtension( ptr_pointer )
  return ccall((:PSRModel_maxExtension, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_extension( ptr_pointer,ndx )
  return ccall((:PSRModel_extension, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRModel_extension2( ptr_pointer,classname )
  return ccall((:PSRModel_extension2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,classname)
end
function PSRModel_addExtension( ptr_pointer,extension )
  return ccall((:PSRModel_addExtension, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,extension)
end
function PSRModel_maxDimension( ptr_pointer )
  return ccall((:PSRModel_maxDimension, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_dimension( ptr_pointer,ndx )
  return ccall((:PSRModel_dimension, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRModel_dimension2( ptr_pointer,id )
  return ccall((:PSRModel_dimension2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRModel_addDimension( ptr_pointer,dimension )
  return ccall((:PSRModel_addDimension, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,dimension)
end
function PSRModel_maxModel( ptr_pointer )
  return ccall((:PSRModel_maxModel, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_model( ptr_pointer,ndx )
  return ccall((:PSRModel_model, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRModel_model2( ptr_pointer,id )
  return ccall((:PSRModel_model2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRModel_addModel( ptr_pointer,model )
  return ccall((:PSRModel_addModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,model)
end
function PSRModel_delModel( ptr_pointer,model )
  return ccall((:PSRModel_delModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,model)
end
function PSRModel_findModel( ptr_pointer,id )
  return ccall((:PSRModel_findModel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRModel_mergeModel( ptr_pointer,ptrModelSource, opcoes_merge )
  return ccall((:PSRModel_mergeModel, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrModelSource,opcoes_merge)
end
function PSRModel_getCollectionParms( ptr_pointer,ptrCollectionParm )
  return ccall((:PSRModel_getCollectionParms, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollectionParm)
end
function PSRModel_getCollectionVectors( ptr_pointer,ptrCollectionVetor )
  return ccall((:PSRModel_getCollectionVectors, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollectionVetor)
end
function PSRModel_print( ptr_pointer,filename )
  return ccall((:PSRModel_print, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRModel_addValidationRule( ptr_pointer,ptrValidationRule )
  return ccall((:PSRModel_addValidationRule, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrValidationRule)
end
function PSRModel_clone( ptr_pointer )
  return ccall((:PSRModel_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_clear( ptr_pointer )
  return ccall((:PSRModel_clear, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_redim( ptr_pointer,id, dim )
  return ccall((:PSRModel_redim, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,id,dim)
end
function PSRModel_serialize( ptr_pointer )
  return ccall((:PSRModel_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRModel_buildFrom( ptr_pointer,ptrMessageDataModel )
  return ccall((:PSRModel_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataModel)
end
function PSRModel_deleteInstance( ptr_pointer )
  return ccall((:PSRModel_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_TEXTDATA_OUT_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_TEXTDATA_OUT_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_TEXTDATA_OUT_create(iprt)
  ptr_pointer =  ccall((:PSRIO_TEXTDATA_OUT_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRManagerModels_getInstance( ptr_pointer )
  return ccall((:PSRManagerModels_getInstance, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL )
end
function PSRManagerModels_getPathsSize( ptr_pointer )
  return ccall((:PSRManagerModels_getPathsSize, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerModels_getPath( ptr_pointer,i )
  retPtr = "                                                                                                                  "
  ccall((:PSRManagerModels_getPath, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,i,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRManagerModels_addPath( ptr_pointer,nomePath )
  return ccall((:PSRManagerModels_addPath, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nomePath)
end
function PSRManagerModels_setPath( ptr_pointer,nomePath )
  return ccall((:PSRManagerModels_setPath, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nomePath)
end
function PSRManagerModels_importFile( ptr_pointer,nome )
  return ccall((:PSRManagerModels_importFile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRManagerModels_getCodeModel( ptr_pointer,nome )
  return ccall((:PSRManagerModels_getCodeModel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRManagerModels_addCodeModel( ptr_pointer,ptrCodeModel )
  return ccall((:PSRManagerModels_addCodeModel, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCodeModel)
end
function PSRManagerModels_buildModel( ptr_pointer,ptrModel, nomemodelo )
  return ccall((:PSRManagerModels_buildModel, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrModel,nomemodelo)
end
function PSRManagerModels_setDefaultDimension( ptr_pointer,dimension, value )
  return ccall((:PSRManagerModels_setDefaultDimension, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,dimension,value)
end
function PSRManagerModels_getDefaultDimension( ptr_pointer,dimension )
  return ccall((:PSRManagerModels_getDefaultDimension, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,dimension)
end
function PSRManagerModels_deleteInstance( ptr_pointer )
  return ccall((:PSRManagerModels_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRManagerModels_create(iprt)
  ptr_pointer =  ccall((:PSRManagerModels_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOBinary_initSave( ptr_pointer,filename, numberAgents )
  return ccall((:PSRIOBinary_initSave, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,filename,numberAgents)
end
function PSRIOBinary_initLoad( ptr_pointer,filename, numberAgents )
  return ccall((:PSRIOBinary_initLoad, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,filename,numberAgents)
end
function PSRIOBinary_reload( ptr_pointer )
  return ccall((:PSRIOBinary_reload, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOBinary_close( ptr_pointer )
  return ccall((:PSRIOBinary_close, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOBinary_putSequentialReal( ptr_pointer,value )
  return ccall((:PSRIOBinary_putSequentialReal, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,value)
end
function PSRIOBinary_getReal( ptr_pointer,agentNumber, registry )
  return ccall((:PSRIOBinary_getReal, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,agentNumber,registry)
end
function PSRIOBinary_deleteInstance( ptr_pointer )
  return ccall((:PSRIOBinary_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOBinary_create(iprt)
  ptr_pointer =  ccall((:PSRIOBinary_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConstraintList_maxConstraint( ptr_pointer )
  return ccall((:PSRConstraintList_maxConstraint, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintList_constraint( ptr_pointer,ndx )
  return ccall((:PSRConstraintList_constraint, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRConstraintList_constraint2( ptr_pointer,ptrElement )
  return ccall((:PSRConstraintList_constraint2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRConstraintList_getConstraint( ptr_pointer,code )
  return ccall((:PSRConstraintList_getConstraint, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRConstraintList_getConstraint2( ptr_pointer,code, class_type )
  return ccall((:PSRConstraintList_getConstraint2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,code,class_type)
end
function PSRConstraintList_system( ptr_pointer )
  return ccall((:PSRConstraintList_system, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintList_addConstraint( ptr_pointer,ptrRestricaoData )
  return ccall((:PSRConstraintList_addConstraint, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrRestricaoData)
end
function PSRConstraintList_delConstraint( ptr_pointer,ptrRestricaoData )
  return ccall((:PSRConstraintList_delConstraint, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrRestricaoData)
end
function PSRConstraintList_asCollectionElements( ptr_pointer )
  return ccall((:PSRConstraintList_asCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintList_deleteInstance( ptr_pointer )
  return ccall((:PSRConstraintList_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintList_create(iprt)
  ptr_pointer =  ccall((:PSRConstraintList_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRCollectionVetor_maxVector( ptr_pointer )
  return ccall((:PSRCollectionVetor_maxVector, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionVetor_vector( ptr_pointer,ndx )
  return ccall((:PSRCollectionVetor_vector, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRCollectionVetor_addVector( ptr_pointer,ptrVetor )
  return ccall((:PSRCollectionVetor_addVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrVetor)
end
function PSRCollectionVetor_deleteInstance( ptr_pointer )
  return ccall((:PSRCollectionVetor_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionVetor_create(iprt)
  ptr_pointer =  ccall((:PSRCollectionVetor_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRCollectionString_create(iprt)
  ptr_pointer =  ccall((:PSRCollectionString_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRCollectionString_create2( iprt,str )
  ptr_pointer =  ccall((:PSRCollectionString_create2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,str)
  return ptr_pointer
end
function PSRCollectionString_maxString( ptr_pointer )
  return ccall((:PSRCollectionString_maxString, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionString_addString( ptr_pointer,str )
  return ccall((:PSRCollectionString_addString, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,str)
end
function PSRCollectionString_line( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRCollectionString_line, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRCollectionString_setData( ptr_pointer,ndx, str )
  return ccall((:PSRCollectionString_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ndx,str)
end
function PSRCollectionString_clear( ptr_pointer )
  return ccall((:PSRCollectionString_clear, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionString_exist( ptr_pointer,str )
  return ccall((:PSRCollectionString_exist, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,str)
end
function PSRCollectionString_loadFromFile( ptr_pointer,filename )
  return ccall((:PSRCollectionString_loadFromFile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRCollectionString_saveToFile( ptr_pointer,filename )
  return ccall((:PSRCollectionString_saveToFile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRCollectionString_deleteInstance( ptr_pointer )
  return ccall((:PSRCollectionString_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGrafConfiguration_create(iprt)
  ptr_pointer =  ccall((:PSRGrafConfiguration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGrafConfiguration_addConfiguration( ptr_pointer,num, attr )
  return ccall((:PSRGrafConfiguration_addConfiguration, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,num,attr)
end
function PSRGrafConfiguration_addConfiguration2( ptr_pointer,num, name, unidade, fileName, classe, _type, attr )
  return ccall((:PSRGrafConfiguration_addConfiguration2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Int32, Ptr{UInt8},),ptr_pointer ,num,name,unidade,fileName,classe,_type,attr)
end
function PSRGrafConfiguration_hasConfiguration( ptr_pointer,num )
  return ccall((:PSRGrafConfiguration_hasConfiguration, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,num)
end
function PSRGrafConfiguration_hasConfiguration2( ptr_pointer,filename )
  return ccall((:PSRGrafConfiguration_hasConfiguration2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRGrafConfiguration_selectConfiguration( ptr_pointer,num )
  return ccall((:PSRGrafConfiguration_selectConfiguration, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,num)
end
function PSRGrafConfiguration_selectConfiguration2( ptr_pointer,filename )
  return ccall((:PSRGrafConfiguration_selectConfiguration2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRGrafConfiguration_resultConfiguration( ptr_pointer,index )
  return ccall((:PSRGrafConfiguration_resultConfiguration, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRGrafConfiguration_totalOfResults( ptr_pointer )
  return ccall((:PSRGrafConfiguration_totalOfResults, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGrafConfiguration_getConfig( ptr_pointer,num )
  return ccall((:PSRGrafConfiguration_getConfig, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,num)
end
function PSRGrafConfiguration_deleteInstance( ptr_pointer )
  return ccall((:PSRGrafConfiguration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExtensionModel_create(iprt)
  ptr_pointer =  ccall((:PSRExtensionModel_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExtensionModel_getExtensionClassName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExtensionModel_getExtensionClassName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExtensionModel_isExtensionOf( ptr_pointer,extension_id )
  return ccall((:PSRExtensionModel_isExtensionOf, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,extension_id)
end
function PSRExtensionModel_maxExtensionIds( ptr_pointer )
  return ccall((:PSRExtensionModel_maxExtensionIds, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExtensionModel_extensionId( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRExtensionModel_extensionId, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExtensionModel_model( ptr_pointer )
  return ccall((:PSRExtensionModel_model, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExtensionModel_deleteInstance( ptr_pointer )
  return ccall((:PSRExtensionModel_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCodeModel_create(iprt,_nome)
  ptr_pointer =  ccall((:PSRCodeModel_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_nome)
  return ptr_pointer
end
function PSRCodeModel_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRCodeModel_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRCodeModel_maxCode( ptr_pointer )
  return ccall((:PSRCodeModel_maxCode, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCodeModel_code( ptr_pointer,ndx )
  return ccall((:PSRCodeModel_code, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRCodeModel_buildValidationRules( ptr_pointer,initialCodeRow )
  return ccall((:PSRCodeModel_buildValidationRules, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,initialCodeRow)
end
function PSRCodeModel_associateValidationRules( ptr_pointer,ptrModel )
  return ccall((:PSRCodeModel_associateValidationRules, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrModel)
end
function PSRCodeModel_isAdditionalModel( ptr_pointer )
  return ccall((:PSRCodeModel_isAdditionalModel, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRCodeModel_isClassDefinition( ptr_pointer )
  return ccall((:PSRCodeModel_isClassDefinition, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRCodeModel_setClassName( ptr_pointer,name )
  return ccall((:PSRCodeModel_setClassName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name)
end
function PSRCodeModel_getClassName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRCodeModel_getClassName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRCodeModel_deleteInstance( ptr_pointer )
  return ccall((:PSRCodeModel_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionParm_maxParm( ptr_pointer )
  return ccall((:PSRCollectionParm_maxParm, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionParm_parm( ptr_pointer,ndx )
  return ccall((:PSRCollectionParm_parm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRCollectionParm_addParm( ptr_pointer,ptrParm )
  return ccall((:PSRCollectionParm_addParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrParm)
end
function PSRCollectionParm_deleteInstance( ptr_pointer )
  return ccall((:PSRCollectionParm_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionParm_create(iprt)
  ptr_pointer =  ccall((:PSRCollectionParm_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRCollectionInteger_maxInteger( ptr_pointer )
  return ccall((:PSRCollectionInteger_maxInteger, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionInteger_addInteger( ptr_pointer,number )
  return ccall((:PSRCollectionInteger_addInteger, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number)
end
function PSRCollectionInteger_integer( ptr_pointer,ndx )
  return ccall((:PSRCollectionInteger_integer, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRCollectionInteger_setData( ptr_pointer,ndx, number )
  return ccall((:PSRCollectionInteger_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,ndx,number)
end
function PSRCollectionInteger_clear( ptr_pointer )
  return ccall((:PSRCollectionInteger_clear, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionInteger_exist( ptr_pointer,number )
  return ccall((:PSRCollectionInteger_exist, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,number)
end
function PSRCollectionInteger_deleteInstance( ptr_pointer )
  return ccall((:PSRCollectionInteger_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionInteger_create(iprt)
  ptr_pointer =  ccall((:PSRCollectionInteger_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSREnvironmentalData_create(iprt)
  ptr_pointer =  ccall((:PSREnvironmentalData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSREnvironmentalData_maxWeatherStation( ptr_pointer )
  return ccall((:PSREnvironmentalData_maxWeatherStation, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSREnvironmentalData_weatherStation( ptr_pointer,index )
  return ccall((:PSREnvironmentalData_weatherStation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSREnvironmentalData_addWeatherStation( ptr_pointer,ptrWeatherStation )
  return ccall((:PSREnvironmentalData_addWeatherStation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrWeatherStation)
end
function PSREnvironmentalData_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSREnvironmentalData_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSREnvironmentalData_deleteInstance( ptr_pointer )
  return ccall((:PSREnvironmentalData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExpansionData_maxProjects( ptr_pointer )
  return ccall((:PSRExpansionData_maxProjects, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_project( ptr_pointer,index )
  return ccall((:PSRExpansionData_project, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRExpansionData_getProject( ptr_pointer,code )
  return ccall((:PSRExpansionData_getProject, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRExpansionData_getProject2( ptr_pointer,projectType, code )
  return ccall((:PSRExpansionData_getProject2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,projectType,code)
end
function PSRExpansionData_getProject3( ptr_pointer,name )
  return ccall((:PSRExpansionData_getProject3, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name)
end
function PSRExpansionData_getProject4( ptr_pointer,projectType, name )
  return ccall((:PSRExpansionData_getProject4, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,projectType,name)
end
function PSRExpansionData_getProject5( ptr_pointer,ptrElement )
  return ccall((:PSRExpansionData_getProject5, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRExpansionData_getDecision( ptr_pointer,ptrExpansionProject )
  return ccall((:PSRExpansionData_getDecision, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrExpansionProject)
end
function PSRExpansionData_addProject( ptr_pointer,ptrProject )
  return ccall((:PSRExpansionData_addProject, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrProject)
end
function PSRExpansionData_maxDecisions( ptr_pointer )
  return ccall((:PSRExpansionData_maxDecisions, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_decision( ptr_pointer,index )
  return ccall((:PSRExpansionData_decision, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRExpansionData_addDecision( ptr_pointer,ptrDecision )
  return ccall((:PSRExpansionData_addDecision, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDecision)
end
function PSRExpansionData_removeDecision( ptr_pointer,ptrDecision )
  return ccall((:PSRExpansionData_removeDecision, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDecision)
end
function PSRExpansionData_maxDisbursements( ptr_pointer )
  return ccall((:PSRExpansionData_maxDisbursements, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_disbursement( ptr_pointer,index )
  return ccall((:PSRExpansionData_disbursement, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRExpansionData_getDisbursement( ptr_pointer,id )
  return ccall((:PSRExpansionData_getDisbursement, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,id)
end
function PSRExpansionData_addDisbursement( ptr_pointer,ptrDisbursement )
  return ccall((:PSRExpansionData_addDisbursement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDisbursement)
end
function PSRExpansionData_maxCapacities( ptr_pointer )
  return ccall((:PSRExpansionData_maxCapacities, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_capacity( ptr_pointer,index )
  return ccall((:PSRExpansionData_capacity, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRExpansionData_addCapacity( ptr_pointer,ptrCapacity )
  return ccall((:PSRExpansionData_addCapacity, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCapacity)
end
function PSRExpansionData_maxPrecedence( ptr_pointer )
  return ccall((:PSRExpansionData_maxPrecedence, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_precedence( ptr_pointer,index )
  return ccall((:PSRExpansionData_precedence, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRExpansionData_addPrecedence( ptr_pointer,ptrPrecedence )
  return ccall((:PSRExpansionData_addPrecedence, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrPrecedence)
end
function PSRExpansionData_maxGeneric( ptr_pointer )
  return ccall((:PSRExpansionData_maxGeneric, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_genericConstraint( ptr_pointer,index )
  return ccall((:PSRExpansionData_genericConstraint, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRExpansionData_addGeneric( ptr_pointer,ptrGenericConstraint )
  return ccall((:PSRExpansionData_addGeneric, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGenericConstraint)
end
function PSRExpansionData_maxSatisfaction( ptr_pointer )
  return ccall((:PSRExpansionData_maxSatisfaction, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_satisfaction( ptr_pointer,index )
  return ccall((:PSRExpansionData_satisfaction, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRExpansionData_addSatisfaction( ptr_pointer,ptrSatisfactionConstraint )
  return ccall((:PSRExpansionData_addSatisfaction, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSatisfactionConstraint)
end
function PSRExpansionData_maxRiverTopologies( ptr_pointer )
  return ccall((:PSRExpansionData_maxRiverTopologies, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_riverTopology( ptr_pointer,index )
  return ccall((:PSRExpansionData_riverTopology, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRExpansionData_addRiverTopology( ptr_pointer,ptrRiverTopology )
  return ccall((:PSRExpansionData_addRiverTopology, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrRiverTopology)
end
function PSRExpansionData_exclusiveProjects( ptr_pointer )
  return ccall((:PSRExpansionData_exclusiveProjects, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_associatedProjects( ptr_pointer )
  return ccall((:PSRExpansionData_associatedProjects, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_removeDecisions( ptr_pointer )
  return ccall((:PSRExpansionData_removeDecisions, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_removeTransmissionDecisions( ptr_pointer )
  return ccall((:PSRExpansionData_removeTransmissionDecisions, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionData_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRExpansionData_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRExpansionData_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOAutomaticMaskConfiguration_create(iprt)
  ptr_pointer =  ccall((:PSRIOAutomaticMaskConfiguration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOAutomaticMaskConfiguration_setFilename( ptr_pointer,filename )
  return ccall((:PSRIOAutomaticMaskConfiguration_setFilename, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRIOAutomaticMaskConfiguration_getFilename( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOAutomaticMaskConfiguration_getFilename, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOAutomaticMaskConfiguration_setVersion( ptr_pointer,version )
  return ccall((:PSRIOAutomaticMaskConfiguration_setVersion, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,version)
end
function PSRIOAutomaticMaskConfiguration_getVersion( ptr_pointer )
  return ccall((:PSRIOAutomaticMaskConfiguration_getVersion, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOAutomaticMaskConfiguration_setClassName( ptr_pointer,class_name )
  return ccall((:PSRIOAutomaticMaskConfiguration_setClassName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,class_name)
end
function PSRIOAutomaticMaskConfiguration_getClassName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOAutomaticMaskConfiguration_getClassName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOAutomaticMaskConfiguration_setContextRead( ptr_pointer,context_read )
  return ccall((:PSRIOAutomaticMaskConfiguration_setContextRead, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,context_read)
end
function PSRIOAutomaticMaskConfiguration_getContextRead( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOAutomaticMaskConfiguration_getContextRead, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOAutomaticMaskConfiguration_setContextWrite( ptr_pointer,context_write )
  return ccall((:PSRIOAutomaticMaskConfiguration_setContextWrite, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,context_write)
end
function PSRIOAutomaticMaskConfiguration_getContextWrite( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOAutomaticMaskConfiguration_getContextWrite, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOAutomaticMaskConfiguration_setModelId( ptr_pointer,model_id )
  return ccall((:PSRIOAutomaticMaskConfiguration_setModelId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,model_id)
end
function PSRIOAutomaticMaskConfiguration_getModelId( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOAutomaticMaskConfiguration_getModelId, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOAutomaticMaskConfiguration_setCreateObjects( ptr_pointer,status )
  return ccall((:PSRIOAutomaticMaskConfiguration_setCreateObjects, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOAutomaticMaskConfiguration_isCreateObjects( ptr_pointer )
  return ccall((:PSRIOAutomaticMaskConfiguration_isCreateObjects, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOAutomaticMaskConfiguration_setMergeObjects( ptr_pointer,status )
  return ccall((:PSRIOAutomaticMaskConfiguration_setMergeObjects, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOAutomaticMaskConfiguration_isMergeObjects( ptr_pointer )
  return ccall((:PSRIOAutomaticMaskConfiguration_isMergeObjects, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOAutomaticMaskConfiguration_deleteInstance( ptr_pointer )
  return ccall((:PSRIOAutomaticMaskConfiguration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionElement_maxElements( ptr_pointer )
  return ccall((:PSRCollectionElement_maxElements, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionElement_addElement( ptr_pointer,ptrElement )
  return ccall((:PSRCollectionElement_addElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRCollectionElement_element( ptr_pointer,ndx )
  return ccall((:PSRCollectionElement_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRCollectionElement_delElement( ptr_pointer,index )
  return ccall((:PSRCollectionElement_delElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRCollectionElement_removeRedundant( ptr_pointer )
  return ccall((:PSRCollectionElement_removeRedundant, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionElement_clear( ptr_pointer )
  return ccall((:PSRCollectionElement_clear, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionElement_addCollection( ptr_pointer,colElements )
  return ccall((:PSRCollectionElement_addCollection, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,colElements)
end
function PSRCollectionElement_indexOf( ptr_pointer,ptrElement )
  return ccall((:PSRCollectionElement_indexOf, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRCollectionElement_mapRelationShip( ptr_pointer,ptrColElement, output, relation_type, zerobased )
  return ccall((:PSRCollectionElement_mapRelationShip, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{Int32}, Int32, Bool,),ptr_pointer ,ptrColElement,output,relation_type,zerobased)
end
function PSRCollectionElement_mapComplexRelationShip( ptr_pointer,ptrColElement, output_first, output_next, output_index, relation_type, zerobased )
  return ccall((:PSRCollectionElement_mapComplexRelationShip, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{Int32}, Ptr{Int32}, Ptr{Int32}, Int32, Bool,),ptr_pointer ,ptrColElement,output_first,output_next,output_index,relation_type,zerobased)
end
function PSRCollectionElement_mapInverseRelationShip( ptr_pointer,ptrColElement, output_first, output_next, output_index, relation_type, zerobased )
  return ccall((:PSRCollectionElement_mapInverseRelationShip, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{Int32}, Ptr{Int32}, Ptr{Int32}, Int32, Bool,),ptr_pointer ,ptrColElement,output_first,output_next,output_index,relation_type,zerobased)
end
function PSRCollectionElement_mapOrderedRelationShip( ptr_pointer,ptrColElement, output_first, relation_type, zerobased )
  return ccall((:PSRCollectionElement_mapOrderedRelationShip, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{Int32}, Int32, Bool,),ptr_pointer ,ptrColElement,output_first,relation_type,zerobased)
end
function PSRCollectionElement_sortByArray( ptr_pointer,parmname, array_values, array_dimension )
  return ccall((:PSRCollectionElement_sortByArray, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,parmname,pointer(array_values),array_dimension)
end
function PSRCollectionElement_sortOn( ptr_pointer,parmname )
  return ccall((:PSRCollectionElement_sortOn, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,parmname)
end
function PSRCollectionElement_setOrders( ptr_pointer )
  return ccall((:PSRCollectionElement_setOrders, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionElement_printElements( ptr_pointer,filename )
  return ccall((:PSRCollectionElement_printElements, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRCollectionElement_printElementsInDate( ptr_pointer,filename, date, variable )
  return ccall((:PSRCollectionElement_printElementsInDate, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int64, Ptr{UInt8},),ptr_pointer ,filename,date,variable)
end
function PSRCollectionElement_query( ptr_pointer,querystring )
  return ccall((:PSRCollectionElement_query, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,querystring)
end
function PSRCollectionElement_deleteInstance( ptr_pointer )
  return ccall((:PSRCollectionElement_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionElement_create(iprt)
  ptr_pointer =  ccall((:PSRCollectionElement_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConstraintSumList_maxConstraint( ptr_pointer )
  return ccall((:PSRConstraintSumList_maxConstraint, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintSumList_constraint( ptr_pointer,ndx )
  return ccall((:PSRConstraintSumList_constraint, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRConstraintSumList_getConstraint( ptr_pointer,code )
  return ccall((:PSRConstraintSumList_getConstraint, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRConstraintSumList_getConstraint2( ptr_pointer,code, class_type )
  return ccall((:PSRConstraintSumList_getConstraint2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,code,class_type)
end
function PSRConstraintSumList_addConstraint( ptr_pointer,ptrRestricaoData )
  return ccall((:PSRConstraintSumList_addConstraint, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrRestricaoData)
end
function PSRConstraintSumList_delConstraint( ptr_pointer,ptrRestricaoData )
  return ccall((:PSRConstraintSumList_delConstraint, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrRestricaoData)
end
function PSRConstraintSumList_asCollectionElements( ptr_pointer )
  return ccall((:PSRConstraintSumList_asCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintSumList_deleteInstance( ptr_pointer )
  return ccall((:PSRConstraintSumList_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintSumList_create(iprt)
  ptr_pointer =  ccall((:PSRConstraintSumList_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRHydrology_create(iprt)
  ptr_pointer =  ccall((:PSRHydrology_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRHydrology_hydrologicalNetwork( ptr_pointer )
  return ccall((:PSRHydrology_hydrologicalNetwork, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrology_hydrologicalPlantNetwork( ptr_pointer )
  return ccall((:PSRHydrology_hydrologicalPlantNetwork, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrology_riverNetwork( ptr_pointer )
  return ccall((:PSRHydrology_riverNetwork, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrology_deleteInstance( ptr_pointer )
  return ccall((:PSRHydrology_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGround_create(iprt,_ptrElement)
  ptr_pointer =  ccall((:PSRGround_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_ptrElement)
  return ptr_pointer
end
function PSRGround_element( ptr_pointer )
  return ccall((:PSRGround_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGround_node( ptr_pointer )
  return ccall((:PSRGround_node, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGround_deleteInstance( ptr_pointer )
  return ccall((:PSRGround_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenericGrouping_create(iprt)
  ptr_pointer =  ccall((:PSRGenericGrouping_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGenericGrouping_getId( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGenericGrouping_getId, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGenericGrouping_parent( ptr_pointer )
  return ccall((:PSRGenericGrouping_parent, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGenericGrouping_maxGroup( ptr_pointer )
  return ccall((:PSRGenericGrouping_maxGroup, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenericGrouping_groupByIndex( ptr_pointer,index )
  return ccall((:PSRGenericGrouping_groupByIndex, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRGenericGrouping_group( ptr_pointer,idgroup )
  return ccall((:PSRGenericGrouping_group, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,idgroup)
end
function PSRGenericGrouping_group2( ptr_pointer,classType )
  return ccall((:PSRGenericGrouping_group2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,classType)
end
function PSRGenericGrouping_createGroup( ptr_pointer,id, classType )
  return ccall((:PSRGenericGrouping_createGroup, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,id,classType)
end
function PSRGenericGrouping_removeGroup( ptr_pointer,ptrGroup )
  return ccall((:PSRGenericGrouping_removeGroup, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGroup)
end
function PSRGenericGrouping_maxElement( ptr_pointer )
  return ccall((:PSRGenericGrouping_maxElement, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenericGrouping_element( ptr_pointer,index )
  return ccall((:PSRGenericGrouping_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRGenericGrouping_element2( ptr_pointer,parm, value )
  return ccall((:PSRGenericGrouping_element2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,parm,value)
end
function PSRGenericGrouping_element3( ptr_pointer,parm, value )
  return ccall((:PSRGenericGrouping_element3, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,parm,value)
end
function PSRGenericGrouping_findByGenericIdentification( ptr_pointer,identification )
  return ccall((:PSRGenericGrouping_findByGenericIdentification, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,identification)
end
function PSRGenericGrouping_hasElement( ptr_pointer,ptrElement )
  return ccall((:PSRGenericGrouping_hasElement, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGenericGrouping_indexOf( ptr_pointer,ptrElement )
  return ccall((:PSRGenericGrouping_indexOf, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGenericGrouping_addElement( ptr_pointer,ptrElement )
  return ccall((:PSRGenericGrouping_addElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGenericGrouping_removeElement( ptr_pointer,index )
  return ccall((:PSRGenericGrouping_removeElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRGenericGrouping_removeElement2( ptr_pointer,ptrElement )
  return ccall((:PSRGenericGrouping_removeElement2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGenericGrouping_clear( ptr_pointer )
  return ccall((:PSRGenericGrouping_clear, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenericGrouping_addToCollection( ptr_pointer,ptrColElement )
  return ccall((:PSRGenericGrouping_addToCollection, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrColElement)
end
function PSRGenericGrouping_serialize( ptr_pointer )
  return ccall((:PSRGenericGrouping_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGenericGrouping_buildFrom( ptr_pointer,ptrMessageData )
  return ccall((:PSRGenericGrouping_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageData)
end
function PSRGenericGrouping_buildRelationShipsFrom( ptr_pointer,ptrMessageData )
  return ccall((:PSRGenericGrouping_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageData)
end
function PSRGenericGrouping_deleteInstance( ptr_pointer )
  return ccall((:PSRGenericGrouping_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMapParm_create(iprt)
  ptr_pointer =  ccall((:PSRMapParm_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMapParm_useReal4( ptr_pointer,status )
  return ccall((:PSRMapParm_useReal4, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRMapParm_setMemory( ptr_pointer,_memory, _size )
  return ccall((:PSRMapParm_setMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,pointer(_memory),_size)
end
function PSRMapParm_setGain( ptr_pointer,_gain )
  return ccall((:PSRMapParm_setGain, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,_gain)
end
function PSRMapParm_add( ptr_pointer,ptrElement, parmData, defaultParmData )
  return ccall((:PSRMapParm_add, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement,parmData,defaultParmData)
end
function PSRMapParm_pullToMemory( ptr_pointer,ptrElement )
  return ccall((:PSRMapParm_pullToMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRMapParm_pushFromMemory( ptr_pointer,ptrElement )
  return ccall((:PSRMapParm_pushFromMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRMapParm_deleteInstance( ptr_pointer )
  return ccall((:PSRMapParm_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMapData_create(iprt)
  ptr_pointer =  ccall((:PSRMapData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMapData_addElements( ptr_pointer,ptrCollection )
  return ccall((:PSRMapData_addElements, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection)
end
function PSRMapData_maxElements( ptr_pointer )
  return ccall((:PSRMapData_maxElements, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMapData_useReal4( ptr_pointer,status )
  return ccall((:PSRMapData_useReal4, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRMapData_mapVector( ptr_pointer,idvetor, memory, size )
  return ccall((:PSRMapData_mapVector, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,idvetor,pointer(memory),size)
end
function PSRMapData_mapVectorD1( ptr_pointer,idvetor, dim1, memory, size, gain, noparm_behavior )
  return ccall((:PSRMapData_mapVectorD1, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8}, Int32, Float64, Int32,),ptr_pointer ,idvetor,dim1,pointer(memory),size,gain,noparm_behavior)
end
function PSRMapData_mapDimensionedVector( ptr_pointer,idvetor, memory, size, dimension1, dimension2 )
  return ccall((:PSRMapData_mapDimensionedVector, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,idvetor,pointer(memory),size,dimension1,dimension2)
end
function PSRMapData_mapParm( ptr_pointer,idparm, memory, size )
  return ccall((:PSRMapData_mapParm, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,idparm,pointer(memory),size)
end
function PSRMapData_mapParmD1( ptr_pointer,idparm, dim1, memory, size, gain, noparm_behavior )
  return ccall((:PSRMapData_mapParmD1, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8}, Int32, Float64, Int32,),ptr_pointer ,idparm,dim1,pointer(memory),size,gain,noparm_behavior)
end
function PSRMapData_mapDimensionedParm( ptr_pointer,idparm, memory, size, dimension1, dimension2 )
  return ccall((:PSRMapData_mapDimensionedParm, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,idparm,pointer(memory),size,dimension1,dimension2)
end
function PSRMapData_pullToMemory( ptr_pointer )
  return ccall((:PSRMapData_pullToMemory, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMapData_pullToMemory2( ptr_pointer,ptrElement )
  return ccall((:PSRMapData_pullToMemory2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRMapData_pushFromMemory( ptr_pointer )
  return ccall((:PSRMapData_pushFromMemory, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMapData_pushFromMemory2( ptr_pointer,ptrElement )
  return ccall((:PSRMapData_pushFromMemory2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRMapData_addReferenceMemory( ptr_pointer,address, memory )
  return ccall((:PSRMapData_addReferenceMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,pointer(address),pointer(memory))
end
function PSRMapData_clearReferenceMemory( ptr_pointer )
  return ccall((:PSRMapData_clearReferenceMemory, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMapData_deleteInstance( ptr_pointer )
  return ccall((:PSRMapData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRElement_getComparisonPreferenceType( ptr_pointer )
  return ccall((:PSRElement_getComparisonPreferenceType, "PSRClasses"),Int32,(Ptr{UInt8},),C_NULL )
end
function PSRElement_setComparisonPreferenceType( ptr_pointer,_type )
  return ccall((:PSRElement_setComparisonPreferenceType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),C_NULL ,_type)
end
function PSRElement_create(iprt)
  ptr_pointer =  ccall((:PSRElement_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRElement_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRElement_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRElement_classType( ptr_pointer )
  return ccall((:PSRElement_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRElement_isClassType( ptr_pointer,class_type )
  return ccall((:PSRElement_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRElement_getGenericIdentification( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRElement_getGenericIdentification, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRElement_setSerializationType( ptr_pointer,serialization_type )
  return ccall((:PSRElement_setSerializationType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,serialization_type)
end
function PSRElement_serializationType( ptr_pointer )
  return ccall((:PSRElement_serializationType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRElement_model( ptr_pointer )
  return ccall((:PSRElement_model, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRElement_maxModel( ptr_pointer )
  return ccall((:PSRElement_maxModel, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRElement_model2( ptr_pointer,ndx )
  return ccall((:PSRElement_model2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRElement_getModel( ptr_pointer,id, find_submodel )
  return ccall((:PSRElement_getModel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,id,find_submodel)
end
function PSRElement_addModel( ptr_pointer,ptrModel )
  return ccall((:PSRElement_addModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrModel)
end
function PSRElement_delModel( ptr_pointer,ptrModel )
  return ccall((:PSRElement_delModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrModel)
end
function PSRElement_setMainModel( ptr_pointer,model )
  return ccall((:PSRElement_setMainModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,model)
end
function PSRElement_groups( ptr_pointer )
  return ccall((:PSRElement_groups, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRElement_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRElement_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRElement_getCollectionElementsRelatedTo( ptr_pointer,ptrCollectionReference, tipoRelacionamento, ptrClassNameFilters )
  return ccall((:PSRElement_getCollectionElementsRelatedTo, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrCollectionReference,tipoRelacionamento,ptrClassNameFilters)
end
function PSRElement_getCollectionParms( ptr_pointer,ptrCollectionParm )
  return ccall((:PSRElement_getCollectionParms, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollectionParm)
end
function PSRElement_getCollectionVectors( ptr_pointer,ptrCollectionVetor )
  return ccall((:PSRElement_getCollectionVectors, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollectionVetor)
end
function PSRElement_isInsideClassFilters( ptr_pointer,ptrClassNameFilters )
  return ccall((:PSRElement_isInsideClassFilters, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrClassNameFilters)
end
function PSRElement_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRElement_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRElement_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRElement_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRElement_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRElement_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRElement_listOrder( ptr_pointer )
  return ccall((:PSRElement_listOrder, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRElement_setOrder( ptr_pointer,order )
  return ccall((:PSRElement_setOrder, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,order)
end
function PSRElement_serialize( ptr_pointer )
  return ccall((:PSRElement_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRElement_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRElement_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRElement_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRElement_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRElement_deleteInstance( ptr_pointer )
  return ccall((:PSRElement_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRArc_create(iprt,_ptrElement)
  ptr_pointer =  ccall((:PSRArc_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_ptrElement)
  return ptr_pointer
end
function PSRArc_element( ptr_pointer )
  return ccall((:PSRArc_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRArc_node( ptr_pointer,ndx )
  return ccall((:PSRArc_node, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRArc_numNode( ptr_pointer,ptrNo )
  return ccall((:PSRArc_numNode, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNo)
end
function PSRArc_numArc( ptr_pointer )
  return ccall((:PSRArc_numArc, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRArc_numArc2( ptr_pointer,number )
  return ccall((:PSRArc_numArc2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number)
end
function PSRArc_getStatus( ptr_pointer )
  return ccall((:PSRArc_getStatus, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRArc_setStatus( ptr_pointer,_status )
  return ccall((:PSRArc_setStatus, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_status)
end
function PSRArc_deleteInstance( ptr_pointer )
  return ccall((:PSRArc_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionBarra_maxBus( ptr_pointer )
  return ccall((:PSRCollectionBarra_maxBus, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionBarra_bus( ptr_pointer,ndx )
  return ccall((:PSRCollectionBarra_bus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRCollectionBarra_getBus( ptr_pointer,codBus )
  return ccall((:PSRCollectionBarra_getBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codBus)
end
function PSRCollectionBarra_addBus( ptr_pointer,ptrBarra )
  return ccall((:PSRCollectionBarra_addBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBarra)
end
function PSRCollectionBarra_delBus( ptr_pointer,ptrBarra )
  return ccall((:PSRCollectionBarra_delBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBarra)
end
function PSRCollectionBarra_deleteInstance( ptr_pointer )
  return ccall((:PSRCollectionBarra_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCollectionBarra_create(iprt)
  ptr_pointer =  ccall((:PSRCollectionBarra_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRFinancialData_create(iprt)
  ptr_pointer =  ccall((:PSRFinancialData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRFinancialData_maxContracts( ptr_pointer )
  return ccall((:PSRFinancialData_maxContracts, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFinancialData_contract( ptr_pointer,index )
  return ccall((:PSRFinancialData_contract, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRFinancialData_addContract( ptr_pointer,ptrContract )
  return ccall((:PSRFinancialData_addContract, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrContract)
end
function PSRFinancialData_maxFuelContracts( ptr_pointer )
  return ccall((:PSRFinancialData_maxFuelContracts, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFinancialData_maxFuelContracts2( ptr_pointer,ptrSystem )
  return ccall((:PSRFinancialData_maxFuelContracts2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRFinancialData_fuelContract( ptr_pointer,index )
  return ccall((:PSRFinancialData_fuelContract, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRFinancialData_fuelContract2( ptr_pointer,ptrSystem, index )
  return ccall((:PSRFinancialData_fuelContract2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSystem,index)
end
function PSRFinancialData_getContract( ptr_pointer,ptrSystem, code )
  return ccall((:PSRFinancialData_getContract, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSystem,code)
end
function PSRFinancialData_addFuelContract( ptr_pointer,ptrFuelContract )
  return ccall((:PSRFinancialData_addFuelContract, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrFuelContract)
end
function PSRFinancialData_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRFinancialData_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRFinancialData_deleteInstance( ptr_pointer )
  return ccall((:PSRFinancialData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRBuilderDemand_buildFromBus( ptr_pointer,ptrEstudo, numPatamares )
  return ccall((:PSRBuilderDemand_buildFromBus, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrEstudo,numPatamares)
end
function PSRBuilderDemand_deleteInstance( ptr_pointer )
  return ccall((:PSRBuilderDemand_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRBuilderDemand_create(iprt)
  ptr_pointer =  ccall((:PSRBuilderDemand_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOAnarede_load( ptr_pointer,ptrEstudo, nome, idmask )
  return ccall((:PSRIOAnarede_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome,idmask)
end
function PSRIOAnarede_deleteInstance( ptr_pointer )
  return ccall((:PSRIOAnarede_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOAnarede_create(iprt)
  ptr_pointer =  ccall((:PSRIOAnarede_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRElementFactory_get( ptr_pointer,CLASS_Element, modelTemplate, Study )
  return ccall((:PSRElementFactory_get, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Ptr{UInt8}, Ptr{UInt8},),C_NULL ,CLASS_Element,modelTemplate,Study)
end
function PSRElementFactory_put( ptr_pointer,prtElement, ptrIOMask, block )
  return ccall((:PSRElementFactory_put, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),C_NULL ,prtElement,ptrIOMask,block)
end
function PSRElementFactory_createGeneric( ptr_pointer,class_name )
  return ccall((:PSRElementFactory_createGeneric, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL ,class_name)
end
function PSRElementFactory_classNameToType( ptr_pointer,class_name )
  return ccall((:PSRElementFactory_classNameToType, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),C_NULL ,class_name)
end
function PSRElementFactory_getClassAndChildNames( ptr_pointer,class_name )
  return ccall((:PSRElementFactory_getClassAndChildNames, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL ,class_name)
end
function PSRElementFactory_createInterconnection( ptr_pointer,modelTemplate, Study )
  return ccall((:PSRElementFactory_createInterconnection, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL ,modelTemplate,Study)
end
function PSRElementFactory_getInterconnection( ptr_pointer,modelTemplate, Study )
  return ccall((:PSRElementFactory_getInterconnection, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL ,modelTemplate,Study)
end
function PSRElementFactory_deleteInstance( ptr_pointer )
  return ccall((:PSRElementFactory_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRElementFactory_create(iprt)
  ptr_pointer =  ccall((:PSRElementFactory_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGraph_create(iprt,_ptrElement)
  ptr_pointer =  ccall((:PSRGraph_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_ptrElement)
  return ptr_pointer
end
function PSRGraph_element( ptr_pointer )
  return ccall((:PSRGraph_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGraph_maxNode( ptr_pointer )
  return ccall((:PSRGraph_maxNode, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGraph_node( ptr_pointer,ndx )
  return ccall((:PSRGraph_node, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRGraph_node2( ptr_pointer,ptrElement )
  return ccall((:PSRGraph_node2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGraph_addNode( ptr_pointer,ptrNo )
  return ccall((:PSRGraph_addNode, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNo)
end
function PSRGraph_swapToEnd( ptr_pointer,ptrNo )
  return ccall((:PSRGraph_swapToEnd, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNo)
end
function PSRGraph_delNode( ptr_pointer,ptrNo )
  return ccall((:PSRGraph_delNode, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNo)
end
function PSRGraph_exists( ptr_pointer,ptrNode )
  return ccall((:PSRGraph_exists, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNode)
end
function PSRGraph_maxArc( ptr_pointer )
  return ccall((:PSRGraph_maxArc, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGraph_arc( ptr_pointer,ndx )
  return ccall((:PSRGraph_arc, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRGraph_addArc( ptr_pointer,ptrNo01, ptrNo02, ptrRamo )
  return ccall((:PSRGraph_addArc, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNo01,ptrNo02,ptrRamo)
end
function PSRGraph_delArc( ptr_pointer,ptrRamo )
  return ccall((:PSRGraph_delArc, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrRamo)
end
function PSRGraph_addGround( ptr_pointer,ptrNo, ptrGround )
  return ccall((:PSRGraph_addGround, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNo,ptrGround)
end
function PSRGraph_delGround( ptr_pointer,ptrGround )
  return ccall((:PSRGraph_delGround, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGround)
end
function PSRGraph_addGraph( ptr_pointer,ptrGraph )
  return ccall((:PSRGraph_addGraph, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGraph)
end
function PSRGraph_delGraph( ptr_pointer,ptrGraph )
  return ccall((:PSRGraph_delGraph, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGraph)
end
function PSRGraph_configure( ptr_pointer )
  return ccall((:PSRGraph_configure, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGraph_sort( ptr_pointer,order_type )
  return ccall((:PSRGraph_sort, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,order_type)
end
function PSRGraph_setOrders( ptr_pointer )
  return ccall((:PSRGraph_setOrders, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGraph_maxLogicGraphs( ptr_pointer )
  return ccall((:PSRGraph_maxLogicGraphs, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGraph_logicGraph( ptr_pointer,index )
  return ccall((:PSRGraph_logicGraph, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRGraph_isDSeparated( ptr_pointer,ptrNo01, ptrNo02 )
  return ccall((:PSRGraph_isDSeparated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNo01,ptrNo02)
end
function PSRGraph_serialize( ptr_pointer )
  return ccall((:PSRGraph_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGraph_buildFrom( ptr_pointer,ptrDataGraph )
  return ccall((:PSRGraph_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataGraph)
end
function PSRGraph_deleteInstance( ptr_pointer )
  return ccall((:PSRGraph_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function TimeControllerDimensionMapper_create(iprt,name, total_dimensioned_vectors, total_dimensioned_parameters)
  ptr_pointer =  ccall((:TimeControllerDimensionMapper_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),C_NULL,name,total_dimensioned_vectors,total_dimensioned_parameters)
  return ptr_pointer
end
function TimeControllerDimensionMapper_setDimensionIndexInVector( ptr_pointer,index_vector, index_dimension )
  return ccall((:TimeControllerDimensionMapper_setDimensionIndexInVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,index_vector,index_dimension)
end
function TimeControllerDimensionMapper_setDimensionIndexInParameter( ptr_pointer,index_parameter, index_dimension )
  return ccall((:TimeControllerDimensionMapper_setDimensionIndexInParameter, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,index_parameter,index_dimension)
end
function TimeControllerDimensionMapper_deleteInstance( ptr_pointer )
  return ccall((:TimeControllerDimensionMapper_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenScenarioWeight_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenScenarioWeight_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenScenarioWeight_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenScenarioWeight_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenScenarioWeight_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenScenarioWeight_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenScenarioWeight_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenScenarioWeight_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFutureCost_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPFutureCost_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFutureCost_load( ptr_pointer,filename )
  return ccall((:PSRIOSDDPFutureCost_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRIOSDDPFutureCost_initSave( ptr_pointer,filename )
  return ccall((:PSRIOSDDPFutureCost_initSave, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRIOSDDPFutureCost_initAppend( ptr_pointer,filename )
  return ccall((:PSRIOSDDPFutureCost_initAppend, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRIOSDDPFutureCost_maxVolume( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_maxVolume, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_maxInflow( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_maxInflow, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_maxOrd( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_maxOrd, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_idVolume( ptr_pointer,index )
  return ccall((:PSRIOSDDPFutureCost_idVolume, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRIOSDDPFutureCost_idInflow( ptr_pointer,index )
  return ccall((:PSRIOSDDPFutureCost_idInflow, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRIOSDDPFutureCost_setTotalStages( ptr_pointer,number_stages )
  return ccall((:PSRIOSDDPFutureCost_setTotalStages, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number_stages)
end
function PSRIOSDDPFutureCost_setInitialStage( ptr_pointer,stage, year )
  return ccall((:PSRIOSDDPFutureCost_setInitialStage, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,year)
end
function PSRIOSDDPFutureCost_setOrd( ptr_pointer,orden )
  return ccall((:PSRIOSDDPFutureCost_setOrd, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,orden)
end
function PSRIOSDDPFutureCost_addVolume( ptr_pointer,id )
  return ccall((:PSRIOSDDPFutureCost_addVolume, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,id)
end
function PSRIOSDDPFutureCost_addInflow( ptr_pointer,id )
  return ccall((:PSRIOSDDPFutureCost_addInflow, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,id)
end
function PSRIOSDDPFutureCost_maxStage( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_maxStage, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_gotoStage( ptr_pointer,stage )
  return ccall((:PSRIOSDDPFutureCost_gotoStage, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,stage)
end
function PSRIOSDDPFutureCost_restartStage( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_restartStage, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_getCut( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_getCut, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_numberCuts( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_numberCuts, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_numberCutsOfStage( ptr_pointer,stage )
  return ccall((:PSRIOSDDPFutureCost_numberCutsOfStage, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,stage)
end
function PSRIOSDDPFutureCost_getRHS( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_getRHS, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_getVolume( ptr_pointer,index )
  return ccall((:PSRIOSDDPFutureCost_getVolume, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRIOSDDPFutureCost_getInflow( ptr_pointer,index, ord )
  return ccall((:PSRIOSDDPFutureCost_getInflow, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,index,ord)
end
function PSRIOSDDPFutureCost_addCut( ptr_pointer,stage )
  return ccall((:PSRIOSDDPFutureCost_addCut, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,stage)
end
function PSRIOSDDPFutureCost_setRHS( ptr_pointer,rhs )
  return ccall((:PSRIOSDDPFutureCost_setRHS, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,rhs)
end
function PSRIOSDDPFutureCost_setVolume( ptr_pointer,index, coef )
  return ccall((:PSRIOSDDPFutureCost_setVolume, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Float64,),ptr_pointer ,index,coef)
end
function PSRIOSDDPFutureCost_setInflow( ptr_pointer,index, ord, coef )
  return ccall((:PSRIOSDDPFutureCost_setInflow, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Float64,),ptr_pointer ,index,ord,coef)
end
function PSRIOSDDPFutureCost_setHeaderInformation( ptr_pointer,simulation, iter, cluster )
  return ccall((:PSRIOSDDPFutureCost_setHeaderInformation, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,simulation,iter,cluster)
end
function PSRIOSDDPFutureCost_getHeaderInformationForSimulation( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_getHeaderInformationForSimulation, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_getHeaderInformationForIteration( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_getHeaderInformationForIteration, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_getHeaderInformationForCluster( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_getHeaderInformationForCluster, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_close( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_close, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFutureCost_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPFutureCost_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenSatisfactionConstraint_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenSatisfactionConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenSatisfactionConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenSatisfactionConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenSatisfactionConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenSatisfactionConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOTypicalProfiles_load( ptr_pointer,ptrStudy, pathdata )
  return ccall((:PSRIOTypicalProfiles_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathdata)
end
function PSRIOTypicalProfiles_deleteInstance( ptr_pointer )
  return ccall((:PSRIOTypicalProfiles_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOTypicalProfiles_create(iprt)
  ptr_pointer =  ccall((:PSRIOTypicalProfiles_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPImmediateCost_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPImmediateCost_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPImmediateCost_setTotalStages( ptr_pointer,number_stages )
  return ccall((:PSRIOSDDPImmediateCost_setTotalStages, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number_stages)
end
function PSRIOSDDPImmediateCost_setTotalScenarios( ptr_pointer,number_scenarios )
  return ccall((:PSRIOSDDPImmediateCost_setTotalScenarios, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number_scenarios)
end
function PSRIOSDDPImmediateCost_addVariable( ptr_pointer,_type, id )
  return ccall((:PSRIOSDDPImmediateCost_addVariable, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,_type,id)
end
function PSRIOSDDPImmediateCost_initSave( ptr_pointer,filename )
  return ccall((:PSRIOSDDPImmediateCost_initSave, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRIOSDDPImmediateCost_addCut( ptr_pointer,stage, scenary, coef, RHS )
  return ccall((:PSRIOSDDPImmediateCost_addCut, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32, Ptr{Float64}, Ptr{Float64},),ptr_pointer ,stage,scenary,coef,RHS)
end
function PSRIOSDDPImmediateCost_initLoad( ptr_pointer,filename )
  return ccall((:PSRIOSDDPImmediateCost_initLoad, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRIOSDDPImmediateCost_gotoStage( ptr_pointer,stage, scenario )
  return ccall((:PSRIOSDDPImmediateCost_gotoStage, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,scenario)
end
function PSRIOSDDPImmediateCost_totalCuts( ptr_pointer )
  return ccall((:PSRIOSDDPImmediateCost_totalCuts, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPImmediateCost_getCut( ptr_pointer,coef, RHS )
  return ccall((:PSRIOSDDPImmediateCost_getCut, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{Float64}, Ptr{Float64},),ptr_pointer ,coef,RHS)
end
function PSRIOSDDPImmediateCost_sortVariables( ptr_pointer,_type, id )
  return ccall((:PSRIOSDDPImmediateCost_sortVariables, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{Int32}, Ptr{Int32},),ptr_pointer ,_type,id)
end
function PSRIOSDDPImmediateCost_close( ptr_pointer )
  return ccall((:PSRIOSDDPImmediateCost_close, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPImmediateCost_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPImmediateCost_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGenericConstraint_hasDataToWrite( ptr_pointer,ptrStudy )
  return ccall((:PSRIOSDDPGenericConstraint_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIOSDDPGenericConstraint_load( ptr_pointer,ptrStudy, filename, version )
  return ccall((:PSRIOSDDPGenericConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrStudy,filename,version)
end
function PSRIOSDDPGenericConstraint_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGenericConstraint_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGenericConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGenericConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGenericConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGenericConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOPCA_selectConfiguration( ptr_pointer,stage, scenario, block )
  return ccall((:PSRIOPCA_selectConfiguration, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,scenario,block)
end
function PSRIOPCA_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOPCA_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOPCA_deleteInstance( ptr_pointer )
  return ccall((:PSRIOPCA_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOPCA_create(iprt)
  ptr_pointer =  ccall((:PSRIOPCA_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGrafIntegration_create(iprt,elementname, grafname, attributename)
  ptr_pointer =  ccall((:PSRIOGrafIntegration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL,elementname,grafname,attributename)
  return ptr_pointer
end
function PSRIOGrafIntegration_elementName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOGrafIntegration_elementName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOGrafIntegration_grafName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOGrafIntegration_grafName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOGrafIntegration_attributeName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOGrafIntegration_attributeName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOGrafIntegration_getCollectionElements( ptr_pointer,ptrStudy )
  return ccall((:PSRIOGrafIntegration_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIOGrafIntegration_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGrafIntegration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGenericTSData_create(iprt)
  ptr_pointer =  ccall((:PSRIOGenericTSData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGenericTSData_useMask( ptr_pointer,ptrIOMask )
  return ccall((:PSRIOGenericTSData_useMask, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrIOMask)
end
function PSRIOGenericTSData_addCollection( ptr_pointer,ptrColElements )
  return ccall((:PSRIOGenericTSData_addCollection, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrColElements)
end
function PSRIOGenericTSData_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOGenericTSData_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOGenericTSData_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOGenericTSData_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOGenericTSData_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGenericTSData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenariosList_addHourlyScenario( ptr_pointer,ptrIOHourlyScenario )
  return ccall((:PSRIOElementHourlyScenariosList_addHourlyScenario, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrIOHourlyScenario)
end
function PSRIOElementHourlyScenariosList_maxHourlyScenario( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenariosList_maxHourlyScenario, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenariosList_hourlyScenario( ptr_pointer,index )
  return ccall((:PSRIOElementHourlyScenariosList_hourlyScenario, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRIOElementHourlyScenariosList_getHourlyScenario( ptr_pointer,class_type, attribute_name )
  return ccall((:PSRIOElementHourlyScenariosList_getHourlyScenario, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,class_type,attribute_name)
end
function PSRIOElementHourlyScenariosList_getHourlyScenario2( ptr_pointer,class_name, attribute_name )
  return ccall((:PSRIOElementHourlyScenariosList_getHourlyScenario2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,class_name,attribute_name)
end
function PSRIOElementHourlyScenariosList_removeHourlyScenarios( ptr_pointer,class_type, attribute_name )
  return ccall((:PSRIOElementHourlyScenariosList_removeHourlyScenarios, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,class_type,attribute_name)
end
function PSRIOElementHourlyScenariosList_closeAll( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenariosList_closeAll, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenariosList_serialize( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenariosList_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenariosList_deleteInstance( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenariosList_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenariosList_create(iprt)
  ptr_pointer =  ccall((:PSRIOElementHourlyScenariosList_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGenericMaskBuilder_addModel( ptr_pointer,model_id )
  return ccall((:PSRIOGenericMaskBuilder_addModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,model_id)
end
function PSRIOGenericMaskBuilder_addModelDefinitionFile( ptr_pointer,filename )
  return ccall((:PSRIOGenericMaskBuilder_addModelDefinitionFile, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRIOGenericMaskBuilder_buildMaskFile( ptr_pointer,filename, context, version )
  return ccall((:PSRIOGenericMaskBuilder_buildMaskFile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,filename,context,version)
end
function PSRIOGenericMaskBuilder_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGenericMaskBuilder_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGenericMaskBuilder_create(iprt)
  ptr_pointer =  ccall((:PSRIOGenericMaskBuilder_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPThermalPlantEnergyBid_load( ptr_pointer,ptrStudy, filename, fieldname, num_discretization_hour )
  return ccall((:PSRIONCPThermalPlantEnergyBid_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrStudy,filename,fieldname,num_discretization_hour)
end
function PSRIONCPThermalPlantEnergyBid_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantEnergyBid_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantEnergyBid_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantEnergyBid_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGeneralStructuredFileLoader_create(iprt)
  ptr_pointer =  ccall((:PSRIOGeneralStructuredFileLoader_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGeneralStructuredFileLoader_load( ptr_pointer,filename )
  return ccall((:PSRIOGeneralStructuredFileLoader_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRIOGeneralStructuredFileLoader_getFile( ptr_pointer )
  return ccall((:PSRIOGeneralStructuredFileLoader_getFile, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGeneralStructuredFileLoader_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGeneralStructuredFileLoader_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGeneralStructuredFile_create(iprt)
  ptr_pointer =  ccall((:PSRIOGeneralStructuredFile_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGeneralStructuredFile_createFile( ptr_pointer,filename )
  return ccall((:PSRIOGeneralStructuredFile_createFile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRIOGeneralStructuredFile_read_registries( ptr_pointer )
  return ccall((:PSRIOGeneralStructuredFile_read_registries, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGeneralStructuredFile_close( ptr_pointer )
  return ccall((:PSRIOGeneralStructuredFile_close, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGeneralStructuredFile_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGeneralStructuredFile_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenGenericConstraint_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenGenericConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenGenericConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenGenericConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenGenericConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenGenericConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenDisbursement_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenDisbursement_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenDisbursement_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenDisbursement_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenDisbursement_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenDisbursement_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenDisbursement_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenDisbursement_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANDemandFactor_buildDemandFactor( ptr_pointer,headerfile, binaryfileP, binaryfileQ, binaryfileFP )
  return ccall((:PSRIONETPLANDemandFactor_buildDemandFactor, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,headerfile,binaryfileP,binaryfileQ,binaryfileFP)
end
function PSRIONETPLANDemandFactor_buildReativeDemand( ptr_pointer,headerfile, binaryfileP, binaryfileQ, binaryfileFP )
  return ccall((:PSRIONETPLANDemandFactor_buildReativeDemand, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,headerfile,binaryfileP,binaryfileQ,binaryfileFP)
end
function PSRIONETPLANDemandFactor_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANDemandFactor_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANDemandFactor_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANDemandFactor_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenPrecedenceConstraint_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenPrecedenceConstraint_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenPrecedenceConstraint_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenPrecedenceConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenPrecedenceConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenPrecedenceConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenPrecedenceConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenPrecedenceConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenFirmConstraint_save( ptr_pointer,ptrStudy, filename, vectorid )
  return ccall((:PSRIOOptgenFirmConstraint_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename,vectorid)
end
function PSRIOOptgenFirmConstraint_load( ptr_pointer,ptrStudy, filename, vectorid )
  return ccall((:PSRIOOptgenFirmConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename,vectorid)
end
function PSRIOOptgenFirmConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenFirmConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenFirmConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenFirmConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenExpansionDecision_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenExpansionDecision_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenExpansionDecision_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenExpansionDecision_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenExpansionDecision_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenExpansionDecision_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenExpansionDecision_ignoreUndefined( ptr_pointer,status )
  return ccall((:PSRIOOptgenExpansionDecision_ignoreUndefined, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOOptgenExpansionDecision_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenExpansionDecision_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenFirmElements_save( ptr_pointer,ptrStudy, filename, class_type, parmid )
  return ccall((:PSRIOOptgenFirmElements_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename,class_type,parmid)
end
function PSRIOOptgenFirmElements_load( ptr_pointer,ptrStudy, filename, class_type, parmid )
  return ccall((:PSRIOOptgenFirmElements_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename,class_type,parmid)
end
function PSRIOOptgenFirmElements_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenFirmElements_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenFirmElements_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenFirmElements_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLAN_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLAN_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLAN_setVersion( ptr_pointer,base_netplan_version )
  return ccall((:PSRIONETPLAN_setVersion, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,base_netplan_version)
end
function PSRIONETPLAN_selectScenarios( ptr_pointer,numberScenarios, scenarios )
  return ccall((:PSRIONETPLAN_selectScenarios, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{Int32},),ptr_pointer ,numberScenarios,scenarios)
end
function PSRIONETPLAN_load( ptr_pointer,ptrEstudo, nomePath )
  return ccall((:PSRIONETPLAN_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomePath)
end
function PSRIONETPLAN_save( ptr_pointer,ptrEstudo, nomePath )
  return ccall((:PSRIONETPLAN_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomePath)
end
function PSRIONETPLAN_updateFromExpansionData( ptr_pointer,ptrStudy, nomePath, removePreviusOptimized )
  return ccall((:PSRIONETPLAN_updateFromExpansionData, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrStudy,nomePath,removePreviusOptimized)
end
function PSRIONETPLAN_updateShuntsFromExpansionData( ptr_pointer,ptrStudy, nomePath, removePreviusOptimized )
  return ccall((:PSRIONETPLAN_updateShuntsFromExpansionData, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrStudy,nomePath,removePreviusOptimized)
end
function PSRIONETPLAN_updateToExpansionData( ptr_pointer,ptrStudy )
  return ccall((:PSRIONETPLAN_updateToExpansionData, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIONETPLAN_useIncrementalToSDDP( ptr_pointer,flag )
  return ccall((:PSRIONETPLAN_useIncrementalToSDDP, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIONETPLAN_useGenerationScenario( ptr_pointer,flag )
  return ccall((:PSRIONETPLAN_useGenerationScenario, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIONETPLAN_useOnlyDemandConstant( ptr_pointer,flag )
  return ccall((:PSRIONETPLAN_useOnlyDemandConstant, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIONETPLAN_useOnlyActiveDemand( ptr_pointer,flag )
  return ccall((:PSRIONETPLAN_useOnlyActiveDemand, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIONETPLAN_useOnlySelectedSystems( ptr_pointer,flag )
  return ccall((:PSRIONETPLAN_useOnlySelectedSystems, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIONETPLAN_useOnlyCircuits( ptr_pointer,flag )
  return ccall((:PSRIONETPLAN_useOnlyCircuits, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIONETPLAN_useDefaultScenarios( ptr_pointer,ptrEstudo )
  return ccall((:PSRIONETPLAN_useDefaultScenarios, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo)
end
function PSRIONETPLAN_useCandidates( ptr_pointer,usingCandidates )
  return ccall((:PSRIONETPLAN_useCandidates, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,usingCandidates)
end
function PSRIONETPLAN_useSerieCandidates( ptr_pointer,usingSerieCandidates )
  return ccall((:PSRIONETPLAN_useSerieCandidates, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,usingSerieCandidates)
end
function PSRIONETPLAN_useFictitious( ptr_pointer,usingFictitious )
  return ccall((:PSRIONETPLAN_useFictitious, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,usingFictitious)
end
function PSRIONETPLAN_createDemandFactorScenarios( ptr_pointer,pathdata )
  return ccall((:PSRIONETPLAN_createDemandFactorScenarios, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,pathdata)
end
function PSRIONETPLAN_useSDDPScenarios( ptr_pointer,pathsddp )
  return ccall((:PSRIONETPLAN_useSDDPScenarios, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,pathsddp)
end
function PSRIONETPLAN_useSDDPDeficit( ptr_pointer,flag )
  return ccall((:PSRIONETPLAN_useSDDPDeficit, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIONETPLAN_mapScenario( ptr_pointer,elementname, attributename, grafname )
  return ccall((:PSRIONETPLAN_mapScenario, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,elementname,attributename,grafname)
end
function PSRIONETPLAN_setBlocks( ptr_pointer,numero )
  return ccall((:PSRIONETPLAN_setBlocks, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,numero)
end
function PSRIONETPLAN_compareVersion( ptr_pointer,filename, version )
  return ccall((:PSRIONETPLAN_compareVersion, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename,version)
end
function PSRIONETPLAN_mergeNetwork( ptr_pointer,targetStudy, pathdata )
  return ccall((:PSRIONETPLAN_mergeNetwork, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,targetStudy,pathdata)
end
function PSRIONETPLAN_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLAN_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroParameters_load( ptr_pointer,ptrEstudo, nome, sddp_version )
  return ccall((:PSRIOSDDPHydroParameters_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrEstudo,nome,sddp_version)
end
function PSRIOSDDPHydroParameters_save( ptr_pointer,ptrEstudo, nome, sddp_version )
  return ccall((:PSRIOSDDPHydroParameters_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrEstudo,nome,sddp_version)
end
function PSRIOSDDPHydroParameters_importCSV( ptr_pointer,ptrEstudo, nome, sddp_version )
  return ccall((:PSRIOSDDPHydroParameters_importCSV, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrEstudo,nome,sddp_version)
end
function PSRIOSDDPHydroParameters_exportCSV( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPHydroParameters_exportCSV, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPHydroParameters_generateIndexedHydroParameters( ptr_pointer )
  return ccall((:PSRIOSDDPHydroParameters_generateIndexedHydroParameters, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroParameters_generateIndexedHydroParameters2( ptr_pointer,ptrStudy )
  return ccall((:PSRIOSDDPHydroParameters_generateIndexedHydroParameters2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIOSDDPHydroParameters_hasDataToWrite( ptr_pointer,ptrStudy )
  return ccall((:PSRIOSDDPHydroParameters_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIOSDDPHydroParameters_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPHydroParameters_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroParameters_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPHydroParameters_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPHydroForwardBackward_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPHydroForwardBackward_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPHydroForwardBackward_load( ptr_pointer,_ptrEstudo, filename_forward, filename_backward )
  return ccall((:PSRIOSDDPHydroForwardBackward_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,filename_forward,filename_backward)
end
function PSRIOSDDPHydroForwardBackward_initSave( ptr_pointer,_ptrEstudo, filename_forward, filename_backward )
  return ccall((:PSRIOSDDPHydroForwardBackward_initSave, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,filename_forward,filename_backward)
end
function PSRIOSDDPHydroForwardBackward_maxStations( ptr_pointer )
  return ccall((:PSRIOSDDPHydroForwardBackward_maxStations, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroForwardBackward_mapTo( ptr_pointer,mappedValues )
  return ccall((:PSRIOSDDPHydroForwardBackward_mapTo, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,pointer(mappedValues))
end
function PSRIOSDDPHydroForwardBackward_setForward( ptr_pointer,stage, simulation )
  return ccall((:PSRIOSDDPHydroForwardBackward_setForward, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,simulation)
end
function PSRIOSDDPHydroForwardBackward_setBackward( ptr_pointer,stage, simulation, opening )
  return ccall((:PSRIOSDDPHydroForwardBackward_setBackward, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,simulation,opening)
end
function PSRIOSDDPHydroForwardBackward_writeForward( ptr_pointer,stage, simulation )
  return ccall((:PSRIOSDDPHydroForwardBackward_writeForward, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,simulation)
end
function PSRIOSDDPHydroForwardBackward_writeBackward( ptr_pointer,stage, simulation, opening )
  return ccall((:PSRIOSDDPHydroForwardBackward_writeBackward, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,simulation,opening)
end
function PSRIOSDDPHydroForwardBackward_close( ptr_pointer )
  return ccall((:PSRIOSDDPHydroForwardBackward_close, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroForwardBackward_addReferenceMemory( ptr_pointer,address, memory )
  return ccall((:PSRIOSDDPHydroForwardBackward_addReferenceMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,pointer(address),pointer(memory))
end
function PSRIOSDDPHydroForwardBackward_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPHydroForwardBackward_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroTable_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPHydroTable_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPHydroTable_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPHydroTable_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPHydroTable_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPHydroTable_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPHydroTable_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPHydroTable_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroTable_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPHydroTable_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenProjectsList_save( ptr_pointer,ptrGroup, filename )
  return ccall((:PSRIOOptgenProjectsList_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGroup,filename)
end
function PSRIOOptgenProjectsList_load( ptr_pointer,ptrStudy, ptrGroup, filename, class_type )
  return ccall((:PSRIOOptgenProjectsList_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrStudy,ptrGroup,filename,class_type)
end
function PSRIOOptgenProjectsList_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenProjectsList_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenProjectsList_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenProjectsList_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONEWAVE_create(iprt)
  ptr_pointer =  ccall((:PSRIONEWAVE_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONEWAVE_load( ptr_pointer,ptrEstudo, nomePath )
  return ccall((:PSRIONEWAVE_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomePath)
end
function PSRIONEWAVE_deleteInstance( ptr_pointer )
  return ccall((:PSRIONEWAVE_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenCapacityConstraint_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenCapacityConstraint_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenCapacityConstraint_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenCapacityConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenCapacityConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenCapacityConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenCapacityConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenCapacityConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgen_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgen_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgen_save( ptr_pointer,ptrStudy, pathdata )
  return ccall((:PSRIOOptgen_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathdata)
end
function PSRIOOptgen_load( ptr_pointer,ptrStudy, pathdata )
  return ccall((:PSRIOOptgen_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathdata)
end
function PSRIOOptgen_loadTypicalDays( ptr_pointer,ptrStudy, pathdata )
  return ccall((:PSRIOOptgen_loadTypicalDays, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathdata)
end
function PSRIOOptgen_overrideConfiguration( ptr_pointer,status )
  return ccall((:PSRIOOptgen_overrideConfiguration, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOOptgen_useContraints( ptr_pointer,status )
  return ccall((:PSRIOOptgen_useContraints, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOOptgen_useOutputDecisionsAsPlan( ptr_pointer,status )
  return ccall((:PSRIOOptgen_useOutputDecisionsAsPlan, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOOptgen_useOutputDecisionsFromIteration( ptr_pointer,optgen_iteration )
  return ccall((:PSRIOOptgen_useOutputDecisionsFromIteration, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,optgen_iteration)
end
function PSRIOOptgen_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgen_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessem_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIODessem_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIODessem_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessem_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessem_create(iprt)
  ptr_pointer =  ccall((:PSRIODessem_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRResult_create(iprt,nomeCSV, descricao, unidade, idagente)
  ptr_pointer =  ccall((:PSRResult_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL,nomeCSV,descricao,unidade,idagente)
  return ptr_pointer
end
function PSRResult_nameCSV( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRResult_nameCSV, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRResult_description( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRResult_description, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRResult_unit( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRResult_unit, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRResult_idAgent( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRResult_idAgent, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRResult_deleteInstance( ptr_pointer )
  return ccall((:PSRResult_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRReserveConstraintList_maxConstraint( ptr_pointer )
  return ccall((:PSRReserveConstraintList_maxConstraint, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRReserveConstraintList_constraint( ptr_pointer,ndx )
  return ccall((:PSRReserveConstraintList_constraint, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRReserveConstraintList_addConstraint( ptr_pointer,ptrConstraint )
  return ccall((:PSRReserveConstraintList_addConstraint, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrConstraint)
end
function PSRReserveConstraintList_deleteInstance( ptr_pointer )
  return ccall((:PSRReserveConstraintList_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRReserveConstraintList_create(iprt)
  ptr_pointer =  ccall((:PSRReserveConstraintList_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRParsers_getInstance( ptr_pointer )
  return ccall((:PSRParsers_getInstance, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL )
end
function PSRParsers_create(iprt)
  ptr_pointer =  ccall((:PSRParsers_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRParsers_config( ptr_pointer )
  return ccall((:PSRParsers_config, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParsers_isInteger( ptr_pointer,str )
  return ccall((:PSRParsers_isInteger, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,str)
end
function PSRParsers_isNumber( ptr_pointer,str )
  return ccall((:PSRParsers_isNumber, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,str)
end
function PSRParsers_toString( ptr_pointer,valor )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_toString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,valor,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_toString2( ptr_pointer,valor, Width )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_toString2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,Ptr{UInt8},),ptr_pointer ,valor,Width,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_toString3( ptr_pointer,valor )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_toString3, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,valor,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_toString4( ptr_pointer,valor )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_toString4, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,Ptr{UInt8},),ptr_pointer ,valor,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_toString5( ptr_pointer,Value, Precision, Width )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_toString5, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64, Int32, Int32,Ptr{UInt8},),ptr_pointer ,Value,Precision,Width,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_toString6( ptr_pointer,RN, BUFF_SIZE )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_toString6, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64, Int32,Ptr{UInt8},),ptr_pointer ,RN,BUFF_SIZE,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_toString7( ptr_pointer,valor )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_toString7, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64,Ptr{UInt8},),ptr_pointer ,valor,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_toHex( ptr_pointer,valor )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_toHex, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,valor,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_toInt( ptr_pointer,valor )
  return ccall((:PSRParsers_toInt, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,valor)
end
function PSRParsers_toReal( ptr_pointer,valor )
  return ccall((:PSRParsers_toReal, "PSRClasses"),Float64,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,valor)
end
function PSRParsers_toDate( ptr_pointer,valor )
  return ccall((:PSRParsers_toDate, "PSRClasses"),Int64,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,valor)
end
function PSRParsers_toDateWithFormat( ptr_pointer,valor, format )
  return ccall((:PSRParsers_toDateWithFormat, "PSRClasses"),Int64,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,valor,format)
end
function PSRParsers_toDate2( ptr_pointer,mes, ano )
  return ccall((:PSRParsers_toDate2, "PSRClasses"),Int64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,mes,ano)
end
function PSRParsers_toDate3( ptr_pointer,dia, mes, ano )
  return ccall((:PSRParsers_toDate3, "PSRClasses"),Int64,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,dia,mes,ano)
end
function PSRParsers_toDate4( ptr_pointer,dia, mes, ano, hora, minuto )
  return ccall((:PSRParsers_toDate4, "PSRClasses"),Int64,(Ptr{UInt8}, Int32, Int32, Int32, Int32, Int32,),ptr_pointer ,dia,mes,ano,hora,minuto)
end
function PSRParsers_dateOffset( ptr_pointer,date, offset, stageType )
  return ccall((:PSRParsers_dateOffset, "PSRClasses"),Int64,(Ptr{UInt8}, Int64, Int32, Int32,),ptr_pointer ,date,offset,stageType)
end
function PSRParsers_getDifferenceInStages( ptr_pointer,datemin, datemax, stageType )
  return ccall((:PSRParsers_getDifferenceInStages, "PSRClasses"),Int32,(Ptr{UInt8}, Int64, Int64, Int32,),ptr_pointer ,datemin,datemax,stageType)
end
function PSRParsers_getDateFromWeekYear( ptr_pointer,semana, ano )
  return ccall((:PSRParsers_getDateFromWeekYear, "PSRClasses"),Int64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,semana,ano)
end
function PSRParsers_getDayFromDate( ptr_pointer,data )
  return ccall((:PSRParsers_getDayFromDate, "PSRClasses"),Int32,(Ptr{UInt8}, Int64,),ptr_pointer ,data)
end
function PSRParsers_getMonthFromDate( ptr_pointer,data )
  return ccall((:PSRParsers_getMonthFromDate, "PSRClasses"),Int32,(Ptr{UInt8}, Int64,),ptr_pointer ,data)
end
function PSRParsers_getYearFromDate( ptr_pointer,data )
  return ccall((:PSRParsers_getYearFromDate, "PSRClasses"),Int32,(Ptr{UInt8}, Int64,),ptr_pointer ,data)
end
function PSRParsers_getWeekFromDate( ptr_pointer,data )
  return ccall((:PSRParsers_getWeekFromDate, "PSRClasses"),Int32,(Ptr{UInt8}, Int64,),ptr_pointer ,data)
end
function PSRParsers_toInt2( ptr_pointer,text, start, _end )
  return ccall((:PSRParsers_toInt2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),ptr_pointer ,text,start,_end)
end
function PSRParsers_toReal2( ptr_pointer,text, start, _end )
  return ccall((:PSRParsers_toReal2, "PSRClasses"),Float64,(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),ptr_pointer ,text,start,_end)
end
function PSRParsers_getTypeStageVariation( ptr_pointer,vData )
  return ccall((:PSRParsers_getTypeStageVariation, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,vData)
end
function PSRParsers_trim( ptr_pointer,texto )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_trim, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,texto,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_noSpace( ptr_pointer,texto )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_noSpace, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,texto,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_toLowerCase( ptr_pointer,texto )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_toLowerCase, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,texto,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_toUpperCase( ptr_pointer,texto )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_toUpperCase, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,texto,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_substring( ptr_pointer,text, start, _end )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_substring, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,Ptr{UInt8},),ptr_pointer ,text,start,_end,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_replace( ptr_pointer,text, substring1, newsubstring )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_replace, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,text,substring1,newsubstring,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_contains( ptr_pointer,text, substring )
  return ccall((:PSRParsers_contains, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,text,substring)
end
function PSRParsers_fillWidth( ptr_pointer,text, width )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_fillWidth, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,text,width,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_fillWidth2( ptr_pointer,number, width )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_fillWidth2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,Ptr{UInt8},),ptr_pointer ,number,width,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_rightAlign( ptr_pointer,text, width )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_rightAlign, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,text,width,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_textDelimitedBy( ptr_pointer,texto, delimit_first, delimit_last )
  retPtr = "                                                                                                                  "
  ccall((:PSRParsers_textDelimitedBy, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,texto,delimit_first,delimit_last,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParsers_deleteInstance( ptr_pointer )
  return ccall((:PSRParsers_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRQueryStatement_select( ptr_pointer,sourceElement, classname, idString, queryString )
  return ccall((:PSRQueryStatement_select, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,sourceElement,classname,idString,queryString)
end
function PSRQueryStatement_insert( ptr_pointer,targetElement, sourceSheet, classname, idString )
  return ccall((:PSRQueryStatement_insert, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,targetElement,sourceSheet,classname,idString)
end
function PSRQueryStatement_updateCollection( ptr_pointer,ptrColElement, querystring )
  return ccall((:PSRQueryStatement_updateCollection, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrColElement,querystring)
end
function PSRQueryStatement_deleteInstance( ptr_pointer )
  return ccall((:PSRQueryStatement_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRQueryStatement_create(iprt)
  ptr_pointer =  ccall((:PSRQueryStatement_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOMapOptions_create(iprt)
  ptr_pointer =  ccall((:PSRIOMapOptions_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOMapOptions_delimitBySpace( ptr_pointer,status )
  return ccall((:PSRIOMapOptions_delimitBySpace, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOMapOptions_load( ptr_pointer,nome )
  return ccall((:PSRIOMapOptions_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIOMapOptions_load2( ptr_pointer,ptrSpreadSheet )
  return ccall((:PSRIOMapOptions_load2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSpreadSheet)
end
function PSRIOMapOptions_insertScriptFrom( ptr_pointer,filename )
  return ccall((:PSRIOMapOptions_insertScriptFrom, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRIOMapOptions_exist( ptr_pointer,opcao )
  return ccall((:PSRIOMapOptions_exist, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,opcao)
end
function PSRIOMapOptions_getString( ptr_pointer,opcao, index )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMapOptions_getString, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,opcao,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMapOptions_getInteger( ptr_pointer,opcao, index )
  return ccall((:PSRIOMapOptions_getInteger, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,opcao,index)
end
function PSRIOMapOptions_getReal( ptr_pointer,opcao, index )
  return ccall((:PSRIOMapOptions_getReal, "PSRClasses"),Float64,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,opcao,index)
end
function PSRIOMapOptions_getCurrentIndex( ptr_pointer )
  return ccall((:PSRIOMapOptions_getCurrentIndex, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMapOptions_first( ptr_pointer )
  return ccall((:PSRIOMapOptions_first, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMapOptions_next( ptr_pointer )
  return ccall((:PSRIOMapOptions_next, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMapOptions_isEof( ptr_pointer )
  return ccall((:PSRIOMapOptions_isEof, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMapOptions_goTo( ptr_pointer,index )
  return ccall((:PSRIOMapOptions_goTo, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRIOMapOptions_getOpcao( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMapOptions_getOpcao, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMapOptions_getString2( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMapOptions_getString2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMapOptions_getInteger2( ptr_pointer,index )
  return ccall((:PSRIOMapOptions_getInteger2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRIOMapOptions_getReal2( ptr_pointer,index )
  return ccall((:PSRIOMapOptions_getReal2, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRIOMapOptions_getStringFrom( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMapOptions_getStringFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMapOptions_putString( ptr_pointer,index, parameter )
  return ccall((:PSRIOMapOptions_putString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,index,parameter)
end
function PSRIOMapOptions_setVariable( ptr_pointer,name, value )
  return ccall((:PSRIOMapOptions_setVariable, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name,value)
end
function PSRIOMapOptions_getVariable( ptr_pointer,name )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMapOptions_getVariable, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,name,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMapOptions_delVariable( ptr_pointer,name )
  return ccall((:PSRIOMapOptions_delVariable, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name)
end
function PSRIOMapOptions_autoset( ptr_pointer,ptrModel, remove_noparm )
  return ccall((:PSRIOMapOptions_autoset, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrModel,remove_noparm)
end
function PSRIOMapOptions_deleteInstance( ptr_pointer )
  return ccall((:PSRIOMapOptions_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRQueryConditionVariable_create(iprt,ptrParm)
  ptr_pointer =  ccall((:PSRQueryConditionVariable_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,ptrParm)
  return ptr_pointer
end
function PSRQueryConditionVariable_create2( iprt,ptrVector )
  ptr_pointer =  ccall((:PSRQueryConditionVariable_create2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,ptrVector)
  return ptr_pointer
end
function PSRQueryConditionVariable_getInteger( ptr_pointer,index )
  return ccall((:PSRQueryConditionVariable_getInteger, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRQueryConditionVariable_getReal( ptr_pointer,index )
  return ccall((:PSRQueryConditionVariable_getReal, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRQueryConditionVariable_getDataType( ptr_pointer )
  return ccall((:PSRQueryConditionVariable_getDataType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRQueryConditionVariable_size( ptr_pointer )
  return ccall((:PSRQueryConditionVariable_size, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRQueryConditionVariable_getVector( ptr_pointer )
  return ccall((:PSRQueryConditionVariable_getVector, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRQueryConditionVariable_deleteInstance( ptr_pointer )
  return ccall((:PSRQueryConditionVariable_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParserConfig_create(iprt)
  ptr_pointer =  ccall((:PSRParserConfig_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRParserConfig_getDateFormat( ptr_pointer )
  return ccall((:PSRParserConfig_getDateFormat, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRParserConfig_getDateSpace( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParserConfig_getDateSpace, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParserConfig_setDecimals( ptr_pointer,_decimals )
  return ccall((:PSRParserConfig_setDecimals, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_decimals)
end
function PSRParserConfig_setDateFormat( ptr_pointer,_date_format )
  return ccall((:PSRParserConfig_setDateFormat, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_date_format)
end
function PSRParserConfig_setDateSpace( ptr_pointer,_date_space )
  return ccall((:PSRParserConfig_setDateSpace, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_date_space)
end
function PSRParserConfig_deleteInstance( ptr_pointer )
  return ccall((:PSRParserConfig_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRNode_create(iprt,_ptrElement)
  ptr_pointer =  ccall((:PSRNode_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_ptrElement)
  return ptr_pointer
end
function PSRNode_element( ptr_pointer )
  return ccall((:PSRNode_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRNode_replaceElement( ptr_pointer,ptrElement )
  return ccall((:PSRNode_replaceElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRNode_maxGround( ptr_pointer )
  return ccall((:PSRNode_maxGround, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRNode_maxArc( ptr_pointer )
  return ccall((:PSRNode_maxArc, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRNode_ground( ptr_pointer,ndx )
  return ccall((:PSRNode_ground, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRNode_arc( ptr_pointer,ndx )
  return ccall((:PSRNode_arc, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRNode_graph( ptr_pointer )
  return ccall((:PSRNode_graph, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRNode_logicalGraph( ptr_pointer )
  return ccall((:PSRNode_logicalGraph, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRNode_hasFromConnection( ptr_pointer,ptrNode )
  return ccall((:PSRNode_hasFromConnection, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNode)
end
function PSRNode_hasToConnection( ptr_pointer,ptrNode )
  return ccall((:PSRNode_hasToConnection, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNode)
end
function PSRNode_sortArcs( ptr_pointer,order_type )
  return ccall((:PSRNode_sortArcs, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,order_type)
end
function PSRNode_deleteInstance( ptr_pointer )
  return ccall((:PSRNode_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRResultConfiguration_create(iprt,num, attr)
  ptr_pointer =  ccall((:PSRResultConfiguration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Ptr{UInt8},),C_NULL,num,attr)
  return ptr_pointer
end
function PSRResultConfiguration_create2( iprt,num, name, unidade, fileName, classe, attr, _type )
  ptr_pointer =  ccall((:PSRResultConfiguration_create2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8}, Int32,),C_NULL,num,name,unidade,fileName,classe,attr,_type)
  return ptr_pointer
end
function PSRResultConfiguration_getNum( ptr_pointer )
  return ccall((:PSRResultConfiguration_getNum, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRResultConfiguration_getAttr( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRResultConfiguration_getAttr, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRResultConfiguration_getName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRResultConfiguration_getName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRResultConfiguration_getUnity( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRResultConfiguration_getUnity, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRResultConfiguration_getFileName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRResultConfiguration_getFileName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRResultConfiguration_getClasse( ptr_pointer )
  return ccall((:PSRResultConfiguration_getClasse, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRResultConfiguration_getType( ptr_pointer )
  return ccall((:PSRResultConfiguration_getType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRResultConfiguration_getSelected( ptr_pointer )
  return ccall((:PSRResultConfiguration_getSelected, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRResultConfiguration_setNum( ptr_pointer,num )
  return ccall((:PSRResultConfiguration_setNum, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,num)
end
function PSRResultConfiguration_setAttr( ptr_pointer,attr )
  return ccall((:PSRResultConfiguration_setAttr, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,attr)
end
function PSRResultConfiguration_setName( ptr_pointer,name )
  return ccall((:PSRResultConfiguration_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name)
end
function PSRResultConfiguration_setUnity( ptr_pointer,unity )
  return ccall((:PSRResultConfiguration_setUnity, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,unity)
end
function PSRResultConfiguration_setFileName( ptr_pointer,fileName )
  return ccall((:PSRResultConfiguration_setFileName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,fileName)
end
function PSRResultConfiguration_setClasse( ptr_pointer,classe )
  return ccall((:PSRResultConfiguration_setClasse, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,classe)
end
function PSRResultConfiguration_setType( ptr_pointer,_type )
  return ccall((:PSRResultConfiguration_setType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_type)
end
function PSRResultConfiguration_setSelected( ptr_pointer,flag )
  return ccall((:PSRResultConfiguration_setSelected, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,flag)
end
function PSRResultConfiguration_deleteInstance( ptr_pointer )
  return ccall((:PSRResultConfiguration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationTest_validate( ptr_pointer,ptrColElements )
  return ccall((:PSRValidationTest_validate, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrColElements)
end
function PSRValidationTest_nextError( ptr_pointer )
  return ccall((:PSRValidationTest_nextError, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationTest_element( ptr_pointer )
  return ccall((:PSRValidationTest_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationTest_elementIndex( ptr_pointer )
  return ccall((:PSRValidationTest_elementIndex, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationTest_errorCode( ptr_pointer )
  return ccall((:PSRValidationTest_errorCode, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationTest_indexPosition( ptr_pointer )
  return ccall((:PSRValidationTest_indexPosition, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationTest_namePosition( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRValidationTest_namePosition, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRValidationTest_deleteInstance( ptr_pointer )
  return ccall((:PSRValidationTest_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationTest_create(iprt)
  ptr_pointer =  ccall((:PSRValidationTest_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRValidationResult_create(iprt,ptrElement, retcode, indexvector, indexposition)
  ptr_pointer =  ccall((:PSRValidationResult_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8}, Int32,),C_NULL,ptrElement,retcode,indexvector,indexposition)
  return ptr_pointer
end
function PSRValidationResult_element( ptr_pointer )
  return ccall((:PSRValidationResult_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationResult_errorCode( ptr_pointer )
  return ccall((:PSRValidationResult_errorCode, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationResult_indexVector( ptr_pointer )
  return ccall((:PSRValidationResult_indexVector, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationResult_indexPosition( ptr_pointer )
  return ccall((:PSRValidationResult_indexPosition, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRValidationResult_deleteInstance( ptr_pointer )
  return ccall((:PSRValidationResult_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParm_create(iprt,id)
  ptr_pointer =  ccall((:PSRParm_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,id)
  return ptr_pointer
end
function PSRParm_id( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParm_id, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParm_setId( ptr_pointer,Id )
  return ccall((:PSRParm_setId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,Id)
end
function PSRParm_getDataType( ptr_pointer )
  return ccall((:PSRParm_getDataType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRParm_noParm( ptr_pointer )
  return ccall((:PSRParm_noParm, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRParm_noParm2( ptr_pointer,_noParm )
  return ccall((:PSRParm_noParm2, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,_noParm)
end
function PSRParm_isIdentification( ptr_pointer )
  return ccall((:PSRParm_isIdentification, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRParm_setIsIdentification( ptr_pointer,status )
  return ccall((:PSRParm_setIsIdentification, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRParm_getString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParm_getString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParm_getInteger( ptr_pointer )
  return ccall((:PSRParm_getInteger, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRParm_getReal( ptr_pointer )
  return ccall((:PSRParm_getReal, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRParm_getDate( ptr_pointer )
  return ccall((:PSRParm_getDate, "PSRClasses"),Int64,(Ptr{UInt8},),ptr_pointer )
end
function PSRParm_toString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParm_toString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParm_setDataFromString( ptr_pointer,data )
  return ccall((:PSRParm_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParm_setData( ptr_pointer,data )
  return ccall((:PSRParm_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParm_setData2( ptr_pointer,data )
  return ccall((:PSRParm_setData2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,data)
end
function PSRParm_setData3( ptr_pointer,data )
  return ccall((:PSRParm_setData3, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,data)
end
function PSRParm_setData4( ptr_pointer,date )
  return ccall((:PSRParm_setData4, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64,),ptr_pointer ,date)
end
function PSRParm_setDefault( ptr_pointer )
  return ccall((:PSRParm_setDefault, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParm_clone( ptr_pointer )
  return ccall((:PSRParm_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParm_copyTo( ptr_pointer,TargetParm )
  return ccall((:PSRParm_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,TargetParm)
end
function PSRParm_serialize( ptr_pointer )
  return ccall((:PSRParm_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParm_buildFrom( ptr_pointer,ptrMessageDataParm )
  return ccall((:PSRParm_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataParm)
end
function PSRParm_setDimensionInformation( ptr_pointer,dimension )
  return ccall((:PSRParm_setDimensionInformation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,dimension)
end
function PSRParm_getDimensionInformation( ptr_pointer )
  return ccall((:PSRParm_getDimensionInformation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParm_deleteInstance( ptr_pointer )
  return ccall((:PSRParm_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRModelDimension_create(iprt)
  ptr_pointer =  ccall((:PSRModelDimension_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRModelDimension_id( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRModelDimension_id, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRModelDimension_setId( ptr_pointer,_id )
  return ccall((:PSRModelDimension_setId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_id)
end
function PSRModelDimension_value( ptr_pointer )
  return ccall((:PSRModelDimension_value, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRModelDimension_setValue( ptr_pointer,_value )
  return ccall((:PSRModelDimension_setValue, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_value)
end
function PSRModelDimension_deleteInstance( ptr_pointer )
  return ccall((:PSRModelDimension_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRObject_create(iprt,id)
  ptr_pointer =  ccall((:PSRObject_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,id)
  return ptr_pointer
end
function PSRObject_create2( iprt,id, _ptrObject )
  ptr_pointer =  ccall((:PSRObject_create2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL,id,pointer(_ptrObject))
  return ptr_pointer
end
function PSRObject_id( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRObject_id, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRObject_setObject( ptr_pointer,_ptrObject )
  return ccall((:PSRObject_setObject, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,pointer(_ptrObject))
end
function PSRObject_clone( ptr_pointer )
  return ccall((:PSRObject_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRObject_deleteInstance( ptr_pointer )
  return ccall((:PSRObject_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDimensionInformation_getNumberDimensions( ptr_pointer )
  return ccall((:PSRParmDimensionInformation_getNumberDimensions, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDimensionInformation_getNumberParms( ptr_pointer )
  return ccall((:PSRParmDimensionInformation_getNumberParms, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDimensionInformation_getDimensionName( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmDimensionInformation_getDimensionName, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmDimensionInformation_setNumberDimensions( ptr_pointer,number )
  return ccall((:PSRParmDimensionInformation_setNumberDimensions, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number)
end
function PSRParmDimensionInformation_setDimensionName( ptr_pointer,index, dimension )
  return ccall((:PSRParmDimensionInformation_setDimensionName, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,index,dimension)
end
function PSRParmDimensionInformation_setDimensionSize( ptr_pointer,index, size )
  return ccall((:PSRParmDimensionInformation_setDimensionSize, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,index,size)
end
function PSRParmDimensionInformation_getDimensionSize( ptr_pointer,index )
  return ccall((:PSRParmDimensionInformation_getDimensionSize, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRParmDimensionInformation_useDimension( ptr_pointer,dimension, value )
  return ccall((:PSRParmDimensionInformation_useDimension, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,dimension,value)
end
function PSRParmDimensionInformation_useDimension2( ptr_pointer,dimension, value )
  return ccall((:PSRParmDimensionInformation_useDimension2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,dimension,value)
end
function PSRParmDimensionInformation_baseParm( ptr_pointer )
  return ccall((:PSRParmDimensionInformation_baseParm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDimensionInformation_setBaseParm( ptr_pointer,ptrParm )
  return ccall((:PSRParmDimensionInformation_setBaseParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrParm)
end
function PSRParmDimensionInformation_getCurrent( ptr_pointer )
  return ccall((:PSRParmDimensionInformation_getCurrent, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDimensionInformation_parm( ptr_pointer,index )
  return ccall((:PSRParmDimensionInformation_parm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRParmDimensionInformation_addParm( ptr_pointer,ptrParm )
  return ccall((:PSRParmDimensionInformation_addParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrParm)
end
function PSRParmDimensionInformation_sortVectors( ptr_pointer )
  return ccall((:PSRParmDimensionInformation_sortVectors, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDimensionInformation_serialize( ptr_pointer )
  return ccall((:PSRParmDimensionInformation_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDimensionInformation_buildFrom( ptr_pointer,ptrMessageDataParm )
  return ccall((:PSRParmDimensionInformation_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataParm)
end
function PSRParmDimensionInformation_deleteInstance( ptr_pointer )
  return ccall((:PSRParmDimensionInformation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDimensionInformation_create(iprt)
  ptr_pointer =  ccall((:PSRParmDimensionInformation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGrafIntegrationError_create(iprt,grafname, elementid, agent_name, is_agent_not_found, is_element_not_found)
  ptr_pointer =  ccall((:PSRIOGrafIntegrationError_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool, Bool,),C_NULL,grafname,elementid,agent_name,is_agent_not_found,is_element_not_found)
  return ptr_pointer
end
function PSRIOGrafIntegrationError_grafName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOGrafIntegrationError_grafName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOGrafIntegrationError_elementId( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOGrafIntegrationError_elementId, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOGrafIntegrationError_agentName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOGrafIntegrationError_agentName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOGrafIntegrationError_isAgentNotFound( ptr_pointer )
  return ccall((:PSRIOGrafIntegrationError_isAgentNotFound, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafIntegrationError_isElementNotFound( ptr_pointer )
  return ccall((:PSRIOGrafIntegrationError_isElementNotFound, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafIntegrationError_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGrafIntegrationError_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONetplanGrafIntegration_create(iprt,elementname, grafname, attributename)
  ptr_pointer =  ccall((:PSRIONetplanGrafIntegration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL,elementname,grafname,attributename)
  return ptr_pointer
end
function PSRIONetplanGrafIntegration_elementName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIONetplanGrafIntegration_elementName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIONetplanGrafIntegration_grafName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIONetplanGrafIntegration_grafName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIONetplanGrafIntegration_attributeName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIONetplanGrafIntegration_attributeName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIONetplanGrafIntegration_getCollectionElements( ptr_pointer,ptrStudy )
  return ccall((:PSRIONetplanGrafIntegration_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIONetplanGrafIntegration_deleteInstance( ptr_pointer )
  return ccall((:PSRIONetplanGrafIntegration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_VAR_create(iprt,_nome, _min, _max)
  ptr_pointer =  ccall((:PSRIOMask_VAR_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),C_NULL,_nome,_min,_max)
  return ptr_pointer
end
function PSRIOMask_VAR_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMask_VAR_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMask_VAR_Min( ptr_pointer )
  return ccall((:PSRIOMask_VAR_Min, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_VAR_Max( ptr_pointer )
  return ccall((:PSRIOMask_VAR_Max, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_VAR_current( ptr_pointer )
  return ccall((:PSRIOMask_VAR_current, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_VAR_reset( ptr_pointer )
  return ccall((:PSRIOMask_VAR_reset, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_VAR_increment( ptr_pointer )
  return ccall((:PSRIOMask_VAR_increment, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_VAR_deleteInstance( ptr_pointer )
  return ccall((:PSRIOMask_VAR_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemThermalContractMap_load( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemThermalContractMap_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemThermalContractMap_save( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemThermalContractMap_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemThermalContractMap_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemThermalContractMap_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemThermalContractMap_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemThermalContractMap_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOContainer_checkVersion( ptr_pointer,filename )
  return ccall((:PSRIOContainer_checkVersion, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRIOContainer_deleteInstance( ptr_pointer )
  return ccall((:PSRIOContainer_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOContainer_create(iprt)
  ptr_pointer =  ccall((:PSRIOContainer_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemHydroPlantMap_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemHydroPlantMap_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemHydroPlantMap_load( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemHydroPlantMap_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemHydroPlantMap_save( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemHydroPlantMap_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemHydroPlantMap_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemHydroPlantMap_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_create(iprt)
  ptr_pointer =  ccall((:PSRIOElementHourlyScenarios_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOElementHourlyScenarios_add( ptr_pointer,ptrIOChild )
  return ccall((:PSRIOElementHourlyScenarios_add, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrIOChild)
end
function PSRIOElementHourlyScenarios_maxElements( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_maxElements, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_getElementFromAgentIndex( ptr_pointer,index )
  return ccall((:PSRIOElementHourlyScenarios_getElementFromAgentIndex, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRIOElementHourlyScenarios_type( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_type, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_setVectorName( ptr_pointer,name )
  return ccall((:PSRIOElementHourlyScenarios_setVectorName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name)
end
function PSRIOElementHourlyScenarios_getVectorName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOElementHourlyScenarios_getVectorName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOElementHourlyScenarios_isControlling( ptr_pointer,class_type, attribute_name )
  return ccall((:PSRIOElementHourlyScenarios_isControlling, "PSRClasses"),Bool,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,class_type,attribute_name)
end
function PSRIOElementHourlyScenarios_isControlling2( ptr_pointer,class_name, attribute_name )
  return ccall((:PSRIOElementHourlyScenarios_isControlling2, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,class_name,attribute_name)
end
function PSRIOElementHourlyScenarios_createFromDat( ptr_pointer,ptrStudy, pathname )
  return ccall((:PSRIOElementHourlyScenarios_createFromDat, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathname)
end
function PSRIOElementHourlyScenarios_createFromDimensionedVectors( ptr_pointer,ptrStudy, pathname, source_vector_name )
  return ccall((:PSRIOElementHourlyScenarios_createFromDimensionedVectors, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathname,source_vector_name)
end
function PSRIOElementHourlyScenarios_createFromGrafBinary( ptr_pointer,ptrStudy, path, id_attribute )
  return ccall((:PSRIOElementHourlyScenarios_createFromGrafBinary, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,path,id_attribute)
end
function PSRIOElementHourlyScenarios_createFromGrafBinary2( ptr_pointer,ptrStudy, header_name, binary_name, id_attribute )
  return ccall((:PSRIOElementHourlyScenarios_createFromGrafBinary2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,header_name,binary_name,id_attribute)
end
function PSRIOElementHourlyScenarios_createFromExternal( ptr_pointer,ptrStudy, pathname )
  return ccall((:PSRIOElementHourlyScenarios_createFromExternal, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathname)
end
function PSRIOElementHourlyScenarios_createFromAggregatedExternal( ptr_pointer,ptrStudy, pathname )
  return ccall((:PSRIOElementHourlyScenarios_createFromAggregatedExternal, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathname)
end
function PSRIOElementHourlyScenarios_createFromHistorical( ptr_pointer,ptrStudy, pathname )
  return ccall((:PSRIOElementHourlyScenarios_createFromHistorical, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathname)
end
function PSRIOElementHourlyScenarios_gotoStageHour( ptr_pointer,stage, hour, simulation )
  return ccall((:PSRIOElementHourlyScenarios_gotoStageHour, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,hour,simulation)
end
function PSRIOElementHourlyScenarios_gotoRegistryHour( ptr_pointer,stage, hour, simulation )
  return ccall((:PSRIOElementHourlyScenarios_gotoRegistryHour, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,hour,simulation)
end
function PSRIOElementHourlyScenarios_gotoStageHour2( ptr_pointer,stage, hour )
  return ccall((:PSRIOElementHourlyScenarios_gotoStageHour2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,hour)
end
function PSRIOElementHourlyScenarios_gotoRegistryHour2( ptr_pointer,stage, hour )
  return ccall((:PSRIOElementHourlyScenarios_gotoRegistryHour2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,hour)
end
function PSRIOElementHourlyScenarios_gotoPeriodHour( ptr_pointer,stage, hour, simulation )
  return ccall((:PSRIOElementHourlyScenarios_gotoPeriodHour, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,hour,simulation)
end
function PSRIOElementHourlyScenarios_gotoStageBlock( ptr_pointer,stage, block, simulation )
  return ccall((:PSRIOElementHourlyScenarios_gotoStageBlock, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,block,simulation)
end
function PSRIOElementHourlyScenarios_gotoStageBlock2( ptr_pointer,stage, block )
  return ccall((:PSRIOElementHourlyScenarios_gotoStageBlock2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,block)
end
function PSRIOElementHourlyScenarios_gotoRegistryBlock( ptr_pointer,stage, block, simulation )
  return ccall((:PSRIOElementHourlyScenarios_gotoRegistryBlock, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,block,simulation)
end
function PSRIOElementHourlyScenarios_gotoRegistryBlock2( ptr_pointer,stage, block )
  return ccall((:PSRIOElementHourlyScenarios_gotoRegistryBlock2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,block)
end
function PSRIOElementHourlyScenarios_gotoPeriodBlock( ptr_pointer,period, block, simulation )
  return ccall((:PSRIOElementHourlyScenarios_gotoPeriodBlock, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,period,block,simulation)
end
function PSRIOElementHourlyScenarios_gotoAbsoluteHour( ptr_pointer,hour, simulation )
  return ccall((:PSRIOElementHourlyScenarios_gotoAbsoluteHour, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,hour,simulation)
end
function PSRIOElementHourlyScenarios_selectScenario( ptr_pointer,scenario )
  return ccall((:PSRIOElementHourlyScenarios_selectScenario, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,scenario)
end
function PSRIOElementHourlyScenarios_totalScenarios( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_totalScenarios, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_totalStages( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_totalStages, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_totalHours( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_totalHours, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_getInitialDate( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_getInitialDate, "PSRClasses"),Int64,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_usingBlocks( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_usingBlocks, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_isUsingProfile( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_isUsingProfile, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_exportToDat( ptr_pointer,ptrStudy, pathname )
  return ccall((:PSRIOElementHourlyScenarios_exportToDat, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathname)
end
function PSRIOElementHourlyScenarios_getStageType( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_getStageType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_rebaseToMonth( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_rebaseToMonth, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_rebaseTo13Month( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_rebaseTo13Month, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_rebaseToWeek( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_rebaseToWeek, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_aggregateInBlocks( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_aggregateInBlocks, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_close( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_close, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_getBuffer( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_getBuffer, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_serialize( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOElementHourlyScenarios_buildFrom( ptr_pointer,ptrMessage )
  return ccall((:PSRIOElementHourlyScenarios_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessage)
end
function PSRIOElementHourlyScenarios_deleteInstance( ptr_pointer )
  return ccall((:PSRIOElementHourlyScenarios_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONewaveOutput_toGrafBinary( ptr_pointer,nameradical )
  return ccall((:PSRIONewaveOutput_toGrafBinary, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nameradical)
end
function PSRIONewaveOutput_deleteInstance( ptr_pointer )
  return ccall((:PSRIONewaveOutput_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONewaveOutput_create(iprt)
  ptr_pointer =  ccall((:PSRIONewaveOutput_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGrafIntegrationList_create(iprt)
  ptr_pointer =  ccall((:PSRIOGrafIntegrationList_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGrafIntegrationList_setElementAttributeId( ptr_pointer,id )
  return ccall((:PSRIOGrafIntegrationList_setElementAttributeId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRIOGrafIntegrationList_hasCustomIntegrationScenarioFor( ptr_pointer,elementname, attributename )
  return ccall((:PSRIOGrafIntegrationList_hasCustomIntegrationScenarioFor, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,elementname,attributename)
end
function PSRIOGrafIntegrationList_mapScenario( ptr_pointer,elementname, attributename, grafname )
  return ccall((:PSRIOGrafIntegrationList_mapScenario, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,elementname,attributename,grafname)
end
function PSRIOGrafIntegrationList_load( ptr_pointer,ptrStudy, path )
  return ccall((:PSRIOGrafIntegrationList_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,path)
end
function PSRIOGrafIntegrationList_loadNetplanSDDPIntegration( ptr_pointer,ptrStudy, path )
  return ccall((:PSRIOGrafIntegrationList_loadNetplanSDDPIntegration, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,path)
end
function PSRIOGrafIntegrationList_maxErrors( ptr_pointer )
  return ccall((:PSRIOGrafIntegrationList_maxErrors, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafIntegrationList_error( ptr_pointer,index )
  return ccall((:PSRIOGrafIntegrationList_error, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRIOGrafIntegrationList_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGrafIntegrationList_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOImage_create(iprt)
  ptr_pointer =  ccall((:PSRIOImage_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOImage_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOImage_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOImage_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOImage_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOImage_addFilter( ptr_pointer,className )
  return ccall((:PSRIOImage_addFilter, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,className)
end
function PSRIOImage_loadFromMemory( ptr_pointer,ptrStudy, memory, reference_path )
  return ccall((:PSRIOImage_loadFromMemory, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,memory,reference_path)
end
function PSRIOImage_deleteInstance( ptr_pointer )
  return ccall((:PSRIOImage_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_create(iprt)
  ptr_pointer =  ccall((:PSRIOMask_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOMask_type( ptr_pointer )
  return ccall((:PSRIOMask_type, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_id( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMask_id, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMask_setId( ptr_pointer,__id )
  return ccall((:PSRIOMask_setId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,__id)
end
function PSRIOMask_createAutomaticConfiguration( ptr_pointer )
  return ccall((:PSRIOMask_createAutomaticConfiguration, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_getAutomaticConfiguration( ptr_pointer )
  return ccall((:PSRIOMask_getAutomaticConfiguration, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_sizeMaskVar( ptr_pointer )
  return ccall((:PSRIOMask_sizeMaskVar, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_maskVar( ptr_pointer,index )
  return ccall((:PSRIOMask_maskVar, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRIOMask_getMaskVar( ptr_pointer,nome )
  return ccall((:PSRIOMask_getMaskVar, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIOMask_addMaskVar( ptr_pointer,ptrMaskVar )
  return ccall((:PSRIOMask_addMaskVar, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMaskVar)
end
function PSRIOMask_getMaskVarsValue( ptr_pointer,campo, useinterpretador )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMask_getMaskVarsValue, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Bool,Ptr{UInt8},),ptr_pointer ,campo,useinterpretador,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMask_getMaskExpressionValue( ptr_pointer,expressao )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMask_getMaskExpressionValue, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,expressao,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMask_maxAutoSet( ptr_pointer )
  return ccall((:PSRIOMask_maxAutoSet, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_autoSet( ptr_pointer,index )
  return ccall((:PSRIOMask_autoSet, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRIOMask_getAutoSet( ptr_pointer,idautoset )
  return ccall((:PSRIOMask_getAutoSet, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,idautoset)
end
function PSRIOMask_addAutoSet( ptr_pointer,autoset, parmAssociado )
  return ccall((:PSRIOMask_addAutoSet, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,autoset,parmAssociado)
end
function PSRIOMask_associateAutoSet( ptr_pointer,idautoset, ptrModel, attributeMustExist )
  return ccall((:PSRIOMask_associateAutoSet, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,idautoset,ptrModel,attributeMustExist)
end
function PSRIOMask_disassociateAutoSet( ptr_pointer )
  return ccall((:PSRIOMask_disassociateAutoSet, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_resetAutoSetVector( ptr_pointer )
  return ccall((:PSRIOMask_resetAutoSetVector, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_reset( ptr_pointer )
  return ccall((:PSRIOMask_reset, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_makeDefaultModelTemplate( ptr_pointer )
  return ccall((:PSRIOMask_makeDefaultModelTemplate, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_deleteInstance( ptr_pointer )
  return ccall((:PSRIOMask_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_create(iprt)
  ptr_pointer =  ccall((:PSRIOGrafResultBase_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGrafResultBase_setSequencialModel( ptr_pointer,status )
  return ccall((:PSRIOGrafResultBase_setSequencialModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOGrafResultBase_setNameLength( ptr_pointer,maxname )
  return ccall((:PSRIOGrafResultBase_setNameLength, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,maxname)
end
function PSRIOGrafResultBase_getNameLength( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getNameLength, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_grafFormat( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_grafFormat, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_mapTo( ptr_pointer,colElements, mappedValues )
  return ccall((:PSRIOGrafResultBase_mapTo, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,colElements,pointer(mappedValues))
end
function PSRIOGrafResultBase_maxAgent( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_maxAgent, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getAgent( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOGrafResultBase_getAgent, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOGrafResultBase_addAgent( ptr_pointer,nome )
  return ccall((:PSRIOGrafResultBase_addAgent, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIOGrafResultBase_renameAgent( ptr_pointer,index, name )
  return ccall((:PSRIOGrafResultBase_renameAgent, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,index,name)
end
function PSRIOGrafResultBase_getAgentIndex( ptr_pointer,name )
  return ccall((:PSRIOGrafResultBase_getAgentIndex, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name)
end
function PSRIOGrafResultBase_clearAgents( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_clearAgents, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getData( ptr_pointer,index_agent )
  return ccall((:PSRIOGrafResultBase_getData, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIOGrafResultBase_setData( ptr_pointer,index_agent, data )
  return ccall((:PSRIOGrafResultBase_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Float64,),ptr_pointer ,index_agent,data)
end
function PSRIOGrafResultBase_getCurrentStage( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getCurrentStage, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getCurrentSerie( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getCurrentSerie, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getCurrentBlock( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getCurrentBlock, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_setCurrentStage( ptr_pointer,etapa )
  return ccall((:PSRIOGrafResultBase_setCurrentStage, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,etapa)
end
function PSRIOGrafResultBase_setCurrentSerie( ptr_pointer,serie )
  return ccall((:PSRIOGrafResultBase_setCurrentSerie, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,serie)
end
function PSRIOGrafResultBase_setCurrentBlock( ptr_pointer,patamar )
  return ccall((:PSRIOGrafResultBase_setCurrentBlock, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,patamar)
end
function PSRIOGrafResultBase_getVariableByBlock( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getVariableByBlock, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getVariableBySerie( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getVariableBySerie, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getVariableByHour( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getVariableByHour, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getAgentsNumber( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getAgentsNumber, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getUnit( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOGrafResultBase_getUnit, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOGrafResultBase_getStageType( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getStageType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getInitialStage( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getInitialStage, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getInitialYear( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getInitialYear, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getRelativeInitialStage( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getRelativeInitialStage, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getTotalStages( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getTotalStages, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getTotalSeries( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getTotalSeries, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getTotalBlocks( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getTotalBlocks, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getTotalBlocks2( ptr_pointer,stage )
  return ccall((:PSRIOGrafResultBase_getTotalBlocks2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,stage)
end
function PSRIOGrafResultBase_getStageDate( ptr_pointer,stage )
  return ccall((:PSRIOGrafResultBase_getStageDate, "PSRClasses"),Int64,(Ptr{UInt8}, Int32,),ptr_pointer ,stage)
end
function PSRIOGrafResultBase_setVariableByBlock( ptr_pointer,_varia_por_patamar )
  return ccall((:PSRIOGrafResultBase_setVariableByBlock, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_varia_por_patamar)
end
function PSRIOGrafResultBase_setVariableBySerie( ptr_pointer,_varia_por_serie )
  return ccall((:PSRIOGrafResultBase_setVariableBySerie, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_varia_por_serie)
end
function PSRIOGrafResultBase_setVariableByHour( ptr_pointer,_varia_por_hora )
  return ccall((:PSRIOGrafResultBase_setVariableByHour, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_varia_por_hora)
end
function PSRIOGrafResultBase_setAgentsNumber( ptr_pointer,_numero_agentes )
  return ccall((:PSRIOGrafResultBase_setAgentsNumber, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_numero_agentes)
end
function PSRIOGrafResultBase_setUnit( ptr_pointer,_unidade )
  return ccall((:PSRIOGrafResultBase_setUnit, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_unidade)
end
function PSRIOGrafResultBase_setStageType( ptr_pointer,_tipo_estagio )
  return ccall((:PSRIOGrafResultBase_setStageType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_tipo_estagio)
end
function PSRIOGrafResultBase_setInitialStage( ptr_pointer,_estagio_inicial )
  return ccall((:PSRIOGrafResultBase_setInitialStage, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_estagio_inicial)
end
function PSRIOGrafResultBase_setRelativeInitialStage( ptr_pointer,relative_initial_stage )
  return ccall((:PSRIOGrafResultBase_setRelativeInitialStage, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,relative_initial_stage)
end
function PSRIOGrafResultBase_setInitialYear( ptr_pointer,_anoinicial )
  return ccall((:PSRIOGrafResultBase_setInitialYear, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_anoinicial)
end
function PSRIOGrafResultBase_setTotalSeries( ptr_pointer,_total_series )
  return ccall((:PSRIOGrafResultBase_setTotalSeries, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_total_series)
end
function PSRIOGrafResultBase_setTotalBlocks( ptr_pointer,_total_patamares )
  return ccall((:PSRIOGrafResultBase_setTotalBlocks, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_total_patamares)
end
function PSRIOGrafResultBase_setTotalStages( ptr_pointer,_total_etapas )
  return ccall((:PSRIOGrafResultBase_setTotalStages, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_total_etapas)
end
function PSRIOGrafResultBase_useHourlyDurationFrom( ptr_pointer,ptrStudy )
  return ccall((:PSRIOGrafResultBase_useHourlyDurationFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIOGrafResultBase_generateDefautHourInformation( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_generateDefautHourInformation, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_nextRegistry( ptr_pointer,ignore_data )
  return ccall((:PSRIOGrafResultBase_nextRegistry, "PSRClasses"),Int32,(Ptr{UInt8}, Bool,),ptr_pointer ,ignore_data)
end
function PSRIOGrafResultBase_getLastRegistry( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_getLastRegistry, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_aggregate( ptr_pointer,serieAggregationType, aggregateBlock )
  return ccall((:PSRIOGrafResultBase_aggregate, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Bool,),ptr_pointer ,serieAggregationType,aggregateBlock)
end
function PSRIOGrafResultBase_seekStage( ptr_pointer,numetapa )
  return ccall((:PSRIOGrafResultBase_seekStage, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,numetapa)
end
function PSRIOGrafResultBase_seekStage2( ptr_pointer,numetapa, numserie, numblock )
  return ccall((:PSRIOGrafResultBase_seekStage2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,numetapa,numserie,numblock)
end
function PSRIOGrafResultBase_closeLoad( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_closeLoad, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_closeSave( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_closeSave, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_writeRegistry( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_writeRegistry, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBase_getWeight( ptr_pointer,block )
  return ccall((:PSRIOGrafResultBase_getWeight, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,block)
end
function PSRIOGrafResultBase_setWeight( ptr_pointer,block, weight )
  return ccall((:PSRIOGrafResultBase_setWeight, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Float64,),ptr_pointer ,block,weight)
end
function PSRIOGrafResultBase_useHeaderInformation( ptr_pointer,referenceResult )
  return ccall((:PSRIOGrafResultBase_useHeaderInformation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,referenceResult)
end
function PSRIOGrafResultBase_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGrafResultBase_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPInflowForecast_B_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPInflowForecast_B_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPInflowForecast_B_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPInflowForecast_B_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPInflowForecast_B_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPInflowForecast_B_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPHydroPlantWaveFactor_load( ptr_pointer,ptrStudy, filename, fieldname, travelTimeFieldName )
  return ccall((:PSRIONCPHydroPlantWaveFactor_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename,fieldname,travelTimeFieldName)
end
function PSRIONCPHydroPlantWaveFactor_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPHydroPlantWaveFactor_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantWaveFactor_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPHydroPlantWaveFactor_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOMask_AUTOSET_create(iprt,_elementoid, _ptrIOMask)
  ptr_pointer =  ccall((:PSRIOMask_AUTOSET_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL,_elementoid,_ptrIOMask)
  return ptr_pointer
end
function PSRIOMask_AUTOSET_getModelId( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMask_AUTOSET_getModelId, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMask_AUTOSET_getAtributoId( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMask_AUTOSET_getAtributoId, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMask_AUTOSET_setAttributeMustExist( ptr_pointer,flag )
  return ccall((:PSRIOMask_AUTOSET_setAttributeMustExist, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOMask_AUTOSET_getAttributeMustExist( ptr_pointer )
  return ccall((:PSRIOMask_AUTOSET_getAttributeMustExist, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_AUTOSET_setModel( ptr_pointer,_ptrModel )
  return ccall((:PSRIOMask_AUTOSET_setModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrModel)
end
function PSRIOMask_AUTOSET_setParm( ptr_pointer,_ptrParm )
  return ccall((:PSRIOMask_AUTOSET_setParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrParm)
end
function PSRIOMask_AUTOSET_setDimensionExpression( ptr_pointer,_dimensionExpression )
  return ccall((:PSRIOMask_AUTOSET_setDimensionExpression, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_dimensionExpression)
end
function PSRIOMask_AUTOSET_addSubModelId( ptr_pointer,_submodelid )
  return ccall((:PSRIOMask_AUTOSET_addSubModelId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_submodelid)
end
function PSRIOMask_AUTOSET_get( ptr_pointer )
  return ccall((:PSRIOMask_AUTOSET_get, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_AUTOSET_put( ptr_pointer )
  return ccall((:PSRIOMask_AUTOSET_put, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_AUTOSET_adjustIndexVector( ptr_pointer )
  return ccall((:PSRIOMask_AUTOSET_adjustIndexVector, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_AUTOSET_existVector_NOT_EOF( ptr_pointer )
  return ccall((:PSRIOMask_AUTOSET_existVector_NOT_EOF, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_AUTOSET_setVectorPos( ptr_pointer,position )
  return ccall((:PSRIOMask_AUTOSET_setVectorPos, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,position)
end
function PSRIOMask_AUTOSET_deleteInstance( ptr_pointer )
  return ccall((:PSRIOMask_AUTOSET_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_AgentRule_Area_create(iprt,_ptrStudy)
  ptr_pointer =  ccall((:PSRIO_HEADEREDBIN_AgentRule_Area_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_ptrStudy)
  return ptr_pointer
end
function PSRIO_HEADEREDBIN_AgentRule_Area_getCode( ptr_pointer,name_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Area_getCode, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Area_getCode2( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Area_getCode2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Area_getAgentsNumber( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Area_getAgentsNumber, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_AgentRule_Area_getAgentElement( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Area_getAgentElement, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Area_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Area_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_AgentRule_Battery_create(iprt,_ptrStudy)
  ptr_pointer =  ccall((:PSRIO_HEADEREDBIN_AgentRule_Battery_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_ptrStudy)
  return ptr_pointer
end
function PSRIO_HEADEREDBIN_AgentRule_Battery_getCode( ptr_pointer,name_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Battery_getCode, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Battery_getCode2( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Battery_getCode2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Battery_getAgentsNumber( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Battery_getAgentsNumber, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_AgentRule_Battery_updateScenary( ptr_pointer,stageDate, simulation, block )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Battery_updateScenary, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Int32, Int32,),ptr_pointer ,stageDate,simulation,block)
end
function PSRIO_HEADEREDBIN_AgentRule_Battery_getAgentFactor( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Battery_getAgentFactor, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Battery_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Battery_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalNetwork_create(iprt)
  ptr_pointer =  ccall((:PSRHydrologicalNetwork_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRHydrologicalNetwork_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRHydrologicalNetwork_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRHydrologicalNetwork_classType( ptr_pointer )
  return ccall((:PSRHydrologicalNetwork_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalNetwork_isClassType( ptr_pointer,class_type )
  return ccall((:PSRHydrologicalNetwork_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRHydrologicalNetwork_graph( ptr_pointer )
  return ccall((:PSRHydrologicalNetwork_graph, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalNetwork_addGaugingStation( ptr_pointer,ptrPosto )
  return ccall((:PSRHydrologicalNetwork_addGaugingStation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrPosto)
end
function PSRHydrologicalNetwork_addConnection( ptr_pointer,ptrPosto01, ptrPosto02, ptrConexao )
  return ccall((:PSRHydrologicalNetwork_addConnection, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrPosto01,ptrPosto02,ptrConexao)
end
function PSRHydrologicalNetwork_maxGaugingStation( ptr_pointer )
  return ccall((:PSRHydrologicalNetwork_maxGaugingStation, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalNetwork_gaugingStation( ptr_pointer,index )
  return ccall((:PSRHydrologicalNetwork_gaugingStation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRHydrologicalNetwork_getGaugingStation( ptr_pointer,codigo )
  return ccall((:PSRHydrologicalNetwork_getGaugingStation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codigo)
end
function PSRHydrologicalNetwork_getGaugingStation2( ptr_pointer,nome )
  return ccall((:PSRHydrologicalNetwork_getGaugingStation2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRHydrologicalNetwork_maxConnectionIn( ptr_pointer,ptrPosto )
  return ccall((:PSRHydrologicalNetwork_maxConnectionIn, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrPosto)
end
function PSRHydrologicalNetwork_connectionIn( ptr_pointer,ptrPosto, index )
  return ccall((:PSRHydrologicalNetwork_connectionIn, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrPosto,index)
end
function PSRHydrologicalNetwork_maxConnectionOut( ptr_pointer,ptrPosto )
  return ccall((:PSRHydrologicalNetwork_maxConnectionOut, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrPosto)
end
function PSRHydrologicalNetwork_connectionOut( ptr_pointer,ptrPosto, index )
  return ccall((:PSRHydrologicalNetwork_connectionOut, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrPosto,index)
end
function PSRHydrologicalNetwork_swapToEnd( ptr_pointer,ptrPosto )
  return ccall((:PSRHydrologicalNetwork_swapToEnd, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrPosto)
end
function PSRHydrologicalNetwork_useIncrementalInflow( ptr_pointer )
  return ccall((:PSRHydrologicalNetwork_useIncrementalInflow, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalNetwork_useNaturalInflow( ptr_pointer )
  return ccall((:PSRHydrologicalNetwork_useNaturalInflow, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalNetwork_useIncrementalInflow2( ptr_pointer,vector_id )
  return ccall((:PSRHydrologicalNetwork_useIncrementalInflow2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,vector_id)
end
function PSRHydrologicalNetwork_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRHydrologicalNetwork_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRHydrologicalNetwork_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRHydrologicalNetwork_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRHydrologicalNetwork_configureGraphFrom( ptr_pointer,ptrDataGraph )
  return ccall((:PSRHydrologicalNetwork_configureGraphFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataGraph)
end
function PSRHydrologicalNetwork_deleteInstance( ptr_pointer )
  return ccall((:PSRHydrologicalNetwork_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRDevice_create(iprt)
  ptr_pointer =  ccall((:PSRDevice_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRDevice_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRDevice_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRDevice_classType( ptr_pointer )
  return ccall((:PSRDevice_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRDevice_isClassType( ptr_pointer,class_type )
  return ccall((:PSRDevice_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRDevice_deviceType( ptr_pointer )
  return ccall((:PSRDevice_deviceType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRDevice_code( ptr_pointer )
  return ccall((:PSRDevice_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRDevice_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRDevice_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRDevice_setCode( ptr_pointer,_codigo )
  return ccall((:PSRDevice_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRDevice_setName( ptr_pointer,_nome )
  return ccall((:PSRDevice_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRDevice_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRDevice_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRDevice_serialize( ptr_pointer )
  return ccall((:PSRDevice_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRDevice_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRDevice_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRDevice_getGenericIdentification( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRDevice_getGenericIdentification, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRDevice_deleteInstance( ptr_pointer )
  return ccall((:PSRDevice_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOTSLHistoricalTemperature_deleteInstance( ptr_pointer )
  return ccall((:PSRIOTSLHistoricalTemperature_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOTSLHistoricalTemperature_create(iprt)
  ptr_pointer =  ccall((:PSRIOTSLHistoricalTemperature_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRHydrologicalPlantNetwork_create(iprt)
  ptr_pointer =  ccall((:PSRHydrologicalPlantNetwork_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRHydrologicalPlantNetwork_graph( ptr_pointer )
  return ccall((:PSRHydrologicalPlantNetwork_graph, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalPlantNetwork_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRHydrologicalPlantNetwork_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRHydrologicalPlantNetwork_classType( ptr_pointer )
  return ccall((:PSRHydrologicalPlantNetwork_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalPlantNetwork_isClassType( ptr_pointer,class_type )
  return ccall((:PSRHydrologicalPlantNetwork_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRHydrologicalPlantNetwork_maxPlant( ptr_pointer )
  return ccall((:PSRHydrologicalPlantNetwork_maxPlant, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalPlantNetwork_plant( ptr_pointer,index )
  return ccall((:PSRHydrologicalPlantNetwork_plant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRHydrologicalPlantNetwork_addPlant( ptr_pointer,ptrUsina )
  return ccall((:PSRHydrologicalPlantNetwork_addPlant, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrUsina)
end
function PSRHydrologicalPlantNetwork_addConnection( ptr_pointer,ptrUsina01, ptrUsina02, ptrConexao )
  return ccall((:PSRHydrologicalPlantNetwork_addConnection, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrUsina01,ptrUsina02,ptrConexao)
end
function PSRHydrologicalPlantNetwork_maxConnectionIn( ptr_pointer,ptrUsina )
  return ccall((:PSRHydrologicalPlantNetwork_maxConnectionIn, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrUsina)
end
function PSRHydrologicalPlantNetwork_connectionIn( ptr_pointer,ptrUsina, index )
  return ccall((:PSRHydrologicalPlantNetwork_connectionIn, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrUsina,index)
end
function PSRHydrologicalPlantNetwork_maxConnectionOut( ptr_pointer,ptrUsina )
  return ccall((:PSRHydrologicalPlantNetwork_maxConnectionOut, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrUsina)
end
function PSRHydrologicalPlantNetwork_connectionOut( ptr_pointer,ptrUsina, index )
  return ccall((:PSRHydrologicalPlantNetwork_connectionOut, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrUsina,index)
end
function PSRHydrologicalPlantNetwork_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRHydrologicalPlantNetwork_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRHydrologicalPlantNetwork_configureGraphFrom( ptr_pointer,ptrDataGraph )
  return ccall((:PSRHydrologicalPlantNetwork_configureGraphFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataGraph)
end
function PSRHydrologicalPlantNetwork_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRHydrologicalPlantNetwork_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRHydrologicalPlantNetwork_deleteInstance( ptr_pointer )
  return ccall((:PSRHydrologicalPlantNetwork_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPowerInjectionHourlyScenarios_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPPowerInjectionHourlyScenarios_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPPowerInjectionHourlyScenarios_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPPowerInjectionHourlyScenarios_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_TSDATA_create(iprt)
  ptr_pointer =  ccall((:PSRIOMask_TSDATA_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOMask_TSDATA_sizeParm( ptr_pointer )
  return ccall((:PSRIOMask_TSDATA_sizeParm, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_TSDATA_addParm( ptr_pointer,parm )
  return ccall((:PSRIOMask_TSDATA_addParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,parm)
end
function PSRIOMask_TSDATA_getParm( ptr_pointer,id )
  return ccall((:PSRIOMask_TSDATA_getParm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRIOMask_TSDATA_parm( ptr_pointer,ndx )
  return ccall((:PSRIOMask_TSDATA_parm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRIOMask_TSDATA_deleteInstance( ptr_pointer )
  return ccall((:PSRIOMask_TSDATA_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_ROWDATA_create(iprt)
  ptr_pointer =  ccall((:PSRIOMask_ROWDATA_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOMask_ROWDATA_sizeHeader( ptr_pointer )
  return ccall((:PSRIOMask_ROWDATA_sizeHeader, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_ROWDATA_addHeader( ptr_pointer,header )
  return ccall((:PSRIOMask_ROWDATA_addHeader, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,header)
end
function PSRIOMask_ROWDATA_header( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMask_ROWDATA_header, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMask_ROWDATA_sizeParm( ptr_pointer )
  return ccall((:PSRIOMask_ROWDATA_sizeParm, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_ROWDATA_addColFormat( ptr_pointer,col_format )
  return ccall((:PSRIOMask_ROWDATA_addColFormat, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,col_format)
end
function PSRIOMask_ROWDATA_addParm( ptr_pointer,parm )
  return ccall((:PSRIOMask_ROWDATA_addParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,parm)
end
function PSRIOMask_ROWDATA_getParm( ptr_pointer,id )
  return ccall((:PSRIOMask_ROWDATA_getParm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRIOMask_ROWDATA_parm( ptr_pointer,ndx )
  return ccall((:PSRIOMask_ROWDATA_parm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRIOMask_ROWDATA_colFormat( ptr_pointer,ndx )
  return ccall((:PSRIOMask_ROWDATA_colFormat, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRIOMask_ROWDATA_orderData( ptr_pointer )
  return ccall((:PSRIOMask_ROWDATA_orderData, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_ROWDATA_reset( ptr_pointer )
  return ccall((:PSRIOMask_ROWDATA_reset, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_ROWDATA_makeDefaultModelTemplate( ptr_pointer )
  return ccall((:PSRIOMask_ROWDATA_makeDefaultModelTemplate, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_ROWDATA_deleteInstance( ptr_pointer )
  return ccall((:PSRIOMask_ROWDATA_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalConnection_create(iprt)
  ptr_pointer =  ccall((:PSRHydrologicalConnection_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRHydrologicalConnection_classType( ptr_pointer )
  return ccall((:PSRHydrologicalConnection_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalConnection_gaugingStation( ptr_pointer,index )
  return ccall((:PSRHydrologicalConnection_gaugingStation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRHydrologicalConnection_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRHydrologicalConnection_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRHydrologicalConnection_deleteInstance( ptr_pointer )
  return ccall((:PSRHydrologicalConnection_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPReserveGenerationHourlyScenarios_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPReserveGenerationHourlyScenarios_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPReserveGenerationHourlyScenarios_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPReserveGenerationHourlyScenarios_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_MIXROWDATA_create(iprt)
  ptr_pointer =  ccall((:PSRIOMask_MIXROWDATA_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOMask_MIXROWDATA_setCommentIdent( ptr_pointer,_comentario_ident )
  return ccall((:PSRIOMask_MIXROWDATA_setCommentIdent, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_comentario_ident)
end
function PSRIOMask_MIXROWDATA_getCommentIdent( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMask_MIXROWDATA_getCommentIdent, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMask_MIXROWDATA_totalBlocks( ptr_pointer )
  return ccall((:PSRIOMask_MIXROWDATA_totalBlocks, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_MIXROWDATA_setTotalBlocks( ptr_pointer,total )
  return ccall((:PSRIOMask_MIXROWDATA_setTotalBlocks, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,total)
end
function PSRIOMask_MIXROWDATA_setBlockIdent( ptr_pointer,blc, ident_string )
  return ccall((:PSRIOMask_MIXROWDATA_setBlockIdent, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,blc,ident_string)
end
function PSRIOMask_MIXROWDATA_getBlockIdent( ptr_pointer,blc )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMask_MIXROWDATA_getBlockIdent, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,blc,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMask_MIXROWDATA_setBlockLength( ptr_pointer,blc, length_blc )
  return ccall((:PSRIOMask_MIXROWDATA_setBlockLength, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,blc,length_blc)
end
function PSRIOMask_MIXROWDATA_getBlockLength( ptr_pointer,blc )
  return ccall((:PSRIOMask_MIXROWDATA_getBlockLength, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,blc)
end
function PSRIOMask_MIXROWDATA_setBlockNext( ptr_pointer,blc, next_blc )
  return ccall((:PSRIOMask_MIXROWDATA_setBlockNext, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,blc,next_blc)
end
function PSRIOMask_MIXROWDATA_getBlockNext( ptr_pointer,blc )
  return ccall((:PSRIOMask_MIXROWDATA_getBlockNext, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,blc)
end
function PSRIOMask_MIXROWDATA_setBlockIdentEnd( ptr_pointer,blc, ident_string )
  return ccall((:PSRIOMask_MIXROWDATA_setBlockIdentEnd, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,blc,ident_string)
end
function PSRIOMask_MIXROWDATA_getBlockIdentEnd( ptr_pointer,blc )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMask_MIXROWDATA_getBlockIdentEnd, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,blc,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMask_MIXROWDATA_sizeHeader( ptr_pointer,blc )
  return ccall((:PSRIOMask_MIXROWDATA_sizeHeader, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,blc)
end
function PSRIOMask_MIXROWDATA_addHeader( ptr_pointer,blc, header )
  return ccall((:PSRIOMask_MIXROWDATA_addHeader, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,blc,header)
end
function PSRIOMask_MIXROWDATA_header( ptr_pointer,blc, ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOMask_MIXROWDATA_header, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,Ptr{UInt8},),ptr_pointer ,blc,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOMask_MIXROWDATA_sizeParm( ptr_pointer,blc )
  return ccall((:PSRIOMask_MIXROWDATA_sizeParm, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,blc)
end
function PSRIOMask_MIXROWDATA_addColFormat( ptr_pointer,blc, col_format )
  return ccall((:PSRIOMask_MIXROWDATA_addColFormat, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,blc,col_format)
end
function PSRIOMask_MIXROWDATA_addParm( ptr_pointer,blc, parm )
  return ccall((:PSRIOMask_MIXROWDATA_addParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,blc,parm)
end
function PSRIOMask_MIXROWDATA_getParm( ptr_pointer,blc, id )
  return ccall((:PSRIOMask_MIXROWDATA_getParm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,blc,id)
end
function PSRIOMask_MIXROWDATA_parm( ptr_pointer,blc, ndx )
  return ccall((:PSRIOMask_MIXROWDATA_parm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,blc,ndx)
end
function PSRIOMask_MIXROWDATA_colFormat( ptr_pointer,blc, ndx )
  return ccall((:PSRIOMask_MIXROWDATA_colFormat, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,blc,ndx)
end
function PSRIOMask_MIXROWDATA_orderData( ptr_pointer )
  return ccall((:PSRIOMask_MIXROWDATA_orderData, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_MIXROWDATA_reset( ptr_pointer )
  return ccall((:PSRIOMask_MIXROWDATA_reset, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_MIXROWDATA_makeDefaultModelTemplate( ptr_pointer )
  return ccall((:PSRIOMask_MIXROWDATA_makeDefaultModelTemplate, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOMask_MIXROWDATA_deleteInstance( ptr_pointer )
  return ccall((:PSRIOMask_MIXROWDATA_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRDemandSegment_create(iprt)
  ptr_pointer =  ccall((:PSRDemandSegment_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRDemandSegment_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRDemandSegment_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRDemandSegment_classType( ptr_pointer )
  return ccall((:PSRDemandSegment_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRDemandSegment_isClassType( ptr_pointer,class_type )
  return ccall((:PSRDemandSegment_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRDemandSegment_numberOfLevel( ptr_pointer )
  return ccall((:PSRDemandSegment_numberOfLevel, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRDemandSegment_demand( ptr_pointer )
  return ccall((:PSRDemandSegment_demand, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRDemandSegment_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRDemandSegment_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRDemandSegment_serialize( ptr_pointer )
  return ccall((:PSRDemandSegment_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRDemandSegment_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRDemandSegment_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRDemandSegment_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRDemandSegment_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRDemandSegment_deleteInstance( ptr_pointer )
  return ccall((:PSRDemandSegment_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPowerInjectionPriceHourlyScenarios_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPPowerInjectionPriceHourlyScenarios_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPPowerInjectionPriceHourlyScenarios_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPPowerInjectionPriceHourlyScenarios_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuel_create(iprt)
  ptr_pointer =  ccall((:PSRFuel_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRFuel_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRFuel_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRFuel_classType( ptr_pointer )
  return ccall((:PSRFuel_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuel_isClassType( ptr_pointer,class_type )
  return ccall((:PSRFuel_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRFuel_code( ptr_pointer )
  return ccall((:PSRFuel_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuel_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRFuel_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRFuel_setCode( ptr_pointer,_codigo )
  return ccall((:PSRFuel_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRFuel_setName( ptr_pointer,_nome )
  return ccall((:PSRFuel_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRFuel_system( ptr_pointer )
  return ccall((:PSRFuel_system, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRFuel_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRFuel_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRFuel_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRFuel_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRFuel_serialize( ptr_pointer )
  return ccall((:PSRFuel_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRFuel_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRFuel_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRFuel_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRFuel_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRFuel_deleteInstance( ptr_pointer )
  return ccall((:PSRFuel_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataParmDimension_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataParmDimension_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataParmDimension_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataParmDimension_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionTimeController_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionTimeController_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExpansionTimeController_configureFrom( ptr_pointer,ptrStudy )
  return ccall((:PSRExpansionTimeController_configureFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRExpansionTimeController_getNumberSeasons( ptr_pointer )
  return ccall((:PSRExpansionTimeController_getNumberSeasons, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionTimeController_getNumberTypicalDays( ptr_pointer )
  return ccall((:PSRExpansionTimeController_getNumberTypicalDays, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionTimeController_gotoTypicalDayHour( ptr_pointer,typical_day, hour )
  return ccall((:PSRExpansionTimeController_gotoTypicalDayHour, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,typical_day,hour)
end
function PSRExpansionTimeController_getCurrentSeason( ptr_pointer )
  return ccall((:PSRExpansionTimeController_getCurrentSeason, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionTimeController_getNumberAssociatedStages( ptr_pointer,year, season )
  return ccall((:PSRExpansionTimeController_getNumberAssociatedStages, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,year,season)
end
function PSRExpansionTimeController_getAssociatedStages( ptr_pointer,year, season, seasons )
  return ccall((:PSRExpansionTimeController_getAssociatedStages, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8},),ptr_pointer ,year,season,pointer(seasons))
end
function PSRExpansionTimeController_getNumberAssociatedDays( ptr_pointer,typical_day )
  return ccall((:PSRExpansionTimeController_getNumberAssociatedDays, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,typical_day)
end
function PSRExpansionTimeController_getAssociatedDays( ptr_pointer,typical_day, days )
  return ccall((:PSRExpansionTimeController_getAssociatedDays, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,typical_day,pointer(days))
end
function PSRExpansionTimeController_expansionBlock( ptr_pointer )
  return ccall((:PSRExpansionTimeController_expansionBlock, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionTimeController_commitBlock( ptr_pointer )
  return ccall((:PSRExpansionTimeController_commitBlock, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionTimeController_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionTimeController_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataReference_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataReference_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataReference_setObject( ptr_pointer,_object )
  return ccall((:PSRMessageDataReference_setObject, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,pointer(_object))
end
function PSRMessageDataReference_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataReference_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenRollingHorizon_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenRollingHorizon_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenRollingHorizon_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenRollingHorizon_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenRollingHorizon_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenRollingHorizon_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataModel_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataModel_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataModel_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataModel_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLogTextFile_create(iprt,nomearquivo)
  ptr_pointer =  ccall((:PSRLogTextFile_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,nomearquivo)
  return ptr_pointer
end
function PSRLogTextFile_warning( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogTextFile_warning, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogTextFile_error( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogTextFile_error, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogTextFile_info( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogTextFile_info, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogTextFile_deleteInstance( ptr_pointer )
  return ccall((:PSRLogTextFile_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReal_create(iprt,id)
  ptr_pointer =  ccall((:PSRVectorReal_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,id)
  return ptr_pointer
end
function PSRVectorReal_add( ptr_pointer )
  return ccall((:PSRVectorReal_add, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReal_add2( ptr_pointer,data )
  return ccall((:PSRVectorReal_add2, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,data)
end
function PSRVectorReal_addIndexed( ptr_pointer,date, data )
  return ccall((:PSRVectorReal_addIndexed, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Float64,),ptr_pointer ,date,data)
end
function PSRVectorReal_clear( ptr_pointer )
  return ccall((:PSRVectorReal_clear, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReal_remove( ptr_pointer,index )
  return ccall((:PSRVectorReal_remove, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorReal_resize( ptr_pointer,NewSize )
  return ccall((:PSRVectorReal_resize, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,NewSize)
end
function PSRVectorReal_reserve( ptr_pointer,reserveSize )
  return ccall((:PSRVectorReal_reserve, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,reserveSize)
end
function PSRVectorReal_insert( ptr_pointer,ndx )
  return ccall((:PSRVectorReal_insert, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorReal_size( ptr_pointer )
  return ccall((:PSRVectorReal_size, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReal_get( ptr_pointer,ndx )
  return ccall((:PSRVectorReal_get, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorReal_getReal( ptr_pointer,ndx )
  return ccall((:PSRVectorReal_getReal, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorReal_getCurrent( ptr_pointer )
  return ccall((:PSRVectorReal_getCurrent, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReal_getCurrentReal( ptr_pointer )
  return ccall((:PSRVectorReal_getCurrentReal, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReal_getLast( ptr_pointer )
  return ccall((:PSRVectorReal_getLast, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReal_set( ptr_pointer,ndx, data )
  return ccall((:PSRVectorReal_set, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Float64,),ptr_pointer ,ndx,data)
end
function PSRVectorReal_setCurrent( ptr_pointer,data )
  return ccall((:PSRVectorReal_setCurrent, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,data)
end
function PSRVectorReal_setLast( ptr_pointer,data )
  return ccall((:PSRVectorReal_setLast, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,data)
end
function PSRVectorReal_getPointer( ptr_pointer,ndx )
  return ccall((:PSRVectorReal_getPointer, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorReal_toString( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorReal_toString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorReal_setDataFromString( ptr_pointer,ndx, data )
  return ccall((:PSRVectorReal_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ndx,data)
end
function PSRVectorReal_copyTo( ptr_pointer,ptrTarget )
  return ccall((:PSRVectorReal_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrTarget)
end
function PSRVectorReal_copyTo2( ptr_pointer,data, initialIndex )
  return ccall((:PSRVectorReal_copyTo2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{Float64}, Int32,),ptr_pointer ,data,initialIndex)
end
function PSRVectorReal_copyTo3( ptr_pointer,ptrTarget, fromPosition, toPosition )
  return ccall((:PSRVectorReal_copyTo3, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),ptr_pointer ,ptrTarget,fromPosition,toPosition)
end
function PSRVectorReal_duplicateValue( ptr_pointer,fromPosition, toPosition )
  return ccall((:PSRVectorReal_duplicateValue, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,fromPosition,toPosition)
end
function PSRVectorReal_clone( ptr_pointer )
  return ccall((:PSRVectorReal_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReal_serialize( ptr_pointer )
  return ccall((:PSRVectorReal_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReal_buildFrom( ptr_pointer,ptrMessageDataVector )
  return ccall((:PSRVectorReal_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataVector)
end
function PSRVectorReal_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorReal_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryInterface_create(iprt)
  ptr_pointer =  ccall((:PSRVectorSDDPBinaryInterface_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRVectorSDDPBinaryInterface_setGrafResultBinary( ptr_pointer,ptrIOGrafResultBinary )
  return ccall((:PSRVectorSDDPBinaryInterface_setGrafResultBinary, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrIOGrafResultBinary)
end
function PSRVectorSDDPBinaryInterface_setGrafResultIndex( ptr_pointer,index )
  return ccall((:PSRVectorSDDPBinaryInterface_setGrafResultIndex, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorSDDPBinaryInterface_getGrafResultIndex( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryInterface_getGrafResultIndex, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryInterface_setModeSubtractNextFunction( ptr_pointer,status )
  return ccall((:PSRVectorSDDPBinaryInterface_setModeSubtractNextFunction, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRVectorSDDPBinaryInterface_isHourly( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryInterface_isHourly, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryInterface_setFixedGain( ptr_pointer,gain )
  return ccall((:PSRVectorSDDPBinaryInterface_setFixedGain, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,gain)
end
function PSRVectorSDDPBinaryInterface_getCurrent( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryInterface_getCurrent, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryInterface_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryInterface_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorString_create(iprt,id)
  ptr_pointer =  ccall((:PSRVectorString_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,id)
  return ptr_pointer
end
function PSRVectorString_add( ptr_pointer )
  return ccall((:PSRVectorString_add, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorString_add2( ptr_pointer,data )
  return ccall((:PSRVectorString_add2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRVectorString_addIndexed( ptr_pointer,date, data )
  return ccall((:PSRVectorString_addIndexed, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Ptr{UInt8},),ptr_pointer ,date,data)
end
function PSRVectorString_clear( ptr_pointer )
  return ccall((:PSRVectorString_clear, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorString_remove( ptr_pointer,index )
  return ccall((:PSRVectorString_remove, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorString_resize( ptr_pointer,NewSize )
  return ccall((:PSRVectorString_resize, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,NewSize)
end
function PSRVectorString_reserve( ptr_pointer,reserveSize )
  return ccall((:PSRVectorString_reserve, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,reserveSize)
end
function PSRVectorString_size( ptr_pointer )
  return ccall((:PSRVectorString_size, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorString_get( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorString_get, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorString_getString( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorString_getString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorString_getCurrent( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorString_getCurrent, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorString_getCurrentString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorString_getCurrentString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorString_set( ptr_pointer,ndx, data )
  return ccall((:PSRVectorString_set, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ndx,data)
end
function PSRVectorString_insert( ptr_pointer,ndx )
  return ccall((:PSRVectorString_insert, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorString_indexOf( ptr_pointer,data )
  return ccall((:PSRVectorString_indexOf, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRVectorString_indexOf2( ptr_pointer,data, trim )
  return ccall((:PSRVectorString_indexOf2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,data,trim)
end
function PSRVectorString_setCurrent( ptr_pointer,data )
  return ccall((:PSRVectorString_setCurrent, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRVectorString_swap( ptr_pointer,src_ndx, dst_ndx )
  return ccall((:PSRVectorString_swap, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,src_ndx,dst_ndx)
end
function PSRVectorString_text( ptr_pointer,startpos, delimit )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorString_text, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,startpos,delimit,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorString_toString( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorString_toString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorString_setDataFromString( ptr_pointer,ndx, data )
  return ccall((:PSRVectorString_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ndx,data)
end
function PSRVectorString_copyTo( ptr_pointer,ptrTarget )
  return ccall((:PSRVectorString_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrTarget)
end
function PSRVectorString_copyTo2( ptr_pointer,ptrTarget, fromPosition, toPosition )
  return ccall((:PSRVectorString_copyTo2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),ptr_pointer ,ptrTarget,fromPosition,toPosition)
end
function PSRVectorString_duplicateValue( ptr_pointer,fromPosition, toPosition )
  return ccall((:PSRVectorString_duplicateValue, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,fromPosition,toPosition)
end
function PSRVectorString_clone( ptr_pointer )
  return ccall((:PSRVectorString_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorString_serialize( ptr_pointer )
  return ccall((:PSRVectorString_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorString_buildFrom( ptr_pointer,ptrMessageDataVector )
  return ccall((:PSRVectorString_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataVector)
end
function PSRVectorString_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorString_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGaugingStation_create(iprt)
  ptr_pointer =  ccall((:PSRGaugingStation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGaugingStation_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGaugingStation_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGaugingStation_classType( ptr_pointer )
  return ccall((:PSRGaugingStation_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGaugingStation_isClassType( ptr_pointer,class_type )
  return ccall((:PSRGaugingStation_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRGaugingStation_code( ptr_pointer )
  return ccall((:PSRGaugingStation_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGaugingStation_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGaugingStation_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGaugingStation_setCode( ptr_pointer,_codigo )
  return ccall((:PSRGaugingStation_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRGaugingStation_setName( ptr_pointer,_nome )
  return ccall((:PSRGaugingStation_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRGaugingStation_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRGaugingStation_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRGaugingStation_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRGaugingStation_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGaugingStation_serialize( ptr_pointer )
  return ccall((:PSRGaugingStation_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGaugingStation_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRGaugingStation_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRGaugingStation_deleteInstance( ptr_pointer )
  return ccall((:PSRGaugingStation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalPlantConnection_create(iprt)
  ptr_pointer =  ccall((:PSRHydrologicalPlantConnection_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRHydrologicalPlantConnection_classType( ptr_pointer )
  return ccall((:PSRHydrologicalPlantConnection_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalPlantConnection_isClassType( ptr_pointer,class_type )
  return ccall((:PSRHydrologicalPlantConnection_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRHydrologicalPlantConnection_plant( ptr_pointer,index )
  return ccall((:PSRHydrologicalPlantConnection_plant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRHydrologicalPlantConnection_isTurbining( ptr_pointer )
  return ccall((:PSRHydrologicalPlantConnection_isTurbining, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalPlantConnection_isSpilling( ptr_pointer )
  return ccall((:PSRHydrologicalPlantConnection_isSpilling, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalPlantConnection_isFiltration( ptr_pointer )
  return ccall((:PSRHydrologicalPlantConnection_isFiltration, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalPlantConnection_isStoredenergy( ptr_pointer )
  return ccall((:PSRHydrologicalPlantConnection_isStoredenergy, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalPlantConnection_isAssociatedReservoir( ptr_pointer )
  return ccall((:PSRHydrologicalPlantConnection_isAssociatedReservoir, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydrologicalPlantConnection_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRHydrologicalPlantConnection_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRHydrologicalPlantConnection_deleteInstance( ptr_pointer )
  return ccall((:PSRHydrologicalPlantConnection_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorInteger_create(iprt,id)
  ptr_pointer =  ccall((:PSRVectorInteger_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,id)
  return ptr_pointer
end
function PSRVectorInteger_add( ptr_pointer )
  return ccall((:PSRVectorInteger_add, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorInteger_add2( ptr_pointer,data )
  return ccall((:PSRVectorInteger_add2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,data)
end
function PSRVectorInteger_clear( ptr_pointer )
  return ccall((:PSRVectorInteger_clear, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorInteger_addIndexed( ptr_pointer,date, data )
  return ccall((:PSRVectorInteger_addIndexed, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Int32,),ptr_pointer ,date,data)
end
function PSRVectorInteger_insert( ptr_pointer,ndx )
  return ccall((:PSRVectorInteger_insert, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorInteger_remove( ptr_pointer,index )
  return ccall((:PSRVectorInteger_remove, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorInteger_resize( ptr_pointer,NewSize )
  return ccall((:PSRVectorInteger_resize, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,NewSize)
end
function PSRVectorInteger_reserve( ptr_pointer,reserveSize )
  return ccall((:PSRVectorInteger_reserve, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,reserveSize)
end
function PSRVectorInteger_size( ptr_pointer )
  return ccall((:PSRVectorInteger_size, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorInteger_get( ptr_pointer,ndx )
  return ccall((:PSRVectorInteger_get, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorInteger_getInteger( ptr_pointer,ndx )
  return ccall((:PSRVectorInteger_getInteger, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorInteger_getCurrent( ptr_pointer )
  return ccall((:PSRVectorInteger_getCurrent, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorInteger_getCurrentInteger( ptr_pointer )
  return ccall((:PSRVectorInteger_getCurrentInteger, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorInteger_setCurrent( ptr_pointer,data )
  return ccall((:PSRVectorInteger_setCurrent, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,data)
end
function PSRVectorInteger_getPointer( ptr_pointer,ndx )
  return ccall((:PSRVectorInteger_getPointer, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorInteger_set( ptr_pointer,ndx, data )
  return ccall((:PSRVectorInteger_set, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,ndx,data)
end
function PSRVectorInteger_toString( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorInteger_toString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorInteger_setDataFromString( ptr_pointer,ndx, data )
  return ccall((:PSRVectorInteger_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ndx,data)
end
function PSRVectorInteger_indexOf( ptr_pointer,data )
  return ccall((:PSRVectorInteger_indexOf, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,data)
end
function PSRVectorInteger_copyTo( ptr_pointer,ptrTarget )
  return ccall((:PSRVectorInteger_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrTarget)
end
function PSRVectorInteger_copyTo2( ptr_pointer,ptrTarget, fromPosition, toPosition )
  return ccall((:PSRVectorInteger_copyTo2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),ptr_pointer ,ptrTarget,fromPosition,toPosition)
end
function PSRVectorInteger_copyTo3( ptr_pointer,data, initialIndex )
  return ccall((:PSRVectorInteger_copyTo3, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,data,initialIndex)
end
function PSRVectorInteger_duplicateValue( ptr_pointer,fromPosition, toPosition )
  return ccall((:PSRVectorInteger_duplicateValue, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,fromPosition,toPosition)
end
function PSRVectorInteger_clone( ptr_pointer )
  return ccall((:PSRVectorInteger_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorInteger_serialize( ptr_pointer )
  return ccall((:PSRVectorInteger_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorInteger_buildFrom( ptr_pointer,ptrMessageDataVector )
  return ccall((:PSRVectorInteger_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataVector)
end
function PSRVectorInteger_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorInteger_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDate_create(iprt,id)
  ptr_pointer =  ccall((:PSRVectorDate_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,id)
  return ptr_pointer
end
function PSRVectorDate_isIntervalEndDate( ptr_pointer )
  return ccall((:PSRVectorDate_isIntervalEndDate, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDate_setIntervalEndDate( ptr_pointer,status )
  return ccall((:PSRVectorDate_setIntervalEndDate, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRVectorDate_add( ptr_pointer )
  return ccall((:PSRVectorDate_add, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDate_add2( ptr_pointer,data )
  return ccall((:PSRVectorDate_add2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64,),ptr_pointer ,data)
end
function PSRVectorDate_clear( ptr_pointer )
  return ccall((:PSRVectorDate_clear, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDate_remove( ptr_pointer,index )
  return ccall((:PSRVectorDate_remove, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorDate_resize( ptr_pointer,NewSize )
  return ccall((:PSRVectorDate_resize, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,NewSize)
end
function PSRVectorDate_reserve( ptr_pointer,reserveSize )
  return ccall((:PSRVectorDate_reserve, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,reserveSize)
end
function PSRVectorDate_insert( ptr_pointer,ndx )
  return ccall((:PSRVectorDate_insert, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorDate_size( ptr_pointer )
  return ccall((:PSRVectorDate_size, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDate_get( ptr_pointer,ndx )
  return ccall((:PSRVectorDate_get, "PSRClasses"),Int64,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorDate_getDate( ptr_pointer,ndx )
  return ccall((:PSRVectorDate_getDate, "PSRClasses"),Int64,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorDate_set( ptr_pointer,ndx, data )
  return ccall((:PSRVectorDate_set, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int64,),ptr_pointer ,ndx,data)
end
function PSRVectorDate_getIndexBefore( ptr_pointer,date )
  return ccall((:PSRVectorDate_getIndexBefore, "PSRClasses"),Int32,(Ptr{UInt8}, Int64,),ptr_pointer ,date)
end
function PSRVectorDate_getIndexAfter( ptr_pointer,date )
  return ccall((:PSRVectorDate_getIndexAfter, "PSRClasses"),Int32,(Ptr{UInt8}, Int64,),ptr_pointer ,date)
end
function PSRVectorDate_indexOf( ptr_pointer,date, acceptinterval )
  return ccall((:PSRVectorDate_indexOf, "PSRClasses"),Int32,(Ptr{UInt8}, Int64, Bool,),ptr_pointer ,date,acceptinterval)
end
function PSRVectorDate_getDay( ptr_pointer,ndx )
  return ccall((:PSRVectorDate_getDay, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorDate_getMonth( ptr_pointer,ndx )
  return ccall((:PSRVectorDate_getMonth, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorDate_getYear( ptr_pointer,ndx )
  return ccall((:PSRVectorDate_getYear, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorDate_getWeek( ptr_pointer,ndx )
  return ccall((:PSRVectorDate_getWeek, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorDate_setDate( ptr_pointer,ndx, dia, mes, ano )
  return ccall((:PSRVectorDate_setDate, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32, Int32, Int32,),ptr_pointer ,ndx,dia,mes,ano)
end
function PSRVectorDate_isRegistry( ptr_pointer,ndx )
  return ccall((:PSRVectorDate_isRegistry, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorDate_toString( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorDate_toString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorDate_setDataFromString( ptr_pointer,ndx, data )
  return ccall((:PSRVectorDate_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ndx,data)
end
function PSRVectorDate_clone( ptr_pointer )
  return ccall((:PSRVectorDate_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDate_concatenate( ptr_pointer,ptrVector )
  return ccall((:PSRVectorDate_concatenate, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrVector)
end
function PSRVectorDate_copyTo( ptr_pointer,ptrTarget )
  return ccall((:PSRVectorDate_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrTarget)
end
function PSRVectorDate_copyTo2( ptr_pointer,ptrTarget, fromPosition, toPosition )
  return ccall((:PSRVectorDate_copyTo2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),ptr_pointer ,ptrTarget,fromPosition,toPosition)
end
function PSRVectorDate_duplicateValue( ptr_pointer,fromPosition, toPosition )
  return ccall((:PSRVectorDate_duplicateValue, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,fromPosition,toPosition)
end
function PSRVectorDate_copyDateTo( ptr_pointer,day, month, year, initialIndex )
  return ccall((:PSRVectorDate_copyDateTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,day,month,year,initialIndex)
end
function PSRVectorDate_sort( ptr_pointer,sort_type, exclusive_data )
  return ccall((:PSRVectorDate_sort, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Bool,),ptr_pointer ,sort_type,exclusive_data)
end
function PSRVectorDate_createIndexRepetitions( ptr_pointer )
  return ccall((:PSRVectorDate_createIndexRepetitions, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDate_isHourly( ptr_pointer )
  return ccall((:PSRVectorDate_isHourly, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDate_matchHorizon( ptr_pointer,initialDate, finalDate, stageType )
  return ccall((:PSRVectorDate_matchHorizon, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Int64, Int32,),ptr_pointer ,initialDate,finalDate,stageType)
end
function PSRVectorDate_serialize( ptr_pointer )
  return ccall((:PSRVectorDate_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorDate_buildFrom( ptr_pointer,ptrMessageDataVector )
  return ccall((:PSRVectorDate_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataVector)
end
function PSRVectorDate_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorDate_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReference_create(iprt,id)
  ptr_pointer =  ccall((:PSRVectorReference_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,id)
  return ptr_pointer
end
function PSRVectorReference_setReferenceClassName( ptr_pointer,class_name )
  return ccall((:PSRVectorReference_setReferenceClassName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,class_name)
end
function PSRVectorReference_getReferenceClassName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorReference_getReferenceClassName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorReference_add( ptr_pointer )
  return ccall((:PSRVectorReference_add, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReference_add2( ptr_pointer,data )
  return ccall((:PSRVectorReference_add2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRVectorReference_add3( ptr_pointer,ptrElement )
  return ccall((:PSRVectorReference_add3, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRVectorReference_clear( ptr_pointer )
  return ccall((:PSRVectorReference_clear, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReference_remove( ptr_pointer,index )
  return ccall((:PSRVectorReference_remove, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorReference_resize( ptr_pointer,NewSize )
  return ccall((:PSRVectorReference_resize, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,NewSize)
end
function PSRVectorReference_reserve( ptr_pointer,reserveSize )
  return ccall((:PSRVectorReference_reserve, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,reserveSize)
end
function PSRVectorReference_size( ptr_pointer )
  return ccall((:PSRVectorReference_size, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorReference_get( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorReference_get, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorReference_getString( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorReference_getString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorReference_getReference( ptr_pointer,index )
  return ccall((:PSRVectorReference_getReference, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorReference_set( ptr_pointer,ndx, data )
  return ccall((:PSRVectorReference_set, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ndx,data)
end
function PSRVectorReference_set2( ptr_pointer,ndx, data )
  return ccall((:PSRVectorReference_set2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ndx,data)
end
function PSRVectorReference_insert( ptr_pointer,ndx )
  return ccall((:PSRVectorReference_insert, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRVectorReference_toString( ptr_pointer,ndx )
  retPtr = "                                                                                                                  "
  ccall((:PSRVectorReference_toString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,ndx,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRVectorReference_setDataFromString( ptr_pointer,ndx, data )
  return ccall((:PSRVectorReference_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ndx,data)
end
function PSRVectorReference_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorReference_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryListInterface_create(iprt)
  ptr_pointer =  ccall((:PSRVectorSDDPBinaryListInterface_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRVectorSDDPBinaryListInterface_addGrafResultBinary( ptr_pointer,ptrIOGrafResultBinary, gain )
  return ccall((:PSRVectorSDDPBinaryListInterface_addGrafResultBinary, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Float64,),ptr_pointer ,ptrIOGrafResultBinary,gain)
end
function PSRVectorSDDPBinaryListInterface_setGrafResultIndex( ptr_pointer,index )
  return ccall((:PSRVectorSDDPBinaryListInterface_setGrafResultIndex, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorSDDPBinaryListInterface_getGrafResultIndex( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryListInterface_getGrafResultIndex, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryListInterface_isHourly( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryListInterface_isHourly, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryListInterface_getCurrent( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryListInterface_getCurrent, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryListInterface_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryListInterface_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOTypicalProfilesDemand_deleteInstance( ptr_pointer )
  return ccall((:PSRIOTypicalProfilesDemand_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOTypicalProfilesDemand_create(iprt)
  ptr_pointer =  ccall((:PSRIOTypicalProfilesDemand_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOAreaBinNETPLAN_load( ptr_pointer,ptrEstudo, nomeHeader, nomeData, vectorName )
  return ccall((:PSRIOAreaBinNETPLAN_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomeHeader,nomeData,vectorName)
end
function PSRIOAreaBinNETPLAN_deleteInstance( ptr_pointer )
  return ccall((:PSRIOAreaBinNETPLAN_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOAreaBinNETPLAN_create(iprt)
  ptr_pointer =  ccall((:PSRIOAreaBinNETPLAN_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_create(iprt,_ptrStudy)
  ptr_pointer =  ccall((:PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_ptrStudy)
  return ptr_pointer
end
function PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_getCode( ptr_pointer,name_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_getCode, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_getCode2( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_getCode2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_getAgentFactor( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_getAgentFactor, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_updateScenary( ptr_pointer,stageDate, simulation, block )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_updateScenary, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Int32, Int32,),ptr_pointer ,stageDate,simulation,block)
end
function PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_LoadFromGraf_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_InDirectFile_load( ptr_pointer,nomeheader, nomeData )
  return ccall((:PSRIO_HEADEREDBIN_InDirectFile_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nomeheader,nomeData)
end
function PSRIO_HEADEREDBIN_InDirectFile_getAgentIndex( ptr_pointer,code )
  return ccall((:PSRIO_HEADEREDBIN_InDirectFile_getAgentIndex, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRIO_HEADEREDBIN_InDirectFile_getRegistry( ptr_pointer,stage, simulation, block )
  return ccall((:PSRIO_HEADEREDBIN_InDirectFile_getRegistry, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,simulation,block)
end
function PSRIO_HEADEREDBIN_InDirectFile_getData( ptr_pointer,agent_index )
  return ccall((:PSRIO_HEADEREDBIN_InDirectFile_getData, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,agent_index)
end
function PSRIO_HEADEREDBIN_InDirectFile_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_InDirectFile_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_InDirectFile_create(iprt)
  ptr_pointer =  ccall((:PSRIO_HEADEREDBIN_InDirectFile_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOCircuitoBinNETPLAN_load( ptr_pointer,ptrEstudo, idvector, nomeHeader, nomeData, deviceType )
  return ccall((:PSRIOCircuitoBinNETPLAN_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrEstudo,idvector,nomeHeader,nomeData,deviceType)
end
function PSRIOCircuitoBinNETPLAN_save( ptr_pointer,ptrEstudo, idvector, nomeHeader, nomeData, deviceType, savingModification )
  return ccall((:PSRIOCircuitoBinNETPLAN_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Bool,),ptr_pointer ,ptrEstudo,idvector,nomeHeader,nomeData,deviceType,savingModification)
end
function PSRIOCircuitoBinNETPLAN_deleteInstance( ptr_pointer )
  return ccall((:PSRIOCircuitoBinNETPLAN_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOCircuitoBinNETPLAN_create(iprt)
  ptr_pointer =  ccall((:PSRIOCircuitoBinNETPLAN_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOConverterDCACBinNetplan_create(iprt)
  ptr_pointer =  ccall((:PSRIOConverterDCACBinNetplan_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOConverterDCACBinNetplan_setConverterDeviceType( ptr_pointer,converter_device_type )
  return ccall((:PSRIOConverterDCACBinNetplan_setConverterDeviceType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,converter_device_type)
end
function PSRIOConverterDCACBinNetplan_load( ptr_pointer,ptrEstudo, idvector, nomeHeader, nomeData )
  return ccall((:PSRIOConverterDCACBinNetplan_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,idvector,nomeHeader,nomeData)
end
function PSRIOConverterDCACBinNetplan_deleteInstance( ptr_pointer )
  return ccall((:PSRIOConverterDCACBinNetplan_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOBusShuntBinNETPLAN_load( ptr_pointer,ptrEstudo, nomeHeader, nomeData )
  return ccall((:PSRIOBusShuntBinNETPLAN_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomeHeader,nomeData)
end
function PSRIOBusShuntBinNETPLAN_load2( ptr_pointer,ptrEstudo, nomeHeader, nomeData, deviceType, vectorName )
  return ccall((:PSRIOBusShuntBinNETPLAN_load2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomeHeader,nomeData,deviceType,vectorName)
end
function PSRIOBusShuntBinNETPLAN_save( ptr_pointer,ptrEstudo, nomeHeader, nomeData, deviceType, vectorName, savingModification )
  return ccall((:PSRIOBusShuntBinNETPLAN_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8}, Bool,),ptr_pointer ,ptrEstudo,nomeHeader,nomeData,deviceType,vectorName,savingModification)
end
function PSRIOBusShuntBinNETPLAN_deleteInstance( ptr_pointer )
  return ccall((:PSRIOBusShuntBinNETPLAN_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOBusShuntBinNETPLAN_create(iprt)
  ptr_pointer =  ccall((:PSRIOBusShuntBinNETPLAN_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIO_HEADEREDBIN_AgentRule_Load_create(iprt,_ptrStudy, _factor)
  ptr_pointer =  ccall((:PSRIO_HEADEREDBIN_AgentRule_Load_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Float64,),C_NULL,_ptrStudy,_factor)
  return ptr_pointer
end
function PSRIO_HEADEREDBIN_AgentRule_Load_useOldFormat( ptr_pointer,useOldFormat )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Load_useOldFormat, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,useOldFormat)
end
function PSRIO_HEADEREDBIN_AgentRule_Load_getCode( ptr_pointer,name_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Load_getCode, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Load_getCode2( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Load_getCode2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Load_getGrafAgent( ptr_pointer,index_agent )
  retPtr = "                                                                                                                  "
  ccall((:PSRIO_HEADEREDBIN_AgentRule_Load_getGrafAgent, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index_agent,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIO_HEADEREDBIN_AgentRule_Load_getAgentsNumber( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Load_getAgentsNumber, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_AgentRule_Load_getAgentElement( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Load_getAgentElement, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Load_getAgentFactor( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Load_getAgentFactor, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Load_updateScenary( ptr_pointer,stageDate, simulation, block )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Load_updateScenary, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Int32, Int32,),ptr_pointer ,stageDate,simulation,block)
end
function PSRIO_HEADEREDBIN_AgentRule_Load_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Load_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPStudy_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPStudy_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPStudy_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPStudy_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPStudy_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPStudy_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPStudy_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPStudy_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOTypicalProfilesGndPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRIOTypicalProfilesGndPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOTypicalProfilesGndPlant_create(iprt)
  ptr_pointer =  ccall((:PSRIOTypicalProfilesGndPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRWeatherStation_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRWeatherStation_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRWeatherStation_classType( ptr_pointer )
  return ccall((:PSRWeatherStation_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRWeatherStation_isClassType( ptr_pointer,class_type )
  return ccall((:PSRWeatherStation_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRWeatherStation_deleteInstance( ptr_pointer )
  return ccall((:PSRWeatherStation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRWeatherStation_create(iprt)
  ptr_pointer =  ccall((:PSRWeatherStation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRTypicalData_create(iprt)
  ptr_pointer =  ccall((:PSRTypicalData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRTypicalData_getVectorSeason( ptr_pointer )
  return ccall((:PSRTypicalData_getVectorSeason, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRTypicalData_getVectorDay( ptr_pointer )
  return ccall((:PSRTypicalData_getVectorDay, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRTypicalData_getVectorBlock( ptr_pointer )
  return ccall((:PSRTypicalData_getVectorBlock, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRTypicalData_addVector( ptr_pointer,ptrVector )
  return ccall((:PSRTypicalData_addVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrVector)
end
function PSRTypicalData_gotoTypical( ptr_pointer,season, day, block )
  return ccall((:PSRTypicalData_gotoTypical, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,season,day,block)
end
function PSRTypicalData_gotoTypical2( ptr_pointer,index )
  return ccall((:PSRTypicalData_gotoTypical2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRTypicalData_getTotalTypical( ptr_pointer )
  return ccall((:PSRTypicalData_getTotalTypical, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTypicalData_deleteInstance( ptr_pointer )
  return ccall((:PSRTypicalData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemStudyHorizon_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemStudyHorizon_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemStudyHorizon_load( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemStudyHorizon_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemStudyHorizon_save( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemStudyHorizon_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemStudyHorizon_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemStudyHorizon_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBinary_create(iprt)
  ptr_pointer =  ccall((:PSRIOGrafResultBinary_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGrafResultBinary_getFilenameHeader( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOGrafResultBinary_getFilenameHeader, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOGrafResultBinary_grafFormat( ptr_pointer )
  return ccall((:PSRIOGrafResultBinary_grafFormat, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBinary_setVersion( ptr_pointer,version )
  return ccall((:PSRIOGrafResultBinary_setVersion, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,version)
end
function PSRIOGrafResultBinary_getTotalBlocks( ptr_pointer )
  return ccall((:PSRIOGrafResultBinary_getTotalBlocks, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBinary_getTotalBlocks2( ptr_pointer,stage )
  return ccall((:PSRIOGrafResultBinary_getTotalBlocks2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,stage)
end
function PSRIOGrafResultBinary_writeRegistryData( ptr_pointer,etapa, serie, patamar, data )
  return ccall((:PSRIOGrafResultBinary_writeRegistryData, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32, Int32, Ptr{Float64},),ptr_pointer ,etapa,serie,patamar,data)
end
function PSRIOGrafResultBinary_initLoad( ptr_pointer,nameheader, namebinary )
  return ccall((:PSRIOGrafResultBinary_initLoad, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nameheader,namebinary)
end
function PSRIOGrafResultBinary_nextRegistry( ptr_pointer,ignore_data )
  return ccall((:PSRIOGrafResultBinary_nextRegistry, "PSRClasses"),Int32,(Ptr{UInt8}, Bool,),ptr_pointer ,ignore_data)
end
function PSRIOGrafResultBinary_getLastRegistry( ptr_pointer )
  return ccall((:PSRIOGrafResultBinary_getLastRegistry, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBinary_seekStage( ptr_pointer,numetapa )
  return ccall((:PSRIOGrafResultBinary_seekStage, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,numetapa)
end
function PSRIOGrafResultBinary_seekStage2( ptr_pointer,numberStage, numberScenary, numberBlock )
  return ccall((:PSRIOGrafResultBinary_seekStage2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,numberStage,numberScenary,numberBlock)
end
function PSRIOGrafResultBinary_closeLoad( ptr_pointer )
  return ccall((:PSRIOGrafResultBinary_closeLoad, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBinary_getNumberRegistry( ptr_pointer,stage, scenary, block )
  return ccall((:PSRIOGrafResultBinary_getNumberRegistry, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,scenary,block)
end
function PSRIOGrafResultBinary_initSave( ptr_pointer,nameheader, namebinary )
  return ccall((:PSRIOGrafResultBinary_initSave, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nameheader,namebinary)
end
function PSRIOGrafResultBinary_initUpdate( ptr_pointer,nameheader, namebinary )
  return ccall((:PSRIOGrafResultBinary_initUpdate, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nameheader,namebinary)
end
function PSRIOGrafResultBinary_writeRegistry( ptr_pointer )
  return ccall((:PSRIOGrafResultBinary_writeRegistry, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBinary_closeSave( ptr_pointer )
  return ccall((:PSRIOGrafResultBinary_closeSave, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResultBinary_getRegistry( ptr_pointer,stage, serie, block, index_agent )
  return ccall((:PSRIOGrafResultBinary_getRegistry, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32, Int32, Int32,),ptr_pointer ,stage,serie,block,index_agent)
end
function PSRIOGrafResultBinary_aggregate( ptr_pointer,serieAggregationType, aggregateBlock )
  return ccall((:PSRIOGrafResultBinary_aggregate, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Bool,),ptr_pointer ,serieAggregationType,aggregateBlock)
end
function PSRIOGrafResultBinary_toCSV( ptr_pointer,nameheader, namebinary, namecsv )
  return ccall((:PSRIOGrafResultBinary_toCSV, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nameheader,namebinary,namecsv)
end
function PSRIOGrafResultBinary_fillMemory( ptr_pointer,memory )
  return ccall((:PSRIOGrafResultBinary_fillMemory, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,pointer(memory))
end
function PSRIOGrafResultBinary_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGrafResultBinary_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileStagesInformation_create(iprt)
  ptr_pointer =  ccall((:StructuredFileStagesInformation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function StructuredFileStagesInformation_configure( ptr_pointer,reference_period, reference_year, stage_type )
  return ccall((:StructuredFileStagesInformation_configure, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,reference_period,reference_year,stage_type)
end
function StructuredFileStagesInformation_getInitialDate( ptr_pointer )
  return ccall((:StructuredFileStagesInformation_getInitialDate, "PSRClasses"),Int64,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileStagesInformation_setInitialDate( ptr_pointer,date )
  return ccall((:StructuredFileStagesInformation_setInitialDate, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64,),ptr_pointer ,date)
end
function StructuredFileStagesInformation_getStageType( ptr_pointer )
  return ccall((:StructuredFileStagesInformation_getStageType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileStagesInformation_setStageType( ptr_pointer,stage_type )
  return ccall((:StructuredFileStagesInformation_setStageType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,stage_type)
end
function StructuredFileStagesInformation_getRelativeInitialStage( ptr_pointer )
  return ccall((:StructuredFileStagesInformation_getRelativeInitialStage, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileStagesInformation_setRelativeInitialStage( ptr_pointer,relative_initial_stage )
  return ccall((:StructuredFileStagesInformation_setRelativeInitialStage, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,relative_initial_stage)
end
function StructuredFileStagesInformation_getNumberBlocks( ptr_pointer )
  return ccall((:StructuredFileStagesInformation_getNumberBlocks, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileStagesInformation_getNumberSimulations( ptr_pointer )
  return ccall((:StructuredFileStagesInformation_getNumberSimulations, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileStagesInformation_getNumberStages( ptr_pointer )
  return ccall((:StructuredFileStagesInformation_getNumberStages, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileStagesInformation_setNumberBlocks( ptr_pointer,number_blocks )
  return ccall((:StructuredFileStagesInformation_setNumberBlocks, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number_blocks)
end
function StructuredFileStagesInformation_setNumberSimulations( ptr_pointer,number_simulations )
  return ccall((:StructuredFileStagesInformation_setNumberSimulations, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number_simulations)
end
function StructuredFileStagesInformation_setNumberStages( ptr_pointer,number_stages )
  return ccall((:StructuredFileStagesInformation_setNumberStages, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number_stages)
end
function StructuredFileStagesInformation_getVariableHour( ptr_pointer )
  return ccall((:StructuredFileStagesInformation_getVariableHour, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileStagesInformation_setVariableHour( ptr_pointer,is_variable_hour )
  return ccall((:StructuredFileStagesInformation_setVariableHour, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,is_variable_hour)
end
function StructuredFileStagesInformation_getProfile( ptr_pointer )
  return ccall((:StructuredFileStagesInformation_getProfile, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileStagesInformation_setProfile( ptr_pointer,is_profile )
  return ccall((:StructuredFileStagesInformation_setProfile, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,is_profile)
end
function StructuredFileStagesInformation_getRealStage( ptr_pointer,stage )
  return ccall((:StructuredFileStagesInformation_getRealStage, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,stage)
end
function StructuredFileStagesInformation_getNumberHoursStage2( ptr_pointer,stage )
  return ccall((:StructuredFileStagesInformation_getNumberHoursStage2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,stage)
end
function StructuredFileStagesInformation_getRegistryOffset( ptr_pointer,stage, simulation )
  return ccall((:StructuredFileStagesInformation_getRegistryOffset, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,simulation)
end
function StructuredFileStagesInformation_deleteInstance( ptr_pointer )
  return ccall((:StructuredFileStagesInformation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGndGaugingStation_create(iprt)
  ptr_pointer =  ccall((:PSRGndGaugingStation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGndGaugingStation_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGndGaugingStation_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGndGaugingStation_classType( ptr_pointer )
  return ccall((:PSRGndGaugingStation_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGndGaugingStation_isClassType( ptr_pointer,class_type )
  return ccall((:PSRGndGaugingStation_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRGndGaugingStation_getSystem( ptr_pointer )
  return ccall((:PSRGndGaugingStation_getSystem, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGndGaugingStation_code( ptr_pointer )
  return ccall((:PSRGndGaugingStation_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGndGaugingStation_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGndGaugingStation_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGndGaugingStation_setCode( ptr_pointer,code )
  return ccall((:PSRGndGaugingStation_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRGndGaugingStation_setName( ptr_pointer,name )
  return ccall((:PSRGndGaugingStation_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name)
end
function PSRGndGaugingStation_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRGndGaugingStation_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGndGaugingStation_serialize( ptr_pointer )
  return ccall((:PSRGndGaugingStation_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGndGaugingStation_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRGndGaugingStation_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRGndGaugingStation_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRGndGaugingStation_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRGndGaugingStation_deleteInstance( ptr_pointer )
  return ccall((:PSRGndGaugingStation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPConfiguration_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPConfiguration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPConfiguration_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPConfiguration_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPConfiguration_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPConfiguration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCP_load( ptr_pointer,ptrStudy, path )
  return ccall((:PSRIONCP_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,path)
end
function PSRIONCP_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCP_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCP_create(iprt)
  ptr_pointer =  ccall((:PSRIONCP_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRDemand_create(iprt)
  ptr_pointer =  ccall((:PSRDemand_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRDemand_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRDemand_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRDemand_classType( ptr_pointer )
  return ccall((:PSRDemand_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRDemand_isClassType( ptr_pointer,class_type )
  return ccall((:PSRDemand_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRDemand_code( ptr_pointer )
  return ccall((:PSRDemand_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRDemand_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRDemand_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRDemand_setCode( ptr_pointer,_codigo )
  return ccall((:PSRDemand_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRDemand_setName( ptr_pointer,_nome )
  return ccall((:PSRDemand_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRDemand_system( ptr_pointer )
  return ccall((:PSRDemand_system, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRDemand_maxSegment( ptr_pointer )
  return ccall((:PSRDemand_maxSegment, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRDemand_segment( ptr_pointer,index )
  return ccall((:PSRDemand_segment, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRDemand_addSegment( ptr_pointer,ptrSegmento )
  return ccall((:PSRDemand_addSegment, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSegmento)
end
function PSRDemand_maxLoad( ptr_pointer )
  return ccall((:PSRDemand_maxLoad, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRDemand_load( ptr_pointer,index )
  return ccall((:PSRDemand_load, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRDemand_addLoad( ptr_pointer,ptrCarga )
  return ccall((:PSRDemand_addLoad, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCarga)
end
function PSRDemand_delLoad( ptr_pointer,ptrCarga )
  return ccall((:PSRDemand_delLoad, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCarga)
end
function PSRDemand_getLoad( ptr_pointer,ptrBarra )
  return ccall((:PSRDemand_getLoad, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBarra)
end
function PSRDemand_getCurrentLoad( ptr_pointer,LoadLevel, Segment )
  return ccall((:PSRDemand_getCurrentLoad, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,LoadLevel,Segment)
end
function PSRDemand_getLoad2( ptr_pointer,Stage, LoadLevel, Segment )
  return ccall((:PSRDemand_getLoad2, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,Stage,LoadLevel,Segment)
end
function PSRDemand_setLoad( ptr_pointer,Stage, LoadLevel, Segment )
  return ccall((:PSRDemand_setLoad, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,Stage,LoadLevel,Segment)
end
function PSRDemand_calculateFromLoadBus( ptr_pointer )
  return ccall((:PSRDemand_calculateFromLoadBus, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRDemand_loadBusToEnergy( ptr_pointer )
  return ccall((:PSRDemand_loadBusToEnergy, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRDemand_addFrom( ptr_pointer,ptrDemanda )
  return ccall((:PSRDemand_addFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDemanda)
end
function PSRDemand_subtractFrom( ptr_pointer,ptrDemanda )
  return ccall((:PSRDemand_subtractFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDemanda)
end
function PSRDemand_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRDemand_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRDemand_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRDemand_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRDemand_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRDemand_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRDemand_serialize( ptr_pointer )
  return ccall((:PSRDemand_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRDemand_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRDemand_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRDemand_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRDemand_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRDemand_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRDemand_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRDemand_deleteInstance( ptr_pointer )
  return ccall((:PSRDemand_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPLoadHourlyScenarios_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPLoadHourlyScenarios_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPLoadHourlyScenarios_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPLoadHourlyScenarios_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileData_create(iprt)
  ptr_pointer =  ccall((:StructuredFileData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function StructuredFileData_deleteInstance( ptr_pointer )
  return ccall((:StructuredFileData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalCombinedCycle_create(iprt)
  ptr_pointer =  ccall((:PSRThermalCombinedCycle_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRThermalCombinedCycle_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRThermalCombinedCycle_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRThermalCombinedCycle_classType( ptr_pointer )
  return ccall((:PSRThermalCombinedCycle_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalCombinedCycle_code( ptr_pointer )
  return ccall((:PSRThermalCombinedCycle_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalCombinedCycle_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRThermalCombinedCycle_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRThermalCombinedCycle_setCode( ptr_pointer,codigo )
  return ccall((:PSRThermalCombinedCycle_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,codigo)
end
function PSRThermalCombinedCycle_setName( ptr_pointer,nome )
  return ccall((:PSRThermalCombinedCycle_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRThermalCombinedCycle_getSystem( ptr_pointer )
  return ccall((:PSRThermalCombinedCycle_getSystem, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalCombinedCycle_maxPlant( ptr_pointer )
  return ccall((:PSRThermalCombinedCycle_maxPlant, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalCombinedCycle_plant( ptr_pointer,index )
  return ccall((:PSRThermalCombinedCycle_plant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRThermalCombinedCycle_addPlant( ptr_pointer,ptrThermalPlant )
  return ccall((:PSRThermalCombinedCycle_addPlant, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrThermalPlant)
end
function PSRThermalCombinedCycle_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRThermalCombinedCycle_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRThermalCombinedCycle_serialize( ptr_pointer )
  return ccall((:PSRThermalCombinedCycle_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalCombinedCycle_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRThermalCombinedCycle_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRThermalCombinedCycle_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRThermalCombinedCycle_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRThermalCombinedCycle_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRThermalCombinedCycle_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRThermalCombinedCycle_deleteInstance( ptr_pointer )
  return ccall((:PSRThermalCombinedCycle_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_AgentRule_Generator_create(iprt,_ptrStudy, _plantType)
  ptr_pointer =  ccall((:PSRIO_HEADEREDBIN_AgentRule_Generator_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),C_NULL,_ptrStudy,_plantType)
  return ptr_pointer
end
function PSRIO_HEADEREDBIN_AgentRule_Generator_getCode( ptr_pointer,name_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Generator_getCode, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Generator_getCode2( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Generator_getCode2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Generator_getAgentFactor( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Generator_getAgentFactor, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Generator_updateScenary( ptr_pointer,stageDate, simulation, block )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Generator_updateScenary, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64, Int32, Int32,),ptr_pointer ,stageDate,simulation,block)
end
function PSRIO_HEADEREDBIN_AgentRule_Generator_mapIndexToAgent( ptr_pointer,index, code )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Generator_mapIndexToAgent, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,index,code)
end
function PSRIO_HEADEREDBIN_AgentRule_Generator_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Generator_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileAgentsInformation_create(iprt)
  ptr_pointer =  ccall((:StructuredFileAgentsInformation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function StructuredFileAgentsInformation_getAgentsIdKey( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:StructuredFileAgentsInformation_getAgentsIdKey, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function StructuredFileAgentsInformation_setAgentsIdKey( ptr_pointer,key )
  return ccall((:StructuredFileAgentsInformation_setAgentsIdKey, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,key)
end
function StructuredFileAgentsInformation_getAgentsIdLength( ptr_pointer )
  return ccall((:StructuredFileAgentsInformation_getAgentsIdLength, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileAgentsInformation_setAgentsIdLength( ptr_pointer,length )
  return ccall((:StructuredFileAgentsInformation_setAgentsIdLength, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,length)
end
function StructuredFileAgentsInformation_maxAgent( ptr_pointer )
  return ccall((:StructuredFileAgentsInformation_maxAgent, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileAgentsInformation_getAgent( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:StructuredFileAgentsInformation_getAgent, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function StructuredFileAgentsInformation_addAgent( ptr_pointer,id )
  return ccall((:StructuredFileAgentsInformation_addAgent, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function StructuredFileAgentsInformation_getClassType( ptr_pointer )
  return ccall((:StructuredFileAgentsInformation_getClassType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileAgentsInformation_setClassType( ptr_pointer,class_type )
  return ccall((:StructuredFileAgentsInformation_setClassType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function StructuredFileAgentsInformation_getAgentIndex( ptr_pointer,id )
  return ccall((:StructuredFileAgentsInformation_getAgentIndex, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function StructuredFileAgentsInformation_deleteInstance( ptr_pointer )
  return ccall((:StructuredFileAgentsInformation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_AgentRule_Bus_create(iprt,_ptrStudy)
  ptr_pointer =  ccall((:PSRIO_HEADEREDBIN_AgentRule_Bus_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_ptrStudy)
  return ptr_pointer
end
function PSRIO_HEADEREDBIN_AgentRule_Bus_getCode( ptr_pointer,name_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Bus_getCode, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Bus_getCode2( ptr_pointer,index_agent )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Bus_getCode2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index_agent)
end
function PSRIO_HEADEREDBIN_AgentRule_Bus_getAgentsNumber( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Bus_getAgentsNumber, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_HEADEREDBIN_AgentRule_Bus_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_HEADEREDBIN_AgentRule_Bus_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function StructuredFileVersion_create(iprt,version)
  ptr_pointer =  ccall((:StructuredFileVersion_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),C_NULL,version)
  return ptr_pointer
end
function StructuredFileVersion_deleteInstance( ptr_pointer )
  return ccall((:StructuredFileVersion_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasPipeline_create(iprt)
  ptr_pointer =  ccall((:PSRGasPipeline_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGasPipeline_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGasPipeline_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGasPipeline_classType( ptr_pointer )
  return ccall((:PSRGasPipeline_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasPipeline_isClassType( ptr_pointer,class_type )
  return ccall((:PSRGasPipeline_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRGasPipeline_code( ptr_pointer )
  return ccall((:PSRGasPipeline_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasPipeline_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGasPipeline_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGasPipeline_setCode( ptr_pointer,_code )
  return ccall((:PSRGasPipeline_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_code)
end
function PSRGasPipeline_setName( ptr_pointer,_name )
  return ccall((:PSRGasPipeline_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_name)
end
function PSRGasPipeline_gasNode( ptr_pointer,ndx )
  return ccall((:PSRGasPipeline_gasNode, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRGasPipeline_numGasNode( ptr_pointer,ptrGasNode )
  return ccall((:PSRGasPipeline_numGasNode, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGasNode)
end
function PSRGasPipeline_adjacentGasNode( ptr_pointer,ptrGasNode )
  return ccall((:PSRGasPipeline_adjacentGasNode, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGasNode)
end
function PSRGasPipeline_serialize( ptr_pointer )
  return ccall((:PSRGasPipeline_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGasPipeline_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRGasPipeline_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRGasPipeline_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRGasPipeline_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRGasPipeline_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRGasPipeline_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGasPipeline_deleteInstance( ptr_pointer )
  return ccall((:PSRGasPipeline_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataParm_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataParm_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataParm_setParm( ptr_pointer,ptrParm )
  return ccall((:PSRMessageDataParm_setParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrParm)
end
function PSRMessageDataParm_getParm( ptr_pointer )
  return ccall((:PSRMessageDataParm_getParm, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataParm_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataParm_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroInflows_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPHydroInflows_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPHydroInflows_setStageType( ptr_pointer,_stageType )
  return ccall((:PSRIOSDDPHydroInflows_setStageType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_stageType)
end
function PSRIOSDDPHydroInflows_load( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOSDDPHydroInflows_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOSDDPHydroInflows_save( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOSDDPHydroInflows_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOSDDPHydroInflows_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPHydroInflows_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmIntegerPointer_create(iprt,id, data)
  ptr_pointer =  ccall((:PSRParmIntegerPointer_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL,id,data)
  return ptr_pointer
end
function PSRParmIntegerPointer_setData( ptr_pointer,data )
  return ccall((:PSRParmIntegerPointer_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,data)
end
function PSRParmIntegerPointer_getInteger( ptr_pointer )
  return ccall((:PSRParmIntegerPointer_getInteger, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmIntegerPointer_toString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmIntegerPointer_toString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmIntegerPointer_setDataFromString( ptr_pointer,data )
  return ccall((:PSRParmIntegerPointer_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParmIntegerPointer_setDefault( ptr_pointer )
  return ccall((:PSRParmIntegerPointer_setDefault, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmIntegerPointer_getPointer( ptr_pointer )
  return ccall((:PSRParmIntegerPointer_getPointer, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmIntegerPointer_clone( ptr_pointer )
  return ccall((:PSRParmIntegerPointer_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmIntegerPointer_copyTo( ptr_pointer,TargetParm )
  return ccall((:PSRParmIntegerPointer_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,TargetParm)
end
function PSRParmIntegerPointer_serialize( ptr_pointer )
  return ccall((:PSRParmIntegerPointer_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmIntegerPointer_buildFrom( ptr_pointer,ptrMessageDataParm )
  return ccall((:PSRParmIntegerPointer_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataParm)
end
function PSRParmIntegerPointer_deleteInstance( ptr_pointer )
  return ccall((:PSRParmIntegerPointer_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_ROWDATA_Out_addColFormat( ptr_pointer,col_format )
  return ccall((:PSRIO_ROWDATA_Out_addColFormat, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,col_format)
end
function PSRIO_ROWDATA_Out_addParm( ptr_pointer,parm )
  return ccall((:PSRIO_ROWDATA_Out_addParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,parm)
end
function PSRIO_ROWDATA_Out_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_ROWDATA_Out_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_ROWDATA_Out_create(iprt)
  ptr_pointer =  ccall((:PSRIO_ROWDATA_Out_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGasNode_create(iprt)
  ptr_pointer =  ccall((:PSRGasNode_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGasNode_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGasNode_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGasNode_classType( ptr_pointer )
  return ccall((:PSRGasNode_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasNode_isClassType( ptr_pointer,class_type )
  return ccall((:PSRGasNode_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRGasNode_system( ptr_pointer )
  return ccall((:PSRGasNode_system, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGasNode_network( ptr_pointer )
  return ccall((:PSRGasNode_network, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGasNode_code( ptr_pointer )
  return ccall((:PSRGasNode_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasNode_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGasNode_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGasNode_setCode( ptr_pointer,_code )
  return ccall((:PSRGasNode_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_code)
end
function PSRGasNode_setName( ptr_pointer,_name )
  return ccall((:PSRGasNode_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_name)
end
function PSRGasNode_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRGasNode_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRGasNode_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRGasNode_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGasNode_serialize( ptr_pointer )
  return ccall((:PSRGasNode_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGasNode_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRGasNode_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRGasNode_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRGasNode_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRGasNode_deleteInstance( ptr_pointer )
  return ccall((:PSRGasNode_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRRiverNetwork_create(iprt)
  ptr_pointer =  ccall((:PSRRiverNetwork_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRRiverNetwork_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRRiverNetwork_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRRiverNetwork_classType( ptr_pointer )
  return ccall((:PSRRiverNetwork_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRRiverNetwork_isClassType( ptr_pointer,class_type )
  return ccall((:PSRRiverNetwork_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRRiverNetwork_graph( ptr_pointer )
  return ccall((:PSRRiverNetwork_graph, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRRiverNetwork_addNo( ptr_pointer,ptrElement )
  return ccall((:PSRRiverNetwork_addNo, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRRiverNetwork_addConnection( ptr_pointer,ptrElementFrom, ptrElementTo, ptrConnection )
  return ccall((:PSRRiverNetwork_addConnection, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElementFrom,ptrElementTo,ptrConnection)
end
function PSRRiverNetwork_maxNodes( ptr_pointer )
  return ccall((:PSRRiverNetwork_maxNodes, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRRiverNetwork_replaceNodeElement( ptr_pointer,ptrNodeElement, newNodeElement )
  return ccall((:PSRRiverNetwork_replaceNodeElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNodeElement,newNodeElement)
end
function PSRRiverNetwork_no( ptr_pointer,index )
  return ccall((:PSRRiverNetwork_no, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRRiverNetwork_isPlant( ptr_pointer,ptrElement )
  return ccall((:PSRRiverNetwork_isPlant, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRRiverNetwork_getNodeType( ptr_pointer,ptrNo )
  return ccall((:PSRRiverNetwork_getNodeType, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNo)
end
function PSRRiverNetwork_maxConnectionIn( ptr_pointer,ptrNo )
  return ccall((:PSRRiverNetwork_maxConnectionIn, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNo)
end
function PSRRiverNetwork_connectionIn( ptr_pointer,ptrNo, index )
  return ccall((:PSRRiverNetwork_connectionIn, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrNo,index)
end
function PSRRiverNetwork_maxConnectionOut( ptr_pointer,ptrNo )
  return ccall((:PSRRiverNetwork_maxConnectionOut, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNo)
end
function PSRRiverNetwork_connectionOut( ptr_pointer,ptrNo, index )
  return ccall((:PSRRiverNetwork_connectionOut, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrNo,index)
end
function PSRRiverNetwork_deleteInstance( ptr_pointer )
  return ccall((:PSRRiverNetwork_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRScenarioTimeController_create(iprt)
  ptr_pointer =  ccall((:PSRScenarioTimeController_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRScenarioTimeController_configureFrom( ptr_pointer,ptrStudy )
  return ccall((:PSRScenarioTimeController_configureFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRScenarioTimeController_load( ptr_pointer,filename )
  return ccall((:PSRScenarioTimeController_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,filename)
end
function PSRScenarioTimeController_addToScenarioMap( ptr_pointer,ptrColElement, attribute )
  return ccall((:PSRScenarioTimeController_addToScenarioMap, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrColElement,attribute)
end
function PSRScenarioTimeController_gotoStageScenario( ptr_pointer,stage, scenario )
  return ccall((:PSRScenarioTimeController_gotoStageScenario, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,scenario)
end
function PSRScenarioTimeController_gotoStageHourScenario( ptr_pointer,stage, hour, scenario )
  return ccall((:PSRScenarioTimeController_gotoStageHourScenario, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,hour,scenario)
end
function PSRScenarioTimeController_getCurrentScenarioDate( ptr_pointer )
  return ccall((:PSRScenarioTimeController_getCurrentScenarioDate, "PSRClasses"),Int64,(Ptr{UInt8},),ptr_pointer )
end
function PSRScenarioTimeController_deleteInstance( ptr_pointer )
  return ccall((:PSRScenarioTimeController_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExpansionConstraint_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionConstraint_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionConstraint_classType( ptr_pointer )
  return ccall((:PSRExpansionConstraint_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionConstraint_isClassType( ptr_pointer,class_type )
  return ccall((:PSRExpansionConstraint_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRExpansionConstraint_getGenericIdentification( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionConstraint_getGenericIdentification, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionConstraint_code( ptr_pointer )
  return ccall((:PSRExpansionConstraint_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionConstraint_setCode( ptr_pointer,code )
  return ccall((:PSRExpansionConstraint_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRExpansionConstraint_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionConstraint_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionConstraint_setName( ptr_pointer,_name )
  return ccall((:PSRExpansionConstraint_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_name)
end
function PSRExpansionConstraint_projects( ptr_pointer )
  return ccall((:PSRExpansionConstraint_projects, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionConstraint_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRExpansionConstraint_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRExpansionConstraint_serialize( ptr_pointer )
  return ccall((:PSRExpansionConstraint_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionConstraint_buildFrom( ptr_pointer,ptrMessage )
  return ccall((:PSRExpansionConstraint_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessage)
end
function PSRExpansionConstraint_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRExpansionConstraint_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRExpansionConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDecision_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionDecision_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExpansionDecision_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionDecision_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionDecision_classType( ptr_pointer )
  return ccall((:PSRExpansionDecision_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDecision_isClassType( ptr_pointer,class_type )
  return ccall((:PSRExpansionDecision_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRExpansionDecision_setProject( ptr_pointer,ptrProject, minPeriod, minYear, maxPeriod, maxYear, capacity, decisionType )
  return ccall((:PSRExpansionDecision_setProject, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Int32, Int32, Int32, Int32, Float64, Int32,),ptr_pointer ,ptrProject,minPeriod,minYear,maxPeriod,maxYear,capacity,decisionType)
end
function PSRExpansionDecision_setProject2( ptr_pointer,ptrProject )
  return ccall((:PSRExpansionDecision_setProject2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrProject)
end
function PSRExpansionDecision_project( ptr_pointer )
  return ccall((:PSRExpansionDecision_project, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDecision_minPeriod( ptr_pointer )
  return ccall((:PSRExpansionDecision_minPeriod, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDecision_minYear( ptr_pointer )
  return ccall((:PSRExpansionDecision_minYear, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDecision_maxPeriod( ptr_pointer )
  return ccall((:PSRExpansionDecision_maxPeriod, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDecision_maxYear( ptr_pointer )
  return ccall((:PSRExpansionDecision_maxYear, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDecision_capacity( ptr_pointer )
  return ccall((:PSRExpansionDecision_capacity, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDecision_decisionType( ptr_pointer )
  return ccall((:PSRExpansionDecision_decisionType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDecision_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRExpansionDecision_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRExpansionDecision_serialize( ptr_pointer )
  return ccall((:PSRExpansionDecision_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDecision_buildFrom( ptr_pointer,ptrMessage )
  return ccall((:PSRExpansionDecision_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessage)
end
function PSRExpansionDecision_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionDecision_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRArea_create(iprt)
  ptr_pointer =  ccall((:PSRArea_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRArea_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRArea_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRArea_classType( ptr_pointer )
  return ccall((:PSRArea_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRArea_isClassType( ptr_pointer,class_type )
  return ccall((:PSRArea_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRArea_code( ptr_pointer )
  return ccall((:PSRArea_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRArea_id( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRArea_id, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRArea_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRArea_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRArea_setCode( ptr_pointer,_codigo )
  return ccall((:PSRArea_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRArea_setId( ptr_pointer,_id )
  return ccall((:PSRArea_setId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_id)
end
function PSRArea_setName( ptr_pointer,_nome )
  return ccall((:PSRArea_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRArea_network( ptr_pointer )
  return ccall((:PSRArea_network, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRArea_getSystem( ptr_pointer )
  return ccall((:PSRArea_getSystem, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRArea_setSystem( ptr_pointer,ptrSystem )
  return ccall((:PSRArea_setSystem, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRArea_addBus( ptr_pointer,ptrBus )
  return ccall((:PSRArea_addBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRArea_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRArea_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRArea_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRArea_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRArea_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRArea_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRArea_serialize( ptr_pointer )
  return ccall((:PSRArea_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRArea_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRArea_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRArea_deleteInstance( ptr_pointer )
  return ccall((:PSRArea_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRRiverConnection_classType( ptr_pointer )
  return ccall((:PSRRiverConnection_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRRiverConnection_isClassType( ptr_pointer,class_type )
  return ccall((:PSRRiverConnection_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRRiverConnection_no( ptr_pointer,index )
  return ccall((:PSRRiverConnection_no, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRRiverConnection_deleteInstance( ptr_pointer )
  return ccall((:PSRRiverConnection_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRRiverConnection_create(iprt)
  ptr_pointer =  ccall((:PSRRiverConnection_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRBalancingArea_create(iprt)
  ptr_pointer =  ccall((:PSRBalancingArea_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRBalancingArea_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRBalancingArea_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRBalancingArea_classType( ptr_pointer )
  return ccall((:PSRBalancingArea_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBalancingArea_isClassType( ptr_pointer,class_type )
  return ccall((:PSRBalancingArea_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRBalancingArea_code( ptr_pointer )
  return ccall((:PSRBalancingArea_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBalancingArea_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRBalancingArea_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRBalancingArea_setCode( ptr_pointer,_codigo )
  return ccall((:PSRBalancingArea_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRBalancingArea_setName( ptr_pointer,_nome )
  return ccall((:PSRBalancingArea_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRBalancingArea_maxElement( ptr_pointer )
  return ccall((:PSRBalancingArea_maxElement, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBalancingArea_element( ptr_pointer,index )
  return ccall((:PSRBalancingArea_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRBalancingArea_addElement( ptr_pointer,ptrElement )
  return ccall((:PSRBalancingArea_addElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRBalancingArea_getElementIndex( ptr_pointer,ptrElement )
  return ccall((:PSRBalancingArea_getElementIndex, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRBalancingArea_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRBalancingArea_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRBalancingArea_serialize( ptr_pointer )
  return ccall((:PSRBalancingArea_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRBalancingArea_buildFrom( ptr_pointer,ptrMessageDataElement )
  return ccall((:PSRBalancingArea_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataElement)
end
function PSRBalancingArea_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRBalancingArea_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRBalancingArea_deleteInstance( ptr_pointer )
  return ccall((:PSRBalancingArea_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasNetwork_create(iprt)
  ptr_pointer =  ccall((:PSRGasNetwork_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGasNetwork_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGasNetwork_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGasNetwork_classType( ptr_pointer )
  return ccall((:PSRGasNetwork_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasNetwork_isClassType( ptr_pointer,class_type )
  return ccall((:PSRGasNetwork_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRGasNetwork_graph( ptr_pointer )
  return ccall((:PSRGasNetwork_graph, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGasNetwork_maxGasNode( ptr_pointer )
  return ccall((:PSRGasNetwork_maxGasNode, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasNetwork_gasNode( ptr_pointer,index )
  return ccall((:PSRGasNetwork_gasNode, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRGasNetwork_getGasNode( ptr_pointer,code )
  return ccall((:PSRGasNetwork_getGasNode, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRGasNetwork_addGasNode( ptr_pointer,ptrGasNode )
  return ccall((:PSRGasNetwork_addGasNode, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGasNode)
end
function PSRGasNetwork_maxGasPipeline( ptr_pointer )
  return ccall((:PSRGasNetwork_maxGasPipeline, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasNetwork_gasPipeline( ptr_pointer,index )
  return ccall((:PSRGasNetwork_gasPipeline, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRGasNetwork_getGasPipeline( ptr_pointer,code )
  return ccall((:PSRGasNetwork_getGasPipeline, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRGasNetwork_addPipeline( ptr_pointer,ptrGasNode01, ptrGasNode02, ptrGasPipeline )
  return ccall((:PSRGasNetwork_addPipeline, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGasNode01,ptrGasNode02,ptrGasPipeline)
end
function PSRGasNetwork_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRGasNetwork_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRGasNetwork_serialize( ptr_pointer )
  return ccall((:PSRGasNetwork_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGasNetwork_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRGasNetwork_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRGasNetwork_configureGraphFrom( ptr_pointer,ptrDataGraph )
  return ccall((:PSRGasNetwork_configureGraphFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataGraph)
end
function PSRGasNetwork_updateGraphFrom( ptr_pointer,ptrDataGraph )
  return ccall((:PSRGasNetwork_updateGraphFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataGraph)
end
function PSRGasNetwork_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRGasNetwork_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGasNetwork_deleteInstance( ptr_pointer )
  return ccall((:PSRGasNetwork_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRReservoirSet_create(iprt)
  ptr_pointer =  ccall((:PSRReservoirSet_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRReservoirSet_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRReservoirSet_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRReservoirSet_classType( ptr_pointer )
  return ccall((:PSRReservoirSet_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRReservoirSet_isClassType( ptr_pointer,class_type )
  return ccall((:PSRReservoirSet_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRReservoirSet_code( ptr_pointer )
  return ccall((:PSRReservoirSet_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRReservoirSet_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRReservoirSet_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRReservoirSet_setCode( ptr_pointer,_code )
  return ccall((:PSRReservoirSet_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_code)
end
function PSRReservoirSet_setName( ptr_pointer,_nome )
  return ccall((:PSRReservoirSet_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRReservoirSet_maxReservoir( ptr_pointer )
  return ccall((:PSRReservoirSet_maxReservoir, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRReservoirSet_reservoir( ptr_pointer,index )
  return ccall((:PSRReservoirSet_reservoir, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRReservoirSet_addReservoir( ptr_pointer,ptrHydroPlant )
  return ccall((:PSRReservoirSet_addReservoir, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrHydroPlant)
end
function PSRReservoirSet_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRReservoirSet_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRReservoirSet_serialize( ptr_pointer )
  return ccall((:PSRReservoirSet_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRReservoirSet_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRReservoirSet_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRReservoirSet_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRReservoirSet_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRReservoirSet_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRReservoirSet_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRReservoirSet_deleteInstance( ptr_pointer )
  return ccall((:PSRReservoirSet_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_create(iprt)
  ptr_pointer =  ccall((:PSRBus_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRBus_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRBus_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRBus_classType( ptr_pointer )
  return ccall((:PSRBus_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_isClassType( ptr_pointer,class_type )
  return ccall((:PSRBus_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRBus_system( ptr_pointer )
  return ccall((:PSRBus_system, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_area( ptr_pointer )
  return ccall((:PSRBus_area, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_region( ptr_pointer )
  return ccall((:PSRBus_region, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_network( ptr_pointer )
  return ccall((:PSRBus_network, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_logicalGraph( ptr_pointer )
  return ccall((:PSRBus_logicalGraph, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_code( ptr_pointer )
  return ccall((:PSRBus_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRBus_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRBus_setCode( ptr_pointer,_codigo )
  return ccall((:PSRBus_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRBus_setName( ptr_pointer,_nome )
  return ccall((:PSRBus_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRBus_getBusType( ptr_pointer )
  return ccall((:PSRBus_getBusType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_setBusType( ptr_pointer,tipo )
  return ccall((:PSRBus_setBusType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,tipo)
end
function PSRBus_getIccaFlag( ptr_pointer )
  return ccall((:PSRBus_getIccaFlag, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_setIccaFlag( ptr_pointer,_icca )
  return ccall((:PSRBus_setIccaFlag, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_icca)
end
function PSRBus_maxShunt( ptr_pointer )
  return ccall((:PSRBus_maxShunt, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_shunt( ptr_pointer,ndx )
  return ccall((:PSRBus_shunt, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRBus_maxSerie( ptr_pointer )
  return ccall((:PSRBus_maxSerie, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_serie( ptr_pointer,ndx )
  return ccall((:PSRBus_serie, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRBus_getSerie( ptr_pointer,codserie, deviceType )
  return ccall((:PSRBus_getSerie, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,codserie,deviceType)
end
function PSRBus_getFirstDevice( ptr_pointer,tipo_devc )
  return ccall((:PSRBus_getFirstDevice, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,tipo_devc)
end
function PSRBus_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRBus_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRBus_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRBus_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRBus_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRBus_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRBus_serialize( ptr_pointer )
  return ccall((:PSRBus_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRBus_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRBus_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRBus_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRBus_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRBus_getGenericIdentification( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRBus_getGenericIdentification, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRBus_deleteInstance( ptr_pointer )
  return ccall((:PSRBus_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANDemandP_load( ptr_pointer,ptrEstudo, nomeHeader, nomeData )
  return ccall((:PSRIONETPLANDemandP_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomeHeader,nomeData)
end
function PSRIONETPLANDemandP_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANDemandP_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANDemandP_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANDemandP_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRParmReference_create(iprt,id, data)
  ptr_pointer =  ccall((:PSRParmReference_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL,id,data)
  return ptr_pointer
end
function PSRParmReference_setReferenceClassName( ptr_pointer,class_name )
  return ccall((:PSRParmReference_setReferenceClassName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,class_name)
end
function PSRParmReference_setData( ptr_pointer,data )
  return ccall((:PSRParmReference_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParmReference_setData2( ptr_pointer,data )
  return ccall((:PSRParmReference_setData2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParmReference_getString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmReference_getString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmReference_toString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmReference_toString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmReference_getReferenceClassName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmReference_getReferenceClassName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmReference_getReference( ptr_pointer )
  return ccall((:PSRParmReference_getReference, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmReference_setDefault( ptr_pointer )
  return ccall((:PSRParmReference_setDefault, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmReference_deleteInstance( ptr_pointer )
  return ccall((:PSRParmReference_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmString_create(iprt,id, data)
  ptr_pointer =  ccall((:PSRParmString_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL,id,data)
  return ptr_pointer
end
function PSRParmString_setData( ptr_pointer,data )
  return ccall((:PSRParmString_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParmString_getString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmString_getString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmString_toString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmString_toString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmString_setDataFromString( ptr_pointer,data )
  return ccall((:PSRParmString_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParmString_setDefault( ptr_pointer )
  return ccall((:PSRParmString_setDefault, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmString_clone( ptr_pointer )
  return ccall((:PSRParmString_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmString_copyTo( ptr_pointer,TargetParm )
  return ccall((:PSRParmString_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,TargetParm)
end
function PSRParmString_serialize( ptr_pointer )
  return ccall((:PSRParmString_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmString_buildFrom( ptr_pointer,ptrMessageDataParm )
  return ccall((:PSRParmString_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataParm)
end
function PSRParmString_deleteInstance( ptr_pointer )
  return ccall((:PSRParmString_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmInteger_create(iprt,id, data)
  ptr_pointer =  ccall((:PSRParmInteger_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),C_NULL,id,data)
  return ptr_pointer
end
function PSRParmInteger_setData( ptr_pointer,data )
  return ccall((:PSRParmInteger_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,data)
end
function PSRParmInteger_getInteger( ptr_pointer )
  return ccall((:PSRParmInteger_getInteger, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmInteger_toString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmInteger_toString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmInteger_setDataFromString( ptr_pointer,data )
  return ccall((:PSRParmInteger_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParmInteger_setDefault( ptr_pointer )
  return ccall((:PSRParmInteger_setDefault, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmInteger_getPointer( ptr_pointer )
  return ccall((:PSRParmInteger_getPointer, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmInteger_clone( ptr_pointer )
  return ccall((:PSRParmInteger_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmInteger_copyTo( ptr_pointer,TargetParm )
  return ccall((:PSRParmInteger_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,TargetParm)
end
function PSRParmInteger_serialize( ptr_pointer )
  return ccall((:PSRParmInteger_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmInteger_buildFrom( ptr_pointer,ptrMessageDataParm )
  return ccall((:PSRParmInteger_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataParm)
end
function PSRParmInteger_deleteInstance( ptr_pointer )
  return ccall((:PSRParmInteger_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANDemandQ_load( ptr_pointer,ptrEstudo, nomeHeader, nomeData )
  return ccall((:PSRIONETPLANDemandQ_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomeHeader,nomeData)
end
function PSRIONETPLANDemandQ_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANDemandQ_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANDemandQ_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANDemandQ_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRLogConsole_warning( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogConsole_warning, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogConsole_error( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogConsole_error, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogConsole_info( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogConsole_info, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogConsole_deleteInstance( ptr_pointer )
  return ccall((:PSRLogConsole_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLogConsole_create(iprt)
  ptr_pointer =  ccall((:PSRLogConsole_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRParmDate_getDay( ptr_pointer )
  return ccall((:PSRParmDate_getDay, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDate_getMonth( ptr_pointer )
  return ccall((:PSRParmDate_getMonth, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDate_getYear( ptr_pointer )
  return ccall((:PSRParmDate_getYear, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDate_create(iprt,id, data)
  ptr_pointer =  ccall((:PSRParmDate_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int64,),C_NULL,id,data)
  return ptr_pointer
end
function PSRParmDate_setData( ptr_pointer,date )
  return ccall((:PSRParmDate_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Int64,),ptr_pointer ,date)
end
function PSRParmDate_getDate( ptr_pointer )
  return ccall((:PSRParmDate_getDate, "PSRClasses"),Int64,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDate_toString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmDate_toString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmDate_setDataFromString( ptr_pointer,data )
  return ccall((:PSRParmDate_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParmDate_setDefault( ptr_pointer )
  return ccall((:PSRParmDate_setDefault, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDate_clone( ptr_pointer )
  return ccall((:PSRParmDate_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDate_copyTo( ptr_pointer,TargetParm )
  return ccall((:PSRParmDate_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,TargetParm)
end
function PSRParmDate_serialize( ptr_pointer )
  return ccall((:PSRParmDate_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmDate_buildFrom( ptr_pointer,ptrMessageDataParm )
  return ccall((:PSRParmDate_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataParm)
end
function PSRParmDate_deleteInstance( ptr_pointer )
  return ccall((:PSRParmDate_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRPlant_create(iprt)
  ptr_pointer =  ccall((:PSRPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRPlant_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRPlant_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRPlant_classType( ptr_pointer )
  return ccall((:PSRPlant_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRPlant_isClassType( ptr_pointer,class_type )
  return ccall((:PSRPlant_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRPlant_getGenericIdentification( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRPlant_getGenericIdentification, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRPlant_plantType( ptr_pointer )
  return ccall((:PSRPlant_plantType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRPlant_code( ptr_pointer )
  return ccall((:PSRPlant_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRPlant_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRPlant_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRPlant_setCode( ptr_pointer,_codigo )
  return ccall((:PSRPlant_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRPlant_setName( ptr_pointer,_nome )
  return ccall((:PSRPlant_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRPlant_getSystem( ptr_pointer )
  return ccall((:PSRPlant_getSystem, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRPlant_maxGenerator( ptr_pointer )
  return ccall((:PSRPlant_maxGenerator, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRPlant_generator( ptr_pointer,ndx )
  return ccall((:PSRPlant_generator, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRPlant_addGenerator( ptr_pointer,ptrGenerator )
  return ccall((:PSRPlant_addGenerator, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGenerator)
end
function PSRPlant_delGenerator( ptr_pointer,ptrGenerator )
  return ccall((:PSRPlant_delGenerator, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGenerator)
end
function PSRPlant_maintenances( ptr_pointer )
  return ccall((:PSRPlant_maintenances, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRPlant_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRPlant_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRPlant_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRPlant_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRPlant_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRPlant_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRPlant_serialize( ptr_pointer )
  return ccall((:PSRPlant_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRPlant_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRPlant_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmRealPointer_create(iprt,id, data)
  ptr_pointer =  ccall((:PSRParmRealPointer_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{Float64},),C_NULL,id,data)
  return ptr_pointer
end
function PSRParmRealPointer_setData( ptr_pointer,data )
  return ccall((:PSRParmRealPointer_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,data)
end
function PSRParmRealPointer_getReal( ptr_pointer )
  return ccall((:PSRParmRealPointer_getReal, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmRealPointer_toString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmRealPointer_toString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmRealPointer_setDataFromString( ptr_pointer,data )
  return ccall((:PSRParmRealPointer_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParmRealPointer_setDefault( ptr_pointer )
  return ccall((:PSRParmRealPointer_setDefault, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmRealPointer_getPointer( ptr_pointer )
  return ccall((:PSRParmRealPointer_getPointer, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmRealPointer_clone( ptr_pointer )
  return ccall((:PSRParmRealPointer_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmRealPointer_copyTo( ptr_pointer,TargetParm )
  return ccall((:PSRParmRealPointer_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,TargetParm)
end
function PSRParmRealPointer_serialize( ptr_pointer )
  return ccall((:PSRParmRealPointer_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmRealPointer_buildFrom( ptr_pointer,ptrMessageDataParm )
  return ccall((:PSRParmRealPointer_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataParm)
end
function PSRParmRealPointer_deleteInstance( ptr_pointer )
  return ccall((:PSRParmRealPointer_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusInfoBin_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusInfoBin_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusInfoBin_useDCBus( ptr_pointer,status )
  return ccall((:PSRIONETPLANBusInfoBin_useDCBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIONETPLANBusInfoBin_load( ptr_pointer,ptrEstudo, idvetor, nomeHeader, nomeData )
  return ccall((:PSRIONETPLANBusInfoBin_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,idvetor,nomeHeader,nomeData)
end
function PSRIONETPLANBusInfoBin_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusInfoBin_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_create(iprt)
  ptr_pointer =  ccall((:PSRStudy_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRStudy_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRStudy_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRStudy_classType( ptr_pointer )
  return ccall((:PSRStudy_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_isClassType( ptr_pointer,class_type )
  return ccall((:PSRStudy_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRStudy_description( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRStudy_description, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRStudy_setDescription( ptr_pointer,_descricao )
  return ccall((:PSRStudy_setDescription, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_descricao)
end
function PSRStudy_maxSystem( ptr_pointer )
  return ccall((:PSRStudy_maxSystem, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_system( ptr_pointer,ndx )
  return ccall((:PSRStudy_system, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRStudy_getSystem( ptr_pointer,id )
  return ccall((:PSRStudy_getSystem, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRStudy_getSystemByName( ptr_pointer,name )
  return ccall((:PSRStudy_getSystemByName, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name)
end
function PSRStudy_getSystem2( ptr_pointer,codigo )
  return ccall((:PSRStudy_getSystem2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codigo)
end
function PSRStudy_addSystem( ptr_pointer,ptrSistema )
  return ccall((:PSRStudy_addSystem, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema)
end
function PSRStudy_delSystem( ptr_pointer,ptrSistema )
  return ccall((:PSRStudy_delSystem, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema)
end
function PSRStudy_maxArea( ptr_pointer )
  return ccall((:PSRStudy_maxArea, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_area( ptr_pointer,ndx )
  return ccall((:PSRStudy_area, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRStudy_getArea( ptr_pointer,id )
  return ccall((:PSRStudy_getArea, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRStudy_getAreaByName( ptr_pointer,nome )
  return ccall((:PSRStudy_getAreaByName, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRStudy_getArea2( ptr_pointer,codigo )
  return ccall((:PSRStudy_getArea2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codigo)
end
function PSRStudy_addArea( ptr_pointer,ptrArea )
  return ccall((:PSRStudy_addArea, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrArea)
end
function PSRStudy_delArea( ptr_pointer,ptrArea )
  return ccall((:PSRStudy_delArea, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrArea)
end
function PSRStudy_maxBalancingArea( ptr_pointer )
  return ccall((:PSRStudy_maxBalancingArea, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_balancingArea( ptr_pointer,ndx )
  return ccall((:PSRStudy_balancingArea, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRStudy_addBalancingArea( ptr_pointer,ptrBalancingArea )
  return ccall((:PSRStudy_addBalancingArea, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBalancingArea)
end
function PSRStudy_delBalancingArea( ptr_pointer,ptrBalancingArea )
  return ccall((:PSRStudy_delBalancingArea, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBalancingArea)
end
function PSRStudy_maxRegion( ptr_pointer )
  return ccall((:PSRStudy_maxRegion, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_region( ptr_pointer,ndx )
  return ccall((:PSRStudy_region, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRStudy_addRegion( ptr_pointer,ptrRegion )
  return ccall((:PSRStudy_addRegion, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrRegion)
end
function PSRStudy_getRegion( ptr_pointer,id )
  return ccall((:PSRStudy_getRegion, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRStudy_maxGasEmission( ptr_pointer )
  return ccall((:PSRStudy_maxGasEmission, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_gasEmission( ptr_pointer,ndx )
  return ccall((:PSRStudy_gasEmission, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRStudy_addGasEmission( ptr_pointer,ptrGasEmission )
  return ccall((:PSRStudy_addGasEmission, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGasEmission)
end
function PSRStudy_getGasEmission( ptr_pointer,code )
  return ccall((:PSRStudy_getGasEmission, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRStudy_maxReservoirSet( ptr_pointer )
  return ccall((:PSRStudy_maxReservoirSet, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_reservoirSet( ptr_pointer,ndx )
  return ccall((:PSRStudy_reservoirSet, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRStudy_addReservoirSet( ptr_pointer,ptrReservoirSet )
  return ccall((:PSRStudy_addReservoirSet, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrReservoirSet)
end
function PSRStudy_getReservoirSet( ptr_pointer,code )
  return ccall((:PSRStudy_getReservoirSet, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRStudy_network( ptr_pointer )
  return ccall((:PSRStudy_network, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_networkDC( ptr_pointer )
  return ccall((:PSRStudy_networkDC, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getNetworkByDeviceType( ptr_pointer,device_type )
  return ccall((:PSRStudy_getNetworkByDeviceType, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,device_type)
end
function PSRStudy_gasNetwork( ptr_pointer )
  return ccall((:PSRStudy_gasNetwork, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_addHydroPlant( ptr_pointer,Hydro )
  return ccall((:PSRStudy_addHydroPlant, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,Hydro)
end
function PSRStudy_getBus( ptr_pointer,codbus )
  return ccall((:PSRStudy_getBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codbus)
end
function PSRStudy_getBus2( ptr_pointer,namebus )
  return ccall((:PSRStudy_getBus2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,namebus)
end
function PSRStudy_getShunt( ptr_pointer,codshunt, device_type )
  return ccall((:PSRStudy_getShunt, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,codshunt,device_type)
end
function PSRStudy_getSerie( ptr_pointer,codserie )
  return ccall((:PSRStudy_getSerie, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codserie)
end
function PSRStudy_getSerie2( ptr_pointer,no01, no02, circ )
  return ccall((:PSRStudy_getSerie2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,no01,no02,circ)
end
function PSRStudy_getPlant( ptr_pointer,tipo, codusina )
  return ccall((:PSRStudy_getPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,tipo,codusina)
end
function PSRStudy_getPlant2( ptr_pointer,tipo, nameplant )
  return ccall((:PSRStudy_getPlant2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,tipo,nameplant)
end
function PSRStudy_getHydroPlant( ptr_pointer,codusina )
  return ccall((:PSRStudy_getHydroPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codusina)
end
function PSRStudy_hydroPlant( ptr_pointer,index )
  return ccall((:PSRStudy_hydroPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRStudy_getInterconnection( ptr_pointer,codsysfrom, codsysto )
  return ccall((:PSRStudy_getInterconnection, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,codsysfrom,codsysto)
end
function PSRStudy_getCollectionShunts( ptr_pointer,tipo_devc )
  return ccall((:PSRStudy_getCollectionShunts, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,tipo_devc)
end
function PSRStudy_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRStudy_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRStudy_getCollectionSystems( ptr_pointer )
  return ccall((:PSRStudy_getCollectionSystems, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionAreas( ptr_pointer )
  return ccall((:PSRStudy_getCollectionAreas, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionBalancingAreas( ptr_pointer )
  return ccall((:PSRStudy_getCollectionBalancingAreas, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionRegions( ptr_pointer )
  return ccall((:PSRStudy_getCollectionRegions, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionBuses( ptr_pointer )
  return ccall((:PSRStudy_getCollectionBuses, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionBusesDC( ptr_pointer )
  return ccall((:PSRStudy_getCollectionBusesDC, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionSeries( ptr_pointer )
  return ccall((:PSRStudy_getCollectionSeries, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionSeries2( ptr_pointer,tipo_devc )
  return ccall((:PSRStudy_getCollectionSeries2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,tipo_devc)
end
function PSRStudy_getCollectionTransformers( ptr_pointer )
  return ccall((:PSRStudy_getCollectionTransformers, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionDemands( ptr_pointer )
  return ccall((:PSRStudy_getCollectionDemands, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionDemandSegments( ptr_pointer )
  return ccall((:PSRStudy_getCollectionDemandSegments, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionPlants( ptr_pointer,tipo_usina )
  return ccall((:PSRStudy_getCollectionPlants, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,tipo_usina)
end
function PSRStudy_getCollectionInterconnections( ptr_pointer )
  return ccall((:PSRStudy_getCollectionInterconnections, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionLineReactors( ptr_pointer )
  return ccall((:PSRStudy_getCollectionLineReactors, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionSerieCapacitors( ptr_pointer )
  return ccall((:PSRStudy_getCollectionSerieCapacitors, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionTransformers3Winding( ptr_pointer )
  return ccall((:PSRStudy_getCollectionTransformers3Winding, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionFuels( ptr_pointer )
  return ccall((:PSRStudy_getCollectionFuels, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionFuelConsumptions( ptr_pointer )
  return ccall((:PSRStudy_getCollectionFuelConsumptions, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionFuelReservoirs( ptr_pointer )
  return ccall((:PSRStudy_getCollectionFuelReservoirs, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionFuelContracts( ptr_pointer )
  return ccall((:PSRStudy_getCollectionFuelContracts, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionMaintenanceData( ptr_pointer )
  return ccall((:PSRStudy_getCollectionMaintenanceData, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionGenerationConstraints( ptr_pointer )
  return ccall((:PSRStudy_getCollectionGenerationConstraints, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionReserveGenerationConstraints( ptr_pointer )
  return ccall((:PSRStudy_getCollectionReserveGenerationConstraints, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionReserveConstraints( ptr_pointer )
  return ccall((:PSRStudy_getCollectionReserveConstraints, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionGaugingStations( ptr_pointer )
  return ccall((:PSRStudy_getCollectionGaugingStations, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getCollectionThermalCombinedCycles( ptr_pointer )
  return ccall((:PSRStudy_getCollectionThermalCombinedCycles, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getGrafConfiguration( ptr_pointer )
  return ccall((:PSRStudy_getGrafConfiguration, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRStudy_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRStudy_getConfigurationModel( ptr_pointer )
  return ccall((:PSRStudy_getConfigurationModel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_setConfigurationModel( ptr_pointer,model )
  return ccall((:PSRStudy_setConfigurationModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,model)
end
function PSRStudy_getVariableDurationModel( ptr_pointer )
  return ccall((:PSRStudy_getVariableDurationModel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_setVariableDurationModel( ptr_pointer,ptrModel )
  return ccall((:PSRStudy_setVariableDurationModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrModel)
end
function PSRStudy_extendVariableDurationHorizon( ptr_pointer )
  return ccall((:PSRStudy_extendVariableDurationHorizon, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getResults( ptr_pointer )
  return ccall((:PSRStudy_getResults, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_hydrology( ptr_pointer )
  return ccall((:PSRStudy_hydrology, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_expansionData( ptr_pointer )
  return ccall((:PSRStudy_expansionData, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_financialData( ptr_pointer )
  return ccall((:PSRStudy_financialData, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_environmentalData( ptr_pointer )
  return ccall((:PSRStudy_environmentalData, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_listConstraintSumCircuits( ptr_pointer )
  return ccall((:PSRStudy_listConstraintSumCircuits, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_listConstraintBuses( ptr_pointer )
  return ccall((:PSRStudy_listConstraintBuses, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_listConstraintSumInterconnections( ptr_pointer )
  return ccall((:PSRStudy_listConstraintSumInterconnections, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_listConstraintElectrical( ptr_pointer )
  return ccall((:PSRStudy_listConstraintElectrical, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_listConstraintReserves( ptr_pointer )
  return ccall((:PSRStudy_listConstraintReserves, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_listGenericConstraints( ptr_pointer )
  return ccall((:PSRStudy_listGenericConstraints, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_listHourlyScenario( ptr_pointer )
  return ccall((:PSRStudy_listHourlyScenario, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getSensitivity( ptr_pointer )
  return ccall((:PSRStudy_getSensitivity, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getSensitivity2( ptr_pointer,keyword )
  return ccall((:PSRStudy_getSensitivity2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,keyword)
end
function PSRStudy_getSensitivityFactor( ptr_pointer,ptrSensitivity )
  return ccall((:PSRStudy_getSensitivityFactor, "PSRClasses"),Float64,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSensitivity)
end
function PSRStudy_getSensitivitySystemCode( ptr_pointer,ptrSensitivity )
  return ccall((:PSRStudy_getSensitivitySystemCode, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSensitivity)
end
function PSRStudy_setSensitivity( ptr_pointer,Sensitivity )
  return ccall((:PSRStudy_setSensitivity, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,Sensitivity)
end
function PSRStudy_getStagesPerYear( ptr_pointer )
  return ccall((:PSRStudy_getStagesPerYear, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getStageId( ptr_pointer,width )
  retPtr = "                                                                                                                  "
  ccall((:PSRStudy_getStageId, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,width,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRStudy_getStageType( ptr_pointer )
  return ccall((:PSRStudy_getStageType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getDateFromStage( ptr_pointer,etapa )
  return ccall((:PSRStudy_getDateFromStage, "PSRClasses"),Int64,(Ptr{UInt8}, Int32,),ptr_pointer ,etapa)
end
function PSRStudy_getStageFromDate( ptr_pointer,data_etapa )
  return ccall((:PSRStudy_getStageFromDate, "PSRClasses"),Int32,(Ptr{UInt8}, Int64,),ptr_pointer ,data_etapa)
end
function PSRStudy_getLoadLevelDuration( ptr_pointer,Stage, block )
  return ccall((:PSRStudy_getLoadLevelDuration, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,Stage,block)
end
function PSRStudy_getLoadLevelRatio( ptr_pointer,Stage, block )
  return ccall((:PSRStudy_getLoadLevelRatio, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,Stage,block)
end
function PSRStudy_getStageDuration( ptr_pointer,stage, block )
  return ccall((:PSRStudy_getStageDuration, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,block)
end
function PSRStudy_getStageDuration2( ptr_pointer,etapa )
  return ccall((:PSRStudy_getStageDuration2, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,etapa)
end
function PSRStudy_getStageDuration3( ptr_pointer,data_etapa )
  return ccall((:PSRStudy_getStageDuration3, "PSRClasses"),Float64,(Ptr{UInt8}, Int64,),ptr_pointer ,data_etapa)
end
function PSRStudy_getStageDurationFactor( ptr_pointer,Stage, block )
  return ccall((:PSRStudy_getStageDurationFactor, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,Stage,block)
end
function PSRStudy_getStageDurationBlock( ptr_pointer,data_etapa, block )
  return ccall((:PSRStudy_getStageDurationBlock, "PSRClasses"),Float64,(Ptr{UInt8}, Int64, Int32,),ptr_pointer ,data_etapa,block)
end
function PSRStudy_getVariableStageDuration( ptr_pointer,data_etapa, bloco )
  return ccall((:PSRStudy_getVariableStageDuration, "PSRClasses"),Float64,(Ptr{UInt8}, Int64, Int32,),ptr_pointer ,data_etapa,bloco)
end
function PSRStudy_getVariableStageDuration2( ptr_pointer,stage, bloco )
  return ccall((:PSRStudy_getVariableStageDuration2, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,bloco)
end
function PSRStudy_getNumberStages( ptr_pointer )
  return ccall((:PSRStudy_getNumberStages, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getNumberBlocks( ptr_pointer )
  return ccall((:PSRStudy_getNumberBlocks, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getNumberSimulations( ptr_pointer )
  return ccall((:PSRStudy_getNumberSimulations, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getNumberOpenings( ptr_pointer )
  return ccall((:PSRStudy_getNumberOpenings, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_getBlockFromStageHour( ptr_pointer,stage, hour )
  return ccall((:PSRStudy_getBlockFromStageHour, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,stage,hour)
end
function PSRStudy_getInterconnectionNetwork( ptr_pointer )
  return ccall((:PSRStudy_getInterconnectionNetwork, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_setInterconnectionNetwork( ptr_pointer,Network )
  return ccall((:PSRStudy_setInterconnectionNetwork, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,Network)
end
function PSRStudy_getCVaRConstraints( ptr_pointer )
  return ccall((:PSRStudy_getCVaRConstraints, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_setCVaRConstraints( ptr_pointer,CVaR )
  return ccall((:PSRStudy_setCVaRConstraints, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,CVaR)
end
function PSRStudy_typicalData( ptr_pointer )
  return ccall((:PSRStudy_typicalData, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_serialize( ptr_pointer )
  return ccall((:PSRStudy_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStudy_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRStudy_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRStudy_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRStudy_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRStudy_deleteInstance( ptr_pointer )
  return ccall((:PSRStudy_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSensitivity_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRSensitivity_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRSensitivity_classType( ptr_pointer )
  return ccall((:PSRSensitivity_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSensitivity_isClassType( ptr_pointer,class_type )
  return ccall((:PSRSensitivity_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRSensitivity_deleteInstance( ptr_pointer )
  return ccall((:PSRSensitivity_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSensitivity_create(iprt)
  ptr_pointer =  ccall((:PSRSensitivity_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRLogBufferedSimpleConsole_warning( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogBufferedSimpleConsole_warning, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogBufferedSimpleConsole_error( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogBufferedSimpleConsole_error, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogBufferedSimpleConsole_info( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogBufferedSimpleConsole_info, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogBufferedSimpleConsole_showWarningBuffer( ptr_pointer )
  return ccall((:PSRLogBufferedSimpleConsole_showWarningBuffer, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLogBufferedSimpleConsole_showErrorBuffer( ptr_pointer )
  return ccall((:PSRLogBufferedSimpleConsole_showErrorBuffer, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLogBufferedSimpleConsole_showInfoBuffer( ptr_pointer )
  return ccall((:PSRLogBufferedSimpleConsole_showInfoBuffer, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLogBufferedSimpleConsole_deleteInstance( ptr_pointer )
  return ccall((:PSRLogBufferedSimpleConsole_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLogBufferedSimpleConsole_create(iprt)
  ptr_pointer =  ccall((:PSRLogBufferedSimpleConsole_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRParmStringPointer_create(iprt,id, data)
  ptr_pointer =  ccall((:PSRParmStringPointer_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL,id,data)
  return ptr_pointer
end
function PSRParmStringPointer_setData( ptr_pointer,data )
  return ccall((:PSRParmStringPointer_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParmStringPointer_getString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmStringPointer_getString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmStringPointer_toString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmStringPointer_toString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmStringPointer_setDataFromString( ptr_pointer,data )
  return ccall((:PSRParmStringPointer_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParmStringPointer_setDefault( ptr_pointer )
  return ccall((:PSRParmStringPointer_setDefault, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmStringPointer_clone( ptr_pointer )
  return ccall((:PSRParmStringPointer_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmStringPointer_copyTo( ptr_pointer,TargetParm )
  return ccall((:PSRParmStringPointer_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,TargetParm)
end
function PSRParmStringPointer_serialize( ptr_pointer )
  return ccall((:PSRParmStringPointer_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmStringPointer_buildFrom( ptr_pointer,ptrMessageDataParm )
  return ccall((:PSRParmStringPointer_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataParm)
end
function PSRParmStringPointer_deleteInstance( ptr_pointer )
  return ccall((:PSRParmStringPointer_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANGeneratorInfoBin_load( ptr_pointer,ptrEstudo, idvetor, nomeHeader, nomeData )
  return ccall((:PSRIONETPLANGeneratorInfoBin_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,idvetor,nomeHeader,nomeData)
end
function PSRIONETPLANGeneratorInfoBin_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANGeneratorInfoBin_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANGeneratorInfoBin_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANGeneratorInfoBin_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRParmReal_create(iprt,id, data)
  ptr_pointer =  ccall((:PSRParmReal_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Float64,),C_NULL,id,data)
  return ptr_pointer
end
function PSRParmReal_setData( ptr_pointer,data )
  return ccall((:PSRParmReal_setData, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,data)
end
function PSRParmReal_getReal( ptr_pointer )
  return ccall((:PSRParmReal_getReal, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmReal_setDefault( ptr_pointer )
  return ccall((:PSRParmReal_setDefault, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRParmReal_toString( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRParmReal_toString, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRParmReal_setDataFromString( ptr_pointer,data )
  return ccall((:PSRParmReal_setDataFromString, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,data)
end
function PSRParmReal_getPointer( ptr_pointer )
  return ccall((:PSRParmReal_getPointer, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmReal_clone( ptr_pointer )
  return ccall((:PSRParmReal_clone, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmReal_copyTo( ptr_pointer,TargetParm )
  return ccall((:PSRParmReal_copyTo, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,TargetParm)
end
function PSRParmReal_serialize( ptr_pointer )
  return ccall((:PSRParmReal_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRParmReal_buildFrom( ptr_pointer,ptrMessageDataParm )
  return ccall((:PSRParmReal_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataParm)
end
function PSRParmReal_deleteInstance( ptr_pointer )
  return ccall((:PSRParmReal_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGndGauginStationHourlyScenarios_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGndGauginStationHourlyScenarios_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGndGauginStationHourlyScenarios_type( ptr_pointer )
  return ccall((:PSRIOSDDPGndGauginStationHourlyScenarios_type, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGndGauginStationHourlyScenarios_createReferenceVectors( ptr_pointer )
  return ccall((:PSRIOSDDPGndGauginStationHourlyScenarios_createReferenceVectors, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGndGauginStationHourlyScenarios_isControlling( ptr_pointer,class_type, attribute_name )
  return ccall((:PSRIOSDDPGndGauginStationHourlyScenarios_isControlling, "PSRClasses"),Bool,(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,class_type,attribute_name)
end
function PSRIOSDDPGndGauginStationHourlyScenarios_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGndGauginStationHourlyScenarios_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMaintenanceData_create(iprt)
  ptr_pointer =  ccall((:PSRMaintenanceData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMaintenanceData_create2( iprt,ptrElement, _vetorOrigin, _tipoManutencao, _tipoAcao )
  ptr_pointer =  ccall((:PSRMaintenanceData_create2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Int32,),C_NULL,ptrElement,_vetorOrigin,_tipoManutencao,_tipoAcao)
  return ptr_pointer
end
function PSRMaintenanceData_setConfiguration( ptr_pointer,_tipoManutencao, _tipoAcao )
  return ccall((:PSRMaintenanceData_setConfiguration, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,_tipoManutencao,_tipoAcao)
end
function PSRMaintenanceData_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRMaintenanceData_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRMaintenanceData_classType( ptr_pointer )
  return ccall((:PSRMaintenanceData_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMaintenanceData_isClassType( ptr_pointer,class_type )
  return ccall((:PSRMaintenanceData_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRMaintenanceData_maintenanceType( ptr_pointer )
  return ccall((:PSRMaintenanceData_maintenanceType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMaintenanceData_actionType( ptr_pointer )
  return ccall((:PSRMaintenanceData_actionType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRMaintenanceData_hasDataForYear( ptr_pointer,year )
  return ccall((:PSRMaintenanceData_hasDataForYear, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,year)
end
function PSRMaintenanceData_setElement( ptr_pointer,ptrElement )
  return ccall((:PSRMaintenanceData_setElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRMaintenanceData_element( ptr_pointer )
  return ccall((:PSRMaintenanceData_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRMaintenanceData_vectorOrigin( ptr_pointer )
  return ccall((:PSRMaintenanceData_vectorOrigin, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRMaintenanceData_updateData( ptr_pointer )
  return ccall((:PSRMaintenanceData_updateData, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMaintenanceData_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRMaintenanceData_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRMaintenanceData_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRMaintenanceData_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRMaintenanceData_serialize( ptr_pointer )
  return ccall((:PSRMaintenanceData_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRMaintenanceData_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRMaintenanceData_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRMaintenanceData_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRMaintenanceData_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRMaintenanceData_deleteInstance( ptr_pointer )
  return ccall((:PSRMaintenanceData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelContract_create(iprt)
  ptr_pointer =  ccall((:PSRFuelContract_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRFuelContract_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRFuelContract_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRFuelContract_classType( ptr_pointer )
  return ccall((:PSRFuelContract_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelContract_isClassType( ptr_pointer,class_type )
  return ccall((:PSRFuelContract_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRFuelContract_code( ptr_pointer )
  return ccall((:PSRFuelContract_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelContract_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRFuelContract_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRFuelContract_setCode( ptr_pointer,_code )
  return ccall((:PSRFuelContract_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_code)
end
function PSRFuelContract_setName( ptr_pointer,_name )
  return ccall((:PSRFuelContract_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_name)
end
function PSRFuelContract_fuel( ptr_pointer )
  return ccall((:PSRFuelContract_fuel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelContract_setFuel( ptr_pointer,ptrFuel )
  return ccall((:PSRFuelContract_setFuel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrFuel)
end
function PSRFuelContract_maxPlant( ptr_pointer )
  return ccall((:PSRFuelContract_maxPlant, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelContract_plant( ptr_pointer,index )
  return ccall((:PSRFuelContract_plant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRFuelContract_addPlant( ptr_pointer,ptrThermalPlant )
  return ccall((:PSRFuelContract_addPlant, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrThermalPlant)
end
function PSRFuelContract_maxReservoir( ptr_pointer )
  return ccall((:PSRFuelContract_maxReservoir, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelContract_reservoir( ptr_pointer,index )
  return ccall((:PSRFuelContract_reservoir, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRFuelContract_addReservoir( ptr_pointer,ptrFuelReservoir )
  return ccall((:PSRFuelContract_addReservoir, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrFuelReservoir)
end
function PSRFuelContract_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRFuelContract_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRFuelContract_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRFuelContract_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRFuelContract_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRFuelContract_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRFuelContract_serialize( ptr_pointer )
  return ccall((:PSRFuelContract_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelContract_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRFuelContract_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRFuelContract_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRFuelContract_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRFuelContract_deleteInstance( ptr_pointer )
  return ccall((:PSRFuelContract_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelReservoir_create(iprt)
  ptr_pointer =  ccall((:PSRFuelReservoir_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRFuelReservoir_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRFuelReservoir_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRFuelReservoir_classType( ptr_pointer )
  return ccall((:PSRFuelReservoir_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelReservoir_isClassType( ptr_pointer,class_type )
  return ccall((:PSRFuelReservoir_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRFuelReservoir_code( ptr_pointer )
  return ccall((:PSRFuelReservoir_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelReservoir_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRFuelReservoir_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRFuelReservoir_setCode( ptr_pointer,_code )
  return ccall((:PSRFuelReservoir_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_code)
end
function PSRFuelReservoir_setName( ptr_pointer,_name )
  return ccall((:PSRFuelReservoir_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_name)
end
function PSRFuelReservoir_fuel( ptr_pointer )
  return ccall((:PSRFuelReservoir_fuel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelReservoir_setFuel( ptr_pointer,ptrFuel )
  return ccall((:PSRFuelReservoir_setFuel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrFuel)
end
function PSRFuelReservoir_maxPlant( ptr_pointer )
  return ccall((:PSRFuelReservoir_maxPlant, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelReservoir_plant( ptr_pointer,index )
  return ccall((:PSRFuelReservoir_plant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRFuelReservoir_addPlant( ptr_pointer,ptrThermalPlant )
  return ccall((:PSRFuelReservoir_addPlant, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrThermalPlant)
end
function PSRFuelReservoir_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRFuelReservoir_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRFuelReservoir_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRFuelReservoir_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRFuelReservoir_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRFuelReservoir_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRFuelReservoir_serialize( ptr_pointer )
  return ccall((:PSRFuelReservoir_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelReservoir_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRFuelReservoir_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRFuelReservoir_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRFuelReservoir_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRFuelReservoir_deleteInstance( ptr_pointer )
  return ccall((:PSRFuelReservoir_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataCommand_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataCommand_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataCommand_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataCommand_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataElement_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataElement_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataElement_setElement( ptr_pointer,_ptrElement )
  return ccall((:PSRMessageDataElement_setElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrElement)
end
function PSRMessageDataElement_getElement( ptr_pointer )
  return ccall((:PSRMessageDataElement_getElement, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataElement_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataElement_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataGenericGrouping_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataGenericGrouping_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataGenericGrouping_setGroup( ptr_pointer,ptrGenericGrouping )
  return ccall((:PSRMessageDataGenericGrouping_setGroup, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGenericGrouping)
end
function PSRMessageDataGenericGrouping_getGroup( ptr_pointer )
  return ccall((:PSRMessageDataGenericGrouping_getGroup, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataGenericGrouping_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataGenericGrouping_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDP_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDP_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDP_load( ptr_pointer,ptrEstudo, nomePath, hidroPath, sddp_version )
  return ccall((:PSRIOSDDP_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrEstudo,nomePath,hidroPath,sddp_version)
end
function PSRIOSDDP_loadV10( ptr_pointer,ptrEstudo, nomePath, hidroPath, sddp_version )
  return ccall((:PSRIOSDDP_loadV10, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrEstudo,nomePath,hidroPath,sddp_version)
end
function PSRIOSDDP_saveV12( ptr_pointer,ptrEstudo, nomePath )
  return ccall((:PSRIOSDDP_saveV12, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomePath)
end
function PSRIOSDDP_save( ptr_pointer,ptrEstudo, nomePath )
  return ccall((:PSRIOSDDP_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomePath)
end
function PSRIOSDDP_loadGrafConfiguration( ptr_pointer,ptrEstudo, nomePath )
  return ccall((:PSRIOSDDP_loadGrafConfiguration, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomePath)
end
function PSRIOSDDP_saveIndexGrf( ptr_pointer,ptrEstudo, nomePath )
  return ccall((:PSRIOSDDP_saveIndexGrf, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomePath)
end
function PSRIOSDDP_saveDadgerGrf( ptr_pointer,ptrEstudo, nomePath )
  return ccall((:PSRIOSDDP_saveDadgerGrf, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nomePath)
end
function PSRIOSDDP_getGndHourlyScenarios( ptr_pointer )
  return ccall((:PSRIOSDDP_getGndHourlyScenarios, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDP_getPowerInjectionScenarios( ptr_pointer )
  return ccall((:PSRIOSDDP_getPowerInjectionScenarios, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDP_getReserveGenerationScenarios( ptr_pointer )
  return ccall((:PSRIOSDDP_getReserveGenerationScenarios, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDP_setBlocks( ptr_pointer,numero )
  return ccall((:PSRIOSDDP_setBlocks, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,numero)
end
function PSRIOSDDP_setStageType( ptr_pointer,tipo )
  return ccall((:PSRIOSDDP_setStageType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,tipo)
end
function PSRIOSDDP_setFlagWriteDBUS( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_setFlagWriteDBUS, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useOnlySelectedSystems( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useOnlySelectedSystems, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useAll( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useAll, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useConfiguration( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useConfiguration, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useSystem( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useSystem, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useInterconnection( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useInterconnection, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useHydrology( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useHydrology, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useDemand( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useDemand, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useNetwork( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useNetwork, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useLoad( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useLoad, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useCircuits( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useCircuits, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useAreas( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useAreas, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_usePlant( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_usePlant, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useGndRegistry( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useGndRegistry, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useGndScenarios( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useGndScenarios, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useThermCESP( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useThermCESP, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useHourData( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useHourData, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useIndexedHydroParameters( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useIndexedHydroParameters, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_useGndAggregatedForward( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_useGndAggregatedForward, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_forceHourlyProfile( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_forceHourlyProfile, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_forceSpecificConsumptionPerSegment( ptr_pointer,flag )
  return ccall((:PSRIOSDDP_forceSpecificConsumptionPerSegment, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDP_hasLoadBusFiles( ptr_pointer,ptrStudy, pathdata )
  return ccall((:PSRIOSDDP_hasLoadBusFiles, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,pathdata)
end
function PSRIOSDDP_hasExternalScenario( ptr_pointer,ptrStudy, class_type )
  return ccall((:PSRIOSDDP_hasExternalScenario, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrStudy,class_type)
end
function PSRIOSDDP_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDP_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelConsumption_create(iprt,ptrPlant, ptrFuel)
  ptr_pointer =  ccall((:PSRFuelConsumption_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL,ptrPlant,ptrFuel)
  return ptr_pointer
end
function PSRFuelConsumption_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRFuelConsumption_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRFuelConsumption_classType( ptr_pointer )
  return ccall((:PSRFuelConsumption_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelConsumption_fuel( ptr_pointer )
  return ccall((:PSRFuelConsumption_fuel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelConsumption_plant( ptr_pointer )
  return ccall((:PSRFuelConsumption_plant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelConsumption_setRelationTo( ptr_pointer,ptrPlant, ptrFuel )
  return ccall((:PSRFuelConsumption_setRelationTo, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrPlant,ptrFuel)
end
function PSRFuelConsumption_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRFuelConsumption_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRFuelConsumption_serialize( ptr_pointer )
  return ccall((:PSRFuelConsumption_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRFuelConsumption_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRFuelConsumption_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRFuelConsumption_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRFuelConsumption_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRFuelConsumption_deleteInstance( ptr_pointer )
  return ccall((:PSRFuelConsumption_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataVector_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataVector_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataVector_setVector( ptr_pointer,_ptrVector )
  return ccall((:PSRMessageDataVector_setVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrVector)
end
function PSRMessageDataVector_getVector( ptr_pointer )
  return ccall((:PSRMessageDataVector_getVector, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataVector_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataVector_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataVectorDimension_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataVectorDimension_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataVectorDimension_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataVectorDimension_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionProject_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionProject_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExpansionProject_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionProject_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionProject_classType( ptr_pointer )
  return ccall((:PSRExpansionProject_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionProject_isClassType( ptr_pointer,class_type )
  return ccall((:PSRExpansionProject_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRExpansionProject_getGenericIdentification( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionProject_getGenericIdentification, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionProject_code( ptr_pointer )
  return ccall((:PSRExpansionProject_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionProject_setCode( ptr_pointer,code )
  return ccall((:PSRExpansionProject_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRExpansionProject_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionProject_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionProject_setName( ptr_pointer,_name )
  return ccall((:PSRExpansionProject_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_name)
end
function PSRExpansionProject_element( ptr_pointer )
  return ccall((:PSRExpansionProject_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionProject_setUnitElement( ptr_pointer,ptrElement )
  return ccall((:PSRExpansionProject_setUnitElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRExpansionProject_setUnitExistentElement( ptr_pointer,ptrElement )
  return ccall((:PSRExpansionProject_setUnitExistentElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRExpansionProject_elementType( ptr_pointer )
  return ccall((:PSRExpansionProject_elementType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionProject_elementCode( ptr_pointer )
  return ccall((:PSRExpansionProject_elementCode, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionProject_elementName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionProject_elementName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionProject_disbursement( ptr_pointer )
  return ccall((:PSRExpansionProject_disbursement, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionProject_setDisbursement( ptr_pointer,ptrDisbursement )
  return ccall((:PSRExpansionProject_setDisbursement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDisbursement)
end
function PSRExpansionProject_hasExistentElement( ptr_pointer )
  return ccall((:PSRExpansionProject_hasExistentElement, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionProject_existentElementCode( ptr_pointer )
  return ccall((:PSRExpansionProject_existentElementCode, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionProject_existentElementName( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionProject_existentElementName, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionProject_existentElement( ptr_pointer )
  return ccall((:PSRExpansionProject_existentElement, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionProject_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRExpansionProject_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRExpansionProject_serialize( ptr_pointer )
  return ccall((:PSRExpansionProject_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionProject_buildFrom( ptr_pointer,ptrMessage )
  return ccall((:PSRExpansionProject_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessage)
end
function PSRExpansionProject_buildRelationShipsFrom( ptr_pointer,ptrMessage )
  return ccall((:PSRExpansionProject_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessage)
end
function PSRExpansionProject_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRExpansionProject_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRExpansionProject_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionProject_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLogDataBase_create(iprt)
  ptr_pointer =  ccall((:PSRLogDataBase_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRLogDataBase_warning( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogDataBase_warning, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogDataBase_error( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogDataBase_error, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogDataBase_info( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogDataBase_info, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogDataBase_hasError( ptr_pointer )
  return ccall((:PSRLogDataBase_hasError, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRLogDataBase_nextError( ptr_pointer,classe, codigo )
  return ccall((:PSRLogDataBase_nextError, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{Int32}, Ptr{Int32},),ptr_pointer ,classe,codigo)
end
function PSRLogDataBase_getMessage( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRLogDataBase_getMessage, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRLogDataBase_deleteInstance( ptr_pointer )
  return ccall((:PSRLogDataBase_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenConfiguration_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenConfiguration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenConfiguration_load( ptr_pointer,ptrEstudo, nome, usingAuxFile )
  return ccall((:PSRIOOptgenConfiguration_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrEstudo,nome,usingAuxFile)
end
function PSRIOOptgenConfiguration_save( ptr_pointer,ptrEstudo, nome, usingAuxFile )
  return ccall((:PSRIOOptgenConfiguration_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrEstudo,nome,usingAuxFile)
end
function PSRIOOptgenConfiguration_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenConfiguration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasEmission_create(iprt)
  ptr_pointer =  ccall((:PSRGasEmission_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGasEmission_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGasEmission_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGasEmission_classType( ptr_pointer )
  return ccall((:PSRGasEmission_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasEmission_isClassType( ptr_pointer,class_type )
  return ccall((:PSRGasEmission_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRGasEmission_code( ptr_pointer )
  return ccall((:PSRGasEmission_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasEmission_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGasEmission_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGasEmission_setCode( ptr_pointer,_code )
  return ccall((:PSRGasEmission_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_code)
end
function PSRGasEmission_setName( ptr_pointer,_name )
  return ccall((:PSRGasEmission_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_name)
end
function PSRGasEmission_maxPlant( ptr_pointer )
  return ccall((:PSRGasEmission_maxPlant, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGasEmission_plant( ptr_pointer,index )
  return ccall((:PSRGasEmission_plant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRGasEmission_addPlant( ptr_pointer,ptrThermalPlant )
  return ccall((:PSRGasEmission_addPlant, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrThermalPlant)
end
function PSRGasEmission_getPlantIndex( ptr_pointer,ptrThermalPlant )
  return ccall((:PSRGasEmission_getPlantIndex, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrThermalPlant)
end
function PSRGasEmission_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRGasEmission_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRGasEmission_deleteInstance( ptr_pointer )
  return ccall((:PSRGasEmission_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLogSimpleConsole_warning( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogSimpleConsole_warning, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogSimpleConsole_error( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogSimpleConsole_error, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogSimpleConsole_info( ptr_pointer,classe, codigo, msg, file, line )
  return ccall((:PSRLogSimpleConsole_info, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,classe,codigo,msg,file,line)
end
function PSRLogSimpleConsole_deleteInstance( ptr_pointer )
  return ccall((:PSRLogSimpleConsole_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLogSimpleConsole_create(iprt)
  ptr_pointer =  ccall((:PSRLogSimpleConsole_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataGraphArc_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataGraphArc_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataGraphArc_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataGraphArc_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataIOHourlyScenario_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataIOHourlyScenario_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataIOHourlyScenario_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataIOHourlyScenario_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRContingencyReserve_create(iprt)
  ptr_pointer =  ccall((:PSRContingencyReserve_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRContingencyReserve_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRContingencyReserve_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRContingencyReserve_classType( ptr_pointer )
  return ccall((:PSRContingencyReserve_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRContingencyReserve_isClassType( ptr_pointer,class_type )
  return ccall((:PSRContingencyReserve_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRContingencyReserve_code( ptr_pointer )
  return ccall((:PSRContingencyReserve_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRContingencyReserve_code2( ptr_pointer,_codigo )
  return ccall((:PSRContingencyReserve_code2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRContingencyReserve_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRContingencyReserve_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRContingencyReserve_name2( ptr_pointer,_nome )
  return ccall((:PSRContingencyReserve_name2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRContingencyReserve_element( ptr_pointer )
  return ccall((:PSRContingencyReserve_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRContingencyReserve_setElement( ptr_pointer,ptrElement )
  return ccall((:PSRContingencyReserve_setElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRContingencyReserve_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRContingencyReserve_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRContingencyReserve_serialize( ptr_pointer )
  return ccall((:PSRContingencyReserve_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRContingencyReserve_buildFrom( ptr_pointer,ptrMessageDataElement )
  return ccall((:PSRContingencyReserve_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataElement)
end
function PSRContingencyReserve_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRContingencyReserve_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRContingencyReserve_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRContingencyReserve_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRContingencyReserve_deleteInstance( ptr_pointer )
  return ccall((:PSRContingencyReserve_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataGraph_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataGraph_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataGraph_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataGraph_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGndHourlyScenarios_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGndHourlyScenarios_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGndHourlyScenarios_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGndHourlyScenarios_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataGraphGround_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataGraphGround_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataGraphGround_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataGraphGround_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMessageDataGraphNode_create(iprt)
  ptr_pointer =  ccall((:PSRMessageDataGraphNode_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMessageDataGraphNode_deleteInstance( ptr_pointer )
  return ccall((:PSRMessageDataGraphNode_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRContract_create(iprt)
  ptr_pointer =  ccall((:PSRContract_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRContract_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRContract_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRContract_classType( ptr_pointer )
  return ccall((:PSRContract_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRContract_isClassType( ptr_pointer,class_type )
  return ccall((:PSRContract_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRContract_project( ptr_pointer )
  return ccall((:PSRContract_project, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRContract_setProject( ptr_pointer,ptrProject )
  return ccall((:PSRContract_setProject, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrProject)
end
function PSRContract_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRContract_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRContract_deleteInstance( ptr_pointer )
  return ccall((:PSRContract_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintData_create(iprt)
  ptr_pointer =  ccall((:PSRConstraintData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConstraintData_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRConstraintData_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRConstraintData_classType( ptr_pointer )
  return ccall((:PSRConstraintData_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintData_isClassType( ptr_pointer,class_type )
  return ccall((:PSRConstraintData_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRConstraintData_code( ptr_pointer )
  return ccall((:PSRConstraintData_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintData_code2( ptr_pointer,_codigo )
  return ccall((:PSRConstraintData_code2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRConstraintData_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRConstraintData_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRConstraintData_name2( ptr_pointer,_nome )
  return ccall((:PSRConstraintData_name2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRConstraintData_getConstraintList( ptr_pointer )
  return ccall((:PSRConstraintData_getConstraintList, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintData_penalty( ptr_pointer )
  return ccall((:PSRConstraintData_penalty, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintData_penalty2( ptr_pointer,_penalidade )
  return ccall((:PSRConstraintData_penalty2, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,_penalidade)
end
function PSRConstraintData_maxPlant( ptr_pointer )
  return ccall((:PSRConstraintData_maxPlant, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintData_plant( ptr_pointer,index )
  return ccall((:PSRConstraintData_plant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRConstraintData_getPlant( ptr_pointer,tipoUsina, codigo )
  return ccall((:PSRConstraintData_getPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,tipoUsina,codigo)
end
function PSRConstraintData_addPlant( ptr_pointer,ptrUsina )
  return ccall((:PSRConstraintData_addPlant, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrUsina)
end
function PSRConstraintData_hasPlant( ptr_pointer,ptrUsina )
  return ccall((:PSRConstraintData_hasPlant, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrUsina)
end
function PSRConstraintData_maxBatteries( ptr_pointer )
  return ccall((:PSRConstraintData_maxBatteries, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintData_battery( ptr_pointer,index )
  return ccall((:PSRConstraintData_battery, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRConstraintData_getBattery( ptr_pointer,code )
  return ccall((:PSRConstraintData_getBattery, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRConstraintData_addBattery( ptr_pointer,ptrBattery )
  return ccall((:PSRConstraintData_addBattery, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBattery)
end
function PSRConstraintData_hasBattery( ptr_pointer,ptrBattery )
  return ccall((:PSRConstraintData_hasBattery, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBattery)
end
function PSRConstraintData_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRConstraintData_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRConstraintData_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRConstraintData_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRConstraintData_serialize( ptr_pointer )
  return ccall((:PSRConstraintData_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintData_buildFrom( ptr_pointer,ptrMessageDataElement )
  return ccall((:PSRConstraintData_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataElement)
end
function PSRConstraintData_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRConstraintData_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRConstraintData_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRConstraintData_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRConstraintData_deleteInstance( ptr_pointer )
  return ccall((:PSRConstraintData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPDemandHourlyScenarios_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPDemandHourlyScenarios_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPDemandHourlyScenarios_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPDemandHourlyScenarios_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMapDimensionedVector_pullToMemory( ptr_pointer,ptrElement )
  return ccall((:PSRMapDimensionedVector_pullToMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRMapDimensionedVector_deleteInstance( ptr_pointer )
  return ccall((:PSRMapDimensionedVector_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMapDimensionedVector_create(iprt)
  ptr_pointer =  ccall((:PSRMapDimensionedVector_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRMapDimensionedParm_pullToMemory( ptr_pointer,ptrElement )
  return ccall((:PSRMapDimensionedParm_pullToMemory, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRMapDimensionedParm_deleteInstance( ptr_pointer )
  return ccall((:PSRMapDimensionedParm_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRMapDimensionedParm_create(iprt)
  ptr_pointer =  ccall((:PSRMapDimensionedParm_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSimpleStructuredFile_create(iprt)
  ptr_pointer =  ccall((:PSRIOSimpleStructuredFile_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSimpleStructuredFile_mapTo2( ptr_pointer,values )
  return ccall((:PSRIOSimpleStructuredFile_mapTo2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{Float64},),ptr_pointer ,values)
end
function PSRIOSimpleStructuredFile_countNotMapped( ptr_pointer )
  return ccall((:PSRIOSimpleStructuredFile_countNotMapped, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSimpleStructuredFile_getNotMapped( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRIOSimpleStructuredFile_getNotMapped, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIOSimpleStructuredFile_seekStage( ptr_pointer,stage, hour, simulation )
  return ccall((:PSRIOSimpleStructuredFile_seekStage, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,stage,hour,simulation)
end
function PSRIOSimpleStructuredFile_getFileStagesInformation( ptr_pointer )
  return ccall((:PSRIOSimpleStructuredFile_getFileStagesInformation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSimpleStructuredFile_getFileAgentsInformation( ptr_pointer )
  return ccall((:PSRIOSimpleStructuredFile_getFileAgentsInformation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSimpleStructuredFile_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSimpleStructuredFile_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenYearHorizon_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenYearHorizon_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenYearHorizon_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenYearHorizon_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenYearHorizon_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenYearHorizon_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConstraintSumData_create(iprt)
  ptr_pointer =  ccall((:PSRConstraintSumData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConstraintSumData_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRConstraintSumData_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRConstraintSumData_classType( ptr_pointer )
  return ccall((:PSRConstraintSumData_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintSumData_isClassType( ptr_pointer,class_type )
  return ccall((:PSRConstraintSumData_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRConstraintSumData_code( ptr_pointer )
  return ccall((:PSRConstraintSumData_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintSumData_code2( ptr_pointer,_codigo )
  return ccall((:PSRConstraintSumData_code2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRConstraintSumData_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRConstraintSumData_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRConstraintSumData_name2( ptr_pointer,_nome )
  return ccall((:PSRConstraintSumData_name2, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRConstraintSumData_getCoefficient( ptr_pointer,index )
  return ccall((:PSRConstraintSumData_getCoefficient, "PSRClasses"),Float64,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRConstraintSumData_getCoefficient2( ptr_pointer,ptrElement )
  return ccall((:PSRConstraintSumData_getCoefficient2, "PSRClasses"),Float64,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRConstraintSumData_setCoefficient( ptr_pointer,index, coef )
  return ccall((:PSRConstraintSumData_setCoefficient, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Float64,),ptr_pointer ,index,coef)
end
function PSRConstraintSumData_maxElement( ptr_pointer )
  return ccall((:PSRConstraintSumData_maxElement, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintSumData_element( ptr_pointer,index )
  return ccall((:PSRConstraintSumData_element, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRConstraintSumData_addElement( ptr_pointer,ptrElement, coef )
  return ccall((:PSRConstraintSumData_addElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Float64,),ptr_pointer ,ptrElement,coef)
end
function PSRConstraintSumData_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRConstraintSumData_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRConstraintSumData_serialize( ptr_pointer )
  return ccall((:PSRConstraintSumData_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRConstraintSumData_buildFrom( ptr_pointer,ptrMessageDataElement )
  return ccall((:PSRConstraintSumData_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataElement)
end
function PSRConstraintSumData_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRConstraintSumData_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRConstraintSumData_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRConstraintSumData_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRConstraintSumData_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRConstraintSumData_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRConstraintSumData_deleteInstance( ptr_pointer )
  return ccall((:PSRConstraintSumData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalPlant_create(iprt)
  ptr_pointer =  ccall((:PSRThermalPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRThermalPlant_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRThermalPlant_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRThermalPlant_classType( ptr_pointer )
  return ccall((:PSRThermalPlant_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalPlant_isClassType( ptr_pointer,class_type )
  return ccall((:PSRThermalPlant_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRThermalPlant_thermalType( ptr_pointer )
  return ccall((:PSRThermalPlant_thermalType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalPlant_setThermalType( ptr_pointer,_tipo_termica )
  return ccall((:PSRThermalPlant_setThermalType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_tipo_termica)
end
function PSRThermalPlant_removePlants( ptr_pointer )
  return ccall((:PSRThermalPlant_removePlants, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalPlant_maxPlants( ptr_pointer )
  return ccall((:PSRThermalPlant_maxPlants, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalPlant_thermalPlant( ptr_pointer,ndx )
  return ccall((:PSRThermalPlant_thermalPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRThermalPlant_addThermalPlant( ptr_pointer,ptrThermalPlant )
  return ccall((:PSRThermalPlant_addThermalPlant, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrThermalPlant)
end
function PSRThermalPlant_maxFuel( ptr_pointer )
  return ccall((:PSRThermalPlant_maxFuel, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalPlant_fuel( ptr_pointer,ndx )
  return ccall((:PSRThermalPlant_fuel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRThermalPlant_getFuel( ptr_pointer,codigo )
  return ccall((:PSRThermalPlant_getFuel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codigo)
end
function PSRThermalPlant_addFuel( ptr_pointer,ptrCombustivel )
  return ccall((:PSRThermalPlant_addFuel, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCombustivel)
end
function PSRThermalPlant_maxFuelConsumption( ptr_pointer )
  return ccall((:PSRThermalPlant_maxFuelConsumption, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalPlant_fuelConsumption( ptr_pointer,ndx )
  return ccall((:PSRThermalPlant_fuelConsumption, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRThermalPlant_getFuelComsumption( ptr_pointer,codigoFuel, codigoTermica )
  return ccall((:PSRThermalPlant_getFuelComsumption, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,codigoFuel,codigoTermica)
end
function PSRThermalPlant_addFuelComsumption( ptr_pointer,ptrConsumoCombustivel )
  return ccall((:PSRThermalPlant_addFuelComsumption, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrConsumoCombustivel)
end
function PSRThermalPlant_gasNode( ptr_pointer )
  return ccall((:PSRThermalPlant_gasNode, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalPlant_setGasNode( ptr_pointer,ptrGasNode )
  return ccall((:PSRThermalPlant_setGasNode, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGasNode)
end
function PSRThermalPlant_combinedCycle( ptr_pointer )
  return ccall((:PSRThermalPlant_combinedCycle, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalPlant_setCombinedCycle( ptr_pointer,ptrThermalCombinedCycle )
  return ccall((:PSRThermalPlant_setCombinedCycle, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrThermalCombinedCycle)
end
function PSRThermalPlant_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRThermalPlant_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRThermalPlant_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRThermalPlant_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRThermalPlant_serialize( ptr_pointer )
  return ccall((:PSRThermalPlant_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalPlant_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRThermalPlant_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRThermalPlant_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRThermalPlant_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRThermalPlant_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRThermalPlant_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRThermalPlant_getParent( ptr_pointer )
  return ccall((:PSRThermalPlant_getParent, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRThermalPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRThermalPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenerationConstraintData_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGenerationConstraintData_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGenerationConstraintData_classType( ptr_pointer )
  return ccall((:PSRGenerationConstraintData_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenerationConstraintData_isClassType( ptr_pointer,class_type )
  return ccall((:PSRGenerationConstraintData_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRGenerationConstraintData_deleteInstance( ptr_pointer )
  return ccall((:PSRGenerationConstraintData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenerationConstraintData_create(iprt)
  ptr_pointer =  ccall((:PSRGenerationConstraintData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIO_MIXROWDATA_useMask( ptr_pointer,_ptrIOMask )
  return ccall((:PSRIO_MIXROWDATA_useMask, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrIOMask)
end
function PSRIO_MIXROWDATA_loadfile( ptr_pointer,nome )
  return ccall((:PSRIO_MIXROWDATA_loadfile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIO_MIXROWDATA_savefile( ptr_pointer,nome )
  return ccall((:PSRIO_MIXROWDATA_savefile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIO_MIXROWDATA_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_MIXROWDATA_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_MIXROWDATA_create(iprt)
  ptr_pointer =  ccall((:PSRIO_MIXROWDATA_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRRegion_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRRegion_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRRegion_classType( ptr_pointer )
  return ccall((:PSRRegion_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRRegion_isClassType( ptr_pointer,class_type )
  return ccall((:PSRRegion_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRRegion_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRRegion_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRRegion_addBus( ptr_pointer,ptrBus )
  return ccall((:PSRRegion_addBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRRegion_deleteInstance( ptr_pointer )
  return ccall((:PSRRegion_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRRegion_create(iprt)
  ptr_pointer =  ccall((:PSRRegion_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRTransformer3Winding_create(iprt)
  ptr_pointer =  ccall((:PSRTransformer3Winding_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRTransformer3Winding_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRTransformer3Winding_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRTransformer3Winding_classType( ptr_pointer )
  return ccall((:PSRTransformer3Winding_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTransformer3Winding_isClassType( ptr_pointer,class_type )
  return ccall((:PSRTransformer3Winding_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRTransformer3Winding_bus( ptr_pointer,index )
  return ccall((:PSRTransformer3Winding_bus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRTransformer3Winding_connect( ptr_pointer,ptrBus1, ptrBus2, ptrBus3 )
  return ccall((:PSRTransformer3Winding_connect, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus1,ptrBus2,ptrBus3)
end
function PSRTransformer3Winding_busFicticious( ptr_pointer )
  return ccall((:PSRTransformer3Winding_busFicticious, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRTransformer3Winding_transformerFicticious( ptr_pointer,index )
  return ccall((:PSRTransformer3Winding_transformerFicticious, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRTransformer3Winding_setFicticious( ptr_pointer,bus_ficticious, transf_ficticiuous1, transf_ficticiuous2, transf_ficticiuous3 )
  return ccall((:PSRTransformer3Winding_setFicticious, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,bus_ficticious,transf_ficticiuous1,transf_ficticiuous2,transf_ficticiuous3)
end
function PSRTransformer3Winding_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRTransformer3Winding_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRTransformer3Winding_deleteInstance( ptr_pointer )
  return ccall((:PSRTransformer3Winding_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSubstation_deleteInstance( ptr_pointer )
  return ccall((:PSRSubstation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSubstation_create(iprt)
  ptr_pointer =  ccall((:PSRSubstation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRBusDC_create(iprt)
  ptr_pointer =  ccall((:PSRBusDC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRBusDC_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRBusDC_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRBusDC_classType( ptr_pointer )
  return ccall((:PSRBusDC_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBusDC_isClassType( ptr_pointer,class_type )
  return ccall((:PSRBusDC_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRBusDC_setLinkDC( ptr_pointer,ptrLinkDC )
  return ccall((:PSRBusDC_setLinkDC, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrLinkDC)
end
function PSRBusDC_getLinkDC( ptr_pointer )
  return ccall((:PSRBusDC_getLinkDC, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRBusDC_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRBusDC_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRBusDC_serialize( ptr_pointer )
  return ccall((:PSRBusDC_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRBusDC_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRBusDC_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRBusDC_deleteInstance( ptr_pointer )
  return ccall((:PSRBusDC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRReserveGenerationConstraintData_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRReserveGenerationConstraintData_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRReserveGenerationConstraintData_classType( ptr_pointer )
  return ccall((:PSRReserveGenerationConstraintData_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRReserveGenerationConstraintData_isClassType( ptr_pointer,class_type )
  return ccall((:PSRReserveGenerationConstraintData_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRReserveGenerationConstraintData_maxBackedElements( ptr_pointer )
  return ccall((:PSRReserveGenerationConstraintData_maxBackedElements, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRReserveGenerationConstraintData_backedElement( ptr_pointer,index )
  return ccall((:PSRReserveGenerationConstraintData_backedElement, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRReserveGenerationConstraintData_addBackedElement( ptr_pointer,ptrElement )
  return ccall((:PSRReserveGenerationConstraintData_addBackedElement, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRReserveGenerationConstraintData_serialize( ptr_pointer )
  return ccall((:PSRReserveGenerationConstraintData_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRReserveGenerationConstraintData_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRReserveGenerationConstraintData_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRReserveGenerationConstraintData_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRReserveGenerationConstraintData_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRReserveGenerationConstraintData_deleteInstance( ptr_pointer )
  return ccall((:PSRReserveGenerationConstraintData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRReserveGenerationConstraintData_create(iprt)
  ptr_pointer =  ccall((:PSRReserveGenerationConstraintData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRNetwork_create(iprt,_ptrArea)
  ptr_pointer =  ccall((:PSRNetwork_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_ptrArea)
  return ptr_pointer
end
function PSRNetwork_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRNetwork_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRNetwork_classType( ptr_pointer )
  return ccall((:PSRNetwork_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_isClassType( ptr_pointer,class_type )
  return ccall((:PSRNetwork_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRNetwork_graph( ptr_pointer )
  return ccall((:PSRNetwork_graph, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_area( ptr_pointer )
  return ccall((:PSRNetwork_area, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_configure( ptr_pointer )
  return ccall((:PSRNetwork_configure, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_island( ptr_pointer,index )
  return ccall((:PSRNetwork_island, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRNetwork_maxBusFromIsland( ptr_pointer,index_island )
  return ccall((:PSRNetwork_maxBusFromIsland, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,index_island)
end
function PSRNetwork_busFromIsland( ptr_pointer,index_island, index_bus )
  return ccall((:PSRNetwork_busFromIsland, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,index_island,index_bus)
end
function PSRNetwork_maxIsland( ptr_pointer )
  return ccall((:PSRNetwork_maxIsland, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_maxBus( ptr_pointer )
  return ccall((:PSRNetwork_maxBus, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_bus( ptr_pointer,index )
  return ccall((:PSRNetwork_bus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRNetwork_getBus( ptr_pointer,codBarra )
  return ccall((:PSRNetwork_getBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codBarra)
end
function PSRNetwork_getBus2( ptr_pointer,namebus )
  return ccall((:PSRNetwork_getBus2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,namebus)
end
function PSRNetwork_getBus3( ptr_pointer,codBarra, ptrSystem )
  return ccall((:PSRNetwork_getBus3, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,codBarra,ptrSystem)
end
function PSRNetwork_getBus4( ptr_pointer,namebus, ptrSystem )
  return ccall((:PSRNetwork_getBus4, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,namebus,ptrSystem)
end
function PSRNetwork_maxSerie( ptr_pointer )
  return ccall((:PSRNetwork_maxSerie, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_serie( ptr_pointer,index )
  return ccall((:PSRNetwork_serie, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRNetwork_getSerie( ptr_pointer,codserie, devicetype )
  return ccall((:PSRNetwork_getSerie, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,codserie,devicetype)
end
function PSRNetwork_getSerie2( ptr_pointer,name, devicetype )
  return ccall((:PSRNetwork_getSerie2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,name,devicetype)
end
function PSRNetwork_getSerie3( ptr_pointer,nome )
  return ccall((:PSRNetwork_getSerie3, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRNetwork_getSerie4( ptr_pointer,codBusFrom, codBusTo, circuitNumber )
  return ccall((:PSRNetwork_getSerie4, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,codBusFrom,codBusTo,circuitNumber)
end
function PSRNetwork_getSerie5( ptr_pointer,busFrom, busTo, circuitNumber )
  return ccall((:PSRNetwork_getSerie5, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,busFrom,busTo,circuitNumber)
end
function PSRNetwork_getShunt( ptr_pointer,codshunt, devicetype )
  return ccall((:PSRNetwork_getShunt, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,codshunt,devicetype)
end
function PSRNetwork_addBus( ptr_pointer,ptrBarra )
  return ccall((:PSRNetwork_addBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBarra)
end
function PSRNetwork_addShunt( ptr_pointer,ptrBarra, ptrShunt )
  return ccall((:PSRNetwork_addShunt, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBarra,ptrShunt)
end
function PSRNetwork_addSerie( ptr_pointer,ptrBarra01, ptrBarra02, ptrSerie )
  return ccall((:PSRNetwork_addSerie, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBarra01,ptrBarra02,ptrSerie)
end
function PSRNetwork_delSerie( ptr_pointer,ptrSerie )
  return ccall((:PSRNetwork_delSerie, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSerie)
end
function PSRNetwork_delShunt( ptr_pointer,ptrShunt )
  return ccall((:PSRNetwork_delShunt, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrShunt)
end
function PSRNetwork_delBus( ptr_pointer,ptrBus )
  return ccall((:PSRNetwork_delBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRNetwork_maxSubstations( ptr_pointer )
  return ccall((:PSRNetwork_maxSubstations, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_substation( ptr_pointer,index )
  return ccall((:PSRNetwork_substation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRNetwork_addSubstation( ptr_pointer,ptrSubstation )
  return ccall((:PSRNetwork_addSubstation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSubstation)
end
function PSRNetwork_getSubstationFromBus( ptr_pointer,ptrBus )
  return ccall((:PSRNetwork_getSubstationFromBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRNetwork_maxTransformer3Windings( ptr_pointer )
  return ccall((:PSRNetwork_maxTransformer3Windings, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_transformer3Winding( ptr_pointer,index )
  return ccall((:PSRNetwork_transformer3Winding, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRNetwork_addTransformer3Winding( ptr_pointer,ptrTransformer3Winding )
  return ccall((:PSRNetwork_addTransformer3Winding, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrTransformer3Winding)
end
function PSRNetwork_getTransformer3Winding( ptr_pointer,code )
  return ccall((:PSRNetwork_getTransformer3Winding, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRNetwork_getCollectionBuses( ptr_pointer,ptrCollection )
  return ccall((:PSRNetwork_getCollectionBuses, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection)
end
function PSRNetwork_getCollectionShunts( ptr_pointer,ptrCollection, tipo_devc )
  return ccall((:PSRNetwork_getCollectionShunts, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrCollection,tipo_devc)
end
function PSRNetwork_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRNetwork_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRNetwork_makeEquivalentSeries( ptr_pointer )
  return ccall((:PSRNetwork_makeEquivalentSeries, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_sort( ptr_pointer,order_type )
  return ccall((:PSRNetwork_sort, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,order_type)
end
function PSRNetwork_setOrders( ptr_pointer )
  return ccall((:PSRNetwork_setOrders, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_addNetwork( ptr_pointer,ptrNetwork )
  return ccall((:PSRNetwork_addNetwork, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNetwork)
end
function PSRNetwork_delNetwork( ptr_pointer,ptrNetwork )
  return ccall((:PSRNetwork_delNetwork, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNetwork)
end
function PSRNetwork_serialize( ptr_pointer )
  return ccall((:PSRNetwork_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRNetwork_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRNetwork_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRNetwork_configureGraphFrom( ptr_pointer,ptrDataGraph )
  return ccall((:PSRNetwork_configureGraphFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataGraph)
end
function PSRNetwork_updateGraphFrom( ptr_pointer,ptrDataGraph )
  return ccall((:PSRNetwork_updateGraphFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataGraph)
end
function PSRNetwork_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRNetwork_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRNetwork_deleteInstance( ptr_pointer )
  return ccall((:PSRNetwork_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRBusSumData_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRBusSumData_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRBusSumData_classType( ptr_pointer )
  return ccall((:PSRBusSumData_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBusSumData_isClassType( ptr_pointer,class_type )
  return ccall((:PSRBusSumData_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRBusSumData_deleteInstance( ptr_pointer )
  return ccall((:PSRBusSumData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRBusSumData_create(iprt)
  ptr_pointer =  ccall((:PSRBusSumData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIO_ROWDATA_loadfile( ptr_pointer,nome )
  return ccall((:PSRIO_ROWDATA_loadfile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIO_ROWDATA_savefile( ptr_pointer,nome )
  return ccall((:PSRIO_ROWDATA_savefile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIO_ROWDATA_useMask( ptr_pointer,_ptrIOMask )
  return ccall((:PSRIO_ROWDATA_useMask, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrIOMask)
end
function PSRIO_ROWDATA_setDefaultModelId( ptr_pointer,modelid )
  return ccall((:PSRIO_ROWDATA_setDefaultModelId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,modelid)
end
function PSRIO_ROWDATA_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_ROWDATA_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_ROWDATA_create(iprt)
  ptr_pointer =  ccall((:PSRIO_ROWDATA_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExpansionCapacity_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionCapacity_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExpansionCapacity_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionCapacity_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionCapacity_classType( ptr_pointer )
  return ccall((:PSRExpansionCapacity_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionCapacity_isClassType( ptr_pointer,class_type )
  return ccall((:PSRExpansionCapacity_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRExpansionCapacity_existents( ptr_pointer )
  return ccall((:PSRExpansionCapacity_existents, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionCapacity_serialize( ptr_pointer )
  return ccall((:PSRExpansionCapacity_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionCapacity_buildFrom( ptr_pointer,ptrMessage )
  return ccall((:PSRExpansionCapacity_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessage)
end
function PSRExpansionCapacity_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRExpansionCapacity_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRExpansionCapacity_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionCapacity_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenericConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRGenericConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGenericConstraint_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGenericConstraint_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGenericConstraint_classType( ptr_pointer )
  return ccall((:PSRGenericConstraint_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenericConstraint_isClassType( ptr_pointer,class_type )
  return ccall((:PSRGenericConstraint_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRGenericConstraint_setSystem( ptr_pointer,ptrSystem )
  return ccall((:PSRGenericConstraint_setSystem, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRGenericConstraint_getSystem( ptr_pointer )
  return ccall((:PSRGenericConstraint_getSystem, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGenericConstraint_getVariable( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRGenericConstraint_getVariable, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGenericConstraint_getUnit( ptr_pointer,index )
  retPtr = "                                                                                                                  "
  ccall((:PSRGenericConstraint_getUnit, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,Ptr{UInt8},),ptr_pointer ,index,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGenericConstraint_addElementWithVariable( ptr_pointer,ptrElement, coef, variable, unit )
  return ccall((:PSRGenericConstraint_addElementWithVariable, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Float64, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement,coef,variable,unit)
end
function PSRGenericConstraint_serialize( ptr_pointer )
  return ccall((:PSRGenericConstraint_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGenericConstraint_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRGenericConstraint_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGenericConstraint_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRGenericConstraint_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRGenericConstraint_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRGenericConstraint_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGenericConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRGenericConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_create(iprt,_ptrEstudo)
  ptr_pointer =  ccall((:PSRSystem_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_ptrEstudo)
  return ptr_pointer
end
function PSRSystem_study( ptr_pointer )
  return ccall((:PSRSystem_study, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_setStudy( ptr_pointer,_ptrStudy )
  return ccall((:PSRSystem_setStudy, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrStudy)
end
function PSRSystem_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRSystem_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRSystem_classType( ptr_pointer )
  return ccall((:PSRSystem_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_isClassType( ptr_pointer,class_type )
  return ccall((:PSRSystem_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRSystem_getGenericIdentification( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRSystem_getGenericIdentification, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRSystem_code( ptr_pointer )
  return ccall((:PSRSystem_code, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_name( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRSystem_name, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRSystem_id( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRSystem_id, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRSystem_setCode( ptr_pointer,_codigo )
  return ccall((:PSRSystem_setCode, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_codigo)
end
function PSRSystem_setName( ptr_pointer,_nome )
  return ccall((:PSRSystem_setName, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_nome)
end
function PSRSystem_setId( ptr_pointer,id )
  return ccall((:PSRSystem_setId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,id)
end
function PSRSystem_removeChildThermalPlants( ptr_pointer )
  return ccall((:PSRSystem_removeChildThermalPlants, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_maxThermalPlant( ptr_pointer )
  return ccall((:PSRSystem_maxThermalPlant, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_thermalPlant( ptr_pointer,indexUsina )
  return ccall((:PSRSystem_thermalPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,indexUsina)
end
function PSRSystem_getThermalPlant( ptr_pointer,codUsina )
  return ccall((:PSRSystem_getThermalPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codUsina)
end
function PSRSystem_getThermalPlant2( ptr_pointer,nome )
  return ccall((:PSRSystem_getThermalPlant2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRSystem_addThermalPlant( ptr_pointer,ptrUsinaTermica )
  return ccall((:PSRSystem_addThermalPlant, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrUsinaTermica)
end
function PSRSystem_maxHydroPlant( ptr_pointer )
  return ccall((:PSRSystem_maxHydroPlant, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_hydroPlant( ptr_pointer,indexUsina )
  return ccall((:PSRSystem_hydroPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,indexUsina)
end
function PSRSystem_getHydroPlant( ptr_pointer,codUsina )
  return ccall((:PSRSystem_getHydroPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codUsina)
end
function PSRSystem_getHydroPlant2( ptr_pointer,nome )
  return ccall((:PSRSystem_getHydroPlant2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRSystem_addHydroPlant( ptr_pointer,ptrUsinaHidreletrica )
  return ccall((:PSRSystem_addHydroPlant, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrUsinaHidreletrica)
end
function PSRSystem_maxGndPlant( ptr_pointer )
  return ccall((:PSRSystem_maxGndPlant, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_gndPlant( ptr_pointer,indexUsina )
  return ccall((:PSRSystem_gndPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,indexUsina)
end
function PSRSystem_getGndPlant( ptr_pointer,codUsina )
  return ccall((:PSRSystem_getGndPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codUsina)
end
function PSRSystem_getGndPlant2( ptr_pointer,nome )
  return ccall((:PSRSystem_getGndPlant2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRSystem_addGndPlant( ptr_pointer,ptrGndPlant )
  return ccall((:PSRSystem_addGndPlant, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGndPlant)
end
function PSRSystem_getPlant( ptr_pointer,tipo, codUsina )
  return ccall((:PSRSystem_getPlant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),ptr_pointer ,tipo,codUsina)
end
function PSRSystem_getPlant2( ptr_pointer,tipo, nomeUsina )
  return ccall((:PSRSystem_getPlant2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,tipo,nomeUsina)
end
function PSRSystem_delPlant( ptr_pointer,ptrUsina )
  return ccall((:PSRSystem_delPlant, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrUsina)
end
function PSRSystem_maxGndGaugingStation( ptr_pointer )
  return ccall((:PSRSystem_maxGndGaugingStation, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_gndGaugingStation( ptr_pointer,index )
  return ccall((:PSRSystem_gndGaugingStation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRSystem_getGndGaugingStation( ptr_pointer,code )
  return ccall((:PSRSystem_getGndGaugingStation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRSystem_getGndGaugingStation2( ptr_pointer,name )
  return ccall((:PSRSystem_getGndGaugingStation2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,name)
end
function PSRSystem_addGndGaugingStation( ptr_pointer,ptrGndGaugingStation )
  return ccall((:PSRSystem_addGndGaugingStation, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGndGaugingStation)
end
function PSRSystem_maxFuel( ptr_pointer )
  return ccall((:PSRSystem_maxFuel, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_fuel( ptr_pointer,ndx )
  return ccall((:PSRSystem_fuel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRSystem_getFuel( ptr_pointer,codigo )
  return ccall((:PSRSystem_getFuel, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codigo)
end
function PSRSystem_addFuel( ptr_pointer,ptrCombustivel )
  return ccall((:PSRSystem_addFuel, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCombustivel)
end
function PSRSystem_maxFuelReservoir( ptr_pointer )
  return ccall((:PSRSystem_maxFuelReservoir, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_fuelReservoir( ptr_pointer,ndx )
  return ccall((:PSRSystem_fuelReservoir, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRSystem_getFuelReservoir( ptr_pointer,codigo )
  return ccall((:PSRSystem_getFuelReservoir, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codigo)
end
function PSRSystem_addFuelReservoir( ptr_pointer,ptrFuelReservoir )
  return ccall((:PSRSystem_addFuelReservoir, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrFuelReservoir)
end
function PSRSystem_maxThermalCombinedCycle( ptr_pointer )
  return ccall((:PSRSystem_maxThermalCombinedCycle, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_thermalCombinedCycle( ptr_pointer,ndx )
  return ccall((:PSRSystem_thermalCombinedCycle, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRSystem_getThermalCombinedCycle( ptr_pointer,code )
  return ccall((:PSRSystem_getThermalCombinedCycle, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRSystem_addThermalCombinedCycle( ptr_pointer,ptrThermalCombinedCycle )
  return ccall((:PSRSystem_addThermalCombinedCycle, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrThermalCombinedCycle)
end
function PSRSystem_maxDemand( ptr_pointer )
  return ccall((:PSRSystem_maxDemand, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_demand( ptr_pointer,ndx )
  return ccall((:PSRSystem_demand, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRSystem_getDemand( ptr_pointer,codigo )
  return ccall((:PSRSystem_getDemand, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codigo)
end
function PSRSystem_getDemand2( ptr_pointer,nome )
  return ccall((:PSRSystem_getDemand2, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRSystem_addDemand( ptr_pointer,ptrDemanda )
  return ccall((:PSRSystem_addDemand, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDemanda)
end
function PSRSystem_delDemand( ptr_pointer,ptrDemanda )
  return ccall((:PSRSystem_delDemand, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDemanda)
end
function PSRSystem_getBattery( ptr_pointer,code )
  return ccall((:PSRSystem_getBattery, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,code)
end
function PSRSystem_getConstraintList( ptr_pointer )
  return ccall((:PSRSystem_getConstraintList, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_getReserveGenerationConstraintList( ptr_pointer )
  return ccall((:PSRSystem_getReserveGenerationConstraintList, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_getCollectionElements( ptr_pointer,ptrCollection, ptrClassNameFilters )
  return ccall((:PSRSystem_getCollectionElements, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrCollection,ptrClassNameFilters)
end
function PSRSystem_getSerie( ptr_pointer,codserie )
  return ccall((:PSRSystem_getSerie, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,codserie)
end
function PSRSystem_addBus( ptr_pointer,ptrBarra )
  return ccall((:PSRSystem_addBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBarra)
end
function PSRSystem_maxGasNode( ptr_pointer )
  return ccall((:PSRSystem_maxGasNode, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_gasNode( ptr_pointer,index )
  return ccall((:PSRSystem_gasNode, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRSystem_addGasNode( ptr_pointer,ptrGasNode )
  return ccall((:PSRSystem_addGasNode, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGasNode)
end
function PSRSystem_calculateLoadsPerc( ptr_pointer,num_patamares )
  return ccall((:PSRSystem_calculateLoadsPerc, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,num_patamares)
end
function PSRSystem_getNode( ptr_pointer )
  return ccall((:PSRSystem_getNode, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRSystem_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRSystem_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRSystem_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRSystem_serialize( ptr_pointer )
  return ccall((:PSRSystem_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRSystem_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRSystem_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRSystem_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRSystem_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRSystem_deleteInstance( ptr_pointer )
  return ccall((:PSRSystem_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDisbursement_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionDisbursement_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionDisbursement_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionDisbursement_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRCircuitSumData_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRCircuitSumData_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRCircuitSumData_classType( ptr_pointer )
  return ccall((:PSRCircuitSumData_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitSumData_isClassType( ptr_pointer,class_type )
  return ccall((:PSRCircuitSumData_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRCircuitSumData_deleteInstance( ptr_pointer )
  return ccall((:PSRCircuitSumData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitSumData_create(iprt)
  ptr_pointer =  ccall((:PSRCircuitSumData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExpansionAssociated_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionAssociated_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionAssociated_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionAssociated_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOMask_CSVDATA_create(iprt)
  ptr_pointer =  ccall((:PSRIOMask_CSVDATA_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOMask_CSVDATA_deleteInstance( ptr_pointer )
  return ccall((:PSRIOMask_CSVDATA_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOCurrency_create(iprt)
  ptr_pointer =  ccall((:PSRIOCurrency_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOCurrency_hasDataToWrite( ptr_pointer,ptrEstudo )
  return ccall((:PSRIOCurrency_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo)
end
function PSRIOCurrency_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOCurrency_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOCurrency_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOCurrency_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOCurrency_deleteInstance( ptr_pointer )
  return ccall((:PSRIOCurrency_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryToNETPLANLinkDC_create(iprt,ptrStudy)
  ptr_pointer =  ccall((:PSRVectorSDDPBinaryToNETPLANLinkDC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,ptrStudy)
  return ptr_pointer
end
function PSRVectorSDDPBinaryToNETPLANLinkDC_setOperationTypeParm( ptr_pointer,ptrOperationParm )
  return ccall((:PSRVectorSDDPBinaryToNETPLANLinkDC_setOperationTypeParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrOperationParm)
end
function PSRVectorSDDPBinaryToNETPLANLinkDC_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryToNETPLANLinkDC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionPrecedence_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionPrecedence_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionPrecedence_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionPrecedence_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGndPlant_create(iprt)
  ptr_pointer =  ccall((:PSRGndPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGndPlant_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGndPlant_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGndPlant_classType( ptr_pointer )
  return ccall((:PSRGndPlant_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGndPlant_isClassType( ptr_pointer,class_type )
  return ccall((:PSRGndPlant_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRGndPlant_getGaugingStation( ptr_pointer )
  return ccall((:PSRGndPlant_getGaugingStation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGndPlant_setGaugingStation( ptr_pointer,ptrGndGaugingStation )
  return ccall((:PSRGndPlant_setGaugingStation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrGndGaugingStation)
end
function PSRGndPlant_serialize( ptr_pointer )
  return ccall((:PSRGndPlant_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGndPlant_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRGndPlant_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRGndPlant_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRGndPlant_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRGndPlant_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRGndPlant_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGndPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRGndPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionExclusive_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionExclusive_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionExclusive_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionExclusive_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRHydroPlant_create(iprt)
  ptr_pointer =  ccall((:PSRHydroPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRHydroPlant_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRHydroPlant_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRHydroPlant_classType( ptr_pointer )
  return ccall((:PSRHydroPlant_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydroPlant_isClassType( ptr_pointer,class_type )
  return ccall((:PSRHydroPlant_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRHydroPlant_network( ptr_pointer )
  return ccall((:PSRHydroPlant_network, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydroPlant_getGaugingStation( ptr_pointer )
  return ccall((:PSRHydroPlant_getGaugingStation, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydroPlant_setGaugingStation( ptr_pointer,_ptrPosto )
  return ccall((:PSRHydroPlant_setGaugingStation, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrPosto)
end
function PSRHydroPlant_getDownstreamPlantForSpillage( ptr_pointer )
  return ccall((:PSRHydroPlant_getDownstreamPlantForSpillage, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydroPlant_getDownstreamPlantForTurbining( ptr_pointer )
  return ccall((:PSRHydroPlant_getDownstreamPlantForTurbining, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydroPlant_getDownstreamPlantForFiltration( ptr_pointer )
  return ccall((:PSRHydroPlant_getDownstreamPlantForFiltration, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydroPlant_getDownstreamPlantForStoredEnergy( ptr_pointer )
  return ccall((:PSRHydroPlant_getDownstreamPlantForStoredEnergy, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydroPlant_getAssociatedReservoir( ptr_pointer )
  return ccall((:PSRHydroPlant_getAssociatedReservoir, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydroPlant_getAcumulatedProductionFactor( ptr_pointer )
  return ccall((:PSRHydroPlant_getAcumulatedProductionFactor, "PSRClasses"),Float64,(Ptr{UInt8},),ptr_pointer )
end
function PSRHydroPlant_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRHydroPlant_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRHydroPlant_serialize( ptr_pointer )
  return ccall((:PSRHydroPlant_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRHydroPlant_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRHydroPlant_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRHydroPlant_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRHydroPlant_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRHydroPlant_removeAllReferences( ptr_pointer,ptrElement )
  return ccall((:PSRHydroPlant_removeAllReferences, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRHydroPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRHydroPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSpreadsheetCSVGeneral_create(iprt)
  ptr_pointer =  ccall((:PSRIOSpreadsheetCSVGeneral_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSpreadsheetCSVGeneral_delimitBySpace( ptr_pointer,status )
  return ccall((:PSRIOSpreadsheetCSVGeneral_delimitBySpace, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOSpreadsheetCSVGeneral_load( ptr_pointer,ptrPlanilha, nome )
  return ccall((:PSRIOSpreadsheetCSVGeneral_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrPlanilha,nome)
end
function PSRIOSpreadsheetCSVGeneral_save( ptr_pointer,ptrPlanilha, nome )
  return ccall((:PSRIOSpreadsheetCSVGeneral_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrPlanilha,nome)
end
function PSRIOSpreadsheetCSVGeneral_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSpreadsheetCSVGeneral_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryToNETPLANGeneration_create(iprt,ptrStudy)
  ptr_pointer =  ccall((:PSRVectorSDDPBinaryToNETPLANGeneration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,ptrStudy)
  return ptr_pointer
end
function PSRVectorSDDPBinaryToNETPLANGeneration_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryToNETPLANGeneration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSpinningReserveGroup_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRSpinningReserveGroup_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRSpinningReserveGroup_classType( ptr_pointer )
  return ccall((:PSRSpinningReserveGroup_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSpinningReserveGroup_isClassType( ptr_pointer,class_type )
  return ccall((:PSRSpinningReserveGroup_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRSpinningReserveGroup_deleteInstance( ptr_pointer )
  return ccall((:PSRSpinningReserveGroup_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSpinningReserveGroup_create(iprt)
  ptr_pointer =  ccall((:PSRSpinningReserveGroup_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRInterconnectionSumData_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRInterconnectionSumData_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRInterconnectionSumData_classType( ptr_pointer )
  return ccall((:PSRInterconnectionSumData_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRInterconnectionSumData_isClassType( ptr_pointer,class_type )
  return ccall((:PSRInterconnectionSumData_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRInterconnectionSumData_deleteInstance( ptr_pointer )
  return ccall((:PSRInterconnectionSumData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRInterconnectionSumData_create(iprt)
  ptr_pointer =  ccall((:PSRInterconnectionSumData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSerie_create(iprt)
  ptr_pointer =  ccall((:PSRSerie_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSerie_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRSerie_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRSerie_classType( ptr_pointer )
  return ccall((:PSRSerie_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSerie_isClassType( ptr_pointer,class_type )
  return ccall((:PSRSerie_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRSerie_bus( ptr_pointer,ndx )
  return ccall((:PSRSerie_bus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,ndx)
end
function PSRSerie_numBus( ptr_pointer,ptrBarra )
  return ccall((:PSRSerie_numBus, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBarra)
end
function PSRSerie_adjacentBus( ptr_pointer,ptrBarra )
  return ccall((:PSRSerie_adjacentBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBarra)
end
function PSRSerie_numCircuit( ptr_pointer )
  return ccall((:PSRSerie_numCircuit, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSerie_numCircuit2( ptr_pointer,number )
  return ccall((:PSRSerie_numCircuit2, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,number)
end
function PSRSerie_getStatus( ptr_pointer )
  return ccall((:PSRSerie_getStatus, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSerie_setStatus( ptr_pointer,_status )
  return ccall((:PSRSerie_setStatus, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_status)
end
function PSRSerie_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRSerie_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRSerie_serialize( ptr_pointer )
  return ccall((:PSRSerie_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRSerie_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRSerie_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRSerie_deleteInstance( ptr_pointer )
  return ccall((:PSRSerie_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRShunt_create(iprt)
  ptr_pointer =  ccall((:PSRShunt_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRShunt_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRShunt_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRShunt_classType( ptr_pointer )
  return ccall((:PSRShunt_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRShunt_isClassType( ptr_pointer,class_type )
  return ccall((:PSRShunt_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRShunt_bus( ptr_pointer )
  return ccall((:PSRShunt_bus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRShunt_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRShunt_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRShunt_serialize( ptr_pointer )
  return ccall((:PSRShunt_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRShunt_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRShunt_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRShunt_deleteInstance( ptr_pointer )
  return ccall((:PSRShunt_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionGeneric_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionGeneric_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExpansionGeneric_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionGeneric_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionGeneric_classType( ptr_pointer )
  return ccall((:PSRExpansionGeneric_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionGeneric_isClassType( ptr_pointer,class_type )
  return ccall((:PSRExpansionGeneric_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRExpansionGeneric_existents( ptr_pointer )
  return ccall((:PSRExpansionGeneric_existents, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionGeneric_getCoefficient( ptr_pointer,ptrElement )
  return ccall((:PSRExpansionGeneric_getCoefficient, "PSRClasses"),Float64,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRExpansionGeneric_addCoefficient( ptr_pointer,ptrElement, coef )
  return ccall((:PSRExpansionGeneric_addCoefficient, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Float64,),ptr_pointer ,ptrElement,coef)
end
function PSRExpansionGeneric_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionGeneric_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionRiverTopology_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionRiverTopology_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExpansionRiverTopology_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRExpansionRiverTopology_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRExpansionRiverTopology_classType( ptr_pointer )
  return ccall((:PSRExpansionRiverTopology_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionRiverTopology_isClassType( ptr_pointer,class_type )
  return ccall((:PSRExpansionRiverTopology_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRExpansionRiverTopology_nodes( ptr_pointer )
  return ccall((:PSRExpansionRiverTopology_nodes, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionRiverTopology_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionRiverTopology_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryToNETPLANAngle_create(iprt,ptrStudy)
  ptr_pointer =  ccall((:PSRVectorSDDPBinaryToNETPLANAngle_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,ptrStudy)
  return ptr_pointer
end
function PSRVectorSDDPBinaryToNETPLANAngle_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryToNETPLANAngle_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRVectorSDDPBinaryToNETPLANDemandP_create(iprt,ptrStudy)
  ptr_pointer =  ccall((:PSRVectorSDDPBinaryToNETPLANDemandP_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,ptrStudy)
  return ptr_pointer
end
function PSRVectorSDDPBinaryToNETPLANDemandP_setLoadFactor( ptr_pointer,ptrIOLoadFactor )
  return ccall((:PSRVectorSDDPBinaryToNETPLANDemandP_setLoadFactor, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrIOLoadFactor)
end
function PSRVectorSDDPBinaryToNETPLANDemandP_setLoadFactorIndex( ptr_pointer,index )
  return ccall((:PSRVectorSDDPBinaryToNETPLANDemandP_setLoadFactorIndex, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorSDDPBinaryToNETPLANDemandP_setLeadInstance( ptr_pointer,leadInstance )
  return ccall((:PSRVectorSDDPBinaryToNETPLANDemandP_setLeadInstance, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,leadInstance)
end
function PSRVectorSDDPBinaryToNETPLANDemandP_addSlaveInstance( ptr_pointer,slaveInstance )
  return ccall((:PSRVectorSDDPBinaryToNETPLANDemandP_addSlaveInstance, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,slaveInstance)
end
function PSRVectorSDDPBinaryToNETPLANDemandP_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryToNETPLANDemandP_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_CSVDATA_useMask( ptr_pointer,_ptrIOMask )
  return ccall((:PSRIO_CSVDATA_useMask, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrIOMask)
end
function PSRIO_CSVDATA_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_CSVDATA_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_CSVDATA_create(iprt)
  ptr_pointer =  ccall((:PSRIO_CSVDATA_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPHydroPlantTravelTime_load( ptr_pointer,ptrStudy, filename, vecField, nPointsField )
  return ccall((:PSRIONCPHydroPlantTravelTime_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename,vecField,nPointsField)
end
function PSRIONCPHydroPlantTravelTime_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPHydroPlantTravelTime_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantTravelTime_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPHydroPlantTravelTime_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenFirmElementsModification_load( ptr_pointer,ptrStudy, filename, class_type, parmid )
  return ccall((:PSRIOOptgenFirmElementsModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename,class_type,parmid)
end
function PSRIOOptgenFirmElementsModification_save( ptr_pointer,ptrStudy, filename, class_type, parmid )
  return ccall((:PSRIOOptgenFirmElementsModification_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename,class_type,parmid)
end
function PSRIOOptgenFirmElementsModification_hasDataToWrite( ptr_pointer,ptrStudy, class_type, parmid )
  return ccall((:PSRIOOptgenFirmElementsModification_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrStudy,class_type,parmid)
end
function PSRIOOptgenFirmElementsModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenFirmElementsModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenFirmElementsModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenFirmElementsModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPHydroPlantTargetStorage_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPHydroPlantTargetStorage_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPHydroPlantTargetStorage_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPHydroPlantTargetStorage_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantTargetStorage_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPHydroPlantTargetStorage_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPInflowForecast_A_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPInflowForecast_A_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPInflowForecast_A_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPInflowForecast_A_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPInflowForecast_A_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPInflowForecast_A_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPHydroPlantRamp_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPHydroPlantRamp_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPHydroPlantRamp_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPHydroPlantRamp_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantRamp_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPHydroPlantRamp_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRCircuitPathConstraint_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRCircuitPathConstraint_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRCircuitPathConstraint_classType( ptr_pointer )
  return ccall((:PSRCircuitPathConstraint_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitPathConstraint_isClassType( ptr_pointer,class_type )
  return ccall((:PSRCircuitPathConstraint_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRCircuitPathConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRCircuitPathConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitPathConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRCircuitPathConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRCircuitDC_create(iprt)
  ptr_pointer =  ccall((:PSRCircuitDC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRCircuitDC_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRCircuitDC_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRCircuitDC_classType( ptr_pointer )
  return ccall((:PSRCircuitDC_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitDC_isClassType( ptr_pointer,class_type )
  return ccall((:PSRCircuitDC_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRCircuitDC_deleteInstance( ptr_pointer )
  return ccall((:PSRCircuitDC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitFlowConstraint_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRCircuitFlowConstraint_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRCircuitFlowConstraint_classType( ptr_pointer )
  return ccall((:PSRCircuitFlowConstraint_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitFlowConstraint_isClassType( ptr_pointer,class_type )
  return ccall((:PSRCircuitFlowConstraint_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRCircuitFlowConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRCircuitFlowConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitFlowConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRCircuitFlowConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRCircuitEnviromentalConstraint_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRCircuitEnviromentalConstraint_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRCircuitEnviromentalConstraint_classType( ptr_pointer )
  return ccall((:PSRCircuitEnviromentalConstraint_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitEnviromentalConstraint_isClassType( ptr_pointer,class_type )
  return ccall((:PSRCircuitEnviromentalConstraint_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRCircuitEnviromentalConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRCircuitEnviromentalConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitEnviromentalConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRCircuitEnviromentalConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenSeason_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenSeason_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenSeason_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenSeason_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenSeason_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenSeason_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRExpansionSatisfaction_deleteInstance( ptr_pointer )
  return ccall((:PSRExpansionSatisfaction_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRExpansionSatisfaction_create(iprt)
  ptr_pointer =  ccall((:PSRExpansionSatisfaction_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONetplanSumAnglesConstraint_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONetplanSumAnglesConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONetplanSumAnglesConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIONetplanSumAnglesConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONetplanSumAnglesConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIONetplanSumAnglesConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOMOCCAOutputIndex_create(iprt)
  ptr_pointer =  ccall((:PSRIOMOCCAOutputIndex_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOMOCCAOutputIndex_load( ptr_pointer,nome )
  return ccall((:PSRIOMOCCAOutputIndex_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIOMOCCAOutputIndex_deleteInstance( ptr_pointer )
  return ccall((:PSRIOMOCCAOutputIndex_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenerator_create(iprt)
  ptr_pointer =  ccall((:PSRGenerator_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRGenerator_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRGenerator_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRGenerator_classType( ptr_pointer )
  return ccall((:PSRGenerator_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenerator_isClassType( ptr_pointer,class_type )
  return ccall((:PSRGenerator_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRGenerator_maxPlant( ptr_pointer )
  return ccall((:PSRGenerator_maxPlant, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRGenerator_plant( ptr_pointer,index )
  return ccall((:PSRGenerator_plant, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRGenerator_getControlledBus( ptr_pointer )
  return ccall((:PSRGenerator_getControlledBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGenerator_setControlledBus( ptr_pointer,ptrBus )
  return ccall((:PSRGenerator_setControlledBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRGenerator_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRGenerator_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRGenerator_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRGenerator_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRGenerator_serialize( ptr_pointer )
  return ccall((:PSRGenerator_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRGenerator_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRGenerator_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRGenerator_buildRelationShipsFrom( ptr_pointer,ptrMessageDataElement )
  return ccall((:PSRGenerator_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrMessageDataElement)
end
function PSRGenerator_deleteInstance( ptr_pointer )
  return ccall((:PSRGenerator_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONetplanSumCircuitsConstraint_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONetplanSumCircuitsConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONetplanSumCircuitsConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIONetplanSumCircuitsConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONetplanSumCircuitsConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIONetplanSumCircuitsConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONetplanSumCorridorCircuitsConstraint_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONetplanSumCorridorCircuitsConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONetplanSumCorridorCircuitsConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIONetplanSumCorridorCircuitsConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONetplanSumCorridorCircuitsConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIONetplanSumCorridorCircuitsConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONetplanEnviromentalCircuitsConstraint_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONetplanEnviromentalCircuitsConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONetplanEnviromentalCircuitsConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIONetplanEnviromentalCircuitsConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONetplanEnviromentalCircuitsConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIONetplanEnviromentalCircuitsConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOHourInfoPlant_create(iprt)
  ptr_pointer =  ccall((:PSRIOHourInfoPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOHourInfoPlant_load( ptr_pointer,ptrStudy, filename, attribute )
  return ccall((:PSRIOHourInfoPlant_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename,attribute)
end
function PSRIOHourInfoPlant_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOHourInfoPlant_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOHourInfoPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRIOHourInfoPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResult_create(iprt)
  ptr_pointer =  ccall((:PSRIOGrafResult_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGrafResult_getTotalStages( ptr_pointer )
  return ccall((:PSRIOGrafResult_getTotalStages, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResult_getTotalSeries( ptr_pointer )
  return ccall((:PSRIOGrafResult_getTotalSeries, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResult_getTotalBlocks( ptr_pointer )
  return ccall((:PSRIOGrafResult_getTotalBlocks, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResult_writeRegistryData( ptr_pointer,etapa, serie, patamar, data )
  return ccall((:PSRIOGrafResult_writeRegistryData, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32, Int32, Ptr{Float64},),ptr_pointer ,etapa,serie,patamar,data)
end
function PSRIOGrafResult_initLoad( ptr_pointer,nome, format )
  return ccall((:PSRIOGrafResult_initLoad, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,nome,format)
end
function PSRIOGrafResult_nextRegistry( ptr_pointer,ignore_data )
  return ccall((:PSRIOGrafResult_nextRegistry, "PSRClasses"),Int32,(Ptr{UInt8}, Bool,),ptr_pointer ,ignore_data)
end
function PSRIOGrafResult_getLastRegistry( ptr_pointer )
  return ccall((:PSRIOGrafResult_getLastRegistry, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResult_aggregate( ptr_pointer,serieAggregationType, aggregateBlock )
  return ccall((:PSRIOGrafResult_aggregate, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32, Bool,),ptr_pointer ,serieAggregationType,aggregateBlock)
end
function PSRIOGrafResult_seekStage( ptr_pointer,numetapa )
  return ccall((:PSRIOGrafResult_seekStage, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,numetapa)
end
function PSRIOGrafResult_seekStage2( ptr_pointer,numetapa, numserie, numblock )
  return ccall((:PSRIOGrafResult_seekStage2, "PSRClasses"),Int32,(Ptr{UInt8}, Int32, Int32, Int32,),ptr_pointer ,numetapa,numserie,numblock)
end
function PSRIOGrafResult_reset( ptr_pointer )
  return ccall((:PSRIOGrafResult_reset, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResult_closeLoad( ptr_pointer )
  return ccall((:PSRIOGrafResult_closeLoad, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResult_verifyRegistries( ptr_pointer )
  return ccall((:PSRIOGrafResult_verifyRegistries, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResult_initSave( ptr_pointer,nome, format )
  return ccall((:PSRIOGrafResult_initSave, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,nome,format)
end
function PSRIOGrafResult_writeRegistry( ptr_pointer )
  return ccall((:PSRIOGrafResult_writeRegistry, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResult_closeSave( ptr_pointer )
  return ccall((:PSRIOGrafResult_closeSave, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResult_initMerge( ptr_pointer,nome, srcNames, srcStageIndex )
  return ccall((:PSRIOGrafResult_initMerge, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome,srcNames,srcStageIndex)
end
function PSRIOGrafResult_useRegistryInfo( ptr_pointer )
  return ccall((:PSRIOGrafResult_useRegistryInfo, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafResult_getRegistry( ptr_pointer,stage, serie, block, index_agent )
  return ccall((:PSRIOGrafResult_getRegistry, "PSRClasses"),Float64,(Ptr{UInt8}, Int32, Int32, Int32, Int32,),ptr_pointer ,stage,serie,block,index_agent)
end
function PSRIOGrafResult_toBinary( ptr_pointer,namecsv, nameheader, namebinary )
  return ccall((:PSRIOGrafResult_toBinary, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,namecsv,nameheader,namebinary)
end
function PSRIOGrafResult_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGrafResult_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRTransformer_create(iprt)
  ptr_pointer =  ccall((:PSRTransformer_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRTransformer_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRTransformer_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRTransformer_classType( ptr_pointer )
  return ccall((:PSRTransformer_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRTransformer_isClassType( ptr_pointer,class_type )
  return ccall((:PSRTransformer_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRTransformer_getControlledBus( ptr_pointer )
  return ccall((:PSRTransformer_getControlledBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRTransformer_setControlledBus( ptr_pointer,ptrBus )
  return ccall((:PSRTransformer_setControlledBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRTransformer_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRTransformer_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRTransformer_deleteInstance( ptr_pointer )
  return ccall((:PSRTransformer_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONetplanPathCircuitsConstraint_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONetplanPathCircuitsConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONetplanPathCircuitsConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIONetplanPathCircuitsConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONetplanPathCircuitsConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIONetplanPathCircuitsConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPChronological_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPChronological_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPChronological_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPChronological_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANCircuitList_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANCircuitList_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANCircuitList_useModelTemplate( ptr_pointer,_ptrModelTemplate )
  return ccall((:PSRIONETPLANCircuitList_useModelTemplate, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrModelTemplate)
end
function PSRIONETPLANCircuitList_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANCircuitList_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANCircuitList_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANCircuitList_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRBusAngleConstraint_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRBusAngleConstraint_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRBusAngleConstraint_classType( ptr_pointer )
  return ccall((:PSRBusAngleConstraint_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBusAngleConstraint_isClassType( ptr_pointer,class_type )
  return ccall((:PSRBusAngleConstraint_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRBusAngleConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRBusAngleConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRBusAngleConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRBusAngleConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRBattery_create(iprt)
  ptr_pointer =  ccall((:PSRBattery_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRBattery_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRBattery_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRBattery_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRBattery_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRBattery_classType( ptr_pointer )
  return ccall((:PSRBattery_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRBattery_isClassType( ptr_pointer,class_type )
  return ccall((:PSRBattery_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRBattery_getControlledBus( ptr_pointer )
  return ccall((:PSRBattery_getControlledBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRBattery_setControlledBus( ptr_pointer,ptrBus )
  return ccall((:PSRBattery_setControlledBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRBattery_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRBattery_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRBattery_serialize( ptr_pointer )
  return ccall((:PSRBattery_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRBattery_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRBattery_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRBattery_deleteInstance( ptr_pointer )
  return ccall((:PSRBattery_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSerieCapacitor_create(iprt)
  ptr_pointer =  ccall((:PSRSerieCapacitor_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSerieCapacitor_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRSerieCapacitor_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRSerieCapacitor_classType( ptr_pointer )
  return ccall((:PSRSerieCapacitor_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSerieCapacitor_isClassType( ptr_pointer,class_type )
  return ccall((:PSRSerieCapacitor_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRSerieCapacitor_deleteInstance( ptr_pointer )
  return ccall((:PSRSerieCapacitor_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOBusLoadSDDP_create(iprt)
  ptr_pointer =  ccall((:PSRIOBusLoadSDDP_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOBusLoadSDDP_load( ptr_pointer,ptrSistema, nome, patamares )
  return ccall((:PSRIOBusLoadSDDP_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,nome,patamares)
end
function PSRIOBusLoadSDDP_save( ptr_pointer,ptrSistema, nome, patamares )
  return ccall((:PSRIOBusLoadSDDP_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,nome,patamares)
end
function PSRIOBusLoadSDDP_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOBusLoadSDDP_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOBusLoadSDDP_deleteInstance( ptr_pointer )
  return ccall((:PSRIOBusLoadSDDP_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCapacitor_create(iprt)
  ptr_pointer =  ccall((:PSRCapacitor_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRCapacitor_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRCapacitor_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRCapacitor_classType( ptr_pointer )
  return ccall((:PSRCapacitor_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCapacitor_isClassType( ptr_pointer,class_type )
  return ccall((:PSRCapacitor_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRCapacitor_getControlledBus( ptr_pointer )
  return ccall((:PSRCapacitor_getControlledBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRCapacitor_setControlledBus( ptr_pointer,ptrBus )
  return ccall((:PSRCapacitor_setControlledBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRCapacitor_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRCapacitor_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRCapacitor_serialize( ptr_pointer )
  return ccall((:PSRCapacitor_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRCapacitor_deleteInstance( ptr_pointer )
  return ccall((:PSRCapacitor_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRPowerInjection_create(iprt)
  ptr_pointer =  ccall((:PSRPowerInjection_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRPowerInjection_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRPowerInjection_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRPowerInjection_classType( ptr_pointer )
  return ccall((:PSRPowerInjection_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRPowerInjection_isClassType( ptr_pointer,class_type )
  return ccall((:PSRPowerInjection_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRPowerInjection_deleteInstance( ptr_pointer )
  return ccall((:PSRPowerInjection_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFuelContractPlants_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPFuelContractPlants_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFuelContractPlants_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelContractPlants_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelContractPlants_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelContractPlants_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelContractPlants_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPFuelContractPlants_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPFuelContractPlants_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPFuelContractPlants_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANScenarios_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANScenarios_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANScenarios_load( ptr_pointer,_ptrEstudo, fileName )
  return ccall((:PSRIONETPLANScenarios_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,fileName)
end
function PSRIONETPLANScenarios_save( ptr_pointer,_ptrEstudo, fileName )
  return ccall((:PSRIONETPLANScenarios_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,fileName)
end
function PSRIONETPLANScenarios_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANScenarios_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANShunt_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANShunt_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANShunt_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANShunt_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANShunt_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANShunt_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitCorridorConstraint_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRCircuitCorridorConstraint_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRCircuitCorridorConstraint_classType( ptr_pointer )
  return ccall((:PSRCircuitCorridorConstraint_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitCorridorConstraint_isClassType( ptr_pointer,class_type )
  return ccall((:PSRCircuitCorridorConstraint_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRCircuitCorridorConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRCircuitCorridorConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRCircuitCorridorConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRCircuitCorridorConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPChronological_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPChronological_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPChronological_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPChronological_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPBusMod_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPBusMod_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPBusMod_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPBusMod_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPBusMod_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPBusMod_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPBusMod_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPBusMod_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSpreadsheetParser_create(iprt)
  ptr_pointer =  ccall((:PSRIOSpreadsheetParser_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSpreadsheetParser_setOptionVector( ptr_pointer,_option_vector )
  return ccall((:PSRIOSpreadsheetParser_setOptionVector, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_option_vector)
end
function PSRIOSpreadsheetParser_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSpreadsheetParser_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRNetworkDC_create(iprt,_ptrArea)
  ptr_pointer =  ccall((:PSRNetworkDC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,_ptrArea)
  return ptr_pointer
end
function PSRNetworkDC_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRNetworkDC_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRNetworkDC_classType( ptr_pointer )
  return ccall((:PSRNetworkDC_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRNetworkDC_isClassType( ptr_pointer,class_type )
  return ccall((:PSRNetworkDC_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRNetworkDC_deleteInstance( ptr_pointer )
  return ccall((:PSRNetworkDC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPBus_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPBus_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPBus_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPBus_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPBus_loadV10_3( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPBus_loadV10_3, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPBus_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPBus_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPBus_saveV10_3( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPBus_saveV10_3, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPBus_ignoreGeneration( ptr_pointer,status )
  return ccall((:PSRIOSDDPBus_ignoreGeneration, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOSDDPBus_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPBus_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenTypicalDayBlocks_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenTypicalDayBlocks_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenTypicalDayBlocks_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenTypicalDayBlocks_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenTypicalDayBlocks_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenTypicalDayBlocks_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANConfig_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANConfig_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANConfig_load( ptr_pointer,_ptrEstudo, fileName )
  return ccall((:PSRIONETPLANConfig_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,fileName)
end
function PSRIONETPLANConfig_save( ptr_pointer,_ptrEstudo, fileName )
  return ccall((:PSRIONETPLANConfig_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,fileName)
end
function PSRIONETPLANConfig_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANConfig_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRReactor_create(iprt)
  ptr_pointer =  ccall((:PSRReactor_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRReactor_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRReactor_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRReactor_classType( ptr_pointer )
  return ccall((:PSRReactor_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRReactor_isClassType( ptr_pointer,class_type )
  return ccall((:PSRReactor_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRReactor_getControlledBus( ptr_pointer )
  return ccall((:PSRReactor_getControlledBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRReactor_setControlledBus( ptr_pointer,ptrBus )
  return ccall((:PSRReactor_setControlledBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRReactor_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRReactor_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRReactor_serialize( ptr_pointer )
  return ccall((:PSRReactor_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRReactor_deleteInstance( ptr_pointer )
  return ccall((:PSRReactor_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSelectedOutputs_create(iprt)
  ptr_pointer =  ccall((:PSRIOSelectedOutputs_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSelectedOutputs_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSelectedOutputs_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSelectedOutputs_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSelectedOutputs_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSelectedOutputs_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSelectedOutputs_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenTypicalDay_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenTypicalDay_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenTypicalDay_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenTypicalDay_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenTypicalDay_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenTypicalDay_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSpreadsheetCSV_create(iprt)
  ptr_pointer =  ccall((:PSRIOSpreadsheetCSV_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSpreadsheetCSV_load( ptr_pointer,ptrPlanilha, nome )
  return ccall((:PSRIOSpreadsheetCSV_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrPlanilha,nome)
end
function PSRIOSpreadsheetCSV_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSpreadsheetCSV_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPReserveGenerationBackedElements_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPReserveGenerationBackedElements_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPReserveGenerationBackedElements_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPReserveGenerationBackedElements_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPReserveGenerationBackedElements_load( ptr_pointer,ptrSystem, nome )
  return ccall((:PSRIOSDDPReserveGenerationBackedElements_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,nome)
end
function PSRIOSDDPReserveGenerationBackedElements_save( ptr_pointer,ptrSystem, nome )
  return ccall((:PSRIOSDDPReserveGenerationBackedElements_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,nome)
end
function PSRIOSDDPReserveGenerationBackedElements_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPReserveGenerationBackedElements_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPReserveGenerationConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPReserveGenerationConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPReserveGenerationConstraint_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPReserveGenerationConstraint_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPReserveGenerationConstraint_load( ptr_pointer,ptrSystem, nome )
  return ccall((:PSRIOSDDPReserveGenerationConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,nome)
end
function PSRIOSDDPReserveGenerationConstraint_save( ptr_pointer,ptrSystem, nome )
  return ccall((:PSRIOSDDPReserveGenerationConstraint_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,nome)
end
function PSRIOSDDPReserveGenerationConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPReserveGenerationConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_DATABLOCK_MOD_ELEMENT_readfile( ptr_pointer,nome, numberBlocks )
end
function PSRIO_DATABLOCK_MOD_ELEMENT_savefile( ptr_pointer,nome, numberBlocks )
end
function PSRIO_DATABLOCK_MOD_ELEMENT_configureBlocksWithoutReading( ptr_pointer,numberBlocks )
end
function PSRIO_DATABLOCK_MOD_ELEMENT_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_DATABLOCK_MOD_ELEMENT_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGasEmissionMonthlyData_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasEmissionMonthlyData_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasEmissionMonthlyData_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGasEmissionMonthlyData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGasEmissionMonthlyData_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGasEmissionMonthlyData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConverterDCAC_create(iprt)
  ptr_pointer =  ccall((:PSRConverterDCAC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConverterDCAC_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRConverterDCAC_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRConverterDCAC_classType( ptr_pointer )
  return ccall((:PSRConverterDCAC_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConverterDCAC_isClassType( ptr_pointer,class_type )
  return ccall((:PSRConverterDCAC_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRConverterDCAC_getNeutralBus( ptr_pointer )
  return ccall((:PSRConverterDCAC_getNeutralBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRConverterDCAC_setNeutralBus( ptr_pointer,neutralBus )
  return ccall((:PSRConverterDCAC_setNeutralBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,neutralBus)
end
function PSRConverterDCAC_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRConverterDCAC_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRConverterDCAC_deleteInstance( ptr_pointer )
  return ccall((:PSRConverterDCAC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFuelSeriePrice_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPFuelSeriePrice_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFuelSeriePrice_load( ptr_pointer,ptrSistema, nome )
  return ccall((:PSRIOSDDPFuelSeriePrice_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,nome)
end
function PSRIOSDDPFuelSeriePrice_save( ptr_pointer,ptrSistema, nome )
  return ccall((:PSRIOSDDPFuelSeriePrice_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,nome)
end
function PSRIOSDDPFuelSeriePrice_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPFuelSeriePrice_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLineReactor_create(iprt)
  ptr_pointer =  ccall((:PSRLineReactor_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRLineReactor_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRLineReactor_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRLineReactor_classType( ptr_pointer )
  return ccall((:PSRLineReactor_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRLineReactor_isClassType( ptr_pointer,class_type )
  return ccall((:PSRLineReactor_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRLineReactor_serie( ptr_pointer )
  return ccall((:PSRLineReactor_serie, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRLineReactor_setSerie( ptr_pointer,ptrSerie )
  return ccall((:PSRLineReactor_setSerie, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSerie)
end
function PSRLineReactor_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRLineReactor_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRLineReactor_serialize( ptr_pointer )
  return ccall((:PSRLineReactor_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRLineReactor_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRLineReactor_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRLineReactor_deleteInstance( ptr_pointer )
  return ccall((:PSRLineReactor_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPModification_savefile( ptr_pointer,nome )
  return ccall((:PSRIOSDDPModification_savefile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIOSDDPModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFixedDuration_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPFixedDuration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFixedDuration_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPFixedDuration_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPFixedDuration_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPFixedDuration_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPFixedDuration_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPFixedDuration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFuelContractReservoirs_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPFuelContractReservoirs_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFuelContractReservoirs_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelContractReservoirs_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelContractReservoirs_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelContractReservoirs_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelContractReservoirs_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPFuelContractReservoirs_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPFuelContractReservoirs_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPFuelContractReservoirs_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLoad_create(iprt)
  ptr_pointer =  ccall((:PSRLoad_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRLoad_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRLoad_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRLoad_classType( ptr_pointer )
  return ccall((:PSRLoad_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRLoad_isClassType( ptr_pointer,class_type )
  return ccall((:PSRLoad_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRLoad_getDemand( ptr_pointer )
  return ccall((:PSRLoad_getDemand, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRLoad_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRLoad_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRLoad_isEqual( ptr_pointer,ptrElement )
  return ccall((:PSRLoad_isEqual, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrElement)
end
function PSRLoad_serialize( ptr_pointer )
  return ccall((:PSRLoad_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRLoad_buildFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRLoad_buildFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRLoad_buildRelationShipsFrom( ptr_pointer,ptrDataElement )
  return ccall((:PSRLoad_buildRelationShipsFrom, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrDataElement)
end
function PSRLoad_deleteInstance( ptr_pointer )
  return ccall((:PSRLoad_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHourDemand_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPHourDemand_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPHourDemand_load( ptr_pointer,ptrSystem, filename, attribute_name )
  return ccall((:PSRIOSDDPHourDemand_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,attribute_name)
end
function PSRIOSDDPHourDemand_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPHourDemand_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPHourDemand_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPHourDemand_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHourDemand_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPHourDemand_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIO_ROWDATAELEMENT_readfile( ptr_pointer,nome )
  return ccall((:PSRIO_ROWDATAELEMENT_readfile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIO_ROWDATAELEMENT_savefile( ptr_pointer,nome )
  return ccall((:PSRIO_ROWDATAELEMENT_savefile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIO_ROWDATAELEMENT_mustUpdateExisting( ptr_pointer )
  return ccall((:PSRIO_ROWDATAELEMENT_mustUpdateExisting, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_ROWDATAELEMENT_mustUpdateExisting2( ptr_pointer,status )
  return ccall((:PSRIO_ROWDATAELEMENT_mustUpdateExisting2, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIO_ROWDATAELEMENT_setDefaultModelSourceId( ptr_pointer,source_id )
  return ccall((:PSRIO_ROWDATAELEMENT_setDefaultModelSourceId, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,source_id)
end
function PSRIO_ROWDATAELEMENT_getDefaultModelSourceId( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRIO_ROWDATAELEMENT_getDefaultModelSourceId, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRIO_ROWDATAELEMENT_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_ROWDATAELEMENT_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPDemandVariationCoef_create(iprt,_num_patamares, _etapas_ano)
  ptr_pointer =  ccall((:PSRIOSDDPDemandVariationCoef_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),C_NULL,_num_patamares,_etapas_ano)
  return ptr_pointer
end
function PSRIOSDDPDemandVariationCoef_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPDemandVariationCoef_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPDemandVariationCoef_load( ptr_pointer,ptrSystem, nome )
  return ccall((:PSRIOSDDPDemandVariationCoef_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,nome)
end
function PSRIOSDDPDemandVariationCoef_save( ptr_pointer,ptrSystem, nome )
  return ccall((:PSRIOSDDPDemandVariationCoef_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,nome)
end
function PSRIOSDDPDemandVariationCoef_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPDemandVariationCoef_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFuelReservoirPlants_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPFuelReservoirPlants_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFuelReservoirPlants_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelReservoirPlants_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelReservoirPlants_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelReservoirPlants_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelReservoirPlants_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPFuelReservoirPlants_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPFuelReservoirPlants_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPFuelReservoirPlants_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPVariableDuration_create(iprt,_num_patamares, _etapas_ano)
  ptr_pointer =  ccall((:PSRIOSDDPVariableDuration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),C_NULL,_num_patamares,_etapas_ano)
  return ptr_pointer
end
function PSRIOSDDPVariableDuration_load( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOSDDPVariableDuration_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOSDDPVariableDuration_save( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOSDDPVariableDuration_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOSDDPVariableDuration_hasDataToWrite( ptr_pointer,_ptrEstudo )
  return ccall((:PSRIOSDDPVariableDuration_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo)
end
function PSRIOSDDPVariableDuration_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPVariableDuration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPLinkBusToPlant_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPLinkBusToPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPLinkBusToPlant_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPLinkBusToPlant_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPLinkBusToPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPLinkBusToPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPlantMod_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPPlantMod_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPPlantMod_load( ptr_pointer,ptrSistema, nome, tipousina )
  return ccall((:PSRIOSDDPPlantMod_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,nome,tipousina)
end
function PSRIOSDDPPlantMod_save( ptr_pointer,ptrSistema, nome, tipousina )
  return ccall((:PSRIOSDDPPlantMod_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,nome,tipousina)
end
function PSRIOSDDPPlantMod_fixThermalModificationsData( ptr_pointer )
  return ccall((:PSRIOSDDPPlantMod_fixThermalModificationsData, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPlantMod_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPPlantMod_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIO_CSVDATAELEMENT_savefile( ptr_pointer,nome )
  return ccall((:PSRIO_CSVDATAELEMENT_savefile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIO_CSVDATAELEMENT_openfile( ptr_pointer,nome )
  return ccall((:PSRIO_CSVDATAELEMENT_openfile, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,nome)
end
function PSRIO_CSVDATAELEMENT_deleteInstance( ptr_pointer )
  return ccall((:PSRIO_CSVDATAELEMENT_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHourBlockMap_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPHourBlockMap_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPHourBlockMap_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPHourBlockMap_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPHourBlockMap_generateVariableDurationModel( ptr_pointer )
  return ccall((:PSRIOSDDPHourBlockMap_generateVariableDurationModel, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHourBlockMap_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPHourBlockMap_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHourBlockMap_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPHourBlockMap_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGndScenarios_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGndScenarios_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGndScenarios_useStation( ptr_pointer,using_station )
  return ccall((:PSRIOSDDPGndScenarios_useStation, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,using_station)
end
function PSRIOSDDPGndScenarios_load( ptr_pointer,_ptrSistema, nome )
  return ccall((:PSRIOSDDPGndScenarios_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrSistema,nome)
end
function PSRIOSDDPGndScenarios_save( ptr_pointer,_ptrSistema, nome )
  return ccall((:PSRIOSDDPGndScenarios_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrSistema,nome)
end
function PSRIOSDDPGndScenarios_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGndScenarios_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPDemandCost_create(iprt,_num_patamares, _etapas_ano)
  ptr_pointer =  ccall((:PSRIOSDDPDemandCost_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),C_NULL,_num_patamares,_etapas_ano)
  return ptr_pointer
end
function PSRIOSDDPDemandCost_load( ptr_pointer,_ptrSistema, nome )
  return ccall((:PSRIOSDDPDemandCost_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrSistema,nome)
end
function PSRIOSDDPDemandCost_save( ptr_pointer,_ptrSistema, nome )
  return ccall((:PSRIOSDDPDemandCost_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrSistema,nome)
end
function PSRIOSDDPDemandCost_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPDemandCost_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPDemandCost_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPDemandCost_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGeneric_create(iprt)
  ptr_pointer =  ccall((:PSRIOGeneric_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGeneric_load( ptr_pointer,Filename, Collection, CLASS_Element, modelid, Study )
  return ccall((:PSRIOGeneric_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,Filename,Collection,CLASS_Element,modelid,Study)
end
function PSRIOGeneric_load2( ptr_pointer,Filename, group, CLASS_Element, modelid, Study )
  return ccall((:PSRIOGeneric_load2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,Filename,group,CLASS_Element,modelid,Study)
end
function PSRIOGeneric_save( ptr_pointer,Filename, Collection, option_vector )
  return ccall((:PSRIOGeneric_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,Filename,Collection,option_vector)
end
function PSRIOGeneric_save2( ptr_pointer,Filename, group, option_vector )
  return ccall((:PSRIOGeneric_save2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,Filename,group,option_vector)
end
function PSRIOGeneric_setPrimaryKey( ptr_pointer,keyName )
  return ccall((:PSRIOGeneric_setPrimaryKey, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,keyName)
end
function PSRIOGeneric_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGeneric_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemBase_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemBase_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemBase_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemBase_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRVectorSDDPBinaryToNETPLANDemandQ_create(iprt,ptrStudy)
  ptr_pointer =  ccall((:PSRVectorSDDPBinaryToNETPLANDemandQ_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Ptr{UInt8},),C_NULL,ptrStudy)
  return ptr_pointer
end
function PSRVectorSDDPBinaryToNETPLANDemandQ_setDemandFactor( ptr_pointer,ptrIODemandBinary )
  return ccall((:PSRVectorSDDPBinaryToNETPLANDemandQ_setDemandFactor, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrIODemandBinary)
end
function PSRVectorSDDPBinaryToNETPLANDemandQ_setDemandFactorIndex( ptr_pointer,index )
  return ccall((:PSRVectorSDDPBinaryToNETPLANDemandQ_setDemandFactorIndex, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,index)
end
function PSRVectorSDDPBinaryToNETPLANDemandQ_setDemandFactorDefault( ptr_pointer,value )
  return ccall((:PSRVectorSDDPBinaryToNETPLANDemandQ_setDemandFactorDefault, "PSRClasses"),Nothing,(Ptr{UInt8}, Float64,),ptr_pointer ,value)
end
function PSRVectorSDDPBinaryToNETPLANDemandQ_deleteInstance( ptr_pointer )
  return ccall((:PSRVectorSDDPBinaryToNETPLANDemandQ_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGrafIndex_create(iprt)
  ptr_pointer =  ccall((:PSRIOGrafIndex_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGrafIndex_load( ptr_pointer,_ptrPlanilha, nome )
  return ccall((:PSRIOGrafIndex_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrPlanilha,nome)
end
function PSRIOGrafIndex_load2( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOGrafIndex_load2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOGrafIndex_save( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOGrafIndex_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOGrafIndex_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGrafIndex_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGeorefenceElement_create(iprt)
  ptr_pointer =  ccall((:PSRIOGeorefenceElement_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGeorefenceElement_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOGeorefenceElement_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOGeorefenceElement_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOGeorefenceElement_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOGeorefenceElement_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGeorefenceElement_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSynchronousCompensator_create(iprt)
  ptr_pointer =  ccall((:PSRSynchronousCompensator_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSynchronousCompensator_getControlledBus( ptr_pointer )
  return ccall((:PSRSynchronousCompensator_getControlledBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRSynchronousCompensator_setControlledBus( ptr_pointer,ptrBus )
  return ccall((:PSRSynchronousCompensator_setControlledBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRSynchronousCompensator_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRSynchronousCompensator_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRSynchronousCompensator_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRSynchronousCompensator_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRSynchronousCompensator_classType( ptr_pointer )
  return ccall((:PSRSynchronousCompensator_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRSynchronousCompensator_isClassType( ptr_pointer,class_type )
  return ccall((:PSRSynchronousCompensator_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRSynchronousCompensator_serialize( ptr_pointer )
  return ccall((:PSRSynchronousCompensator_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRSynchronousCompensator_deleteInstance( ptr_pointer )
  return ccall((:PSRSynchronousCompensator_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOAnatemCDU_create(iprt)
  ptr_pointer =  ccall((:PSRIOAnatemCDU_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOAnatemCDU_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOAnatemCDU_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOAnatemCDU_deleteInstance( ptr_pointer )
  return ccall((:PSRIOAnatemCDU_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBlocks_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBlocks_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBlocks_load( ptr_pointer,_ptrEstudo, fileName )
  return ccall((:PSRIONETPLANBlocks_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,fileName)
end
function PSRIONETPLANBlocks_save( ptr_pointer,_ptrEstudo, fileName )
  return ccall((:PSRIONETPLANBlocks_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,fileName)
end
function PSRIONETPLANBlocks_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBlocks_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOAnaredeStudy_create(iprt)
  ptr_pointer =  ccall((:PSRIOAnaredeStudy_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOAnaredeStudy_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOAnaredeStudy_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOAnaredeStudy_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOAnaredeStudy_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOAnaredeStudy_deleteInstance( ptr_pointer )
  return ccall((:PSRIOAnaredeStudy_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRStaticVarCompensator_create(iprt)
  ptr_pointer =  ccall((:PSRStaticVarCompensator_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRStaticVarCompensator_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRStaticVarCompensator_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRStaticVarCompensator_classType( ptr_pointer )
  return ccall((:PSRStaticVarCompensator_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRStaticVarCompensator_isClassType( ptr_pointer,class_type )
  return ccall((:PSRStaticVarCompensator_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRStaticVarCompensator_getControlledBus( ptr_pointer )
  return ccall((:PSRStaticVarCompensator_getControlledBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStaticVarCompensator_setControlledBus( ptr_pointer,ptrBus )
  return ccall((:PSRStaticVarCompensator_setControlledBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRStaticVarCompensator_isRelated( ptr_pointer,ptrElement, tipoRelacionamento )
  return ccall((:PSRStaticVarCompensator_isRelated, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrElement,tipoRelacionamento)
end
function PSRStaticVarCompensator_serialize( ptr_pointer )
  return ccall((:PSRStaticVarCompensator_serialize, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRStaticVarCompensator_deleteInstance( ptr_pointer )
  return ccall((:PSRStaticVarCompensator_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBlocksDuration_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBlocksDuration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBlocksDuration_load( ptr_pointer,_ptrEstudo, fileName )
  return ccall((:PSRIONETPLANBlocksDuration_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,fileName)
end
function PSRIONETPLANBlocksDuration_save( ptr_pointer,_ptrEstudo, fileName )
  return ccall((:PSRIONETPLANBlocksDuration_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,fileName)
end
function PSRIONETPLANBlocksDuration_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBlocksDuration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPlantMaintenance_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPPlantMaintenance_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPPlantMaintenance_getInfoType( ptr_pointer )
  return ccall((:PSRIOSDDPPlantMaintenance_getInfoType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPlantMaintenance_getUnitType( ptr_pointer )
  return ccall((:PSRIOSDDPPlantMaintenance_getUnitType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPlantMaintenance_setInfoType( ptr_pointer,_tipoInfo )
  return ccall((:PSRIOSDDPPlantMaintenance_setInfoType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_tipoInfo)
end
function PSRIOSDDPPlantMaintenance_setUnitType( ptr_pointer,_tipoUnidades )
  return ccall((:PSRIOSDDPPlantMaintenance_setUnitType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_tipoUnidades)
end
function PSRIOSDDPPlantMaintenance_load( ptr_pointer,ptrSistema, nome, tipoUsina )
  return ccall((:PSRIOSDDPPlantMaintenance_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,nome,tipoUsina)
end
function PSRIOSDDPPlantMaintenance_save( ptr_pointer,ptrSistema, nome, tipoUsina )
  return ccall((:PSRIOSDDPPlantMaintenance_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,nome,tipoUsina)
end
function PSRIOSDDPPlantMaintenance_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPPlantMaintenance_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPResultDemand_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPResultDemand_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPResultDemand_load( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOSDDPResultDemand_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOSDDPResultDemand_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPResultDemand_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPlantConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPPlantConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPPlantConstraint_getInfoType( ptr_pointer )
  return ccall((:PSRIOSDDPPlantConstraint_getInfoType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPlantConstraint_getUnitType( ptr_pointer )
  return ccall((:PSRIOSDDPPlantConstraint_getUnitType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPlantConstraint_setInfoType( ptr_pointer,_tipoInfo )
  return ccall((:PSRIOSDDPPlantConstraint_setInfoType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_tipoInfo)
end
function PSRIOSDDPPlantConstraint_setUnitType( ptr_pointer,_tipoUnidades )
  return ccall((:PSRIOSDDPPlantConstraint_setUnitType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,_tipoUnidades)
end
function PSRIOSDDPPlantConstraint_load( ptr_pointer,ptrSistema, nome )
  return ccall((:PSRIOSDDPPlantConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,nome)
end
function PSRIOSDDPPlantConstraint_save( ptr_pointer,ptrSistema, nome )
  return ccall((:PSRIOSDDPPlantConstraint_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,nome)
end
function PSRIOSDDPPlantConstraint_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPPlantConstraint_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPPlantConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPPlantConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGndGaugingStation_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGndGaugingStation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGndGaugingStation_load( ptr_pointer,ptrSystem, name )
  return ccall((:PSRIOSDDPGndGaugingStation_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,name)
end
function PSRIOSDDPGndGaugingStation_save( ptr_pointer,ptrSystem, name )
  return ccall((:PSRIOSDDPGndGaugingStation_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,name)
end
function PSRIOSDDPGndGaugingStation_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGndGaugingStation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPResultDuration_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPResultDuration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPResultDuration_load( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOSDDPResultDuration_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOSDDPResultDuration_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPResultDuration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRLinkDC_create(iprt)
  ptr_pointer =  ccall((:PSRLinkDC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRLinkDC_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRLinkDC_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRLinkDC_classType( ptr_pointer )
  return ccall((:PSRLinkDC_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRLinkDC_isClassType( ptr_pointer,class_type )
  return ccall((:PSRLinkDC_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRLinkDC_deleteInstance( ptr_pointer )
  return ccall((:PSRLinkDC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantTemperatureEffects_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIONCPThermalPlantTemperatureEffects_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIONCPThermalPlantTemperatureEffects_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantTemperatureEffects_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantTemperatureEffects_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantTemperatureEffects_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGenericPlantData_setAdditionalModel( ptr_pointer,additional_model )
  return ccall((:PSRIOGenericPlantData_setAdditionalModel, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,additional_model)
end
function PSRIOGenericPlantData_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOGenericPlantData_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOGenericPlantData_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGenericPlantData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGenericPlantData_create(iprt)
  ptr_pointer =  ccall((:PSRIOGenericPlantData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGenericClassData_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOGenericClassData_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOGenericClassData_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOGenericClassData_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOGenericClassData_hasCreatedAnyObject( ptr_pointer )
  return ccall((:PSRIOGenericClassData_hasCreatedAnyObject, "PSRClasses"),Bool,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGenericClassData_getCurrentGroup( ptr_pointer )
  return ccall((:PSRIOGenericClassData_getCurrentGroup, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGenericClassData_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGenericClassData_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGenericClassData_create(iprt)
  ptr_pointer =  ccall((:PSRIOGenericClassData_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPDemand_create(iprt,_num_patamares, _etapas_ano)
  ptr_pointer =  ccall((:PSRIOSDDPDemand_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8}, Int32, Int32,),C_NULL,_num_patamares,_etapas_ano)
  return ptr_pointer
end
function PSRIOSDDPDemand_load( ptr_pointer,_ptrSistema, nome, sddp_versao )
  return ccall((:PSRIOSDDPDemand_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,_ptrSistema,nome,sddp_versao)
end
function PSRIOSDDPDemand_save( ptr_pointer,_ptrSistema, nome, sddp_versao )
  return ccall((:PSRIOSDDPDemand_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,_ptrSistema,nome,sddp_versao)
end
function PSRIOSDDPDemand_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPDemand_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPDemand_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPDemand_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGenericModification_load( ptr_pointer,Filename, CLASS_Element, Study )
  return ccall((:PSRIOGenericModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,Filename,CLASS_Element,Study)
end
function PSRIOGenericModification_save( ptr_pointer,Filename, ptrColElements, Study )
  return ccall((:PSRIOGenericModification_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,Filename,ptrColElements,Study)
end
function PSRIOGenericModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGenericModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGenericModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOGenericModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPThermalPlant_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPThermalPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPThermalPlant_load( ptr_pointer,_ptrSistema, nome )
  return ccall((:PSRIOSDDPThermalPlant_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrSistema,nome)
end
function PSRIOSDDPThermalPlant_save( ptr_pointer,_ptrSistema, nome )
  return ccall((:PSRIOSDDPThermalPlant_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrSistema,nome)
end
function PSRIOSDDPThermalPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPThermalPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPEmissionCostInfo_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPEmissionCostInfo_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPEmissionCostInfo_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPEmissionCostInfo_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPEmissionCostInfo_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPEmissionCostInfo_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPEmissionCostInfo_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPEmissionCostInfo_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPEmissionCostInfo_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPEmissionCostInfo_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPConstraintSumDataModification_load( ptr_pointer,ptrStudy, fileName, _ptrConstraintSumList )
  return ccall((:PSRIOSDDPConstraintSumDataModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,fileName,_ptrConstraintSumList)
end
function PSRIOSDDPConstraintSumDataModification_configureBlocks( ptr_pointer,ptrStudy, _ptrConstraintSumList )
  return ccall((:PSRIOSDDPConstraintSumDataModification_configureBlocks, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,_ptrConstraintSumList)
end
function PSRIOSDDPConstraintSumDataModification_save( ptr_pointer,ptrStudy, fileName, _ptrConstraintSumList )
  return ccall((:PSRIOSDDPConstraintSumDataModification_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,fileName,_ptrConstraintSumList)
end
function PSRIOSDDPConstraintSumDataModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPConstraintSumDataModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPConstraintSumDataModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPConstraintSumDataModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPCircuitMod_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPCircuitMod_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPCircuitMod_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPCircuitMod_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPCircuitMod_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPCircuitMod_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPCircuitMod_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPCircuitMod_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGasPipelineModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGasPipelineModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGasPipelineModification_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasPipelineModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasPipelineModification_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasPipelineModification_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasPipelineModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGasPipelineModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGasPipeline_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGasPipeline_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGasPipeline_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasPipeline_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasPipeline_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasPipeline_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasPipeline_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGasPipeline_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPDemandInfo_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPDemandInfo_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPDemandInfo_load( ptr_pointer,_ptrSistema, nome )
  return ccall((:PSRIOSDDPDemandInfo_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrSistema,nome)
end
function PSRIOSDDPDemandInfo_save( ptr_pointer,_ptrSistema, nome )
  return ccall((:PSRIOSDDPDemandInfo_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrSistema,nome)
end
function PSRIOSDDPDemandInfo_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPDemandInfo_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFuelReservoirModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPFuelReservoirModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFuelReservoirModification_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelReservoirModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelReservoirModification_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelReservoirModification_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelReservoirModification_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPFuelReservoirModification_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPFuelReservoirModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPFuelReservoirModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOBusInformation_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOBusInformation_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOBusInformation_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOBusInformation_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOBusInformation_deleteInstance( ptr_pointer )
  return ccall((:PSRIOBusInformation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOBusInformation_create(iprt)
  ptr_pointer =  ccall((:PSRIOBusInformation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPRiskAversionCurve_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPRiskAversionCurve_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPRiskAversionCurve_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPRiskAversionCurve_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPRiskAversionCurve_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPRiskAversionCurve_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPRiskAversionCurve_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPRiskAversionCurve_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPRiskAversionCurve_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPRiskAversionCurve_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroPlant_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPHydroPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPHydroPlant_useSystemToInformation( ptr_pointer,status )
  return ccall((:PSRIOSDDPHydroPlant_useSystemToInformation, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOSDDPHydroPlant_load( ptr_pointer,ptrSistema, nome )
  return ccall((:PSRIOSDDPHydroPlant_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,nome)
end
function PSRIOSDDPHydroPlant_save( ptr_pointer,ptrSistema, nome )
  return ccall((:PSRIOSDDPHydroPlant_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,nome)
end
function PSRIOSDDPHydroPlant_ignoreHydrology( ptr_pointer,status )
  return ccall((:PSRIOSDDPHydroPlant_ignoreHydrology, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOSDDPHydroPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPHydroPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPReservoirSetConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPReservoirSetConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPReservoirSetConstraint_load( ptr_pointer,ptrStudy, filename, vector_baseid )
  return ccall((:PSRIOSDDPReservoirSetConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename,vector_baseid)
end
function PSRIOSDDPReservoirSetConstraint_save( ptr_pointer,ptrStudy, filename, vector_baseid )
  return ccall((:PSRIOSDDPReservoirSetConstraint_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename,vector_baseid)
end
function PSRIOSDDPReservoirSetConstraint_hasDataToWrite( ptr_pointer,ptrStudy, vector_baseid )
  return ccall((:PSRIOSDDPReservoirSetConstraint_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,vector_baseid)
end
function PSRIOSDDPReservoirSetConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPReservoirSetConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPSecondaryReserve_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPSecondaryReserve_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPSecondaryReserve_load( ptr_pointer,ptrSystem, filename, attribute_type )
  return ccall((:PSRIOSDDPSecondaryReserve_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSystem,filename,attribute_type)
end
function PSRIOSDDPSecondaryReserve_save( ptr_pointer,ptrSystem, filename, attribute_type )
  return ccall((:PSRIOSDDPSecondaryReserve_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSystem,filename,attribute_type)
end
function PSRIOSDDPSecondaryReserve_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPSecondaryReserve_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFuelContract_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPFuelContract_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFuelContract_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelContract_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelContract_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelContract_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelContract_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPFuelContract_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPFuelContract_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPFuelContract_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFuelReservoir_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPFuelReservoir_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFuelReservoir_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelReservoir_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelReservoir_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPFuelReservoir_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPFuelReservoir_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPFuelReservoir_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPFuelReservoir_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPFuelReservoir_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFuelConsumptionMax_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPFuelConsumptionMax_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFuelConsumptionMax_hasDataToWrite( ptr_pointer,ptrSystem, attribute )
  return ccall((:PSRIOSDDPFuelConsumptionMax_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,attribute)
end
function PSRIOSDDPFuelConsumptionMax_load( ptr_pointer,ptrSystem, filename, attribute )
  return ccall((:PSRIOSDDPFuelConsumptionMax_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,attribute)
end
function PSRIOSDDPFuelConsumptionMax_save( ptr_pointer,ptrSystem, filename, attribute )
  return ccall((:PSRIOSDDPFuelConsumptionMax_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,attribute)
end
function PSRIOSDDPFuelConsumptionMax_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPFuelConsumptionMax_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPFuel_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPFuel_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPFuel_load( ptr_pointer,ptrSystem, nome )
  return ccall((:PSRIOSDDPFuel_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,nome)
end
function PSRIOSDDPFuel_save( ptr_pointer,ptrSystem, nome )
  return ccall((:PSRIOSDDPFuel_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,nome)
end
function PSRIOSDDPFuel_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPFuel_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGndPlant_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGndPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGndPlant_load( ptr_pointer,_ptrSistema, nome )
  return ccall((:PSRIOSDDPGndPlant_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrSistema,nome)
end
function PSRIOSDDPGndPlant_save( ptr_pointer,_ptrSistema, nome )
  return ccall((:PSRIOSDDPGndPlant_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrSistema,nome)
end
function PSRIOSDDPGndPlant_saveModifications( ptr_pointer,_ptrSistema, nome )
  return ccall((:PSRIOSDDPGndPlant_saveModifications, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrSistema,nome)
end
function PSRIOSDDPGndPlant_useNetwork( ptr_pointer,status )
  return ccall((:PSRIOSDDPGndPlant_useNetwork, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOSDDPGndPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGndPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantColdRamp_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPHydroPlantColdRamp_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPHydroPlantColdRamp_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPHydroPlantColdRamp_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantColdRamp_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPHydroPlantColdRamp_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPHydroPlantDiscreteGeneration_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPHydroPlantDiscreteGeneration_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPHydroPlantDiscreteGeneration_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPHydroPlantDiscreteGeneration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantDiscreteGeneration_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPHydroPlantDiscreteGeneration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPPlantSecondarySpinningReserveNumberOfLevels_load( ptr_pointer,ptrStudy, filename, PSRPlantType )
  return ccall((:PSRIONCPPlantSecondarySpinningReserveNumberOfLevels_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrStudy,filename,PSRPlantType)
end
function PSRIONCPPlantSecondarySpinningReserveNumberOfLevels_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPPlantSecondarySpinningReserveNumberOfLevels_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPPlantSecondarySpinningReserveNumberOfLevels_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPPlantSecondarySpinningReserveNumberOfLevels_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPHydroPlantMaxStartUp_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPHydroPlantMaxStartUp_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPHydroPlantMaxStartUp_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPHydroPlantMaxStartUp_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantMaxStartUp_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPHydroPlantMaxStartUp_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPSpinningReserveGroupConstraint_load( ptr_pointer,ptrSystem, filename, modelField )
  return ccall((:PSRIONCPSpinningReserveGroupConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,modelField)
end
function PSRIONCPSpinningReserveGroupConstraint_loadExtraInfos( ptr_pointer,ptrSystem, pathdata )
  return ccall((:PSRIONCPSpinningReserveGroupConstraint_loadExtraInfos, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL ,ptrSystem,pathdata)
end
function PSRIONCPSpinningReserveGroupConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPSpinningReserveGroupConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPSpinningReserveGroupConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPSpinningReserveGroupConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPThermalPlantConsumptionCoefficients_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPThermalPlantConsumptionCoefficients_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPThermalPlantConsumptionCoefficients_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantConsumptionCoefficients_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantConsumptionCoefficients_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantConsumptionCoefficients_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPHydroPlantInitialStatus_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPHydroPlantInitialStatus_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPHydroPlantInitialStatus_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPHydroPlantInitialStatus_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantInitialStatus_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPHydroPlantInitialStatus_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPPlantSecondarySpinningReserveTableElementReal_load( ptr_pointer,ptrStudy, filename, modelField, PSRPlantType )
  return ccall((:PSRIONCPPlantSecondarySpinningReserveTableElementReal_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrStudy,filename,modelField,PSRPlantType)
end
function PSRIONCPPlantSecondarySpinningReserveTableElementReal_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPPlantSecondarySpinningReserveTableElementReal_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPPlantSecondarySpinningReserveTableElementReal_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPPlantSecondarySpinningReserveTableElementReal_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPHydroPlantVWIT_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPHydroPlantVWIT_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPHydroPlantVWIT_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPHydroPlantVWIT_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantVWIT_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPHydroPlantVWIT_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGasEmissionAssociation_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGasEmissionAssociation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGasEmissionAssociation_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasEmissionAssociation_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasEmissionAssociation_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGasEmissionAssociation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGasEmission_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGasEmission_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGasEmission_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasEmission_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasEmission_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGasEmission_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroConstantFlow_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPHydroConstantFlow_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPHydroConstantFlow_load( ptr_pointer,ptrSistema, filename, version )
  return ccall((:PSRIOSDDPHydroConstantFlow_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,filename,version)
end
function PSRIOSDDPHydroConstantFlow_save( ptr_pointer,ptrSistema, filename )
  return ccall((:PSRIOSDDPHydroConstantFlow_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,filename)
end
function PSRIOSDDPHydroConstantFlow_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPHydroConstantFlow_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroConstantFlow_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPHydroConstantFlow_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPHydroAdditionalModification_load( ptr_pointer,ptrSystem, fileName )
  return ccall((:PSRIOSDDPHydroAdditionalModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,fileName)
end
function PSRIOSDDPHydroAdditionalModification_save( ptr_pointer,ptrSystem, fileName )
  return ccall((:PSRIOSDDPHydroAdditionalModification_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,fileName)
end
function PSRIOSDDPHydroAdditionalModification_configureBlocks( ptr_pointer,ptrSystem, numberOfBlocks )
  return ccall((:PSRIOSDDPHydroAdditionalModification_configureBlocks, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSystem,numberOfBlocks)
end
function PSRIOSDDPHydroAdditionalModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPHydroAdditionalModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroAdditionalModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPHydroAdditionalModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGasNode_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGasNode_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGasNode_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasNode_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasNode_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasNode_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasNode_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGasNode_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantReservoirOperation_load( ptr_pointer,ptrSystem, filename, modelVecField, modelPenaltyField )
  return ccall((:PSRIONCPHydroPlantReservoirOperation_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,modelVecField,modelPenaltyField)
end
function PSRIONCPHydroPlantReservoirOperation_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPHydroPlantReservoirOperation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroPlantReservoirOperation_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPHydroPlantReservoirOperation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGasEmissionCost_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasEmissionCost_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasEmissionCost_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGasEmissionCost_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPGasEmissionCost_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGasEmissionCost_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGasNodeModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPGasNodeModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPGasNodeModification_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasNodeModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasNodeModification_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPGasNodeModification_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPGasNodeModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPGasNodeModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSpreadsheetDemand_create(iprt)
  ptr_pointer =  ccall((:PSRIOSpreadsheetDemand_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSpreadsheetDemand_load( ptr_pointer,ptrSistema, _ptrPlanilha, numblocos, modelid )
  return ccall((:PSRIOSpreadsheetDemand_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrSistema,_ptrPlanilha,numblocos,modelid)
end
function PSRIOSpreadsheetDemand_save( ptr_pointer,ptrSistema, _ptrPlanilha, numblocos )
  return ccall((:PSRIOSpreadsheetDemand_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,_ptrPlanilha,numblocos)
end
function PSRIOSpreadsheetDemand_save2( ptr_pointer,ptrDemanda, _ptrPlanilha, numblocos )
  return ccall((:PSRIOSpreadsheetDemand_save2, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrDemanda,_ptrPlanilha,numblocos)
end
function PSRIOSpreadsheetDemand_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSpreadsheetDemand_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPBattery_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPBattery_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPBattery_useNetwork( ptr_pointer,status )
  return ccall((:PSRIOSDDPBattery_useNetwork, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOSDDPBattery_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPBattery_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPBattery_save( ptr_pointer,ptrSistema, filename )
  return ccall((:PSRIOSDDPBattery_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,filename)
end
function PSRIOSDDPBattery_load( ptr_pointer,ptrSistema, filename )
  return ccall((:PSRIOSDDPBattery_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,filename)
end
function PSRIOSDDPBattery_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPBattery_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSpreadsheetLoad_create(iprt)
  ptr_pointer =  ccall((:PSRIOSpreadsheetLoad_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSpreadsheetLoad_load( ptr_pointer,ptrNetwork, _ptrPlanilha, modelid )
  return ccall((:PSRIOSpreadsheetLoad_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNetwork,_ptrPlanilha,modelid)
end
function PSRIOSpreadsheetLoad_save( ptr_pointer,ptrNetwork, _ptrPlanilha )
  return ccall((:PSRIOSpreadsheetLoad_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNetwork,_ptrPlanilha)
end
function PSRIOSpreadsheetLoad_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSpreadsheetLoad_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPAreaModification_load( ptr_pointer,ptrEstudo, fileName )
  return ccall((:PSRIOSDDPAreaModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,fileName)
end
function PSRIOSDDPAreaModification_save( ptr_pointer,ptrEstudo, fileName )
  return ccall((:PSRIOSDDPAreaModification_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,fileName)
end
function PSRIOSDDPAreaModification_configureBlocks( ptr_pointer,ptrEstudo )
  return ccall((:PSRIOSDDPAreaModification_configureBlocks, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo)
end
function PSRIOSDDPAreaModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPAreaModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPAreaModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPAreaModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPAreaExportMod_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPAreaExportMod_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPAreaExportMod_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPAreaExportMod_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPAreaExportMod_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPAreaExportMod_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPAreaExport_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPAreaExport_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPAreaExport_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPAreaExport_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPAreaExport_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPAreaExport_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSpreadsheetPlant_create(iprt)
  ptr_pointer =  ccall((:PSRIOSpreadsheetPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSpreadsheetPlant_load( ptr_pointer,ptrSistema, _ptrPlanilha, _tipoElemento, modelid )
  return ccall((:PSRIOSpreadsheetPlant_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrSistema,_ptrPlanilha,_tipoElemento,modelid)
end
function PSRIOSpreadsheetPlant_save( ptr_pointer,ptrSistema, _ptrPlanilha, _tipoElemento )
  return ccall((:PSRIOSpreadsheetPlant_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,_ptrPlanilha,_tipoElemento)
end
function PSRIOSpreadsheetPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSpreadsheetPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPBatteryMod_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPBatteryMod_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPBatteryMod_useNetwork( ptr_pointer,status )
  return ccall((:PSRIOSDDPBatteryMod_useNetwork, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOSDDPBatteryMod_save( ptr_pointer,ptrSistema, filename )
  return ccall((:PSRIOSDDPBatteryMod_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,filename)
end
function PSRIOSDDPBatteryMod_load( ptr_pointer,ptrSistema, filename )
  return ccall((:PSRIOSDDPBatteryMod_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,filename)
end
function PSRIOSDDPBatteryMod_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPBatteryMod_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSpreadsheetFuelSeriePrice_create(iprt)
  ptr_pointer =  ccall((:PSRIOSpreadsheetFuelSeriePrice_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSpreadsheetFuelSeriePrice_load( ptr_pointer,ptrSistema, _ptrPlanilha, _tipoElemento, modelid )
  return ccall((:PSRIOSpreadsheetFuelSeriePrice_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrSistema,_ptrPlanilha,_tipoElemento,modelid)
end
function PSRIOSpreadsheetFuelSeriePrice_save( ptr_pointer,ptrSistema, _ptrPlanilha, _tipoElemento )
  return ccall((:PSRIOSpreadsheetFuelSeriePrice_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,_ptrPlanilha,_tipoElemento)
end
function PSRIOSpreadsheetFuelSeriePrice_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSpreadsheetFuelSeriePrice_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSpreadsheetPlantMaintenance_create(iprt)
  ptr_pointer =  ccall((:PSRIOSpreadsheetPlantMaintenance_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSpreadsheetPlantMaintenance_load( ptr_pointer,ptrSistema, _ptrPlanilha, _tipoElemento, modelid, tipoInfo, tipoManutencao )
  return ccall((:PSRIOSpreadsheetPlantMaintenance_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8}, Int32, Int32,),ptr_pointer ,ptrSistema,_ptrPlanilha,_tipoElemento,modelid,tipoInfo,tipoManutencao)
end
function PSRIOSpreadsheetPlantMaintenance_save( ptr_pointer,ptrSistema, _ptrPlanilha, _tipoElemento )
  return ccall((:PSRIOSpreadsheetPlantMaintenance_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,_ptrPlanilha,_tipoElemento)
end
function PSRIOSpreadsheetPlantMaintenance_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSpreadsheetPlantMaintenance_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPArea_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPArea_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPArea_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPArea_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPArea_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPArea_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPArea_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPArea_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSpreadsheetBus_create(iprt)
  ptr_pointer =  ccall((:PSRIOSpreadsheetBus_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSpreadsheetBus_load( ptr_pointer,ptrNetwork, _ptrPlanilha, modelid )
  return ccall((:PSRIOSpreadsheetBus_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNetwork,_ptrPlanilha,modelid)
end
function PSRIOSpreadsheetBus_save( ptr_pointer,ptrNetwork, _ptrPlanilha )
  return ccall((:PSRIOSpreadsheetBus_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrNetwork,_ptrPlanilha)
end
function PSRIOSpreadsheetBus_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSpreadsheetBus_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDFuelContractChronologicalInfo_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDFuelContractChronologicalInfo_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDFuelContractChronologicalInfo_load( ptr_pointer,ptrSystem, filename, vectorId )
  return ccall((:PSRIOSDDFuelContractChronologicalInfo_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,vectorId)
end
function PSRIOSDDFuelContractChronologicalInfo_save( ptr_pointer,ptrSystem, filename, vectorId )
  return ccall((:PSRIOSDDFuelContractChronologicalInfo_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,vectorId)
end
function PSRIOSDDFuelContractChronologicalInfo_hasDataToWrite( ptr_pointer,ptrSystem, vectorId )
  return ccall((:PSRIOSDDFuelContractChronologicalInfo_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,vectorId)
end
function PSRIOSDDFuelContractChronologicalInfo_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDFuelContractChronologicalInfo_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDFuelReservoirChronologicalInfo_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDFuelReservoirChronologicalInfo_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDFuelReservoirChronologicalInfo_load( ptr_pointer,ptrSystem, filename, vectorId )
  return ccall((:PSRIOSDDFuelReservoirChronologicalInfo_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,vectorId)
end
function PSRIOSDDFuelReservoirChronologicalInfo_save( ptr_pointer,ptrSystem, filename, vectorId )
  return ccall((:PSRIOSDDFuelReservoirChronologicalInfo_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,vectorId)
end
function PSRIOSDDFuelReservoirChronologicalInfo_hasDataToWrite( ptr_pointer,ptrSystem, vectorId )
  return ccall((:PSRIOSDDFuelReservoirChronologicalInfo_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,vectorId)
end
function PSRIOSDDFuelReservoirChronologicalInfo_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDFuelReservoirChronologicalInfo_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenRiverTopologyConstraint_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenRiverTopologyConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenRiverTopologyConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenRiverTopologyConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenRiverTopologyConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenRiverTopologyConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenRiverNetwork_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenRiverNetwork_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenRiverNetwork_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenRiverNetwork_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenRiverNetwork_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenRiverNetwork_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPCircuit_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPCircuit_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPCircuit_useSystemToInformation( ptr_pointer,status )
  return ccall((:PSRIOSDDPCircuit_useSystemToInformation, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOSDDPCircuit_needExtendedIdentification( ptr_pointer,ptrStudy )
  return ccall((:PSRIOSDDPCircuit_needExtendedIdentification, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIOSDDPCircuit_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPCircuit_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPCircuit_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPCircuit_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPCircuit_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPCircuit_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPLinkDCModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPLinkDCModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPLinkDCModification_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPLinkDCModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPLinkDCModification_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPLinkDCModification_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPLinkDCModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPLinkDCModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPSpinningReserveInfo_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPSpinningReserveInfo_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPSpinningReserveInfo_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPSpinningReserveInfo_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPSpinningReserveInfo_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPSpinningReserveInfo_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPSpinningReserveInfo_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPSpinningReserveInfo_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPSpinningReserveInfo_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPSpinningReserveInfo_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPSumConstraint_create(iprt)
end
function PSRIOSDDPSumConstraint_load( ptr_pointer,ptrEstudo, fileName )
end
function PSRIOSDDPSumConstraint_save( ptr_pointer,ptrEstudo, fileName )
end
function PSRIOSDDPSumConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPSumConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPowerInjection_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPPowerInjection_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPPowerInjection_save( ptr_pointer,ptrSistema, filename )
  return ccall((:PSRIOSDDPPowerInjection_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,filename)
end
function PSRIOSDDPPowerInjection_load( ptr_pointer,ptrSistema, filename )
  return ccall((:PSRIOSDDPPowerInjection_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSistema,filename)
end
function PSRIOSDDPPowerInjection_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPPowerInjection_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPowerInjection_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPPowerInjection_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPSumCircuitsModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPSumCircuitsModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPSumCircuitsModification_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPSumCircuitsModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPSumCircuitsModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPSumCircuitsModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPReservoirSet_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPReservoirSet_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPReservoirSet_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPReservoirSet_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPReservoirSet_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPReservoirSet_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPReservoirSet_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPReservoirSet_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPReserveGenerationInfo_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPReserveGenerationInfo_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPReserveGenerationInfo_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPReserveGenerationInfo_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPReserveGenerationInfo_save( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIOSDDPReserveGenerationInfo_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIOSDDPReserveGenerationInfo_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPReserveGenerationInfo_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPSystem_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPSystem_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPSystem_useOnlySelectedSystems( ptr_pointer,flag )
  return ccall((:PSRIOSDDPSystem_useOnlySelectedSystems, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIOSDDPSystem_load( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOSDDPSystem_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOSDDPSystem_save( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOSDDPSystem_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOSDDPSystem_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPSystem_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPSumInterconnectionsModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPSumInterconnectionsModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPSumInterconnectionsModification_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPSumInterconnectionsModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPSumInterconnectionsModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPSumInterconnectionsModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPInterconnectionModification_load( ptr_pointer,ptrEstudo, fileName )
  return ccall((:PSRIOSDDPInterconnectionModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,fileName)
end
function PSRIOSDDPInterconnectionModification_save( ptr_pointer,ptrEstudo, fileName )
  return ccall((:PSRIOSDDPInterconnectionModification_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,fileName)
end
function PSRIOSDDPInterconnectionModification_configureBlocks( ptr_pointer,ptrStudy )
  return ccall((:PSRIOSDDPInterconnectionModification_configureBlocks, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIOSDDPInterconnectionModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPInterconnectionModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPInterconnectionModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPInterconnectionModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPPlantOperationalConstraint_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPPlantOperationalConstraint_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPPlantOperationalConstraint_save( ptr_pointer,ptrSistema, filename, plant_type )
  return ccall((:PSRIOSDDPPlantOperationalConstraint_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,filename,plant_type)
end
function PSRIOSDDPPlantOperationalConstraint_load( ptr_pointer,ptrSistema, filename, plant_type )
  return ccall((:PSRIOSDDPPlantOperationalConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,filename,plant_type)
end
function PSRIOSDDPPlantOperationalConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPPlantOperationalConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPlantOperationalConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPPlantOperationalConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPPlantChronologicalInfo_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPPlantChronologicalInfo_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPPlantChronologicalInfo_load( ptr_pointer,ptrSystem, filename, plantType, vector_baseid )
  return ccall((:PSRIOSDDPPlantChronologicalInfo_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,plantType,vector_baseid)
end
function PSRIOSDDPPlantChronologicalInfo_save( ptr_pointer,ptrSystem, filename, plantType, vector_baseid )
  return ccall((:PSRIOSDDPPlantChronologicalInfo_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,plantType,vector_baseid)
end
function PSRIOSDDPPlantChronologicalInfo_hasDataToWrite( ptr_pointer,ptrSystem, plantType, vector_baseid )
  return ccall((:PSRIOSDDPPlantChronologicalInfo_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrSystem,plantType,vector_baseid)
end
function PSRIOSDDPPlantChronologicalInfo_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPPlantChronologicalInfo_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPLinkDC_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPLinkDC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPLinkDC_hasDataToWrite( ptr_pointer,ptrStudy )
  return ccall((:PSRIOSDDPLinkDC_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIOSDDPLinkDC_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPLinkDC_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPLinkDC_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPLinkDC_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPLinkDC_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPLinkDC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPPlantAdditionalInfo_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPPlantAdditionalInfo_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPPlantAdditionalInfo_load( ptr_pointer,ptrSistema, nome, typePlant )
  return ccall((:PSRIOSDDPPlantAdditionalInfo_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,nome,typePlant)
end
function PSRIOSDDPPlantAdditionalInfo_save( ptr_pointer,ptrSistema, nome, typePlant )
  return ccall((:PSRIOSDDPPlantAdditionalInfo_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSistema,nome,typePlant)
end
function PSRIOSDDPPlantAdditionalInfo_mustHaveParm( ptr_pointer,parmid )
  return ccall((:PSRIOSDDPPlantAdditionalInfo_mustHaveParm, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,parmid)
end
function PSRIOSDDPPlantAdditionalInfo_mustBeNonZero( ptr_pointer,parmid )
  return ccall((:PSRIOSDDPPlantAdditionalInfo_mustBeNonZero, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,parmid)
end
function PSRIOSDDPPlantAdditionalInfo_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPPlantAdditionalInfo_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPInterconnectionCost_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPInterconnectionCost_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPInterconnectionCost_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPInterconnectionCost_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPInterconnectionCost_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPInterconnectionCost_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPInterconnectionCost_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPInterconnectionCost_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPInternationalCircuitCost_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPInternationalCircuitCost_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPInternationalCircuitCost_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPInternationalCircuitCost_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPInternationalCircuitCost_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOSDDPInternationalCircuitCost_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOSDDPInternationalCircuitCost_hasDataToWrite( ptr_pointer,ptrStudy )
  return ccall((:PSRIOSDDPInternationalCircuitCost_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy)
end
function PSRIOSDDPInternationalCircuitCost_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPInternationalCircuitCost_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPInterconnection_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPInterconnection_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPInterconnection_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPInterconnection_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPInterconnection_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIOSDDPInterconnection_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIOSDDPInterconnection_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPInterconnection_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPHydroTopology_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPHydroTopology_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPHydroTopology_load( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOSDDPHydroTopology_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOSDDPHydroTopology_save( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIOSDDPHydroTopology_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIOSDDPHydroTopology_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPHydroTopology_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantConsumptionFunction_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPThermalPlantConsumptionFunction_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPThermalPlantConsumptionFunction_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantConsumptionFunction_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantConsumptionFunction_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantConsumptionFunction_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusShunt_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusShunt_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusShunt_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANBusShunt_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANBusShunt_save( ptr_pointer,ptrEstudo, nome, savingCandidates )
  return ccall((:PSRIONETPLANBusShunt_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrEstudo,nome,savingCandidates)
end
function PSRIONETPLANBusShunt_saveModifications( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANBusShunt_saveModifications, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANBusShunt_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusShunt_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusDC_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusDC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusDC_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANBusDC_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANBusDC_save( ptr_pointer,ptrEstudo, nome, saveModifications )
  return ccall((:PSRIONETPLANBusDC_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrEstudo,nome,saveModifications)
end
function PSRIONETPLANBusDC_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusDC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusGraf_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusGraf_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusGraf_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANBusGraf_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANBusGraf_save( ptr_pointer,ptrEstudo, fileName )
  return ccall((:PSRIONETPLANBusGraf_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,fileName)
end
function PSRIONETPLANBusGraf_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusGraf_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusMonitoredList_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusMonitoredList_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusMonitoredList_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANBusMonitoredList_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANBusMonitoredList_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusMonitoredList_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBus_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBus_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBus_load( ptr_pointer,ptrEstudo, nome, readingAditionalInfo )
  return ccall((:PSRIONETPLANBus_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrEstudo,nome,readingAditionalInfo)
end
function PSRIONETPLANBus_save( ptr_pointer,ptrEstudo, nome, saveModifications, savingAditionalInfo, saving_ficticious )
  return ccall((:PSRIONETPLANBus_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool, Bool, Bool,),ptr_pointer ,ptrEstudo,nome,saveModifications,savingAditionalInfo,saving_ficticious)
end
function PSRIONETPLANBus_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBus_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPPlantPrimaryReserve_A_load( ptr_pointer,ptrSystem, filename, PSRPlantType )
  return ccall((:PSRSIONCPPlantPrimaryReserve_A_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSystem,filename,PSRPlantType)
end
function PSRSIONCPPlantPrimaryReserve_A_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPPlantPrimaryReserve_A_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPPlantPrimaryReserve_A_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPPlantPrimaryReserve_A_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANArea_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANArea_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANArea_load( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIONETPLANArea_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIONETPLANArea_save( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIONETPLANArea_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIONETPLANArea_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANArea_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBattery_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBattery_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBattery_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANBattery_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANBattery_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANBattery_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANBattery_saveModifications( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANBattery_saveModifications, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANBattery_setIncrementalSDDP( ptr_pointer,is_incremental_sddp )
  return ccall((:PSRIONETPLANBattery_setIncrementalSDDP, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,is_incremental_sddp)
end
function PSRIONETPLANBattery_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBattery_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantStartupMinimumTime_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPThermalPlantStartupMinimumTime_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPThermalPlantStartupMinimumTime_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantStartupMinimumTime_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantStartupMinimumTime_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantStartupMinimumTime_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPThermalPlantPowerInflection_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPThermalPlantPowerInflection_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPThermalPlantPowerInflection_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantPowerInflection_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantPowerInflection_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantPowerInflection_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPThermalPlantPrimaryReserveDirection_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIONCPThermalPlantPrimaryReserveDirection_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIONCPThermalPlantPrimaryReserveDirection_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantPrimaryReserveDirection_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantPrimaryReserveDirection_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantPrimaryReserveDirection_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANCircuit_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANCircuit_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANCircuit_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANCircuit_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANCircuit_save( ptr_pointer,ptrEstudo, nome, deviceType, savingCandidates, savingMonitored, savingContingency, saving_ficticious )
  return ccall((:PSRIONETPLANCircuit_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Bool, Bool, Bool, Bool,),ptr_pointer ,ptrEstudo,nome,deviceType,savingCandidates,savingMonitored,savingContingency,saving_ficticious)
end
function PSRIONETPLANCircuit_saveModifications( ptr_pointer,ptrEstudo, nome, deviceType, saving_ficticious )
  return ccall((:PSRIONETPLANCircuit_saveModifications, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32, Bool,),ptr_pointer ,ptrEstudo,nome,deviceType,saving_ficticious)
end
function PSRIONETPLANCircuit_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANCircuit_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantOperationalConstraint_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPThermalPlantOperationalConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPThermalPlantOperationalConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantOperationalConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantOperationalConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantOperationalConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPThermalPlantInitialStatus_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPThermalPlantInitialStatus_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPThermalPlantInitialStatus_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantInitialStatus_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantInitialStatus_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantInitialStatus_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPThermalPlantMaxShutdown_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPThermalPlantMaxShutdown_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPThermalPlantMaxShutdown_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantMaxShutdown_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantMaxShutdown_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantMaxShutdown_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPThermalPlantMaxStartUp_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPThermalPlantMaxStartUp_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPThermalPlantMaxStartUp_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantMaxStartUp_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantMaxStartUp_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantMaxStartUp_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPThermalPlantForbiddenZone_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPThermalPlantForbiddenZone_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPThermalPlantForbiddenZone_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPThermalPlantForbiddenZone_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPThermalPlantForbiddenZone_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPThermalPlantForbiddenZone_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSIONCPPlantPrimaryReserve_B_load( ptr_pointer,ptrSystem, filename, PSRPlantType )
  return ccall((:PSRSIONCPPlantPrimaryReserve_B_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSystem,filename,PSRPlantType)
end
function PSRSIONCPPlantPrimaryReserve_B_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPPlantPrimaryReserve_B_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPPlantPrimaryReserve_B_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPPlantPrimaryReserve_B_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSIONCPPlantGenerationConstraint_load( ptr_pointer,ptrSystem, filename, PSRPlantType )
  return ccall((:PSRSIONCPPlantGenerationConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSystem,filename,PSRPlantType)
end
function PSRSIONCPPlantGenerationConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPPlantGenerationConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPPlantGenerationConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPPlantGenerationConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSIONCPHydroPlantNullWater_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRSIONCPHydroPlantNullWater_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRSIONCPHydroPlantNullWater_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPHydroPlantNullWater_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPHydroPlantNullWater_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPHydroPlantNullWater_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConverterDCAC_P2P_create(iprt)
  ptr_pointer =  ccall((:PSRConverterDCAC_P2P_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConverterDCAC_P2P_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRConverterDCAC_P2P_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRConverterDCAC_P2P_classType( ptr_pointer )
  return ccall((:PSRConverterDCAC_P2P_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConverterDCAC_P2P_isClassType( ptr_pointer,class_type )
  return ccall((:PSRConverterDCAC_P2P_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRConverterDCAC_P2P_deleteInstance( ptr_pointer )
  return ccall((:PSRConverterDCAC_P2P_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPPlantColdReserve_load( ptr_pointer,ptrSystem, filename, modelField, PSRPlantType )
  return ccall((:PSRSIONCPPlantColdReserve_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSystem,filename,modelField,PSRPlantType)
end
function PSRSIONCPPlantColdReserve_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPPlantColdReserve_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPPlantColdReserve_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPPlantColdReserve_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenProjectModification_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenProjectModification_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenProjectModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenProjectModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenProjectModification_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenProjectModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPThermalSpecificConsumption_load( ptr_pointer,ptrSystem, fileName )
  return ccall((:PSRIOSDDPThermalSpecificConsumption_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,fileName)
end
function PSRIOSDDPThermalSpecificConsumption_save( ptr_pointer,ptrSystem, fileName )
  return ccall((:PSRIOSDDPThermalSpecificConsumption_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,fileName)
end
function PSRIOSDDPThermalSpecificConsumption_hasDataToWrite( ptr_pointer,ptrSystem )
  return ccall((:PSRIOSDDPThermalSpecificConsumption_hasDataToWrite, "PSRClasses"),Bool,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem)
end
function PSRIOSDDPThermalSpecificConsumption_configureBlocks( ptr_pointer,ptrStudy, ptrSystem )
  return ccall((:PSRIOSDDPThermalSpecificConsumption_configureBlocks, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,ptrSystem)
end
function PSRIOSDDPThermalSpecificConsumption_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPThermalSpecificConsumption_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPThermalSpecificConsumption_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPThermalSpecificConsumption_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSIONCPMarketPrice_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRSIONCPMarketPrice_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRSIONCPMarketPrice_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPMarketPrice_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPMarketPrice_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPMarketPrice_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDThermalCombinedCycleChronologicalInfo_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDThermalCombinedCycleChronologicalInfo_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDThermalCombinedCycleChronologicalInfo_load( ptr_pointer,ptrSystem, filename, vectorId )
  return ccall((:PSRIOSDDThermalCombinedCycleChronologicalInfo_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,vectorId)
end
function PSRIOSDDThermalCombinedCycleChronologicalInfo_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDThermalCombinedCycleChronologicalInfo_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRConverterDCAC_LCC_create(iprt)
  ptr_pointer =  ccall((:PSRConverterDCAC_LCC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConverterDCAC_LCC_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRConverterDCAC_LCC_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRConverterDCAC_LCC_classType( ptr_pointer )
  return ccall((:PSRConverterDCAC_LCC_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConverterDCAC_LCC_isClassType( ptr_pointer,class_type )
  return ccall((:PSRConverterDCAC_LCC_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRConverterDCAC_LCC_deleteInstance( ptr_pointer )
  return ccall((:PSRConverterDCAC_LCC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPThermalPlantForcedUnit_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRSIONCPThermalPlantForcedUnit_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRSIONCPThermalPlantForcedUnit_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPThermalPlantForcedUnit_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPThermalPlantForcedUnit_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPThermalPlantForcedUnit_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSIONCPThermalPlantHourlyTemperature_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRSIONCPThermalPlantHourlyTemperature_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRSIONCPThermalPlantHourlyTemperature_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPThermalPlantHourlyTemperature_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPThermalPlantHourlyTemperature_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPThermalPlantHourlyTemperature_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSIONCPPlantMaintenance_load( ptr_pointer,ptrSystem, filename, plantType )
  return ccall((:PSRSIONCPPlantMaintenance_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename,plantType)
end
function PSRSIONCPPlantMaintenance_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPPlantMaintenance_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPPlantMaintenance_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPPlantMaintenance_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConverterDCAC_VSC_create(iprt)
  ptr_pointer =  ccall((:PSRConverterDCAC_VSC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRConverterDCAC_VSC_className( ptr_pointer )
  retPtr = "                                                                                                                  "
  ccall((:PSRConverterDCAC_VSC_className, "PSRClasses"),Nothing,(Ptr{UInt8},Ptr{UInt8},),ptr_pointer ,retPtr)
  return split(retPtr,"\u00")[1]
end
function PSRConverterDCAC_VSC_classType( ptr_pointer )
  return ccall((:PSRConverterDCAC_VSC_classType, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRConverterDCAC_VSC_isClassType( ptr_pointer,class_type )
  return ccall((:PSRConverterDCAC_VSC_isClassType, "PSRClasses"),Bool,(Ptr{UInt8}, Int32,),ptr_pointer ,class_type)
end
function PSRConverterDCAC_VSC_getControlledBus( ptr_pointer )
  return ccall((:PSRConverterDCAC_VSC_getControlledBus, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),ptr_pointer )
end
function PSRConverterDCAC_VSC_setControlledBus( ptr_pointer,ptrBus )
  return ccall((:PSRConverterDCAC_VSC_setControlledBus, "PSRClasses"),Nothing,(Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrBus)
end
function PSRConverterDCAC_VSC_deleteInstance( ptr_pointer )
  return ccall((:PSRConverterDCAC_VSC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPHydroPlantEnergyBid_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRSIONCPHydroPlantEnergyBid_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRSIONCPHydroPlantEnergyBid_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPHydroPlantEnergyBid_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPHydroPlantEnergyBid_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPHydroPlantEnergyBid_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSIONCPHydroPlantMinimumTurbining_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRSIONCPHydroPlantMinimumTurbining_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRSIONCPHydroPlantMinimumTurbining_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPHydroPlantMinimumTurbining_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPHydroPlantMinimumTurbining_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPHydroPlantMinimumTurbining_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSIONCPPlantSecondarySpinningReserveChronological_load( ptr_pointer,ptrSystem, filename, modelField, reserveDirectionToRead, PSRPlantType )
  return ccall((:PSRSIONCPPlantSecondarySpinningReserveChronological_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSystem,filename,modelField,reserveDirectionToRead,PSRPlantType)
end
function PSRSIONCPPlantSecondarySpinningReserveChronological_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPPlantSecondarySpinningReserveChronological_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPPlantSecondarySpinningReserveChronological_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPPlantSecondarySpinningReserveChronological_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRSIONCPPlantSecondarySpinningReserveChronological_2_load( ptr_pointer,ptrSystem, filename, modelField, reserveDirectionToRead, PSRPlantType )
  return ccall((:PSRSIONCPPlantSecondarySpinningReserveChronological_2_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrSystem,filename,modelField,reserveDirectionToRead,PSRPlantType)
end
function PSRSIONCPPlantSecondarySpinningReserveChronological_2_deleteInstance( ptr_pointer )
  return ccall((:PSRSIONCPPlantSecondarySpinningReserveChronological_2_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRSIONCPPlantSecondarySpinningReserveChronological_2_create(iprt)
  ptr_pointer =  ccall((:PSRSIONCPPlantSecondarySpinningReserveChronological_2_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemPlantReserve_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemPlantReserve_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemPlantReserve_load( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemPlantReserve_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemPlantReserve_save( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemPlantReserve_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemPlantReserve_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemPlantReserve_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemThermalPlantOperation_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemThermalPlantOperation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemThermalPlantOperation_load( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemThermalPlantOperation_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemThermalPlantOperation_save( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemThermalPlantOperation_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemThermalPlantOperation_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemThermalPlantOperation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemDefluence_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemDefluence_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemDefluence_load( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemDefluence_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemDefluence_save( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemDefluence_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemDefluence_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemDefluence_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemHydroPlantOperation_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemHydroPlantOperation_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemHydroPlantOperation_load( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemHydroPlantOperation_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemHydroPlantOperation_save( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemHydroPlantOperation_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemHydroPlantOperation_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemHydroPlantOperation_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPGenerationConstraint_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIONCPGenerationConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIONCPGenerationConstraint_ReadGerUnit( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIONCPGenerationConstraint_ReadGerUnit, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),C_NULL ,ptrSystem,filename)
end
function PSRIONCPGenerationConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPGenerationConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPGenerationConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPGenerationConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGrafConfiguration_create(iprt)
  ptr_pointer =  ccall((:PSRIOGrafConfiguration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGrafConfiguration_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOGrafConfiguration_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOGrafConfiguration_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOGrafConfiguration_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOGrafConfiguration_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGrafConfiguration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPColdReserve_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIONCPColdReserve_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIONCPColdReserve_afterConfigHeaderInfo( ptr_pointer,row )
  return ccall((:PSRIONCPColdReserve_afterConfigHeaderInfo, "PSRClasses"),Int32,(Ptr{UInt8}, Int32,),ptr_pointer ,row)
end
function PSRIONCPColdReserve_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPColdReserve_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPColdReserve_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPColdReserve_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPDemand_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIONCPDemand_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIONCPDemand_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPDemand_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPDemand_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPDemand_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemReserveConstraint_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemReserveConstraint_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemReserveConstraint_load( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemReserveConstraint_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemReserveConstraint_save( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemReserveConstraint_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemReserveConstraint_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemReserveConstraint_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPPlantSpinningReserveDirection_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPPlantSpinningReserveDirection_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPPlantSpinningReserveDirection_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPPlantSpinningReserveDirection_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPPlantSpinningReserveDirection_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPPlantSpinningReserveDirection_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPSecondarySpinningReservePrice_load( ptr_pointer,ptrSystem, filename )
  return ccall((:PSRIONCPSecondarySpinningReservePrice_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrSystem,filename)
end
function PSRIONCPSecondarySpinningReservePrice_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPSecondarySpinningReservePrice_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPSecondarySpinningReservePrice_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPSecondarySpinningReservePrice_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPSpinningReserveGroups_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPSpinningReserveGroups_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPSpinningReserveGroups_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPSpinningReserveGroups_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPSpinningReserveGroups_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPSpinningReserveGroups_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONCPPlantSelfConsumption_load( ptr_pointer,ptrStudy, filename, PSRPlantType )
  return ccall((:PSRIONCPPlantSelfConsumption_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int32,),ptr_pointer ,ptrStudy,filename,PSRPlantType)
end
function PSRIONCPPlantSelfConsumption_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPPlantSelfConsumption_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPPlantSelfConsumption_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPPlantSelfConsumption_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemStudy_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemStudy_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemStudy_load( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemStudy_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemStudy_save( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemStudy_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemStudy_clearThermalplantsDump( ptr_pointer )
  return ccall((:PSRIODessemStudy_clearThermalplantsDump, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemStudy_generateLoadPerBus( ptr_pointer )
  return ccall((:PSRIODessemStudy_generateLoadPerBus, "PSRClasses"),Int32,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemStudy_setHydroPlantGenerators( ptr_pointer )
  return ccall((:PSRIODessemStudy_setHydroPlantGenerators, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemStudy_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemStudy_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIODessemThermalPlant_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemThermalPlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemThermalPlant_load( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemThermalPlant_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemThermalPlant_save( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemThermalPlant_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemThermalPlant_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemThermalPlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroCommitment_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONCPHydroCommitment_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONCPHydroCommitment_deleteInstance( ptr_pointer )
  return ccall((:PSRIONCPHydroCommitment_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONCPHydroCommitment_create(iprt)
  ptr_pointer =  ccall((:PSRIONCPHydroCommitment_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemReserveGeneration_create(iprt)
  ptr_pointer =  ccall((:PSRIODessemReserveGeneration_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIODessemReserveGeneration_load( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemReserveGeneration_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemReserveGeneration_save( ptr_pointer,ptrStudy, nome )
  return ccall((:PSRIODessemReserveGeneration_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,nome)
end
function PSRIODessemReserveGeneration_deleteInstance( ptr_pointer )
  return ccall((:PSRIODessemReserveGeneration_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGeorefenceBus_create(iprt)
  ptr_pointer =  ccall((:PSRIOGeorefenceBus_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGeorefenceBus_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGeorefenceBus_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenHydro_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenHydro_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenHydro_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenHydro_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenHydro_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenHydro_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenProject_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenProject_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenProject_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenProject_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenProject_load( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenProject_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenProject_useOnlyCircuits( ptr_pointer,status )
  return ccall((:PSRIOOptgenProject_useOnlyCircuits, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,status)
end
function PSRIOOptgenProject_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenProject_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANRegion_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANRegion_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANRegion_load( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIONETPLANRegion_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIONETPLANRegion_save( ptr_pointer,_ptrEstudo, nome )
  return ccall((:PSRIONETPLANRegion_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,nome)
end
function PSRIONETPLANRegion_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANRegion_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOOptgenFuel_create(iprt)
  ptr_pointer =  ccall((:PSRIOOptgenFuel_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOOptgenFuel_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIOOptgenFuel_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIOOptgenFuel_deleteInstance( ptr_pointer )
  return ccall((:PSRIOOptgenFuel_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANStaticVarCompensator_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANStaticVarCompensator_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANStaticVarCompensator_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANStaticVarCompensator_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANStaticVarCompensator_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANStaticVarCompensator_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANSystem_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANSystem_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANSystem_useOnlySelectedSystems( ptr_pointer,flag )
  return ccall((:PSRIONETPLANSystem_useOnlySelectedSystems, "PSRClasses"),Nothing,(Ptr{UInt8}, Bool,),ptr_pointer ,flag)
end
function PSRIONETPLANSystem_load( ptr_pointer,_ptrEstudo, fileName )
  return ccall((:PSRIONETPLANSystem_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,fileName)
end
function PSRIONETPLANSystem_save( ptr_pointer,_ptrEstudo, fileName )
  return ccall((:PSRIONETPLANSystem_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,_ptrEstudo,fileName)
end
function PSRIONETPLANSystem_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANSystem_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANTransformer3Winding_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANTransformer3Winding_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANTransformer3Winding_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANTransformer3Winding_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANTransformer3Winding_save( ptr_pointer,ptrEstudo, nome, saving_modifications )
  return ccall((:PSRIONETPLANTransformer3Winding_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrEstudo,nome,saving_modifications)
end
function PSRIONETPLANTransformer3Winding_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANTransformer3Winding_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANSerieCapacitor_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANSerieCapacitor_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANSerieCapacitor_load( ptr_pointer,ptrEstudo, filename )
  return ccall((:PSRIONETPLANSerieCapacitor_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,filename)
end
function PSRIONETPLANSerieCapacitor_save( ptr_pointer,ptrEstudo, nome, saving_modifications )
  return ccall((:PSRIONETPLANSerieCapacitor_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrEstudo,nome,saving_modifications)
end
function PSRIONETPLANSerieCapacitor_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANSerieCapacitor_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANDemand_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANDemand_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANDemand_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANDemand_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANDemand_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANDemand_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANLineReactor_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANLineReactor_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANLineReactor_load( ptr_pointer,ptrEstudo, filename )
  return ccall((:PSRIONETPLANLineReactor_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,filename)
end
function PSRIONETPLANLineReactor_save( ptr_pointer,ptrEstudo, filename )
  return ccall((:PSRIONETPLANLineReactor_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,filename)
end
function PSRIONETPLANLineReactor_saveModifications( ptr_pointer,ptrEstudo, filename )
  return ccall((:PSRIONETPLANLineReactor_saveModifications, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,filename)
end
function PSRIONETPLANLineReactor_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANLineReactor_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANLinkDC_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANLinkDC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANLinkDC_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANLinkDC_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANLinkDC_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANLinkDC_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANLinkDC_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANLinkDC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGeorefenceCircuit_create(iprt)
  ptr_pointer =  ccall((:PSRIOGeorefenceCircuit_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGeorefenceCircuit_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGeorefenceCircuit_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOGeorefencePlant_create(iprt)
  ptr_pointer =  ccall((:PSRIOGeorefencePlant_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOGeorefencePlant_load( ptr_pointer,ptrSystem, plantType, nome )
  return ccall((:PSRIOGeorefencePlant_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrSystem,plantType,nome)
end
function PSRIOGeorefencePlant_save( ptr_pointer,ptrSystem, plantType, nome )
  return ccall((:PSRIOGeorefencePlant_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Int32, Ptr{UInt8},),ptr_pointer ,ptrSystem,plantType,nome)
end
function PSRIOGeorefencePlant_deleteInstance( ptr_pointer )
  return ccall((:PSRIOGeorefencePlant_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANCircuitDC_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANCircuitDC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANCircuitDC_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANCircuitDC_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANCircuitDC_save( ptr_pointer,ptrEstudo, nome, saveModifications )
  return ccall((:PSRIONETPLANCircuitDC_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool,),ptr_pointer ,ptrEstudo,nome,saveModifications)
end
function PSRIONETPLANCircuitDC_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANCircuitDC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANConverterDCAC_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANConverterDCAC_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANConverterDCAC_setConverterDeviceType( ptr_pointer,converter_device_type )
  return ccall((:PSRIONETPLANConverterDCAC_setConverterDeviceType, "PSRClasses"),Nothing,(Ptr{UInt8}, Int32,),ptr_pointer ,converter_device_type)
end
function PSRIONETPLANConverterDCAC_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANConverterDCAC_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANConverterDCAC_save( ptr_pointer,ptrEstudo, nome, saveModifications, converter_device_type )
  return ccall((:PSRIONETPLANConverterDCAC_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Bool, Int32,),ptr_pointer ,ptrEstudo,nome,saveModifications,converter_device_type)
end
function PSRIONETPLANConverterDCAC_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANConverterDCAC_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANLoadBus_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANLoadBus_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANLoadBus_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANLoadBus_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANLoadBus_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANLoadBus_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANLoadBus_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANLoadBus_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANGenerator_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANGenerator_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANGenerator_load( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANGenerator_load, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANGenerator_save( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANGenerator_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANGenerator_saveModifications( ptr_pointer,ptrEstudo, nome )
  return ccall((:PSRIONETPLANGenerator_saveModifications, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrEstudo,nome)
end
function PSRIONETPLANGenerator_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANGenerator_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANCircuitDCModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANCircuitDCModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANCircuitDCModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANCircuitDCModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANCircuitDCModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANCircuitDCModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusShuntModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANBusShuntModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANBusShuntModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusShuntModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusShuntModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusShuntModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANConverterDCAC_VSCModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANConverterDCAC_VSCModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANConverterDCAC_VSCModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANConverterDCAC_VSCModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANConverterDCAC_VSCModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANConverterDCAC_VSCModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANGeneratorModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANGeneratorModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANGeneratorModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANGeneratorModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANGeneratorModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANGeneratorModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusFicticiousModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANBusFicticiousModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANBusFicticiousModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusFicticiousModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusFicticiousModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusFicticiousModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANBusModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANBusModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusDCRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANBusDCRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANBusDCRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusDCRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusDCRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusDCRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusShuntCandidates_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANBusShuntCandidates_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANBusShuntCandidates_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusShuntCandidates_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusShuntCandidates_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusShuntCandidates_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANBusRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANBusRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANCircuitRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANCircuitRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANCircuitRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANCircuitRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANCircuitRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANCircuitRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANCircuitDCRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANCircuitDCRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANCircuitDCRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANCircuitDCRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANCircuitDCRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANCircuitDCRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANConverterDCACModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANConverterDCACModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANConverterDCACModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANConverterDCACModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANConverterDCACModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANConverterDCACModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANConverterDCACRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANConverterDCACRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANConverterDCACRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANConverterDCACRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANConverterDCACRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANConverterDCACRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANCircuitModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANCircuitModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANCircuitModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANCircuitModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANCircuitModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANCircuitModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANLineReactorRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANLineReactorRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANLineReactorRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANLineReactorRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANLineReactorRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANLineReactorRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANGeneratorRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANGeneratorRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANGeneratorRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANGeneratorRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANGeneratorRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANGeneratorRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANConverterDCAC_P2PModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANConverterDCAC_P2PModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANConverterDCAC_P2PModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANConverterDCAC_P2PModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANConverterDCAC_P2PModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANConverterDCAC_P2PModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANLineReactorModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANLineReactorModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANLineReactorModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANLineReactorModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANLineReactorModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANLineReactorModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPSumInterconnections_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPSumInterconnections_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPSumInterconnections_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPSumInterconnections_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIOSDDPSumCircuits_deleteInstance( ptr_pointer )
  return ccall((:PSRIOSDDPSumCircuits_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIOSDDPSumCircuits_create(iprt)
  ptr_pointer =  ccall((:PSRIOSDDPSumCircuits_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANTransformer3WindingRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANTransformer3WindingRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANTransformer3WindingRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANTransformer3WindingRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANTransformer3WindingRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANTransformer3WindingRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANTransformerRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANTransformerRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANTransformerRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANTransformerRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANTransformerRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANTransformerRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANTransformerFicticiousRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANTransformerFicticiousRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANTransformerFicticiousRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANTransformerFicticiousRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANTransformerFicticiousRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANTransformerFicticiousRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANSerieCapacitorModification_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANSerieCapacitorModification_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANSerieCapacitorModification_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANSerieCapacitorModification_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANSerieCapacitorModification_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANSerieCapacitorModification_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusFicticiousRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANBusFicticiousRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANBusFicticiousRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusFicticiousRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusFicticiousRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusFicticiousRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANTransformerModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANTransformerModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANTransformerModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANTransformerModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANTransformerModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANTransformerModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANTransformer_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANTransformer_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANTransformer_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANTransformer_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBatteryModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANBatteryModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANBatteryModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBatteryModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBatteryModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBatteryModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusDCModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANBusDCModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANBusDCModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusDCModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusDCModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusDCModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANCircuitCandidates_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANCircuitCandidates_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANCircuitCandidates_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANCircuitCandidates_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANCircuitCandidates_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANCircuitCandidates_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANABatteryRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANABatteryRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANABatteryRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANABatteryRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANABatteryRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANABatteryRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANBusShuntRegistries_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANBusShuntRegistries_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANBusShuntRegistries_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANBusShuntRegistries_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANBusShuntRegistries_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANBusShuntRegistries_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANTransformerCandidates_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANTransformerCandidates_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANTransformerCandidates_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANTransformerCandidates_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANTransformerCandidates_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANTransformerCandidates_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANTransformer3WindingModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANTransformer3WindingModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANTransformer3WindingModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANTransformer3WindingModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANTransformer3WindingModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANTransformer3WindingModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANTransformerFicticiousCandidates_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANTransformerFicticiousCandidates_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANTransformerFicticiousCandidates_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANTransformerFicticiousCandidates_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANTransformerFicticiousCandidates_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANTransformerFicticiousCandidates_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
function PSRIONETPLANTransformerFicticiousModifications_save( ptr_pointer,ptrStudy, filename )
  return ccall((:PSRIONETPLANTransformerFicticiousModifications_save, "PSRClasses"),Int32,(Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},),ptr_pointer ,ptrStudy,filename)
end
function PSRIONETPLANTransformerFicticiousModifications_deleteInstance( ptr_pointer )
  return ccall((:PSRIONETPLANTransformerFicticiousModifications_deleteInstance, "PSRClasses"),Nothing,(Ptr{UInt8},),ptr_pointer )
end
function PSRIONETPLANTransformerFicticiousModifications_create(iprt)
  ptr_pointer =  ccall((:PSRIONETPLANTransformerFicticiousModifications_create, "PSRClasses"),Ptr{UInt8},(Ptr{UInt8},),C_NULL)
  return ptr_pointer
end
