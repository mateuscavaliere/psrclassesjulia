"""
    Teste 1 - Funcoes do tipo Graf
"""

function psrc_test01_01( path )

    @test typeof( constr_graph( path , "cmgdem" ) ) == Graf

    MyGraf = constr_graph( path , "cmgdem" )

    @test graph_load_init( MyGraf ) == 0

    @test graph_close( MyGraf , "load" ) == 0

end

function psrc_test01_02( path )

    @test typeof( constr_graph( path , "preco" ) ) == Graf

    MyGraf = constr_graph( path , "preco" )

    MyGraf.Ext = 1
    MyGraf.IniStg = 1
    MyGraf.IniYear = 2020
    MyGraf.NumStg = 15
    MyGraf.NumSer = 10
    MyGraf.NumBlc = 3
    MyGraf.NumAgentes = 2
    MyGraf.Unit2 = "MWh"
    MyGraf.StgType = 2
    MyGraf.Simb = 1
    MyGraf.Sims = 1
    MyGraf.Simh = 0
    MyGraf.Agentes = [ "Agente1" , "Agente2" ]

    @test graph_save_init( MyGraf ) == 0
     
    @test graph_close( MyGraf , "save" ) == 0
end

psrc_test01_01( joinpath( dirname(@__FILE__) , ".." , "example" , "example_1" ) )
psrc_test01_01( joinpath( dirname(@__FILE__) , ".." , "example" , "example_2" ) )

psrc_test01_02( joinpath( dirname(@__FILE__) , ".." , "example" , "example_1" ) )
