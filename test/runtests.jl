using Test
using PSRClassesJulia

PSRClasses_init( joinpath( dirname(@__FILE__) , ".." , "deps" ) )
load_masks( joinpath( dirname(@__FILE__) , ".." , "deps" ) )

for i = 1:2
    try
        s_i = lpad(string(i), 2, "0")
        include("psrclasses_test$s_i.jl")
    catch err
        println()
        rethrow(err)
    end
end
