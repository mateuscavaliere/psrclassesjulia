"""
    Teste 2 - Funcoes de Leitura dos dados de Entrada
"""

function psrc_test02_01( path )

    MyStdy = SddpStudy()

    @test read_sddp_config!( MyStdy , path ) == 0

end

function psrc_test02_02( path )

    MyStdy = SddpStudy()

    @test read_sistemas!( MyStdy , path ) == 0
end

function psrc_test02_03( path )

    MyStdy = SddpStudy()

    @test read_duration!( MyStdy , path ) == 0
end

psrc_test02_01( joinpath( dirname(@__FILE__) , ".." , "example" , "example_3" ) )
psrc_test02_01( joinpath( dirname(@__FILE__) , ".." , "example" , "example_4" ) )

psrc_test02_02( joinpath( dirname(@__FILE__) , ".." , "example" , "example_3" ) )

psrc_test02_03( joinpath( dirname(@__FILE__) , ".." , "example" , "example_3" ) )
psrc_test02_03( joinpath( dirname(@__FILE__) , ".." , "example" , "example_4" ) )
