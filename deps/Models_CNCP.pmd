//--------------------------------------------------------------------------------------------------
// Configuracao do Estudo
//--------------------------------------------------------------------------------------------------
DEFINE_MODEL MODL:CNCP_StudyConfiguration
	PARM INTEGER	Presolve                                     // Presolve (0=No;1=Yes)								
	PARM INTEGER	MaxTime                                      // Maximum execution time (minutes)							
	PARM REAL	Tolerance	                             // Convergence Tolerance (k$)							
	PARM INTEGER	SolutionMethod                               // Solution Method									
	PARM INTEGER	OptimizationCriteria                         // Optimization Criteria								
	PARM INTEGER	HydroRepresentationLevel                     // Hydro Representation Level							
	PARM INTEGER	HeuristicEnfasis                             // Heuristics Enfasis								
	PARM INTEGER	CutFileFormat                                // Cut's File Format								
	PARM INTEGER	CommitmentRepresentation                     // Commitment representation							
	PARM INTEGER	RepresentationHeadStorage                    // Represent the Head x Storage variation effect					
	PARM REAL	RelativeTolerance                            // Relative Convergence Tolerance (%)						
	PARM INTEGER	RepresentationGenerationZone                 // Represent allowed generation zone						
	PARM INTEGER	UseDailyHydroProductionFactor	             // Use daily hydro variable production factor					
	PARM INTEGER	UseHourlyHydroProductionFactor	             // Use hourly hydro variable production factor					
	PARM INTEGER	UseGenerationDamping                         // Use generation damping								
	PARM INTEGER	StageDuration                                // Stage duration (1=60 minutes;2=30 minutes;4=15  minutes)				
	PARM INTEGER	SecondarySpinningSellOption                  // Secondary spinning reserve sell option						
	PARM INTEGER	ExecutionType                                // Execution type (0=NCP; 1=NCP with Optflow; 2=Chronological NCP)			
	PARM INTEGER	EnableRollingHorizon                         // Habilitar Horizonte Rolante? [0]: N�o / 1: Sim					
	PARM INTEGER	EnableLocalSearch                            // Habilitar Busca Local? [0]: N�o / 1: Sim						
	PARM INTEGER	EnableIntegerVariablesAgreggation            // Habilitar agrega��o intra-hor�ria de vari�veis inteiras? [0]: N�o / 1: Sim	
	PARM INTEGER	InitialHour                                  // Initial Hour									
	PARM INTEGER	FinalHour                                    // Final Hour									
	PARM INTEGER	StudyType                                    // Study Type (0=Deterministic; 1=Stochastic)					
	PARM INTEGER	NumberSeries                                 // Number of series									
END_MODEL	
//--------------------------------------------------------------------------------------------------
// Segmento de Demanda
//--------------------------------------------------------------------------------------------------
DEFINE_MODEL MODL:CNCP_DemandSegment
	PARM INTEGER BlockType
	VETOR DATE InitialDate
	VETOR DATE EndDate INTERVAL InitialDate
	VETOR REAL ShortTermDemand INDEX InitialDate
END_MODEL
//--------------------------------------------------------------------------------------------------
// Sistema
//--------------------------------------------------------------------------------------------------
DEFINE_MODEL MODL:CNCP_System    
	PARM INTEGER UnitTypeGenerationConstraint    
	PARM INTEGER BlockTypeGenerationConstraint   
	
	VETOR DATE InitialDateMarketPrice
	VETOR DATE EndDateMarketPrice INTERVAL InitialDateMarketPrice
	VETOR REAL ShortTermMarketPrice INDEX InitialDateMarketPrice
	
	VETOR DATE InitialDateSecondarySpinningPrice
	VETOR DATE EndDateSecondarySpinningPrice INTERVAL InitialDateSecondarySpinningPrice
	VETOR REAL SecondarySpinningReservePrice INDEX InitialDateSecondarySpinningPrice	
	
	PARM INTEGER UnitTypeColdReserve
	PARM REAL    ConstantValueColdReserve
	VETOR DATE   InitialDateColdReserve
	VETOR DATE   EndDateColdReserve INTERVAL InitialDateColdReserve
	VETOR REAL   ColdReserve INDEX InitialDateColdReserve
	
	PARM INTEGER ThermalPlant_MaintenanceValueType
	PARM INTEGER ThermalPlant_MaintenanceValueUnit
	
	PARM INTEGER ThermalPlant_OperationalConstraints_UseMinimumUpTime
	PARM INTEGER ThermalPlant_OperationalConstraints_UseMinimumDownTime
	PARM INTEGER ThermalPlant_OperationalConstraints_UseMaximumRampUp
	PARM INTEGER ThermalPlant_OperationalConstraints_UseMaximumRampDown
	PARM INTEGER ThermalPlant_OperationalConstraints_UseMaximumUpTime
	
	//to-do: aqui h� uma 'op��o' entre dois tipos de arquivo. O que fazer com os outros valores, quando n�o estiverem sendo usados?
	PARM INTEGER ThermalPlant_PrimaryReserveValueType
	//unit: 1 = % generation; 2 = % nominal power; 3 = MW; 14 = % Avaiable Capacity
	PARM INTEGER ThermalPlant_PrimaryReserveValueUnit
	PARM REAL	 ThermalPlant_PrimaryReserveViolationPenalty
	
	PARM INTEGER	ThermalPlant_SecondarySpinningReserveUnit
	
	PARM INTEGER HydroPlant_MaintenanceValueType
	PARM INTEGER HydroPlant_MaintenanceValueUnit
	
	PARM INTEGER HydroPlant_UseTargetStorage
	PARM INTEGER HydroPlant_TargetStorageUnit
	
	PARM INTEGER HydroPlant_AutomaticRecoverTravelTimeInitialConditions
	
	PARM INTEGER HydroPlant_UseRampUpPowerConstraint
	PARM INTEGER HydroPlant_UseRampDownPowerConstraint
	PARM INTEGER HydroPlant_UseOutflowRampUpConstraint
	PARM INTEGER HydroPlant_UseOutflowRampDownConstraint
	PARM INTEGER HydroPlant_UseForebayFillUpConstraint
	PARM INTEGER HydroPlant_UseForebayDrawDownConstraint
	
	PARM INTEGER HydroPlant_PrimaryReserveValueType
	//unit: 1 = % generation; 2 = % nominal power; 3 = MW; 14 = % Avaiable Capacity
	PARM INTEGER HydroPlant_PrimaryReserveValueUnit
	PARM REAL	 HydroPlant_PrimaryReserveViolationPenalty
	
	PARM INTEGER HydroPlant_SecondarySpinningReserveUnit
	
END_MODEL
//--------------------------------------------------------------------------------------------------
// Restricao de Geracao
//--------------------------------------------------------------------------------------------------
DEFINE_MODEL MODL:CNCP_GenerationConstraint
	VETOR DATE InitialDate
	VETOR DATE EndDate INTERVAL InitialDate
	VETOR REAL ShortTermLimit INDEX InitialDate
	PARM STRING	sign
END_MODEL
//--------------------------------------------------------------------------------------------------
// Restricao dos Spinning Reserve Groups
//--------------------------------------------------------------------------------------------------
DEFINE_MODEL MODL:CNCP_SpinningReserveGroup
	PARM REAL InitialCondition
	PARM INTEGER NumberOfLevels
	
	VETOR DATE InitialDateMinimumReserve
	VETOR DATE EndDateMinimumReserve INTERVAL InitialDateMinimumReserve
	VETOR REAL MinimumReserve INDEX InitialDateMinimumReserve
	
	VETOR DATE InitialDateMaximumReserve1
	VETOR DATE EndDateMaximumReserve1 INTERVAL InitialDateMaximumReserve1
	VETOR REAL MaximumReserve1 INDEX InitialDateMaximumReserve1
	
	VETOR DATE InitialDateBidPrice1
	VETOR DATE EndDateBidPrice1 INTERVAL InitialDateBidPrice1
	VETOR REAL BidPrice1 INDEX InitialDateBidPrice1
	
	VETOR DATE InitialDateMaximumReserve2
	VETOR DATE EndDateMaximumReserve2 INTERVAL InitialDateMaximumReserve2
	VETOR REAL MaximumReserve2 INDEX InitialDateMaximumReserve2
	
	VETOR DATE InitialDateBidPrice2
	VETOR DATE EndDateBidPrice2 INTERVAL InitialDateBidPrice2
	VETOR REAL BidPrice2 INDEX InitialDateBidPrice2
	
	
END_MODEL

//--------------------------------------------------------------------------------------------------
// Usina Termica
//--------------------------------------------------------------------------------------------------
DEFINE_MODEL MODL:CNCP_ThermalPlant    
	
	VETOR DATE InitialDateMaintenance
	VETOR DATE EndDateMaintenance INTERVAL InitialDateMaintenance
	VETOR REAL MaintenanceValue INDEX InitialDateMaintenance
	
	PARM INTEGER MinimumUptime
	PARM INTEGER MinimumDownTime
	PARM REAL    MaximumRampUp
	PARM REAL    MaximumRampDown
	PARM INTEGER MaximumUptime
	
	PARM INTEGER InitialStatus
	PARM INTEGER NumberOfHours
	PARM REAL	 PreviousGeneration
	//period with constant power + previous load condition(sign)???
	PARM INTEGER PeriodWithConstantPower
	//MXAR,MXVC??
	
	//maximum number of startups
	PARM INTEGER MaxStudyStartUps
	PARM INTEGER MaxDailyStartUps
	
	//maximum number of shutdowns
	PARM INTEGER MaxStudyShutdowns
	PARM INTEGER MaxDailyShutdowns
	
	
	VETOR DATE InitialDatePrimaryReserve
	VETOR DATE EndDatePrimaryReserve INTERVAL InitialDatePrimaryReserve
	VETOR REAL PrimaryReserveValue INDEX InitialDatePrimaryReserve
	
	PARM STRING PrimaryReserveDirection
	
	PARM REAL 		SecondarySpinningReserve_ControlRange
	PARM REAL 		SecondarySpinningReserve_Minimum
	PARM REAL 		SecondarySpinningReserve_Maximum
	PARM REAL 		SecondarySpinningReserve_InitialCondition
	PARM STRING 	SecondarySpinningReserve_Direction
	PARM INTEGER 	SecondarySpinningReserve_NumberOfLevels
	
	VETOR DATE SSR_InitialDateMinimum
	VETOR DATE SSR_EndDateMinimum INTERVAL SSR_InitialDateMinimum
	VETOR REAL SSR_Minimum INDEX SSR_InitialDateMinimum
	
	VETOR DATE SSR_InitialDateMaximum
	VETOR DATE SSR_EndDateMaximum INTERVAL SSR_InitialDateMaximum
	VETOR REAL SSR_Maximum INDEX SSR_InitialDateMaximum
	
	VETOR DATE SSR_InitialDateBidPrice
	VETOR DATE SSR_EndDateBidPrice INTERVAL SSR_InitialDateBidPrice
	VETOR REAL SSR_BidPrice INDEX SSR_InitialDateBidPrice
	
	VETOR DATE SSR_InitialDateMaximum2
	VETOR DATE SSR_EndDateMaximum2 INTERVAL SSR_InitialDateMaximum2
	VETOR REAL SSR_Maximum2 INDEX SSR_InitialDateMaximum2
	
	VETOR DATE SSR_InitialDateBidPrice2
	VETOR DATE SSR_EndDateBidPrice2 INTERVAL SSR_InitialDateBidPrice2
	VETOR REAL SSR_BidPrice2 INDEX SSR_InitialDateBidPrice2
	
	
	VETOR DATE InitialDateForcedGeneration
	VETOR DATE EndDateForcedGeneration INTERVAL InitialDateForcedGeneration
	VETOR REAL ForcedGeneration INDEX InitialDateForcedGeneration	
	
	PARM REAL SelfConsumption
	
	//infos associadas ao Temperature effects
	PARM  REAL MinGenerationPercent
	VETOR REAL Temperature
	VETOR REAL MaxGeneration
	VETOR REAL Segment1
	VETOR REAL Segment2
	VETOR REAL Segment3
	
	VETOR DATE InitialDateHourlyTemperature
	VETOR DATE EndDateHourlyTemperature INTERVAL InitialDateHourlyTemperature
	VETOR REAL HourlyTemperature INDEX InitialDateHourlyTemperature
	
	VETOR DATE InitialDateColdReserveAmount
	VETOR DATE EndDateColdReserveAmount INTERVAL InitialDateColdReserveAmount
	VETOR REAL ColdReserveAmount INDEX InitialDateColdReserveAmount
	
	VETOR DATE InitialDateColdReservePrice
	VETOR DATE EndDateColdReservePrice INTERVAL InitialDateColdReservePrice
	VETOR REAL ColdReservePrice INDEX InitialDateColdReservePrice
	
	//power inflection constraints
	PARM INTEGER PowerIncreaseHours
	PARM INTEGER PowerDecreaseHours
	PARM INTEGER MaxLoadVariations
	
	//forbidden zone
	PARM REAL ForbiddenZone_InferiorLimit
	PARM REAL ForbiddenZone_SuperiorLimit
	
	//start-up minimum time (all fields in hours)
	PARM REAL StartupMinimumTime_Hot
	PARM REAL StartupMinimumTime_Warm
	PARM REAL StartupMinimumTime_Cold
	PARM REAL StartupMinimumTime_TotalCoolingTime
	PARM REAL StartupMinimumTime_PartialCoolingTime
	PARM REAL StartupMinimumTime_InitialConditionForSynchronism
	
	//consumption coefficients
	PARM REAL ConsumptionCoefficient_A
	PARM REAL ConsumptionCoefficient_B
	PARM REAL ConsumptionCoefficient_C
	
	
	//generation constraints
	//less equal
	VETOR DATE InitialDateGenConstraint_LE
	VETOR DATE EndDateGenConstraint_LE INTERVAL InitialDateGenConstraint_LE
	VETOR REAL GenConstraint_LE INDEX InitialDateGenConstraint_LE
	
	//equals
	VETOR DATE InitialDateGenConstraint_EQ
	VETOR DATE EndDateGenConstraint_EQ INTERVAL InitialDateGenConstraint_EQ
	VETOR REAL GenConstraint_EQ INDEX InitialDateGenConstraint_EQ
	
	//greater equal
	VETOR DATE InitialDateGenConstraint_GE
	VETOR DATE EndDateGenConstraint_GE INTERVAL InitialDateGenConstraint_GE
	VETOR REAL GenConstraint_GE INDEX InitialDateGenConstraint_GE
	
	//energy bid
	// o numero maximo de levels para energy bid
	DIMENSION EnergyBid_MaxLevel 5
	
	VETOR DATE EnergyBid_InitialDateAmount
	VETOR DATE EnergyBid_EndDateAmount INTERVAL EnergyBid_InitialDateAmount
	VETOR REAL EnergyBid_Amount DIM(EnergyBid_MaxLevel) INDEX EnergyBid_InitialDateAmount
	
	VETOR DATE EnergyBid_InitialDatePrice
	VETOR DATE EnergyBid_EndDatePrice INTERVAL EnergyBid_InitialDatePrice
	VETOR REAL EnergyBid_Price DIM(EnergyBid_MaxLevel) INDEX EnergyBid_InitialDatePrice
	
	
	//consumption functions
	VETOR REAL ConsumptionFuntion_Power
	VETOR REAL ConsumptionFuntion_Consumption
	
END_MODEL




//--------------------------------------------------------------------------------------------------
// Usina Hidro
//--------------------------------------------------------------------------------------------------
DEFINE_MODEL MODL:CNCP_HydroPlant
	
	//maintenance
	VETOR DATE InitialDateMaintenance
	VETOR DATE EndDateMaintenance INTERVAL InitialDateMaintenance
	VETOR REAL MaintenanceValue INDEX InitialDateMaintenance
	
	//initial status
	PARM INTEGER InitialStatus
	PARM INTEGER NumberOfHours
	PARM REAL	 PreviousGeneration
	//mxar?
	
	//target storage
	PARM REAL MinimumTargetStorage
	PARM REAL MaximumTargetStorage
	
	
	//reservoir operation
	PARM REAL  MaximumTotalOutflowPenalty
	
	VETOR DATE InitialDateMaximumTotalOutflow
	VETOR DATE EndDateMaximumTotalOutflow INTERVAL InitialDateMaximumTotalOutflow
	VETOR REAL MaximumTotalOutflow INDEX InitialDateMaximumTotalOutflow
	
	PARM REAL  MinimumTotalOutflowPenalty
	
	VETOR DATE InitialDateMinimumTotalOutflow
	VETOR DATE EndDateMinimumTotalOutflow INTERVAL InitialDateMinimumTotalOutflow
	VETOR REAL MinimumTotalOutflow INDEX InitialDateMinimumTotalOutflow
	
	PARM REAL  IrrigationPenalty //pode ser um valor fixo, -1 se irrigacao eh prioridade, ou -2 se energia eh prioridade
	
	VETOR DATE InitialDateIrrigation
	VETOR DATE EndDateIrrigation INTERVAL InitialDateIrrigation
	VETOR REAL Irrigation INDEX InitialDateIrrigation
	
	PARM REAL  MinimumSpillagePenalty // ate onde entendi eh sempre -1
	
	VETOR DATE InitialDateMinimumSpillage
	VETOR DATE EndDateMinimumSpillage INTERVAL InitialDateMinimumSpillage
	VETOR REAL MinimumSpillage INDEX InitialDateMinimumSpillage
	
	PARM REAL  MaximumStoragePenalty
	
	VETOR DATE InitialDateMaximumStorage
	VETOR DATE EndDateMaximumStorage INTERVAL InitialDateMaximumStorage
	VETOR REAL MaximumStorage INDEX InitialDateMaximumStorage
	
	PARM REAL  MinimumStoragePenalty
	
	VETOR DATE InitialDateMinimumStorage
	VETOR DATE EndDateMinimumStorage INTERVAL InitialDateMinimumStorage
	VETOR REAL MinimumStorage INDEX InitialDateMinimumStorage
	
	//para estes dois proximos, foi necessario usar o prefixo NCP para diferenciar de um campo de mesmo nome em outro modelo
	PARM REAL  NCPAlertStoragePenalty
	
	VETOR DATE InitialDateNCPAlertStorage
	VETOR DATE EndDateNCPAlertStorage INTERVAL InitialDateNCPAlertStorage
	VETOR REAL NCPAlertStorage INDEX InitialDateNCPAlertStorage
	
	VETOR DATE InitialDateNCPFloodControlStorage
	VETOR DATE EndDateNCPFloodControlStorage INTERVAL InitialDateNCPFloodControlStorage
	VETOR REAL NCPFloodControlStorage INDEX InitialDateNCPFloodControlStorage


	//travel time(turbined)	
	PARM INTEGER TurbinedTravelTime
	VETOR REAL TurbinedOutflow
	VETOR REAL TurbinedWavePropagationFactor
	PARM INTEGER ValueWaterInTransit
	
	//travel time(spilled)
	PARM INTEGER SpilledTravelTime
	VETOR REAL SpilledOutflow
	VETOR REAL SpilledWavePropagationFactor
	
	//ramps
	PARM REAL RampUpPower
	PARM REAL RampDownPower
	PARM REAL OutflowRampUp
	PARM REAL OutflowRampDown
	PARM REAL ForebayFillUp
	PARM REAL ForebayDrawDown
	
	//maximum number of shutdowns
	PARM INTEGER HydroCommitment
	PARM INTEGER MaxStudyStartUps
	PARM INTEGER MaxDailyStartUps
	
	//primary reserve
	VETOR DATE InitialDatePrimaryReserve
	VETOR DATE EndDatePrimaryReserve INTERVAL InitialDatePrimaryReserve
	VETOR REAL PrimaryReserveValue INDEX InitialDatePrimaryReserve
	
	//secondary spinning reserve
	PARM REAL 		SecondarySpinningReserve_ControlRange
	PARM REAL 		SecondarySpinningReserve_Minimum
	PARM REAL 		SecondarySpinningReserve_Maximum
	PARM REAL 		SecondarySpinningReserve_InitialCondition
	PARM STRING 	SecondarySpinningReserve_Direction
	PARM INTEGER 	SecondarySpinningReserve_NumberOfLevels
	
	VETOR DATE SSR_InitialDateMinimum
	VETOR DATE SSR_EndDateMinimum INTERVAL SSR_InitialDateMinimum
	VETOR REAL SSR_Minimum INDEX SSR_InitialDateMinimum
	
	VETOR DATE SSR_InitialDateMaximum
	VETOR DATE SSR_EndDateMaximum INTERVAL SSR_InitialDateMaximum
	VETOR REAL SSR_Maximum INDEX SSR_InitialDateMaximum
	
	VETOR DATE SSR_InitialDateBidPrice
	VETOR DATE SSR_EndDateBidPrice INTERVAL SSR_InitialDateBidPrice
	VETOR REAL SSR_BidPrice INDEX SSR_InitialDateBidPrice
	
	VETOR DATE SSR_InitialDateMaximum2
	VETOR DATE SSR_EndDateMaximum2 INTERVAL SSR_InitialDateMaximum2
	VETOR REAL SSR_Maximum2 INDEX SSR_InitialDateMaximum2
	
	VETOR DATE SSR_InitialDateBidPrice2
	VETOR DATE SSR_EndDateBidPrice2 INTERVAL SSR_InitialDateBidPrice2
	VETOR REAL SSR_BidPrice2 INDEX SSR_InitialDateBidPrice2
	

	//generation constraints
	//less equal
	VETOR DATE InitialDateGenConstraint_LE
	VETOR DATE EndDateGenConstraint_LE INTERVAL InitialDateGenConstraint_LE
	VETOR REAL GenConstraint_LE INDEX InitialDateGenConstraint_LE
	
	//equals
	VETOR DATE InitialDateGenConstraint_EQ
	VETOR DATE EndDateGenConstraint_EQ INTERVAL InitialDateGenConstraint_EQ
	VETOR REAL GenConstraint_EQ INDEX InitialDateGenConstraint_EQ
	
	//greater equal
	VETOR DATE InitialDateGenConstraint_GE
	VETOR DATE EndDateGenConstraint_GE INTERVAL InitialDateGenConstraint_GE
	VETOR REAL GenConstraint_GE INDEX InitialDateGenConstraint_GE
	
	PARM REAL SelfConsumption
	
	//energy bid
	VETOR DATE InitialDateEnergyBid
	VETOR DATE EndDateEnergyBid INTERVAL InitialDateEnergyBid
	VETOR REAL EnergyBid INDEX InitialDateEnergyBid
	
	//cold reserve bid
	VETOR DATE InitialDateColdReserveAmount
	VETOR DATE EndDateColdReserveAmount INTERVAL InitialDateColdReserveAmount
	VETOR REAL ColdReserveAmount INDEX InitialDateColdReserveAmount
	
	VETOR DATE InitialDateColdReservePrice
	VETOR DATE EndDateColdReservePrice INTERVAL InitialDateColdReservePrice
	VETOR REAL ColdReservePrice INDEX InitialDateColdReservePrice
	
	//null water value
	VETOR DATE InitialDateNullWaterValue
	VETOR DATE EndDateNullWaterValue INTERVAL InitialDateNullWaterValue
	VETOR REAL NullWaterValue INDEX InitialDateNullWaterValue
	
	//cold ramp
	PARM REAL	 MaximumRampUp
	PARM INTEGER CoolingTime
	
	//discrete generation
	VETOR REAL DiscreteGeneration
	
	//minimum turbining
	VETOR DATE InitialDateMinimumTurbining
	VETOR DATE EndDateMinimumTurbining INTERVAL InitialDateMinimumTurbining
	VETOR REAL MinimumTurbining INDEX InitialDateMinimumTurbining
	
	
END_MODEL

//--------------------------------------------------------------------------------------------------
// Gauging Station
//--------------------------------------------------------------------------------------------------
DEFINE_MODEL MODL:CNCP_GaugingStation
	
	// o numero maximo de cenarios para inflow forecast
	DIMENSION InflowForecast_MaxScenarios 5
	
	VETOR DATE InitialDateInflowForecast
	VETOR DATE EndDateInflowForecast INTERVAL InitialDateInflowForecast
	VETOR REAL InflowForecast DIM(InflowForecast_MaxScenarios) INDEX InitialDateInflowForecast
	
	
END_MODEL


